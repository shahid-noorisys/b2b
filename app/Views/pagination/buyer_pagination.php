<?php

/**
 * @var \CodeIgniter\Pager\PagerRenderer $pager
 */

$pager->setSurroundCount(2);
?>

<?php if(COUNT($pager->links()) > 1): ?>
<div class="shop_toolbar t_bottom">
	<div class="pagination">
        <ul>
            <?php if ($pager->hasPrevious()) : ?>
                <li>
                    <a href="<?= $pager->getFirst() ?>">
                        <span><?= lang('Pager.first') ?></span>
                    </a>
                </li>
                <li class="prev">
                    <a href="<?= $pager->getPreviousPage() ?>">
                        <span><?= lang('Pager.previous') ?></span>
                    </a>
                </li>
            <?php endif; ?>
            
            <?php foreach ($pager->links() as $link) : ?>
                <li <?= $link['active'] ? 'class="current"' : '' ?>>
                    <a href="<?= $link['uri'] ?>">
                        <?= $link['title'] ?>
                    </a>
                </li>
            <?php endforeach; ?>

            <?php if ($pager->hasNext()) : ?>
                <li class="next">
                    <a href="<?= $pager->getNextPage() ?>">
                        <span><?= lang('Pager.next') ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= $pager->getLast() ?>">
                        <span><?= lang('Pager.last') ?></span>
                    </a>
                </li>
            <?php endif; ?>
        </ul>
	</div>
</div>
<?php endif; ?>