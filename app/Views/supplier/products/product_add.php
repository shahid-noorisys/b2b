<div class="row">
    <!-- Column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="<?=base_url('supplier/products')?>" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> <?=display('Back')?></a>
            </div>
            <div class="card-body">
                <?=form_open_multipart('supplier/product_create',' id="product_add_form" ')?>
                    <div class="form-body">
                        <!-- <h5 class="card-title">About Product</h5>
                        <hr> -->
                        <div class="row">
                            <div class="input-group col-md-4 col-sm-12= col-xs-12 mb-3">
                                    <input type="text" id="product_name" name="product_name" class="form-control" placeholder="<?=display('Product Name')?>"> 
                                    <p class="error text-danger"></p>
                            </div>
                            <div class="input-group col-md-4 col-sm-12= col-xs-12 mb-3">
                                   <?=form_dropdown('category_id',$categories,'',' class="select2 form-control" id="category_id" ')?>
                                    <p class="error text-danger cat_error"></p>
                            </div>
                            <div class="input-group col-md-4 col-sm-12 col-xs-12 mb-3" id="sub_category_div">
                               
                            </div>
                        </div>
                        <div class="row">
                            <!-- publish radio -->
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label><?=display('Publish')?></label>
                                    <br/>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="publish_status" class="custom-control-input custom_radio" value="1" checked>
                                        <label class="custom-control-label" for="customRadioInline1"><?=display('Yes')?></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="publish_status" class="custom-control-input custom_radio" value="2">
                                        <label class="custom-control-label" for="customRadioInline2"><?=display('No')?></label>
                                    </div>
                                </div>
                            </div>
                            <!-- publish radio -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?=display('Product Images (upto 5 images)')?></label>
                                    <input type="file" name="product_images[]" class="control-file-input" id="product_images" multiple="multiple">
                                    <p class="error text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Product Description')?></label>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions ">
                        <button type="submit" class="btn btn-success" id="btn_add_product"> <i class="fa fa-check"></i> <?=display('Save')?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<script>
$(function() {
    $("#category_id").change(function (e){
        e.preventDefault();
        var category_id = $(this).val();

        $.ajax({
            type: "post",
            url: "<?=base_url('supplier/sub-category-list')?>",
            data: {category_id: category_id, '<?=csrf_token()?>': '<?=csrf_hash()?>'},
            dataType: "json",
            success: function (data) {
                // console.log(status);
                $("#sub_category_div").html(data.list);
                $(".select2").select2();
            }
        });
    });
    $("#product_add_form").submit(function (e){
        e.preventDefault();
    });

    $("#btn_add_product").click(function (e) { 
        e.preventDefault();
        var product = $("#product_name");
        var category_id = $("#category_id").val();
        var sub_category_id = $("#sub_category_id").val();
        var product_images = $("#product_images");
        var valid = true;
        $(".error").html('');
        if($.trim(product.val()) == ''){
            product.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(category_id) == ''){
            $(".cat_error").html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(sub_category_id) == ''){
            $(".sub_cat_error").html("<?=display('Required')?>");
            valid = false;
        }
        if(product_images.get(0).files.length <= 0){
            product_images.next().html("<?=display('Required')?>");
            valid = false;
        }

        if(valid){
            $("#product_add_form").get(0).submit();
        }
    });

});
</script>