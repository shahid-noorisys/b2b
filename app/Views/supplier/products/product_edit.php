<style type="text/css">
.select2-selection, .select2-selection--single{
    height: 37px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 33px !important;
}
.image-form-body input[type="file"]{
    display: none;
}
.image-form-body button{
    margin-top: 20px;
    width: 100%;
}
.image-form-body .img-thumbnail{
    height: 150px;
    width: 175px;
}
</style>
<div class="row">
    <!-- Column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="<?=base_url('supplier/products')?>" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> <?=display('Back')?></a>
            </div>
            <div class="card-body">
                <?=form_open('supplier/product-update',' id="product_edit_form" ')?>
                <input type="hidden" name="product_id" value="<?=$product->id?>">
                    <div class="form-body">
                        <!-- <h5 class="card-title">About Product</h5>
                        <hr> -->
                        <div class="row">
                            <div class="input-group col-md-4 col-sm-12= col-xs-12 mb-3">
                                    <input type="text" id="product_name" name="product_name" value="<?=($product->name) ?? ''?>" class="form-control" placeholder="<?=display('Product Name')?>"> 
                                    <p class="error text-danger"></p>
                            </div>
                            <div class="input-group col-md-4 col-sm-12= col-xs-12 mb-3">
                                   <?=form_dropdown('category_id',$categories,($product->category_id) ?? '',' class="select2 form-control" id="category_id" ')?>
                                    <p class="error text-danger cat_error"></p>
                            </div>
                            <div class="input-group col-md-4 col-sm-12 col-xs-12 mb-3" id="sub_category_div">
                                <?=form_dropdown('sub_category_id',$sub_categories,($product->sub_category_id) ?? '',' class="select2 form-control" id="sub_category_id" ')?>
                                <p class="error text-danger sub_cat_error"></p>
                            </div>
                        </div>
                        <div class="row">
                            <!-- publish radio -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?=display('Publish')?></label>
                                    <br/>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="publish_status" class="custom-control-input custom_radio" value="1" <?=($product AND $product->is_published == 'Yes')?'checked':''; ?> >
                                        <label class="custom-control-label" for="customRadioInline1"><?=display('Yes')?></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="publish_status" class="custom-control-input custom_radio" value="2" <?=($product AND $product->is_published == 'No')?'checked':''; ?>>
                                        <label class="custom-control-label" for="customRadioInline2"><?=display('No')?></label>
                                    </div>
                                </div>
                            </div>
                            <!-- publish radio -->
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Product Description')?></label>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" name="description"><?=($product->description) ?? '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions ">
                        <button type="submit" class="btn btn-success" id="btn_edit_product"> <i class="fa fa-check"></i> <?=display('Save')?></button>
                    </div>
                </form>
                <hr>
                <?=form_open_multipart('supplier/product-image-upload')?>
                <input type="hidden" name="product_id" value="<?=$product->id?>">
                <div class="image-form-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label for=""><?=display('Main Image')?></label>
                            <img src="<?=($product ANd $product->image_1 != '')? base_url($product->image_1) :base_url('public/assets/img/no-image.jpg')?>" alt="..." class="img-thumbnail">
                            <input type="file" name="image_1" id="image_1">
                        </div>
                        <div class="col-md-2">
                            <label for=""><?=display('Image 2')?></label>
                            <img src="<?=($product ANd $product->image_2 != '')? base_url($product->image_2) :base_url('public/assets/img/no-image.jpg')?>" alt="..." class="img-thumbnail">
                            <input type="file" name="image_2" id="image_2">
                        </div>
                        <div class="col-md-2">
                            <label for=""><?=display('Image 3')?></label>
                            <img src="<?=($product ANd $product->image_3 != '')? base_url($product->image_3) :base_url('public/assets/img/no-image.jpg')?>" alt="..." class="img-thumbnail">
                            <input type="file" name="image_3" id="image_3">
                        </div>
                        <div class="col-md-2">
                            <label for=""><?=display('Image 4')?></label>
                            <img src="<?=($product ANd $product->image_4 != '')? base_url($product->image_4) :base_url('public/assets/img/no-image.jpg')?>" alt="..." class="img-thumbnail">
                            <input type="file" name="image_4" id="image_4">
                        </div>
                        <div class="col-md-2">
                            <label for=""><?=display('Image 5')?></label>
                            <img src="<?=($product ANd $product->image_5 != '')? base_url($product->image_5) :base_url('public/assets/img/no-image.jpg')?>" alt="..." class="img-thumbnail">
                            <input type="file" name="image_5" id="image_5">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-success btn_change"><?=display('Change')?></button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>

<script>
$(function() {
    $("#product_edit_form").trackChanges();
    $("#category_id").change(function (e){
        e.preventDefault();
        var category_id = $(this).val();

        $.ajax({
            type: "post",
            url: "<?=base_url('supplier/sub-category-list')?>",
            data: {category_id: category_id, '<?=csrf_token()?>': '<?=csrf_hash()?>'},
            dataType: "json",
            success: function (data) {
                // console.log(status);
                $("#sub_category_div").html(data.list);
                $(".select2").select2();
            }
        });
    });
    $("#product_edit_form").submit(function (e){
        e.preventDefault();
    });

    $("#btn_edit_product").click(function (e) { 
        e.preventDefault();
        var product = $("#product_name");
        var category_id = $("#category_id").val();
        var sub_category_id = $("#sub_category_id").val();
        var product_images = $("#product_images");
        var valid = true;
        $(".error").html('');
        if($.trim(product.val()) == ''){
            product.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(category_id) == ''){
            $(".cat_error").html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(sub_category_id) == ''){
            $(".sub_cat_error").html("<?=display('Required')?>");
            valid = false;
        }

        if(valid){
            if ($("#product_edit_form").isChanged()) {
                $("#product_edit_form").get(0).submit();
            } else {
                Swal.fire({
                    icon: 'error',
                    text: "<?=display('Nothing is changed')?>",
                    showConfirmButton: false,
                    timer: 1000
                })
            }
        }
    });

    $(".img-thumbnail").click(function (e){
        $(this).next().trigger('click');
    });
    $("input:file").change(function (e){
        readURL($(this));
    });

});
function readURL(input) {
  if (input.get(0).files && input.get(0).files[0]) {
    var reader = new FileReader();
    // console.log("read url called..."+input.prev().attr('name'));
    reader.onload = function(e) {
      input.prev().attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.get(0).files[0]); // convert to base64 string
  }
}
</script>