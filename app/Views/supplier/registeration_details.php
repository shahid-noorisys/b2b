
<style type="text/css">
.register-details {
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    height: 100%;
    width: 100%;
    padding: 0% 5%;
/*    position: fixed;
    overflow: scroll;*/
}
@media (min-width:360px) {
    .register-details {
       overflow: hidden;
       padding: 5% 0%;
    }
}
.form-top {
    overflow: hidden;
    padding: 0px 25px 0px 25px;
    background: #fff;
    border-radius: 4px 4px 0 0;
    text-align: left;
}
.form-top-left {
    float: left;
    width: 75%;
    padding-top: 25px;
}
.form-top-right {
    float: left;
    width: 25%;
    padding-top: 5px;
    font-size: 66px;
    color: #e6e6e6;
    line-height: 100px;
    text-align: right;
}
.white-box {
    background: #fff;
    padding: 25px;
    margin-bottom: 15px;
}  
.alert-empty-table-wrapper {
    padding: 0;
    clear: both;
    overflow: hidden;
}
.alert-empty-table-icon {
    margin: 10px 0;
    text-align: center;
}
.alert-empty-table-icon i {
    font-size: 90px;
    color: #EDF1F5;
}
.alert-empty-bot-info {
    margin-top: 50px;
}
.alert-empty-info {
    text-align: center;
    text-transform: none;
    font-size: 20px;
    color: #c9d2de;
} 
.btn-outline-secondary {
    color: #f1f4f7 !important;
    background: none; 
    border-color: #e0d9d9 !important;
}
.btn-outline-secondary:hover {
    color: #400909 !important;
    background-color: #f8f9fa;
}
</style>
    
<div class="register-details">
    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-2 col-md-8 col-sm-12 col-xs-12">
                <?php if (!empty($isExist)):?>
                <div class="card">
                    <div class="card-body wizard-content">
                       
                            <?php echo form_open_multipart('supplier/validate-registration', ' class="form-horizontal form-material sup_register_form" autocomplete="off" '); ?>
                                <!-- Step 1 -->
                                <input type="hidden" name="user_id" value="<?=$isExist->id?>">
                                <input type="hidden" name="token" value="<?=$isExist->token?>">
                                <section class="personal-info">
                                    <div class="form-top">
                                        <div class="form-top-left">
                                            <h3><?=display('Step')?> 1 / 4</h3>
                                            <p><?=display('Tell us who you are')?> : </p>
                                        </div>
                                        <div class="form-top-right">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <hr class="m-t-0 m-b-20">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <label class="control-label"><?=display('Gender')?> :</label>
                                            <div class="form-group">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="customRadio11" name="gender" class="custom-control-input gender" value="male">
                                                    <label class="custom-control-label" for="customRadio11"><?=display('Male')?></label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="customRadio22" name="gender" class="custom-control-input gender" value="female">
                                                    <label class="custom-control-label" for="customRadio22"><?=display('Female')?></label>
                                                </div> <br>
                                                <span class="error text-danger" id="error_gender"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="firstName1"><?=display('First Name')?> :</label>
                                                <input type="text" id="firstName" name="firstName" class="form-control"> 
                                                <span class="error text-danger" id="error_firstName"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="lastName1"><?=display('Last Name')?> :</label>
                                                <input type="text" id="lastName" name="lastName" class="form-control" id="lastName1"> 
                                                <span class="error text-danger" id="error_lastName"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label"><?=display('Country')?> :</label>
                                                <select class="select2 form-control" id="country_id" name="country_id">
                                                   <?php foreach ($countries as $key => $value):?>
                                                        <option value="<?=$value->id?>"><?=$value->name?></option>
                                                   <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                      
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="firstName1"><?=display('City')?> :</label>
                                                <input type="text" id="user_city" name="user_city" class="form-control"> 
                                                <span class="error text-danger" id="error_user_city"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="firstName1"><?=display('State')?> :</label>
                                                <input type="text" id="user_state" name="user_state" class="form-control"> 
                                                <span class="error text-danger" id="error_user_state"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="firstName1"><?=display('Postal Code')?> :</label>
                                                <input type="text" id="user_postal_code" name="user_postal_code" class="form-control"> 
                                                <span class="error text-danger" id="error_user_postal_code"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label"><?=display('Job Title')?> :</label>
                                                <select class="select2 form-control" id="designation" name="designation">
                                                    <option value="0"><?=display('Select Your Job Title')?></option>
                                                    <option value="1"><?=display('Manager')?></option>
                                                    <option value="2"><?=display('Other')?></option>
                                                </select>
                                                <span class="error text-danger" id="error_designation"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row manager-main-doc">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <label class="control-label"><?=display('Upload ID Proof')?> </label>
                                            <input type="file" class="form-control" class="main_doc" name="document">
                                            <span class="error text-danger" id="error_main_doc"></span>
                                        </div>
                                    </div>
                                    <div class="manger-details">
                                        <h3 class="box-title m-t-20"><?=display("Manager's Details")?></h3>
                                        <hr class="m-t-0 m-b-20">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <label class="control-label"><?=display('Gender')?> :</label>
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="customRadio33" name="manger_gender" class="custom-control-input manger_gender" value="male">
                                                        <label class="custom-control-label" for="customRadio33"><?=display('Male')?></label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="customRadio44" name="manger_gender" class="custom-control-input manger_gender" value="female">
                                                        <label class="custom-control-label" for="customRadio44"><?=display('Female')?></label>
                                                    </div> <br>
                                                    <span class="error text-danger" id="error_manager_gender"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="firstName1"><?=display('First Name')?> :</label>
                                                    <input type="text" id="manager_firstName" name="manager_firstName" class="form-control">
                                                     <span class="error text-danger" id="error_manager_firstName"></span> 
                                                </div>
                                            </div>
                                           
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="lastName1"><?=display('Last Name')?> :</label>
                                                    <input type="text" id="manager_lastName" name="manager_lastName" class="form-control"> 
                                                     <span class="error text-danger" id="error_manager_lastName"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <label class="control-label"><?=display('Upload ID Proof')?> </label>
                                                <input type="file" class="form-control" class="manger_avatar" name="manger_avatar">
                                                <span class="error text-danger" id="error_manger_avatar"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <div class="button-group">
                                           <!--  <button type="button" class="btn waves-effect waves-light btn-outline-primary btn-back"><?=display('Previous')?></button> -->
                                            <button type="button" class="btn waves-effect waves-light btn-outline-success btn-next"><?=display('Next')?></button>
                                        </div>                             
                                    </div>
                                </section>
                                <!-- Step 2 -->
                                <section class="company-info">
                                    <div class="form-top">
                                        <div class="form-top-left">
                                            <h3><?=display('Step')?> 2 / 4</h3>
                                            <p><?=display('Tell us about your company')?> : </p>
                                        </div>
                                        <div class="form-top-right">
                                            <i class="fas fa-address-card"></i>
                                        </div>
                                    </div>
                                    <hr class="m-t-0 m-b-20">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="jobTitle1"><?=display('Company Name')?> :</label>
                                                <input type="text" class="form-control" id="company_name" name="company_name"> 
                                                <span class="error text-danger" id="error_com_name"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="videoUrl1"><?=display('Company Address')?> :</label>
                                                <input type="text" class="form-control" id="company_address" name="company_address">
                                                <span class="error text-danger" id="error_com_address"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="jobTitle1"><?=display('City')?> :</label>
                                                <input type="text" class="form-control" id="city" name="city"> 
                                                <span class="error text-danger" id="error_city"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="videoUrl1"><?=display('Zip Code')?> :</label>
                                                <input type="text" class="form-control" id="zipcode" name="zipcode">
                                                <span class="error text-danger" id="error_zipcode"></span>
                                            </div>
                                        </div>
                                    </div>          
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label"><?=display('Company Type')?> :</label>
                                                <select class="select2 form-control" id="company_type" name="company_type">
                                                   <?php foreach ($comapny_types as $key => $value):?>
                                                        <option value="<?=$value->id?>"><?=$value->type?></option>
                                                   <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <label class="control-label"><?=display('Upload document')?> </label>
                                            <input type="file" class="form-control" class="company_document" name="company_document">
                                            <span class="error text-danger" id="error_company_document"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <div class="button-group">
                                            <button type="button" class="btn waves-effect waves-light btn-outline-primary btn-company-back"><?=display('Previous')?></button>
                                            <button type="button" class="btn waves-effect waves-light btn-outline-success btn-company-next"><?=display('Next')?></button>
                                        </div>                         
                                    </div>
                                </section>
                                <!-- Step 3 -->
                                <section class="bank-info">
                                    <div class="form-top">
                                        <div class="form-top-left">
                                            <h3><?=display('Step')?> 3 / 4</h3>
                                            <p><?=display('Provide your bank details')?>: </p>
                                        </div>
                                        <div class="form-top-right">
                                            <i class="far fa-money-bill-alt"></i>
                                        </div>
                                    </div>
                                    <hr class="m-t-0 m-b-20">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="jobTitle1"><?=display('Bank Name')?> :</label>
                                                <input type="text" class="form-control" id="bank_name" name="bank_name"> 
                                                <span class="error text-danger" id="error_bank_name"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="videoUrl1"><?=display('Bank Address')?> :</label>
                                                <input type="text" class="form-control" id="bank_address" name="bank_address">
                                                <span class="error text-danger" id="error_bank_address"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="jobTitle1"><?=display('City')?> :</label>
                                                <input type="text" class="form-control" id="bank_city" name="bank_city"> 
                                                <span class="error text-danger" id="error_bank_city"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="videoUrl1"><?=display('Zip Code')?> :</label>
                                                <input type="text" class="form-control" id="bank_zipcode" name="bank_zipcode">
                                                <span class="error text-danger" id="error_bank_zipcode"></span>
                                            </div>
                                        </div>
                                    </div>          
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="jobTitle1"><?=display('IBAN')?> :</label>
                                                <input type="text" class="form-control" id="iban" name="iban"> 
                                                <span class="error text-danger" id="error_iban"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="jobTitle1"><?=display('BIC/Swift Code')?> :</label>
                                                <input type="text" class="form-control" id="bic" name="bic"> 
                                                <span class="error text-danger" id="error_bic"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <label class="control-label"><?=display('Upload document')?>  </label>
                                            <input type="file" class="form-control" class="bank" name="bank_avatar" id="bank_avatar">
                                            <span class="error text-danger" id="error_bank_doc"></span>
                                        </div>
                                    </div>  
                                    <div class="col-md-12 text-right">
                                        <div class="button-group">
                                            <button type="button" class="btn waves-effect waves-light btn-outline-primary btn-bank-back"><?=display('Previous')?></button>
                                            <button type="button" class="btn waves-effect waves-light btn-outline-success btn-bank-next"><?=display('Next')?></button>
                                        </div>                          
                                    </div>        
                                </section>
                                <!-- Step 4 -->
                                <section class="confirmation">
                                    <div class="form-top">
                                        <div class="form-top-left">
                                            <h3><?=display('Step')?> 4 / 4</h3>
                                            <p><?=display('Completed')?>: </p>
                                        </div>
                                        <div class="form-top-right">
                                            <i class="far fa-handshake"></i>
                                        </div>
                                    </div>
                                    <hr class="m-t-0 m-b-20">
                                    <div class="row">
                                       <div class="col-sm-12">
                                            <div class="white-box">
                                                <div class="alert-empty-table-wrapper">
                                                    <div class="alert-empty-table-icon"><i class="fas fa-paper-plane"></i></div>
                                                    <div class="alert-empty-info alert-empty-bot-info">
                                                        <span>Your Registration Completed Successfully</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>      
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <div class="button-group">
                                            <button type="button" class="btn waves-effect waves-light btn-outline-primary btn-confirm-back"><?=display('Previous')?></button>
                                            <button type="button" class="btn waves-effect waves-light btn-outline-success btn-confirm"><?=display('Finish')?></button>
                                        </div>                     
                                    </div>
                                </section>
                            <?php echo form_close(); ?>
                           
                    </div>
                </div>
                <?php else:?>
                    <div class="card text-center" style="background-image: linear-gradient(#350505, #ef4747);">
                        <div class="card-body">
                            <div class="alert-empty-table-wrapper">
                                <div class="alert-empty-table-icon"><i class="far fa-thumbs-up"></i></div>
                                <div class="alert-empty-info alert-empty-bot-info">
                                    <span>Already, Your Registration Completed Successfully</span>
                                </div>
                                 <a href="<?=base_url()?>" class="btn waves-effect waves-light btn-outline-secondary m-t-20">Go To Home</a>
                                 <a href="<?=base_url('supplier/login')?>" class="btn waves-effect waves-light btn-outline-secondary m-t-20 m-l-10">Go To Login</a>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">        

    $(function() {
        $(".preloader").fadeOut();
        $('[data-toggle="tooltip"]').tooltip()

    });

    function validateFileType(){
      var fileName = file.value,
      idxDot = fileName.lastIndexOf(".") + 1,
      extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
      if (extFile=="jpg" || extFile=="jpeg" || extFile=="png" || extFile=="pdf"){
        return true;
      }else{
        file.value = "";  // Reset the input so no files are uploaded
        return false;
      }
    }

    $(function() {
        // $('.personal-info').hide();
        $('.company-info').hide();
        $('.bank-info').hide();
        $('.confirmation').hide();

        // step 1 
        $(document).on('click','.btn-next',function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($('input[name="gender"]:checked').length == 0)
            {
              $("#error_gender").html("<?php echo display('Please Select Gender'); ?>");
              valid = false;
            }
            if($.trim($("#firstName").val()) == '')
            {
              $("#error_firstName").html("<?php echo display('Please Enter First Name'); ?>");
              valid = false;
            }
            if($.trim($("#lastName").val()) == '')
            {
              $("#error_lastName").html("<?php echo display('Please Enter Last Name'); ?>");
              valid = false;
            }                
            if($.trim($("#designation").val()) == 0)
            {
              $("#error_designation").html("<?php echo display('Please Select Your Job Title'); ?>");
              valid = false;
            }
            // if ($.trim($("#designation").val()) == 1) {
            //     if($.trim($("#main_doc").val()) == '')
            //     {
            //       $("#error_main_doc").html("<?php echo display('Please Your Company ID Proof'); ?>");
            //       valid = false;
            //     }
            // } else if($.trim($("#designation").val()) == 2) {
            //     if($('input[name="manager_gender"]:checked').length == 0)
            //     {
            //       $("#error_manager_gender").html("<?php echo display('Please Select Gender'); ?>");
            //       valid = false;
            //     }
            //     if($.trim($("#manager_firstName").val()) == '')
            //     {
            //       $("#error_manager_firstName").html("<?php echo display('Please Enter First Name'); ?>");
            //       valid = false;
            //     }
            //     if($.trim($("#manager_lastName").val()) == '')
            //     {
            //       $("#error_manager_lastName").html("<?php echo display('Please Enter Last Name'); ?>");
            //       valid = false;
            //     }  
            //     if($.trim($("#manger_avatar").val()) == '')
            //     {
            //       $("#error_manger_avatar").html("<?php echo display('Please Your Company ID Proof'); ?>");
            //       valid = false;
            //     }           
            // }
           
            if(valid){
              $('.personal-info').hide();
              $('.company-info').show();
            }

        });

        // step 2 
        $(document).on('click','.btn-company-next',function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#company_name").val()) == '')
            {
              $("#error_com_name").html("<?php echo display('Please Enter Company Name'); ?>");
              valid = false;
            }
            
            if($.trim($("#company_address").val()) == '')
            {
              $("#error_com_address").html("<?php echo display('Please Enter Company Address'); ?>");
              valid = false;
            }                
            if($.trim($("#city").val()) == 0)
            {
              $("#error_city").html("<?php echo display('Please Enter City Name'); ?>");
              valid = false;
            }
            if($.trim($("#zipcode").val()) == 0)
            {
              $("#error_zipcode").html("<?php echo display('Please Enter Zip Code'); ?>");
              valid = false;
            }              
            // if($.trim($("#company_document").val()) == 0)
            // {
            //   $("#error_company_document").html("<?php echo display('Please Upload Bank Identity Proof'); ?>");
            //   valid = false;
            // }            
            if(valid){
                $('.company-info').hide();
                $('.bank-info').show();
            }
           
        });

        $(document).on('click','.btn-company-back',function(e){
            $('.personal-info').show();
            $('.company-info').hide();
        });

        // step 3 
        $(document).on('click','.btn-bank-next',function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#bank_name").val()) == '')
            {
              $("#error_bank_name").html("<?php echo display('Please Enter Bank Name'); ?>");
              valid = false;
            }
            if($.trim($("#bank_address").val()) == '')
            {
              $("#error_bank_address").html("<?php echo display('Please Enter Bank Address'); ?>");
              valid = false;
            }                
            if($.trim($("#bank_city").val()) == 0)
            {
              $("#error_bank_city").html("<?php echo display('Please Enter City Name'); ?>");
              valid = false;
            }
            if($.trim($("#bank_zipcode").val()) == 0)
            {
              $("#error_bank_zipcode").html("<?php echo display('Please Enter Zip Code'); ?>");
              valid = false;
            }
              if($.trim($("#iban").val()) == 0)
            {
              $("#error_iban").html("<?php echo display('Please Enter IBAN'); ?>");
              valid = false;
            }            
            if($.trim($("#bic").val()) == 0)
            {
              $("#error_bic").html("<?php echo display('Please Enter BIC/Swift Code'); ?>");
              valid = false;
            }            
            // if($.trim($("#bank_avatar").val()) == 0)
            // {
            //   $("#error_bank_doc").html("<?php echo display('Please Upload Bank Identity Proof'); ?>");
            //   valid = false;
            // }
             if(valid){
                $('.bank-info').hide();
                $('.confirmation').show();
            }
           
        }); 
        $(document).on('click','.btn-bank-back',function(){
            $('.bank-info').hide();
            $('.company-info').show();
        });

        // step 4 
        $(document).on('click','.btn-confirm-back',function(){
            $('.bank-info').show();
            $('.confirmation').hide();
        }); 
        $(document).on('click','.btn-confirm',function(){
            $(".sup_register_form").get(0).submit();  
        });
    });


    $(function () {
        $('.manger-details').hide();
        $('.manager-main-doc').hide();
        $(document).on('change','#designation',function(){
            var job_title = $(this).val();
            if (job_title == 1 ) {
                $('.manager-main-doc').show();
                $('.manger-details').hide();
            } else {
                $('.manager-main-doc').hide();
                $('.manger-details').show();
            }
        });

        $(document).on('click','#next',function(){
          alert('Next')
        });
    });
</script>
