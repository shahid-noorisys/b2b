<div class="login-register">
    <div class="login-box card">
        <div class="card-body">
            <?php echo form_open('supplier/login', 'class ="form-horizontal form-material login_form" id="loginform" autocomplete="off"'); ?>
                <h3 class="text-center m-b-20"><?=display('Sign In')?></h3>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="<?=display('Email')?>" name="email" id="email"  autocomplete="off"> 
                        <span class="error text-danger" id="error_email"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="<?=display('Password')?>" name="password" id="password"  autocomplete="off"> 
                        <span class="error text-danger" id="error_password"></span> 
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="d-flex no-block align-items-center">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Remember me</label>
                            </div> 
                            <div class="ml-auto">
                                <a href="javascript:void(0)" id="to-recover" class="text-muted"><i class="fas fa-lock m-r-5"></i> Forgot pwd?</a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center">
                    <div class="col-xs-12 p-b-20">
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary login"><?=display('Log In')?></button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <?=display('Do not have an account?')?> <a href="<?=base_url('supplier/registration')?>" class="text-primary m-l-5"><b><?=display('Sign Up')?></b></a>
                    </div>
                </div>
            <?php echo form_close(); ?>
            <?php echo form_open('supplier/password-forgot', 'class ="form-horizontal form-material pswd_recover_form" id="recoverform" autocomplete="off"'); ?>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3><?=display('Recover Password')?></h3>
                        <p class="text-muted"><?=display('Enter your Email and instructions will be sent to you!')?> </p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="<?=display('Email')?>" name="recover_email" id="recover_email"  autocomplete="off"> 
                        <span class="error text-danger" id="error_recover_email"></span>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary recover-btn"><?=display('Reset')?></button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <?=display('Go back to login ?')?> <a href="javascript:void(0)" class="text-primary m-l-5" id="to-login"><b><?=display('Sign In')?></b></a>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    // ============================================================== 
    // Login and Recover Password 
    // ============================================================== 
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });

    $('#to-login').on("click", function() {
        $("#recoverform").slideUp();
        $("#loginform").fadeIn();
    });

    $(".recover-btn").on('click', function(e){
        e.preventDefault();
        var email = $("#recover_email");
        var valid = true;
        $(".error").html('');
        if($.trim(email.val()) == '')
        {
          email.next().html("<?php echo display('Please Enter Your Registered Email'); ?>");
          valid = false;
        }
        else if(! validEmail($.trim(email.val())))
        {
            email.next().html("<?php echo display('Please Enter Valid Email'); ?>");
            valid = false;
        }
        if(valid){
          $(".pswd_recover_form").get(0).submit();  
        }
    });

    $(".login").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#email").val()) == '')
        {
          $("#error_email").html("<?php echo display('Please Enter Your Email'); ?>");
          valid = false;
        }
        if($.trim($("#password").val()) == '')
        {
          $("#error_password").html("<?php echo display('Please Enter Your Password'); ?>");
          valid = false;
        }
        if(valid){
          $(".login_form").get(0).submit();  
        }
    });
    function validEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            return false;
        }else{
            return true;
        }
    }
</script>
