<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<?php 
    $UserModel = model('App\Models\UserModel');
    $UserDetails = $UserModel->find(session()->get('user_details')->admin_id);
?>
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div><img src="<?=base_url($UserDetails->thumbnail)?>" alt="user-img" class="img-circle"></div>
                <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$UserDetails->firstname.' '.$UserDetails->lastname?><span class="caret"></span></a>
                    <div class="dropdown-menu animated flipInY">
                        <!-- text-->
                        <a href="<?=base_url('supplier/profile')?>" class="dropdown-item"><i class="ti-user"></i> <?=display('My Profile')?></a>
                        
                        <!-- <div class="dropdown-divider"></div> -->
                        <!-- text-->
                       <!--  <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> <?=display('Account Setting')?></a> -->
                        <!-- text-->
                        <div class="dropdown-divider"></div>
                        <!-- text-->
                        <a href="<?=base_url('supplier/logout')?>" class="dropdown-item"><i class="fa fa-power-off"></i> <?=display('Logout')?></a>
                        <!-- text-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
               
                <li class="nav-small-cap">---</li>

                <li> <a class="waves-effect waves-dark" href="<?=base_url('supplier/dashboard')?>" aria-expanded="false"><i class="icon-speedometer"></i><span class="hide-menu"><?=display('Dashboard')?></span></a></li>

                <li> <a class="waves-effect waves-dark" href="<?=base_url('supplier/products')?>" aria-expanded="false"><i class="ti-layout-grid2"></i><span class="hide-menu"><?=display('Products')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('supplier/messages')?>" aria-expanded="false"><i class="icon-cursor"></i><span class="hide-menu"><?=display('Messages')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('supplier/quotations')?>" aria-expanded="false"><i class="icon-tag"></i><span class="hide-menu"><?=display('Quotations')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('supplier/orders')?>" aria-expanded="false"><i class="icon-basket"></i><span class="hide-menu"><?=display('Order History')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('supplier/reviews')?>" aria-expanded="false"><i class="icon-bubbles"></i><span class="hide-menu"><?=display('Reviews & Rating')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('supplier/payment-history')?>" aria-expanded="false"><i class="icon-wallet"></i><span class="hide-menu"><?=display('Payment History')?></span></a></li>

                <!-- <li> 
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu">Masters</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="index.html">Categories </a></li>
                        <li><a href="eco-products.html">Sub-Categories</a></li>
                    </ul>
                </li> -->
                
                <!-- <li> <a class="waves-effect waves-dark" href="../documentation/documentation.html" aria-expanded="false"><i class="far fa-circle text-danger"></i><span class="hide-menu">Documentation</span></a></li> -->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->