<div class="login-register">
    <div class="login-box card">
        <div class="card-body">
        
            <?php echo form_open('supplier/registration', 'class ="form-horizontal form-material login_form" id="loginform" autocomplete="off"'); ?>
                <h3 class="text-center m-b-20">Enquiry</h3>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="<?=display('Email')?>" name="email" id="email"  autocomplete="off"> 
                        <span class="error text-danger" id="error_email"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="<?=display('Mobile')?>" name="mobile" id="mobile"  autocomplete="off"> 
                        <span class="error text-danger" id="error_mobile"></span> 
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <select class="form-control custom-select" id="platform_type" name="platform_type">
                            <option value="0"><?=display('Where did you hear about Yahodihime?')?></option>
                            <?php foreach ($survey as $key => $value):?>
                            <option value="<?=$value->id?>"><?=$value->platform_type?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <span class="error text-danger" id="error_platform_type"></span>
                </div>
                
                <div class="form-group text-center">
                    <div class="col-xs-12 p-b-20">
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary login"><?=display('Get Activation Link')?></button>
                    </div>
                </div>
               <!--  <div class="form-group m-b-0">
                    <div class="col-sm-12 text-right">
                        <a href="<?=base_url('supplier/login')?>" class="text-info m-l-5"><b><?=display('Sign In')?></b></a>
                    </div>
                </div> -->
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                       <?=display('Already have an account?')?> <a href="<?=base_url('supplier/login')?>" class="text-primary m-l-5"><b><?=display('Sign In')?></b></a>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
   
<script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(".login").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#email").val()) == '')
        {
          $("#error_email").html("<?php echo display('Please Enter Your Email'); ?>");
          valid = false;
        }
        else if(!validEmail($.trim($("#email").val())))
        {

            $("#error_email").html("<?php echo display('Please Enter Valid Email'); ?>");
            valid = false;
        }
        if($.trim($("#mobile").val()) == '')
        {
          $("#error_mobile").html("<?php echo display('Please Enter Mobile Number'); ?>");
          valid = false;
        } 
        if($.trim($("#platform_type").val()) == 0)
        {
          $("#error_platform_type").html("<?php echo display('Please select platform type Where did you hear about Yahodihime?'); ?>");
          valid = false;
        }
        if(valid){
          $(".login_form").get(0).submit();  
        }
    });
    function validEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            return false;
        }else{
            return true;
        }
    }
</script>
