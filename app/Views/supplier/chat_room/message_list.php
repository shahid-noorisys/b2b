<?php foreach ($messages as $key => $value) { 
    if ($value->send_by !== 'Seller') {
        $reciever = get_user_details_by_id($value->sender_id,1);
?>
    <!-- Reciever -->
    <li>
        <div class="chat-img"><img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user" /></div>
        <div class="chat-content">
            <h5><?=$reciever->firstname.' '.$reciever->lastname;?></h5>
            <div class="box bg-light-info"><?=$value->text_msg?></div>
            <div class="chat-time"><?= date('H:i A',strtotime($value->created_date));?></div>
        </div>
    </li>
    <!-- Reciever -->
    <?php } else { 
        $sender = get_user_details_by_id($value->sender_id,$value->user_role); 
    ?>
    <!-- Sender -->
    <li class="reverse">
        <div class="chat-content">
            <h5><?=$sender->firstname.' '.$sender->lastname;?></h5>
            <div class="box bg-light-info"><?=$value->text_msg?></div>
            <div class="chat-time"><?= date('H:i A',strtotime($value->created_date));?></div>
        </div>
        <div class="chat-img"><img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user" /></div>
    </li>
    <!-- Sender -->
    <?php } ?>
<?php } ?>