<style type="text/css">
.chat-list li .chat-time {
    display: block;
    font-size: 10px;
    color: #6c757d;
    margin: 5px 0 15px 0px !important;
}
#avatar, .avatar {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  padding: 0;
  /*font-size: 20px;*/
  cursor: pointer;
  opacity: 0;
  filter: alpha(opacity=0);
}
.btn-attachment { 
    background: transparent; 
    /*margin-left: -10px; */
    font-size: 24px;
    padding: 6px 0px 0px 0px;
}
.white-box {
    background: #fff;
    padding: 25px;
    margin-bottom: 15px;
}  
.alert-empty-table-wrapper {
    padding: 0;
    clear: both;
    overflow: hidden;
}
.alert-empty-table-icon {
    margin: 10px 0;
    text-align: center;
}
.alert-empty-table-icon i {
    font-size: 110px;
    color: #EDF1F5;
}
.alert-empty-bot-info {
    margin-top: 50px;
}
.alert-empty-info {
    text-align: center;
    text-transform: none;
    font-size: 20px;
    color: #c9d2de;
} 
.noti_bubble {
    position: relative;
    display: inline-block;
    top: -13px;
    left: -14px;
    background-color: #f4ff00;
    font-weight: bold;
    font-size: 13px;
    border-radius: 2px;
}
.preview {
    margin: 5px 0 0 0;
    padding: 0 0 1px;
    font-weight: 400;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    transition: 1s all ease;
}
.msg-counter {
    position: absolute;
    top: 14px;
    left: 213px;
    margin: -2px 0 0 -2px;
    width: 25px;
    height: 25px;
    /*border-radius: 50%;*/
    background: #fb9678;
    color: #fff !important;
    font-size: 11px;
    font-weight: bold;
    padding-top: 4px;
}
</style>
<?php 
        $uri = service('uri');
        $quotation_id = $uri->getSegment(3);
?>
<!-- ============================================================== -->

<div class="row">
    <div class="col-12">
        <div class="card m-b-0">
            <!-- .chat-row -->
            <div class="chat-main-box">
                <!-- .chat-left-panel -->
                <div class="chat-left-aside">
                    <div class="open-panel"><i class="ti-angle-right"></i></div>
                    <div class="chat-left-inner">
                        <div class="form-material">
                            <input class="form-control p-2"  type="text" onkeyup="Searchquotation()" id="searchquote" placeholder="Search Contact">
                        </div>
                        <ul class="chatonline style-none" id="chat_list">
                            <?php foreach ($quotation as $key => $value) { ?>
                                <?php 
                                     $quote = model('App\Models\QuotationModel');
                                     $quotation = $quote->find($value->id);
                                     $active = ($quotation_id == $value->id)?'active':'';

                                     $message = model('App\Models\MessageModel');
                                     $messages = $message->where(['q_id'=>$value->id,'read_status'=>2])->findAll();
                                    
                                    $message = model('App\Models\MessageModel');
                                    $response = $message->where('q_id',$value->id)->orderBy('created_date','DESC')->limit(1)->find();
                                     $msg_count = $message->where(['q_id'=>$value->id,'read_status'=>'No',])->findAll();
                                ?>
                                <li>
                                    <a href="javascript:void(0)" class="get-message <?=$active?>" data-quotation="<?=$value->id;?>" data-supplier ="<?=$quotation->request_to?>" id="get_message_<?=$value->id;?>">
                                        <img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user-img" class="img-circle">
                                        <!-- <span class="noti_bubble"><?=count($messages)?></span>  -->
                                        <span><?=$quotation->q_no?> 
                                            <?php if(!empty($msg_count)):?>
                                                
                                               <!--  <span class="badge badge-primary ml-auto msg-counter">
                                                    <?=count($msg_count)?>
                                                </span> -->
                                            <?php endif;?>
                                            <?php if (!empty($response)): $sender = '';?>
                                                <?php foreach ($response as $value): ?>
                                                    <?php 
                                                        if($value->send_by == 'Seller'):
                                                            $sender = display('You');
                                                        else:
                                                            $sender = display('yahodehime');
                                                        endif;
                                                    ?>
                                                  <small class="preview"><?=$sender.' : '.$value->text_msg?></small>
                                                <?php endforeach;?>
                                            <?php else:?>
                                                <small class="preview"><?=display('There is no message to display')?></small>
                                            <?php endif;?>
                                        </span>
                                    </a>
                                </li>
                            <?php } ?> 
                            <li class="p-20"></li>
                        </ul>
                    </div>
                </div>
                <div class="chat-right-aside">
                    <div class="card text-center initial-message d-none" style="padding:100px 0px;">
                        <div class="card-body">
                            <div class="alert-empty-table-wrapper">
                                <div class="alert-empty-table-icon"><i class="far fa-comments"></i></div>
                                <div class="alert-empty-info alert-empty-bot-info">
                                    <span>Select the quotation from the list to start messaging</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="messaging-panel d-none">                        
                        <div class="chat-main-header">
                            <div class="p-3 b-b">
                                <h4 class="box-title">Chat Message</h4>
                            </div>
                        </div>
                        <div class="chat-rbox">
                            <ul class="chat-list p-3">
                               
                               
                               
                            </ul>
                        </div>
                        <div class="card-body border-top">
                            <?php echo form_open('', 'class ="form-horizontal" autocomplete="off"'); ?>
                                <input type="hidden" name="quotesion_id" id="quotesion_id" value="">
                                <div class="row">
                                    <div class="col-1">
                                        <div class="">
                                            <span class="btn btn-attachment">
                                                <i class="icon-paper-clip"></i> 
                                            </span>
                                            <input type="file" name="avatar" class="upload avatar" onchange="attachement_file_name(event);" />
                                        </div>
                                    </div>
                                  
                                    <div class="col-9">
                                        <textarea placeholder="Type your message here" class="form-control border-0 message"></textarea>
                                        <span class="attached_file"></span>
                                    </div>
                                    <div class="col-1 text-right">
                                        <button type="button" class="btn btn-outline-info btn-circle btn-lg btn-message"><i class="fas fa-paper-plane"></i> </button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- .chat-right-panel -->
            </div>
            <!-- /.chat-row -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->

<script type="text/javascript">

    function Searchquotation() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("searchquote");
        filter = input.value.toUpperCase();
        ul = document.getElementById("chat_list");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }

    function GetMessagelist(){
        var selected_quotation = $('.chatonline').find('li a.active').attr('id');
        var quotation_id = $('#'+selected_quotation).data('quotation');
        var supplier_id = $('#'+selected_quotation).data('supplier');
        var token = "<?= csrf_hash() ?>";
        // alert(quotation_id);
        $.ajax({
             url: "<?php echo base_url('supplier/get_messages'); ?>",
             type: 'POST',
             dataType: 'html',
             data: {quotation_id:quotation_id,supplier_id:supplier_id,csrf_stream_token:token},
        }).done(function(data) {
            // console.log(data);
            $('.chat-list').html(data);
            $('#quotesion_id').val(quotation_id);
        });
        
    }


    function attachement_file_name(e) { 
        $(".attached_file").addClass('text-success').text(e.target.files[0].name); 
    }
    $(function () {
        
        GetMessagelist();
        var url_quote_id = '<?=$quotation_id?>';
        if (url_quote_id !== '') { 
            $('.initial-message').addClass('d-none'); 
            $('.messaging-panel').removeClass('d-none');
        } else {  
            $('.initial-message').removeClass('d-none'); 
            $('.messaging-panel').addClass('d-none');
        }
        
        
        $(".btn-attachment").on('click', function(e) { e.preventDefault(); 
            $(".avatar").trigger('click'); 
        });

        function validate_message(message) {
            if(message=="") { return; }
            var file = $('.avatar').prop('files')[0];
            var formData = new FormData();
            var token = "<?= csrf_hash() ?>";
            var quotation_id = $('#quotesion_id').val();
            
            formData.append("message",message);
            formData.append("quotation_id",quotation_id);
            formData.append("file_name",file);
            formData.append("csrf_stream_token",token);
    
            $.ajax({
                 url: "<?php echo base_url('supplier/send_message'); ?>",
                 type: 'POST',
                 dataType: 'html',
                 data: formData,
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
            }).done(function(data) {
                // console.log(data);
                $('.chat-list').empty().append(data);
                $(".attached_file").text('');
                $('.avatar').val('');
            });
        };

        $('.btn-message').click(function(e) {
            var message = $('.message').val();
            $(".message").val('');
            return validate_message(message);  
        });

        $(document).on('click','.get-message',function(e) {

            var selected_quotation = $('.chatonline').find('li a.active').attr('id');
            $('#'+selected_quotation).removeClass('active');

            var quotation_id = $(this).data('quotation');
            var supplier_id = $(this).data('supplier');
            var token = "<?= csrf_hash() ?>";

            $.ajax({
                 url: "<?php echo base_url('supplier/get_messages'); ?>",
                 type: 'POST',
                 dataType: 'html',
                 data: {quotation_id:quotation_id,supplier_id:supplier_id,csrf_stream_token:token},
            }).done(function(data) {
                // console.log(data);
                $('.initial-message').addClass('d-none'); 
                $('.messaging-panel').removeClass('d-none');
                $('#get_message_'+quotation_id).addClass('active');
                $('.chat-list').html(data);
                $('#quotesion_id').val(quotation_id);
            });

        });

      
    });
</script>