<link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />
<style type="text/css">
input[type=radio], input[type=checkbox] {
    box-sizing: border-box;
    padding: 0;
    position: absolute;
    opacity: 0;
}
.btn-group>.btn {
    position: relative;
    flex: 1 1 auto;
    display: inline-block;
    border: 1px solid;
    cursor: pointer;
}
</style>
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open('supplier/orders','class="order-filter-form"'); ?>
                            <div class="row">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class=" far fa-lightbulb"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Ordre number / Ref number')?>" aria-describedby="basic-addon1" name="order_no" value ="<?=(isset($_SESSION['order']['order_no']) && $_SESSION['order']['order_no']!="")?$_SESSION['order']['order_no']:''?>">
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class=" far fa-lightbulb"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Quotation number/ Ref number')?>" aria-describedby="basic-addon1" name="quote_no" value ="<?=(isset($_SESSION['order']['quote_no']) && $_SESSION['order']['quote_no']!="")?$_SESSION['order']['quote_no']:''?>">
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                   
                                  <?=form_dropdown('user_id', $users_list, (isset($_SESSION['order']['user_id']) && $_SESSION['order']['user_id']!="")?$_SESSION['order']['user_id']:'' ,' class="select2 form-control" ');?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select From Date')?>" name="start" value="<?=(isset($_SESSION['order']['start']) && $_SESSION['order']['start']!="")?date('m/d/Y',strtotime($_SESSION['order']['start'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select To Date')?>" name="end" value="<?=(isset($_SESSION['order']['end']) && $_SESSION['order']['end']!="")?date('m/d/Y',strtotime($_SESSION['order']['end'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-md-12 pull-left">
                                    <label for="status"><?=display('Status')?> : &nbsp;</label>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['order']['status']) && $_SESSION['order']['status'] == 'pending')?'active':''?>">
                                            <input type="radio" name="status" id="pending" value="pending" <?=(isset($_SESSION['order']['status']) && $_SESSION['order']['status'] == 'pending')?'checked':''?>><?=strtoupper(display('Pending'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['order']['status']) && $_SESSION['order']['status'] == 'in_progress')?'active':''?>">
                                            <input type="radio" name="status" id="in_progress" value="in_progress" <?=(isset($_SESSION['order']['status']) && $_SESSION['order']['status'] == 'in_progress')?'checked':''?>><?=strtoupper(display('In-progress'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['order']['status']) && $_SESSION['order']['status'] == 'delivered')?'active':''?>">
                                            <input type="radio" name="status" id="delivered" value="delivered" <?=(isset($_SESSION['order']['status']) && $_SESSION['order']['status'] == 'delivered')?'checked':''?>><?=strtoupper(display('Delivered'))?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12 text-left">
                                   <div class="row button-group">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-success" name="order_filter" value="filter"><i class="fas fa-filter m-r-5"></i><?=display('Filter')?></button>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-danger" name="remove_filter" value="remove"><i class="fas fa-ban m-r-5"></i><?=display('Cancel')?></button>
                                        </div>
                                    </div>     
                                </div>  
                            </div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <?php if(!empty($orders)): ?>
            <div class="table-responsive">
                <table id="product" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?=display('Order Number')?></th>
                            <th><?=display('Quotation Number')?></th>
                            <th><?=display('Request From')?></th>
                            <th><?=display('Amount')?></th>
                            <th><?=display('Total Items')?></th>
                            <th><?=display('Payment mode')?></th>
                            <th><?=display('Order status')?></th>
                            <th><?=display('Action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orders as $key => $value): ?>
                            <?php $user = get_user_details_by_id($value->buyer_id,3); ?>
                            <tr>
                                <td><?=$key+1?></td>
                                <td><?=$value->order_no?></td>
                                <td><?=$value->q_no?></td>
                                <td><?=$user->firstname.' '.$user->lastname;?></td>
                                <td><?=$value->amount?></td>
                                <td><?=$value->total_items?></td>
                                <td><?=($value->payment_mode == 'Online')?'<span class="label label-success font-weight-100">'.display('Online').'</span>':'<span class="label label-warning font-weight-100">'.display('Cash').'</span>'?> </td>
                                <td>
                                    <?php if($value->order_status == 'pending'):?>
                                        <span class="label label-primary font-weight-100"><?=display('Pending')?></span>
                                    <?php endif;?>
                                    <?php if($value->order_status == 'in_progress'):?>
                                        <span class="label label-warning font-weight-100"><?=display('In-progress')?></span>
                                    <?php endif;?>
                                    <?php if($value->order_status == 'delivered'):?>
                                        <span class="label label-success font-weight-100"><?=display('Delivered')?></span>
                                    <?php endif;?>
                                </td>
                                <td>
                                    <a href="<?=base_url('supplier/orders/view/'.$value->id)?>" class="text-dark m-r-10" title="<?=display('View')?>" data-toggle="tooltip"  data-id="<?=$value->id?>"><i class="fas fa-eye"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="alert-empty-table-wrapper">
                            <div class="alert-empty-table-icon"><i class="fas fa-box"></i></div>
                            <div class="alert-empty-info alert-empty-bot-info">
                                <span><?=display('There is no order to display')?></span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- ============================================================== -->
<script src="<?php echo base_url('public/assets/admin/dist/js/select2.full.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $('#product').DataTable();
    $(".select2").select2();
    jQuery('.mydatepicker, #datepicker').datepicker();
</script>