<style type="text/css">
    .w-30{
        width: 30%;
    }
/*    .dropdown-item:hover {
    color: #ffffff !important;
    background-color: #008f6c !important;
    }*/
    .selected-status {
    color: #ffffff !important;
    background-color: #008f6c !important;
    }
</style>
<?php 
    $seller_company = get_company_by_user_id($order->supplier_id);
    $buyer_company = get_company_by_user_id($order->buyer_id);
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body p-10">
                <div class="row">
                <div class="col text-left"><a class="btn btn-primary" href="<?= base_url('supplier/orders')?>" role="button"> <i class="fa fa-arrow-left"></i> <?=display('Back')?> </a></div>
                <div class="col pull-right text-right">
                    <label for="status-change font-bold" class="col-form-label m-r-10"><?=display('Order status')?> :</label>
                    <div class="btn-group">
                        <button type="button" class="btn btn-block btn-success"><?= ucfirst($order->order_status)?></button>
                        <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item btn-status <?=($order->order_status == 'pending')?'selected-status':''?>" data-state="pending" href="javascript:void(0)"><?=display('Pending')?></a>
                            <a class="dropdown-item btn-status <?=($order->order_status == 'in_progress')?'selected-status':''?>" data-state="in_progress" href="javascript:void(0)"><?=display('In-progress')?></a>
                            <a class="dropdown-item btn-status <?=($order->order_status == 'delivered')?'selected-status':''?>" data-state="delivered" href="javascript:void(0)"><?=display('Delivered')?></a>
                        </div>
                    </div>  
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-body printableArea">
            <h3><b><?= strtoupper(display('Order')) ?></b> <span class="pull-right">#<?=$order->order_no?></span></h3>
            <hr>
          
            <div class="row">
                <div class="col-md-6">
                    <div class="pull-left">
                        <address>
                            <h5><?= strtoupper(display('From'));?>,</h5>
                            <h4> &nbsp;<b class="font-bold"><?=($seller_company->company_name)??''?></b></h4>
                            <p class="text-muted m-l-5"><?=($seller_company->address)??''?>,
                                <br/> <?=($seller_company->city)??''?>  <?=($seller_company->zip_code)??''?>,
                                <br/> <?=(!empty($seller_company) AND $seller_company->country)?get_country_by_id($seller_company->country)->name:''?></p>
                        </address>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pull-right text-right">
                        <address>
                            <h5><?= strtoupper(display('To'));?>,</h5>
                            <h4 class="font-bold"><?=($buyer_company->company_name)??''?>,</h4>
                            <p class="text-muted m-l-30"><?=($buyer_company->address)??''?>,
                                <br/> <?=($buyer_company->city)??''?>  <?=($buyer_company->zip_code)??''?>,
                                <br/> <?=(!empty($buyer_company) AND $buyer_company->country)?get_country_by_id($buyer_company->country)->name:''?>
                            </p> 
                            <h6 class="m-t-30"><b><?=display('Order Date')?> : </b> <span><?= date('d M Y',strtotime($order->created_date)) ?></span></h6>

                            <p class="text-muted m-l-30"><b><?=display('Payment Method')?> :</b> <?=($order->payment_mode == 'Online')?'<span class="label label-success font-bold m-b-5">'.display('Online').'</span>':'<span class="label label-warning label-rounded font-bold m-b-5">'.display('Cash').'</span>'?>

                                <br/> <b><?=display('Order Status')?> :</b> 
                                <?php if($order->order_status == 'pending'):?>
                                    <span class="label label-primary font-bold"><?=display('Pending')?></span>
                                <?php endif;?>
                                <?php if($order->order_status == 'in_progress'):?>
                                    <span class="label label-warning font-bold"><?=display('In-progress')?></span>
                                <?php endif;?>
                                <?php if($order->order_status == 'delivered'):?>
                                    <span class="label label-success font-bold"><?=display('Delivered')?></span>
                                <?php endif;?>
                                <br/> <h5 class="font-bold"><b class="font-bold"><?=display('Quotation Number')?> </b>: <a class="btn waves-effect waves-light btn-outline-info" href="<?=base_url('supplier/quotation/view/'.$order->q_id.'')?>" title="<?=display('View Quotation Details')?>" data-toggle="tooltip">#<?=$order->q_no?></a></h5>  
                            </p> 
                        </address>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
            <div class="col-md-12">
                <div class="pull-right text-right">
                    <address>
                        <h6 class="font-bold"><?= display('Shipping Address');?>,</h6>
                        <h6><?=($order->address)??''?>,</h6>
                        <p class="text-muted m-l-30"><?=($order->city)??''?>  (<?=($order->state)??''?>),
                            <br/> <?=($order->country)??''?>
                        </p> 
                    </address>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive m-t-40" style="clear: both;">
                        <table id="product" class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Image</th>
                                    <th>Description</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Unit Cost</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if(!empty($order->product_json) && isset($order->product_json)):
                                        $products_json = json_decode($order->product_json,true);
                                    endif;
                                if (!empty($products_json)):
                                    foreach ($products_json as $key => $value):
                                       $productModel = model('App\Models\ProductModel');
                                       $products = $productModel->find($value['product_id']);
                                ?>
                                    <tr>
                                        <td class="text-center"><?=$key+1?></td>
                                        <td>
                                            <a href="javascript:void(0)" class="product-view" data-id="<?=$value['product_id']?>">
                                                <?=$products->name?>
                                            </a>
                                        </td>
                                        <td> <img src="<?=base_url($products->image_1)?>" alt="iMac" width="60" height="60"> </td>
                                        <td><?=character_limiter($products->description,80)?></td>
                                        <td class="text-center"><?=$value['quantity'];?></td>
                                       
                                        <td class="text-center product_amount"><?=($value['product_amount'])? number_format($value['product_amount'],2):0?></td>

                                        <td class="text-center product_total"><?= number_format((int)$value['quantity']*(int)$value['product_amount'],2) ?></td>

                                    </tr>
                                    <?php endforeach; ?> 
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right m-t-30 text-right">
                        <p class="font-bold"><b><span class="text-dark">Sub - Total amount :</span> € <?= number_format($order->amount,2); ?></b></p>
                        <p class="font-bold"><b>VAT (8%) : € <?= number_format((int)$order->amount*(8/100),2); ?> </b></p>
                        <hr>
                        <h4 class="font-bold"><b>Total Amount :</b> <span id="final_total">€ <?= number_format((int)$order->amount+(int)$order->amount*(8/100),2); ?></span></h4>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                </div>
            </div> 
        </div>
    </div>
</div>

<div id="product_details" class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl modal-dialog-centered">
    <!-- <div class="modal-dialog modal-dialog-centered"> -->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h4 class="modal-title" id="vcenter">Modal Heading</h4> -->
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body products-info">
            </div>
        </div>       
    </div>
</div>

<script type="text/javascript">
    $('#product').DataTable({
         "lengthChange": false,
         "pageLength": 5,
    });

    $(".select2").select2();

    $(document).on('click','.btn-status', function(e){
        var state = $(this).data('state');
        var token = '<?=csrf_hash()?>';
        var order_id = '<?=$order->id?>';

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        })
        $.ajax({
             url: "<?php echo base_url('supplier/order-status'); ?>",
             type: 'POST',
             dataType: 'json',
             data: {state:state,order_id:order_id,csrf_stream_token:token},
        }).done(function(data) {
            console.log(data);
            if (data.status == 'success') {
                swalWithBootstrapButtons.fire({
                    title: "<?= display('Success'); ?>",
                    text:data.message,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: "<?= display('Okay'); ?>",
                    reverseButtons: true
               });
                setTimeout(function() { window.location.reload(); }, 2000);
            } else {
                swalWithBootstrapButtons.fire({
                    title: "<?= display('Sorry'); ?>",
                    text:data.message,
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonText: "<?= display('Okay'); ?>",
                    reverseButtons: true
                });
                setTimeout(function() { window.location.reload(); }, 2000);
            }
        });
    });
</script>
