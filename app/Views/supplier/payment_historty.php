<link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />
<style type="text/css">
input[type=radio], input[type=checkbox] {
    box-sizing: border-box;
    padding: 0;
    position: absolute;
    opacity: 0;
}
.btn-group>.btn {
    position: relative;
    flex: 1 1 auto;
    display: inline-block;
    border: 1px solid;
    cursor: pointer;
}
.cloumn_fixed{
    position: static;
    right: 0px;
    background-color: #f6f6f6;
}
</style>
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open('supplier/payment-history','class="transaction-filter-form"'); ?>
                            <div class="row">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="far fa-address-card"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Transaction number / Ref number')?>" aria-describedby="basic-addon1" name="transaction_no" value ="<?=(isset($_SESSION['transaction']['transaction_no']) && $_SESSION['transaction']['transaction_no']!="")?$_SESSION['transaction']['transaction_no']:''?>">
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="far fa-address-card"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Ordre number / Ref number')?>" aria-describedby="basic-addon1" name="order_no" value ="<?=(isset($_SESSION['transaction']['order_no']) && $_SESSION['transaction']['order_no']!="")?$_SESSION['transaction']['order_no']:''?>">
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class=" far fa-address-card"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Quotation number/ Ref number')?>" aria-describedby="basic-addon1" name="quote_no" value ="<?=(isset($_SESSION['transaction']['quote_no']) && $_SESSION['transaction']['quote_no']!="")?$_SESSION['transaction']['quote_no']:''?>">
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="input-group col-md-4 mb-3">
                                   
                                  <?=form_dropdown('user_id', $users_list, (isset($_SESSION['transaction']['user_id']) && $_SESSION['transaction']['user_id']!="")?$_SESSION['transaction']['user_id']:'' ,' class="select2 form-control" ');?>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                   
                                  <?=form_dropdown('method', $payment_method, (isset($_SESSION['transaction']['method']) && $_SESSION['transaction']['method']!="")?$_SESSION['transaction']['method']:'' ,' class="select2 form-control" ');?>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select From Date')?>" name="start" value="<?=(isset($_SESSION['transaction']['start']) && $_SESSION['transaction']['start']!="")?date('m/d/Y',strtotime($_SESSION['transaction']['start'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select To Date')?>" name="end" value="<?=(isset($_SESSION['transaction']['end']) && $_SESSION['transaction']['end']!="")?date('m/d/Y',strtotime($_SESSION['transaction']['end'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 pull-left">
                                    <label for="status"><?=display('Transaction Type')?> : &nbsp;</label>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['transaction']['type']) && $_SESSION['transaction']['type'] == 'Cr')?'active':''?>">
                                            <input type="radio" name="type" id="Cr" value="Cr"
                                            <?=(isset($_SESSION['transaction']['type']) && $_SESSION['transaction']['type'] == 'Cr')?'checked':''?>><?=strtoupper(display('Credit'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['transaction']['type']) && $_SESSION['transaction']['type'] == 'Dr')?'active':''?>">
                                            <input type="radio" name="type" id="Dr" value="Dr"  <?=(isset($_SESSION['transaction']['type']) && $_SESSION['transaction']['type'] == 'Dr')?'checked':''?>><?=strtoupper(display('Debit'))?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12 text-left">
                                   <div class="row button-group">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-success" name="transaction_filter" value="filter"><i class="fas fa-filter m-r-5"></i><?=display('Filter')?></button>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-danger" name="remove_filter" value="remove"><i class="fas fa-ban m-r-5"></i><?=display('Cancel')?></button>
                                        </div>
                                    </div>     
                                </div>  
                            </div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <?php if(!empty($transaction)): ?>
            <div class="table-responsive">
                <table id="product" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><?=display('Transaction ID')?></th>
                            <th><?=display('Order Number')?></th>
                            <th><?=display('Quotation Number')?></th>
                            <th><?=display('Paid By')?></th>
                            <th><?=display('Transaction Amount')?></th>
                            <th><?=display('Currency Code')?></th>
                            <th><?=display('Transaction Type')?></th>
                            <th><?=display('Payment Method')?></th>
                            <th><?=display('Status')?></th>
                            <th><?=display('Transaction Date')?></th>
                            <th><?=display('Action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($transaction as $key => $value): ?>
                            <?php $user = get_user_details_by_id($value->paid_by,3); ?>
                            <tr>
                                <td><?=$value->transaction_id?></td>
                                <td><?=$value->order_no?></td>
                                <td><?=$value->q_no?></td>
                                <td><?=$user->firstname.' '.$user->lastname;?></td>
                                <td><?=$value->transaction_amount?></td>
                                <td><?=$value->currency_code?></td>
                                <td><?=($value->type == 'Cr')?display('Cr'):display('Dr')?> </td>
                                <td><?= strtoupper($value->payment_method)?></td>
                                <td>
                                    <?php if($value->status == 'success'):?>
                                         <span class="label label-success font-weight-100"><?= ucfirst($value->status)?></span>
                                    <?php endif;?>
                                    <?php if($value->status == 'failed'):?>
                                        <span class="label label-danger font-weight-100"><?= ucfirst($value->status)?></span>
                                    <?php endif;?>
                                </td>
                                <td><?= date('d F Y',strtotime($value->created_date))?></td>
                                <td class="text-center">
                                    <a href="#" class="text-dark m-r-10" title="<?=display('View')?>" target = "_blank" data-toggle="tooltip" data-id="<?=$value->receipt_id?>"><i class="fas fa-download"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="alert-empty-table-wrapper">
                            <div class="alert-empty-table-icon"><i class="icon-wallet"></i></div>
                            <div class="alert-empty-info alert-empty-bot-info">
                                <span><?=display('There is no payment history to display')?></span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- ============================================================== -->
<script src="<?php echo base_url('public/assets/admin/dist/js/select2.full.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $('#product').DataTable();
    $(".select2").select2();
    jQuery('.mydatepicker, #datepicker').datepicker();
</script>