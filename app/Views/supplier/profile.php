<style type="text/css">
.text-author{
  border: 1px solid rgba(0,0,0,.05);
  max-width: 100%;
  margin: 0 auto;
  padding: 2em;
}
.text-author h3{
  margin-bottom: 0;
}

.avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
}
.avatar-edit {
        position: absolute;
        right: 2px;
        z-index: 1;
        top: 127px; 
}
.avatar-edit input {
    display: none;
}
.avatar-edit label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all .2s ease-in-out;
}
.avatar-edit label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
}
.avatar-edit label:after {
    content: "\f030";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 7px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
}
.avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
}
.avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}

</style>
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> 
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" class="avatar" accept=".png, .jpg, .jpeg" />
                            <input type="hidden" id="old_image" value="<?=$profile->thumbnail?>">
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview"  style="background-image: url(<?=base_url($profile->thumbnail)?>);">
                            </div>
                        </div>
                    </div>
                    <h4 class="card-title m-t-10"><?=$profile->firstname.' '.$profile->lastname?></h4>
                    <div class="row text-center justify-content-md-center">
                        <div class="col-6">
                            <i class="icon-people"></i> <font class="font-medium">Seller</font>
                        </div>
                        <div class="col-6">
                                <i class="icon-tag"></i> 
                                <?php if($profile->status == 'Active'): ?>
                                    <font class="font-medium text-success">Active</font>
                                <?php else: ?>
                                    <font class="font-medium text-danger">Inactive</font>
                                <?php endif; ?>
                        </div>
                    </div>
                </center>
            </div>
            <div><hr> </div>
            <div class="card-body"> 
                <small class="text-muted">Email address </small>
                <h6><?=$profile->email?></h6> 
                <small class="text-muted p-t-30 db">Phone</small>
                <h6>+<?=(!empty($profile->country_code))?:'91'.' '.(!empty($profile->mobile))?$profile->mobile:''?></h6> 
                <small class="text-muted p-t-30 db">Address</small>
                <h6><?=(!empty($address->address))?$address->address:''?></h6>
                <small class="text-muted p-t-30 db">Company Name</small>
                <h6><?=$company->company_name?></h6>
                <small class="text-muted p-t-30 db">Company Email</small>
                <h6><?=$company->company_email?></h6>
               
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <!-- <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Timeline</a> </li> -->
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Change Password</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#company" role="tab">Company</a> </li>
                
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane" id="profile" role="tabpanel">
                   <div class="card-body">
                        <?php echo form_open('supplier/change-password', ' class="form-horizontal form-material sup_change_pswd_form" autocomplete="off" '); ?>
                            <div class="form-group">
                                <label class="col-md-12">Old Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line" name="old_pswd" id="old_pswd">
                                    <span class="error text-danger" id="error_old_pswd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">New Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line" name="new_pswd" id="new_pswd">
                                    <span class="error text-danger" id="error_new_pswd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Confirm Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line" name="confrim_pswd" id="confrim_pswd">
                                    <span class="error text-danger" id="error_confrim_pswd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="button-group">
                                    <button type="submit" class="btn waves-effect waves-light btn-outline-info upd_password">Update Credentials</button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                   </div>
                </div>

                <div class="tab-pane active" id="settings" role="tabpanel">
                    <div class="card-body">
                       <?php echo form_open('supplier/profile-edit', ' class="form-horizontal form-material sup_change_profile_form" autocomplete="off" '); ?>
                            <div class="form-group">
                                <label class="col-md-12">First Name</label>
                                <div class="col-md-12">
                                    <input type="text" name="firstname" id="firstname" class="form-control form-control-line" value="<?=$profile->firstname?>">
                                    <span class="error text-danger" id="error_fname"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Last Name</label>
                                <div class="col-md-12">
                                    <input type="text" name="lastname" id="lastname" class="form-control form-control-line" value="<?=$profile->lastname?>">
                                    <span class="error text-danger" id="error_lname"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" name="email" id="email" value="<?=$profile->email?>" class="form-control form-control-line">
                                    <span class="error text-danger" id="error_email"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Mobile</label>
                                <div class="col-md-12">
                                    <input type="text" name="mobile" id="mobile" value="<?=$profile->mobile?>" class="form-control form-control-line">
                                    <span class="error text-danger" id="error_mobile"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="button-group">
                                    <button type="button" class="btn waves-effect waves-light btn-outline-info update_profile">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane" id="company" role="tabpanel">
                    <div class="card-body">
                        <?php if (empty($company)): ?>
                        <div class="text-author text-center">
                            <span class="position">
                                <h3>There is no information found..</h3>
                            </span>
                            <p>
                              <a href="javascript:void(0)" class="btn btn btn-outline-info profile-btn m-t-20">Add Company Profile</a>
                            </p>
                        </div>
                        <?php else:?>
                             <div class="company_details">
                            <div class="row m-b-10">
                                <div class="col-md-12 text-right">
                                    <a href="javascript:void(0)" class="btn btn btn-outline-info edit-profile m-t-20">Edit Company</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small class="text-muted p-t-30 db">Company Name</small>
                                    <h6><?=$company->company_name?></h6>
                                </div>
                                <div class="col-md-6">
                                    <small class="text-muted p-t-30 db">Company Email</small>
                                    <h6><?=$company->company_email?></h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <small class="text-muted">City </small>
                                    <h6><?=$company->city?></h6> 
                                </div>
                                <div class="col-md-6">
                                    <small class="text-muted p-t-30 db">Contact</small>
                                    <h6>+<?=$company->country_code.' '.$company->contact?></h6> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                   <small class="text-muted p-t-30 db">Country</small>
                                   <h6><?=(!empty($company->country))?get_country_by_id($company->country)->name:''?></h6>
                                </div>
                                <div class="col-md-6">
                                    <small class="text-muted p-t-30 db">Address</small>
                                    <h6><?=(!empty($address->address))?$address->address:''?></h6>
                                </div>
                            </div>
                        </div>
                        <?php echo form_open('supplier/edit-company', 'class ="edit-company-profile" autocomplete="off"'); ?>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Company Name')?></label>
                                    <input type="text" id="company_name_upd" name="company_name" class="form-control" placeholder="<?=display('Company Name')?>" value="<?=$company->company_name?>"> 
                                    <span class="error text-danger" id="error_name_upd"></span>
                                </div>
                            </div>                           
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Company Email')?></label>
                                    <input type="text" id="company_email_upd" name="company_email" class="form-control" placeholder="<?=display('Company Email')?>" value="<?=$company->company_email?>"> 
                                    <span class="error text-danger" id="error_email_upd"></span>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Contact Number')?></label>
                                    <input type="text" id="contact_upd" name="contact" class="form-control" placeholder="<?=display('Contact Number')?>" value="<?=$company->contact?>"> 
                                    <span class="error text-danger" id="error_contact_upd"></span>
                                </div>
                            </div>
                       
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Country')?></label>
                                     <?=form_dropdown('country', $countries,($company->country?? ''),'class="form-control" id="country" ');?>
                                     <p class="text-danger error"></p>
                                    <span class="error text-danger" id="error_name"></span>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?=display('City')?></label>
                                    <input type="text" id="city_upd" name="city" class="form-control" placeholder="<?=display('City Name')?>" value="<?=$company->city?>"> 
                                    <span class="error text-danger" id="error_city_upd"></span>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Address')?></label>
                                    <input type="text" id="address_upd" name="address" class="form-control" placeholder="<?=display('Address')?>" value="<?=$company->address?>"> 
                                    <span class="error text-danger" id="error_address_upd"></span>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?=display('Introduction')?></label>
                                    <textarea class="form-control" id="intro_upd" rows="3" name="intro" placeholder="About your company....."><?=$company->introduction?></textarea>
                                    <span class="error text-danger" id="error_intro_upd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="button-group">
                                    <button type="submit" class="btn btn btn btn-outline-info company-edit-btn m-r-10"><i class="fas fa-pencil-alt"></i> <?=display('Update')?></button>
                                </div>  
                            </div>
                        <?php echo form_close(); ?>
                        
                        <?php endif;?>
                        <?php echo form_open('supplier/add-company', 'class ="company-profile" autocomplete="off"'); ?>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?=display('Company Name')?></label>
                                        <input type="text" id="company_name" name="company_name" class="form-control" placeholder="<?=display('Company Name')?>"> 
                                        <span class="error text-danger" id="error_name"></span>
                                    </div>
                                </div>                           
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?=display('Company Email')?></label>
                                        <input type="text" id="company_email" name="company_email" class="form-control" placeholder="<?=display('Company Email')?>"> 
                                        <span class="error text-danger" id="error_email"></span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?=display('Contact Number')?></label>
                                        <input type="text" id="contact" name="contact" class="form-control" placeholder="<?=display('Contact Number')?>"> 
                                        <span class="error text-danger" id="error_contact"></span>
                                    </div>
                                </div>
                           
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?=display('Country')?></label>
                                         <?=form_dropdown('country', $countries, '','class="form-control" id="country" ');?>
                                         <p class="text-danger error"></p>
                                        <span class="error text-danger" id="error_name"></span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?=display('City')?></label>
                                        <input type="text" id="city" name="city" class="form-control" placeholder="<?=display('City Name')?>"> 
                                        <span class="error text-danger" id="error_city"></span>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?=display('Address')?></label>
                                        <input type="text" id="address" name="address" class="form-control" placeholder="<?=display('Company Email')?>"> 
                                        <span class="error text-danger" id="error_address"></span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><?=display('Address')?></label>
                                        <textarea class="form-control" id="intro" rows="3" name="intro" placeholder="About your company....."></textarea>
                                        <span class="error text-danger" id="error_intro"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="button-group">
                                        <button type="submit" class="btn btn btn btn-outline-info company-btn m-r-10"><i class="fa fa-check"></i> <?=display('Save')?></button>
                                    </div>  
                                </div>
                        <?php echo form_close(); ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<script type="text/javascript">
    function UploadProfilePicture()
    {
        $(document).on('change','.avatar', function() {
            var formData = new FormData();
            var old_pic = $('#old_image').val();
            var file = $('.avatar').prop('files')[0];
            var token = "<?= csrf_hash() ?>";
            formData.append("profile_pic",file);
            formData.append("old_image",old_pic);
            formData.append("csrf_stream_token",token);
            $.ajax({
                 url: "<?php echo base_url('supplier/profile-image-edit'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: formData,
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
            }).done(function(data) {
                // console.log(data);
                 window.location.reload();
               // setTimeout(function() { window.location.reload(); }, 1000);
            });
        });
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
           
            reader.readAsDataURL(input.files[0]);
            UploadProfilePicture();
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });


    $(function () {
        $('.company-profile').hide();
        $('.edit-company-profile').hide();

        $(".edit-profile").on('click', function(e){
            $('.edit-company-profile').show();
            $('.company_details').hide();
        });

        $(".profile-btn").on('click', function(e){
            $('.company-profile').show();
            $('.text-author').hide();
        });
    });


    $(".upd_password").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#confrim_pswd").val()) == '')
        {
          $("#error_confrim_pswd").html("<?php echo display('Please Enter Confirm Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) == '')
        {
          $("#error_new_pswd").html("<?php echo display('Please Enter New Password'); ?>");
          valid = false;
        }
        if($.trim($("#old_pswd").val()) == '')
        {
          $("#error_old_pswd").html("<?php echo display('Please Enter Old Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) !== $.trim($("#confrim_pswd").val()))
        {
          $("#error_confrim_pswd").html("<?php echo display('Password Not Match.....'); ?>");
          valid = false;
        }
        if(valid){
          $(".sup_change_pswd_form").get(0).submit();  
        }
    });

    // Profile
    $(".update_profile").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#firstname").val()) == '')
        {
          $("#error_fname").html("<?php echo display('Please Enter First Name'); ?>");
          valid = false;
        }
        if($.trim($("#lastname").val()) == '')
        {
          $("#error_lname").html("<?php echo display('Please Enter Last Name'); ?>");
          valid = false;
        }
        if($.trim($("#email").val()) == '')
        {
          $("#error_email").html("<?php echo display('Please Enter Email ID'); ?>");
          valid = false;
        }
       
        if($.trim($("#mobile").val()) == '')
        {
          $("#error_mobile").html("<?php echo display('Please Enter Mobile Number'); ?>");
          valid = false;
        }
       
        if(valid){
          $(".sup_change_profile_form").get(0).submit();  
        }
    });

    // company
    $(".company-btn").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#company_name").val()) == '')
        {
          $("#error_name").html("<?php echo display('Please Enter Company Name'); ?>");
          valid = false;
        }
        if($.trim($("#company_email").val()) == '')
        {
          $("#error_email").html("<?php echo display('Please Enter Company Email'); ?>");
          valid = false;
        }
        if($.trim($("#contact").val()) == '')
        {
          $("#error_contact").html("<?php echo display('Please Enter Contact Number'); ?>");
          valid = false;
        }
        if($.trim($("#city").val()) == '')
        {
          $("#error_city").html("<?php echo display('Please Enter City Name'); ?>");
          valid = false;
        }
        if($.trim($("#address").val()) == '')
        {
          $("#error_address").html("<?php echo display('Please Enter Address'); ?>");
          valid = false;
        }
        if($.trim($("#intro").val()) == '')
        {
          $("#error_intro").html("<?php echo display('Please Enter Company information'); ?>");
          valid = false;
        }
        if(valid){
          $(".company-profile").get(0).submit();  
        }
    });

    // company
    $(".company-edit-btn").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#company_name_upd").val()) == '')
        {
          $("#error_name_upd").html("<?php echo display('Please Enter Company Name'); ?>");
          valid = false;
        }
        if($.trim($("#company_email_upd").val()) == '')
        {
          $("#error_email_upd").html("<?php echo display('Please Enter Company Email'); ?>");
          valid = false;
        }
        if($.trim($("#contact_upd").val()) == '')
        {
          $("#error_contact_upd").html("<?php echo display('Please Enter Contact Number'); ?>");
          valid = false;
        }
        if($.trim($("#city_upd").val()) == '')
        {
          $("#error_city_upd").html("<?php echo display('Please Enter City Name'); ?>");
          valid = false;
        }
        if($.trim($("#address_upd").val()) == '')
        {
          $("#error_address_upd").html("<?php echo display('Please Enter Address'); ?>");
          valid = false;
        }
        if($.trim($("#intro_upd").val()) == '')
        {
          $("#error_intro_upd").html("<?php echo display('Please Enter Company information'); ?>");
          valid = false;
        }
        if(valid){
          $(".edit-company-profile").get(0).submit();  
        }
    });
</script>