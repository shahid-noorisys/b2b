<div class="login-register">
    <div class="login-box card">
        <div class="card-body">
            <?php echo form_open('', 'class ="form-horizontal form-material password_update" id="loginform" autocomplete="off"'); ?>
                <h3 class="text-center m-b-20"><?=display('Reset Password')?></h3>
                <div class="form-group">
                    <label class="col-md-12">Old Password</label>
                    <div class="col-md-12">
                        <input type="password" class="form-control form-control-line" name="old_pswd" id="old_pswd">
                        <span class="error text-danger" id="error_old_pswd"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="example-email" class="col-md-12">New Password</label>
                    <div class="col-md-12">
                        <input type="password" class="form-control form-control-line" name="new_pswd" id="new_pswd">
                        <span class="error text-danger" id="error_new_pswd"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Confirm Password</label>
                    <div class="col-md-12">
                        <input type="password" class="form-control form-control-line" name="confrim_pswd" id="confrim_pswd">
                        <span class="error text-danger" id="error_confrim_pswd"></span>
                    </div>
                </div>
                <div class="form-group text-center">
                    <div class="col-xs-12 p-b-20">
                        <button type="button" class="btn btn-lg btn-block btn-outline-primary upd_password"><?=display('Reset')?></button>
                    </div>
                </div>
               
            <?php echo form_close(); ?>
          
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(".upd_password").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#confrim_pswd").val()) == '')
        {
          $("#error_confrim_pswd").html("<?php echo display('Please Enter Confirm Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) == '')
        {
          $("#error_new_pswd").html("<?php echo display('Please Enter New Password'); ?>");
          valid = false;
        }
        if($.trim($("#old_pswd").val()) == '')
        {
          $("#error_old_pswd").html("<?php echo display('Please Enter Old Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) !== $.trim($("#confrim_pswd").val()))
        {
          $("#error_confrim_pswd").html("<?php echo display('Password Not Match.....'); ?>");
          valid = false;
        }
        if(valid){
          // $(".password_update").get(0).submit();  
        }
    });

    function validEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            return false;
        }else{
            return true;
        }
    }
</script>
