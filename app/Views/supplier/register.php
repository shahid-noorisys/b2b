<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('public/assets/img/logo/favicon.png')?>">
    <title><?=display('yahodehime')?></title>
    
    <!-- page css -->
    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/login-register-lock.css')?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('public/assets/admin/dist/css/style.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url('public/assets/admin/dist/css/custom.css')?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label"><?=display('yahodehime')?></p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url(<?php echo base_url('public/assets/admin/images/background/login-register.jpg');?>)">
            <div class="login-box card">
                <div class="card-body">
                    <?php if(session()->getFlashData('message') != null): ?>
                        <p class="text-success"><?=session()->getFlashData('message')?></p>
                    <?php endif; ?>
                    <?php if(session()->getFlashData('exception') != null): ?>
                        <p class="text-danger"><?=session()->getFlashData('exception')?></p>
                    <?php endif; ?>
                    <?php echo form_open('supplier/registration', ' class="form-horizontal form-material sup_register_form" autocomplete="off" '); ?>
                        <h3 class="text-center m-b-20"><?=display('Sign Up')?></h3>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="first_name" name ="first_name" placeholder="<?=display('First Name')?>">
                                </div>
                                <span class="error text-danger" id="error_fname"></span>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="last_name" name ="last_name" placeholder="<?=display('Last Name')?>">
                                </div>
                                <span class="error text-danger" id="error_lname"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="email" name="email" placeholder="<?=display('Email')?>">
                                </div>
                                <span class="error text-danger" id="error_email"></span>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="password" id="password" name="password" placeholder="<?=display('Password')?>">
                                </div>
                                <span class="error text-danger" id="error_password"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                     <select class="form-control custom-select" id="user_type" name="user_type">
                                        <option value="1"><?=display('Individual')?></option>
                                        <option value="2"><?=display('Company')?></option>
                                     </select>
                                </div>
                                <span class="error text-danger" id="error_user_type"></span>
                            </div>
                             <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                     <select class="form-control custom-select" id="country_id" name="country">
                                        <?php foreach ($countries as $key => $value) { ?>
                                            <option value="<?=$value->id?>"><?=$value->name?></option>
                                        <?php } ?>
                                     </select>
                                </div>
                                <span class="error text-danger" id="error_country"></span>
                            </div>
                        </div>
                        <div class="row">
                             <!-- <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="company" name="company_name" placeholder="<?=display('Company Name')?>">
                                </div>
                                <span class="error text-danger" id="error_company"></span>
                            </div> -->
                           <!--  <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="company_email" name="company_email" placeholder="<?=display('Company Email')?>">
                                </div>
                                <span class="error text-danger" id="error_company_email"></span>
                            </div> -->
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="city" name="city" placeholder="<?=display('City')?>">
                                </div>
                                <span class="error text-danger" id="error_city"></span>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="mobile" name= "mobile" placeholder="<?=display('Mobile')?>">
                                </div>
                                <span class="error text-danger" id="error_mobile"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="address_main" name="address_main" placeholder="<?=display('Address 1')?>">
                                </div>
                                <span class="error text-danger" id="error_address_main"></span>
                            </div>
                            <div class="form-group col-md-3 col-sm-6 col-xs-6">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="state" name="state" placeholder="<?=display('State')?>">
                                </div>
                                <span class="error text-danger" id="error_state"></span>
                            </div>
                            <div class="form-group col-md-3 col-sm-6 col-xs-6">
                                <div class="col-xs-12">
                                    <input class="form-control" type="text" id="postal_code" name="postal_code" placeholder="<?=display('Zip Code')?>">
                                </div>
                                <span class="error text-danger" id="error_postal_code"></span>
                            </div>
                        </div>
                        <div class="form-group text-center p-b-20">
                            <div class="offset-md-3 col-md-6 col-xs-12">
                             
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary" id="register_submit"><?=display('Sign Up')?></button>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <?=display('Already have an account?')?> <a href="<?=base_url('supplier/login')?>" class="text-primary m-l-5"><b><?=display('Sign In')?></b></a>
                            </div>
                        </div>
                    <!-- </form> -->
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery-3.2.1.min.js')?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/popper.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/select2.full.min.js')?>" type="text/javascript"></script>
    
    <script type="text/javascript">

        $(function () {
            $(".select2").select2();
        });
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
    </script>
    <script>
      $("#register_submit").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#first_name").val()) == '')
        {
          $("#error_fname").html("<?php echo display('Please Enter First Name'); ?>");
          valid = false;
        }
        if($.trim($("#last_name").val()) == '')
        {
          $("#error_lname").html("<?php echo display('Please Enter Last Name'); ?>");
          valid = false;
        }
        if($.trim($("#email").val()) == '')
        {
          $("#error_email").html("<?php echo display('Please Enter Email ID'); ?>");
          valid = false;
        }
        if($.trim($("#password").val()) == '')
        {
          $("#error_password").html("<?php echo display('Please Enter Password'); ?>");
          valid = false;
        }
        if($.trim($("#city").val()) == '')
        {
          $("#error_city").html("<?php echo display('Please Enter City Name'); ?>");
          valid = false;
        }
        if($.trim($("#mobile").val()) == '')
        {
          $("#error_mobile").html("<?php echo display('Please Enter Mobile Number'); ?>");
          valid = false;
        }
        if($.trim($("#address_main").val()) == '')
        {
          $("#error_address_main").html("<?php echo display('Please Enter Address'); ?>");
          valid = false;
        }
        if($.trim($("#state").val()) == '')
        {
          $("#error_state").html("<?php echo display('Please Enter state Name'); ?>");
          valid = false;
        }
        if($.trim($("#postal_code").val()) == '')
        {
          $("#error_postal_code").html("<?php echo display('Please Enter Zip Code'); ?>");
          valid = false;
        }
        if(valid){
          $(".sup_register_form").get(0).submit();  
        }
      });
    </script>
</body>

</html>