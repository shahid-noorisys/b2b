        <!--header middel start-->
        <div class="header_bottom sticky-header">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="main_menu header_position"> 
                            <nav>  
                                <ul>

                                    <li class="active"><a  href="<?=base_url()?>"><i class="zmdi zmdi-home"></i> <?=display('Home')?> <i></i></a>
                                    </li>
                                    <li class="mega_items"><a href="<?=base_url('list-product/0/0')?>"><i class="zmdi zmdi-store-24"></i> <?=display('Products')?> <i></i></a></li>

                                    <li><a href="<?=base_url('buyer/quotations')?>"><i class="zmdi zmdi-collection-music"></i> <?=display('Quotations')?> </a></li>

                                    <li><a href="<?=base_url('messages')?>"><i class="zmdi zmdi-comments"></i> <?=display('Messages')?> </a></li>

                                    <li><a href="<?=base_url('about-us')?>"><i class="zmdi zmdi-comments"></i> <?=display('about Us')?></a></li>
                                    <li><a href="<?=base_url('contact-us')?>"><i class="zmdi zmdi-account-box-mail"></i>  <?=display('Contact Us')?></a></li>
                                 
                                </ul>  
                            </nav> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header middel end-->
     
    </header>
    <!--header area end-->
    
    <!--Offcanvas menu area start-->
    
    <div class="off_canvars_overlay">
                
    </div>
    <div class="Offcanvas_menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="canvas_open">
                        <span><?=display('Menu')?></span>
                        <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                    </div>
                    <div class="Offcanvas_menu_wrapper">
                        <div class="canvas_close">
                              <a href="javascript:void(0)"><i class="ion-android-close"></i></a>  
                        </div>
                        <div class="welcome_text">
                           <p> <?=display('Welcome to')?> <span><?= display('yahodehime') ?> </span> </p>
                       </div>
                       
                        <div class="top_right">
                            <ul>
                                <?php if(! loggedIn()): ?>
                                <li class="top_linksss"><a href="<?=base_url('supplier/login')?>"><i class="fa fa-sign-in"></i> <?=display('Supplier Login') ?> </a>
                                </li>
                                <li class="top_linksss"><a href="<?=base_url('register-buyer')?>"><i class="fa fa-user-plus"></i> <?=display('Register') ?> </a>
                                </li>
                                <li class="top_linksss"><a href="#" data-toggle="modal" data-target="#login_modal_box"><i class="zmdi zmdi-account"></i> <?=display('Login') ?> </a>
                                </li>
                                <?php else: ?>
                                <li class="top_links"><a href="#"><i class="zmdi zmdi-account"></i> <?php echo session()->get('user_details')->fullname; ?> <i class="zmdi zmdi-caret-down"></i></a>
                                    <ul class="dropdown_links">
                                        <!-- <li><a href="checkout.html">Checkout </a></li> -->
                                        <li><a href="<?=base_url('buyer-profile')?>">My Account </a></li>
                                        <!-- <li><a href="cart.html">Shopping Cart</a></li> -->
                                        <!-- <li><a href="wishlist.html">Wishlist</a></li> -->
                                    </ul>
                                </li>
                                <?php
                                $session = session();
                                $note_count = (isset($note_current_counter))?$note_current_counter:$session->get('user_details')->note_count;
                                ?>
                                <li class=""> <a href="<?=base_url('list-notification')?>"><i class="zmdi zmdi-notifications"></i> <?=display('Notification')?> <span class="badge badge-dark" id="note_counter"><?=$note_count?></span> </a>
                                </li>
                                <li class="top_linksss"><a href="<?=base_url('logout-buyer')?>"> <i class="fa fa-sign-out"></i> <?=display('Logout') ?> </a>
                                </li>
                                <?php endif; ?>

                                <?php 
                                  $text = '<img src="'.base_url('public/assets/img/english.png').'" alt=""> '.display('English');

                                  if(isset($_SESSION['lang']))
                                  {
                                  if($_SESSION['lang']=='fr'){ $text='<img src="'.base_url('public/assets/img/french.png').'" alt=""> '.display('French');} 
                                  else {  $text = '<img src="'.base_url('public/assets/img/english.png').'" alt=""> '.display('English');}
                                  } 
                                  else 
                                  { 
                                    $_SESSION['lang'] = "en"; 
                                    $text = '<img src="'.base_url('public/assets/img/english.png').'" alt=""> '.display('English'); 
                                  }
                                ?>
                                <li class="language"><a><?=$text;?><i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_language">
                                      <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='fr')): ?>
                                        <li>
                                          <a href="#" class="btn-lang" style="cursor: pointer; color: black" data-id="fr"><img src="<?=base_url('public/assets/img/french.png')?>" alt=""> <?=display('French');?></a>
                                        </li>
                                      <?php endif; ?>
                                      <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='en')): ?>
                                        <li>
                                          <a href="#" class="btn-lang" style="cursor: pointer; color: black" data-id="en"><img src="<?=base_url('public/assets/img/english.png')?>" alt=""> <?=display('English');?></a>
                                        </li>
                                      <?php endif; ?>
                                    </ul>
                                </li>
                            </ul>
                        </div> 
                        <div class="search-container">
                           <form action="#">
                                <div class="hover_category">
                                <?php $categories = getAllCategories();
                                $category_id = (isset($category_id))?$category_id:'0';
                                if(!empty($categories)):
                                ?>
                                    <select class="select_option" name="mini_search_category" id="mini_search_category">
                                        <option <?=($category_id == 0)?'selected':''?> value="0"><?=display('All Categories')?></option>
                                    <?php foreach ($categories as $v): ?>
                                        <option value="<?=$v->id?>" <?=($category_id == $v->id)?'selected':''?>><?=$v->name?></option>
                                    <?php endforeach; ?>
                                    </select>                      
                                <?php endif; ?>
                                </div>
                                <div class="search_box">
                                    <input placeholder="Search product..." type="text" name="mini_search_text" id="mini_search_text" value="<?=(isset($search_text))?$search_text:''?>">
                                    <button type="submit" id="btn_mini_search"><i class="zmdi zmdi-search"></i></button> 
                                </div>
                            </form>
                        </div> 
                        <div class="mini_cart_wrapper">
                            <a href="<?=base_url('cart-list')?>" id="btn_cart_list"><i class="zmdi zmdi-shopping-basket"></i> <span>0 items</span> </a>
                            <!--mini cart-->
                            <!-- <div class="mini_cart">
                                <div class="cart_item">
                                   <div class="cart_img">
                                       <a href="#"><img src="assets/img/s-product/product.jpg" alt=""></a>
                                   </div>
                                    <div class="cart_info">
                                        <a href="#">Condimentum Watches</a>

                                        <span class="quantity">Qty: 1</span>
                                        <span class="price_cart">$60.00</span>

                                    </div>
                                    <div class="cart_remove">
                                        <a href="#"><i class="ion-android-close"></i></a>
                                    </div>
                                </div>
                                <div class="cart_item">
                                   <div class="cart_img">
                                       <a href="#"><img src="assets/img/s-product/product2.jpg" alt=""></a>
                                   </div>
                                    <div class="cart_info">
                                        <a href="#">Officiis debitis</a>
                                        <span class="quantity">Qty: 1</span>
                                        <span class="price_cart">$69.00</span>
                                    </div>
                                    <div class="cart_remove">
                                        <a href="#"><i class="ion-android-close"></i></a>
                                    </div>
                                </div>
                                <div class="mini_cart_table">
                                    <div class="cart_total">
                                        <span>Subtotal:</span>
                                        <span class="price">$138.00</span>
                                    </div>
                                </div>

                                <div class="mini_cart_footer">
                                   <div class="cart_button">
                                        <a href="cart.html">View cart</a>
                                        <a href="checkout.html">Checkout</a>
                                    </div>
                                </div>

                            </div> -->
                            <!--mini cart end-->
                        </div>
                        <div id="menu" class="text-left ">
                             <ul class="offcanvas_main_menu">
                                <li class="menu-item-has-children active"><a  href="<?=base_url()?>"><i class="zmdi zmdi-home"></i> <?=display('Home')?> <i></i></a>
                                </li>
                                <li class="menu-item-has-children"><a href="<?=base_url('list-product/0/0')?>"><i class="zmdi zmdi-store-24"></i> <?=display('Products')?> <i></i></a></li>

                                <li class="menu-item-has-children"><a href="<?=base_url('buyer/quotations')?>"><i class="zmdi zmdi-collection-music"></i> <?=display('Quotations')?> </a></li>

                                <li class="menu-item-has-children"><a href="<?=base_url('messages')?>"><i class="zmdi zmdi-comments"></i> <?=display('Messages')?> </a></li>

                                <li class="menu-item-has-children"><a href="<?=base_url('about-us')?>"><i class="zmdi zmdi-comments"></i> <?=display('about Us')?></a></li>
                                <li class="menu-item-has-children"><a href="<?=base_url('contact-us')?>"><i class="zmdi zmdi-account-box-mail"></i>  <?=display('Contact Us')?></a></li>
                            </ul>
                        </div>

                        <!-- <div class="Offcanvas_footer">
                            <span><a href="#"><i class="fa fa-envelope-o"></i> info@yourdomain.com</a></span>
                            <ul>
                                <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="pinterest"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Offcanvas menu area end-->
<!-- ////////////////////////////////-------Menu End-----///////////////////////////////-->