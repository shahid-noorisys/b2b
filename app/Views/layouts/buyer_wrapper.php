<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=strtoupper(display('yahodehime'))?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('public/assets/img/logo/favicon.png')?>">

    
    <!-- CSS 
    ========================= -->

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/plugins.css') ?>">
    
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/style.css') ?>">

    <!-- Sweetalert2 CSS -->
    <link rel="stylesheet" href="<?= base_url('public/assets/css/sweetalert2.min.css') ?>">

    <!-- Plugins JS -->
    <script src="<?= base_url('public/assets/js/plugins.js')?>"></script>
</head>

<body>

    <!-- ============================================================== -->
    <?php echo view('layouts/buyer_header'); ?>
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <?php echo view('layouts/buyer_menus'); ?>
    <!-- ============================================================== -->
   
    <!-- ============================================================== -->
    <!--breadcrumbs area start-->
        <!-- <div class="breadcrumbs_area">
            <div class="container">   
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb_content">
                            <ul>
                                <li><a href="<?=$path?>"><?=$controller_name?></a></li>
                                <li><?=$title?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>         
        </div> -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <?php if(session()->getFlashData('message') != null): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin: 20px 20px 20px 20px;">
            <strong><?=display('Success')?>!</strong> <?php $errors = session()->getFlashData('message');
            if(is_array($errors)){
                foreach ($errors as $value) {
                    echo $value.'<br>';
                }
            } else { echo $errors; }
            ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if(session()->getFlashData('exception') != null): ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin: 20px 20px 20px 20px;">
            <strong><?=display('Sorry')?>!</strong> <?php $errors = session()->getFlashData('exception');
            if(is_array($errors)){
                foreach ($errors as $value) {
                    echo $value.'<br>';
                }
            } else { echo $errors; }
            ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php echo ($content) ?? ''; ?>
    <!-- ============================================================== -->


    <!-- ============================================================== -->
    <?php echo view('layouts/buyer_footer'); ?>
    <!-- ============================================================== -->
    

<!-- JS
============================================ -->
<!-- Main JS -->
<script src="<?= base_url('public/assets/js/main.js')?>"></script>

<!-- Sweetalert2 JS -->
<script src="<?= base_url('public/assets/js/sweetalert2.all.min.js')?>"></script>

<script type="text/javascript">
var pageCount = 4;
var page_url = false;
$(document).ready(function () {

    $(document).on('click','.edit-address', function(e){
        setTimeout(function() { $('#alert_msg').hide(); }, 1000);
        var address_id = $(this).data('address');
        var user_id = $(this).data('user');
        // alert(address_id);
        $.ajax({
            type: "post",
            url: "<?=base_url('get-address')?>",
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>',address_id: address_id,user_id: user_id},
            dataType: "json",
            success: function (response) {
                $("#edit_full_name").val(response.address.full_name);
                $("#edit_new_address").val(response.address.address);
                $("#edit_address_city").val(response.address.city);
                $("#edit_address_state").val(response.address.state);
                // $("#address_country").val(response)address.;
                $("#edit_address_zip_code").val(response.address.zip_code);
                $("#edit_user_id").val(user_id);
                $("#edit_address_id").val(address_id);
                $('#edit_address_modal_box').modal('show');
            }
        });
    });

    $(document).on('click','.company-edit', function(e){
        // setTimeout(function() { $('#alert_msg').hide(); }, 1000);
        var company_id = $(this).data('company');
        var user_id = $(this).data('user');
        $.ajax({
            type: "post",
            url: "<?=base_url('get-company')?>",
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>',company_id: company_id,user_id: user_id},
            dataType: "json",
            success: function (response) {

                $("#edit_company_name").val(response.company.company_name);
                $("#edit_company_email").val(response.company.company_email);
                $("#com_mobile").val(response.company.contact);
                $("#com_city").val(response.company.city);
                $("#edit_company_address").val(response.company.address);
                $("#edit_user_id").val(user_id);
                $("#company_id").val(company_id);
                $('#edit_company_profile_modal_box').modal('show');
            }
        });
    });
    //-------- set Cart Count --------//
    cartCount($("#btn_cart_list"));
    //-------- set Cart Count --------//
    //-------- Forgot password and Login Modal--------//
    $("#forget_password_popup").hide();
    $("#otp_popup").hide();
    $('#forget_password_link').on('click', function(e) {  e.preventDefault();    
        $("#login_popup").hide("slow","swing");
        $("#otp_popup").hide("slow","swing");
        $("#forget_password_popup").show();
    });

    $('#login_link').on('click', function(e) { e.preventDefault();
        $("#forget_password_popup").hide("slow","swing");
        $("#otp_popup").hide("slow","swing");
        $("#login_popup").show();
    });
    //-------- Forgot password and Login Modal--------//
    //-------- Product Details Modal--------//
    $("#modal_box").on("show.bs.modal", function(e){
        // console.log("product details modal show called...");
        var product_id = $("#pd_modal_product_id").val();
        if(product_id != '' || product_id != undefined)
        {
            $.ajax({
                type: "post",
                url: "<?=base_url('view-product-details')?>",
                data: {product_id: product_id, '<?=csrf_token()?>': '<?=csrf_hash()?>'},
                dataType: "json",
                success: function (data) {
                    // console.log("Product Detials : "+data);
                    if(data.status == 'success'){
                        $("#modal_box").find(".modal_title h2").html(data.product.name);
                        var large_product = '';
                        var product_button = '';
                        var image_arr = [];
                        image_arr.push(data.product.image_1);
                        image_arr.push(data.product.image_2);
                        image_arr.push(data.product.image_3);
                        image_arr.push(data.product.image_4);
                        image_arr.push(data.product.image_5);
                        $.each(image_arr, function (i, v) {
                            if(v != '')
                            {
                                image_url = '<?=base_url()?>/'+v;
                            } else {
                                image_url = '<?=base_url('public/assets/img/no-image.jpg')?>';
                            }
                            $("#tab"+(i+1)).find("img").attr('src',image_url);
                            $(".modal_tab_button").find(".button_"+(i+1)+" img").attr('src',image_url);
                            // console.log(i);
                        });
                        var desc = data.product.description.replace(/\n/g,'<br/>');
                        $("#modal_box").find(".modal_description p").html(desc);
                        var cat_n_sub_cat = data.product.category_name+" | "+data.product.sub_category_name;
                        $("#modal_box").find(".modal_price .new_price").html(cat_n_sub_cat);
                        $("#modal_box").find(".modal_add_to_cart .add_cart").attr('data-product_id',data.product.id);
                        $("#modal_box").find(".modal_add_to_cart .add_quote").attr('data-product_id',data.product.id);
                        $("#modal_box #add_to_cart_form").attr('action',"<?=base_url('buyer/quotation-request')?>/"+data.product.id);
                    }
                    $("#pd_modal_product_id").val("");
                }
            });
        }
    });
    // $(document).on("hide.bs.modal", "#modal_box", function(e){
    //     alert();
    //     $("#pd_modal_product_id").val("");
    // });
    //-------- Product Details Modal--------//

    //-------- For Quotation pagination--------//
    ajaxQuotations(page_url = false);
    $(document).on('click','.quotation_list .pagination li a', function(e){
        e.preventDefault();
        var page_url = $(this).attr('href');
        // alert(page_url);
        ajaxQuotations(page_url);
    });
    //-------- For Quotation pagination--------//
    //-------- For Quotation Filter--------//
    $(document).on('click',"#btn_quote_filter", function(e){
        e.preventDefault();
        ajaxQuotations(page_url = false);
    });
    $(document).on('click',"#quote_btn_clear_filter", function(e){
        e.preventDefault();
        $("#quote_no").val("");
        $("#quote_status").val("");
        ajaxQuotations(page_url = false);
    });
    //-------- For Quotation Filter--------//
    //-------- For Orders pagination--------//
    ajaxOrders(page_url = false);
    $(document).on('click','.order_list .pagination li a', function(e){
        e.preventDefault();
        var page_url = $(this).attr('href');
        // alert(page_url);
        ajaxOrders(page_url);
    });
    //-------- For Orders pagination--------//
    //-------- For Order Filter--------//
    $(document).on('click',"#ord_btn_submit", function(e){
        e.preventDefault();
        ajaxOrders(page_url = false);
    });
    $(document).on('click',"#ord_btn_clear_filter", function(e){
        e.preventDefault();
        $("#order_no").val("");
        $("#ord_status").val("");
        ajaxOrders(page_url = false);
    });
    //-------- For Order Filter--------//
    //-------- For Transactions pagination--------//
    ajaxTransactionList(page_url = false);
    $(document).on('click','.transaction_list .pagination li a', function(e){
        e.preventDefault();
        var page_url = $(this).attr('href');
        // alert(page_url);
        ajaxTransactionList(page_url);
    });
    //-------- For Transactions pagination--------//
    //-------- For Transactions Filter--------//
    $(document).on('click',"#trn_btn_submit", function(e){
        e.preventDefault();
        ajaxTransactionList(page_url = false);
    });
    $(document).on('click',"#trn_btn_clear_filter", function(e){
        e.preventDefault();
        $("#reference_no").val("");
        $("#trn_quote_no").val("");
        ajaxTransactionList(page_url = false);
    });
    //-------- For Transactions Filter--------//
     //-------- For Orders pagination--------//
    ajaxReviews(page_url = false);
    $(document).on('click','.review_list .pagination li a', function(e){
        e.preventDefault();
        var page_url = $(this).attr('href');
        // alert(page_url);
        ajaxReviews(page_url);
    });
    //-------- For Orders pagination--------//

    ajaxAddress(page_url = false);
    $(document).on('click','.address_list .pagination li a', function(e){
        e.preventDefault();
        var page_url = $(this).attr('href');
        // alert(page_url);
        ajaxAddress(page_url);
    });
    //-------- For Main Product list pagination--------//
    
    // var pageCount = $('.shop_toolbar_btn > button').find('.active').data('product_count');
    // console.log("page Count "+pageCount);
    console.log("checking... "+$('.btn_product_count').filter(function() {
        return $(this).attr('data-product_count');
    }));
    // $(document).on('click','.btn_product_count', function(e){
    //     pageCount = $(this).data('product_count');
    // });
    var sub_category_id = '<?=isset($sub_category_id)?$sub_category_id:0?>';
    var prodObj = mainProductList(sub_category_id);
    prodObj.done(function(res){
        // console.log(res);
        if(res.status == 'success')
        {
            $("#main_product_list").html(res.html);
            $("#prduct_list_counter").html("<?=display('Showing')?> "+res.count+" <?=display('of')?> "+res.total_count+" <?=display('results')?>");
            $("#search_text").val(res.search_text);
            // $(".btn_product_count").find('.active').attr('data-product_count',res.per_page);
            // $(".btn_product_count").find('.active').click();
        }
    });
    $(document).on('click','.main_product_pagination .pagination li a', function(e){
        e.preventDefault();
        page_url = $(this).attr('href');
        // var productObj = mainProductList(sub_category_id,page_url);
        // productObj.done(function(res){
        //     // console.log(res);
        //     if(res.status == 'success')
        //     {
        //         $("#main_product_list").html(res.html);
        //         $("#prduct_list_counter").html("<?//display('Showing')?> "+res.count+" <?//display('of')?> "+res.total_count+" <?//display('results')?>");
        //         $("#search_text").val(res.search_text);
        //     }
        // });
        $(".shop_toolbar_btn > button.active").click();
        // mainProductListStyle($(".shop_toolbar_btn > button.active"),this);
    });
    //-------- For Main Product list pagination--------//
    
    //-------- For Product Search--------//
    $("#search_form").submit(function (e) { e.preventDefault(); });
    
    $("#btn_search").click(function (e) { 
        e.preventDefault();
        var search_category = $("#search_category"); 
        var search_text = $("#search_text"); 
        var valid = true;
        if($.trim(search_category.val()) == '0' || $.trim(search_text.val()) == ''){
            valid = false;
            Swal.fire({
                text: "<?=display('Please provide category and search criteria')?>",
                icon: 'warning',
                showCancelButton: false,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#D3D3D3',
                confirmButtonText: "<?=display('OK')?>"
                }).then((result) => {
                if (result.isConfirmed) {
                    return false;
                }
            })
        }
        // alert(valid);
        if(valid){
            $("#search_form").attr('action','<?=base_url()?>'+'/list-product/'+$.trim(search_category.val())+'/0');
            $("#search_form").get(0).submit();
        }
    });
    //-------- For Product Search--------//

});
    function validEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            return false;
        }else{
            return true;
        }
    }

    function validPasswordLength(pass){
        if(pass.length >= 8){ return true; } else { return false; }
    }
    function validConfirmPassword(pass,conf_pass){
        if(pass.toString() == conf_pass.toString()){ return true; } else { return false; }
    }
    function validMobileNumber(mobile){
        var exp = /^\d{10}$/;
        if(exp.test(mobile)){ return true; } else { return false; }
    }
    // -----------  Add to cart  -------------//
    function addToCart(context,e)
    {
        e.preventDefault();
        var product_id = $(context).data('product_id');

        $.ajax({
            type: "post",
            url: "<?=base_url('add-to-cart')?>",
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {product_id: product_id, '<?=csrf_token()?>': '<?=csrf_hash()?>'},
            dataType: "json",
            success: function (data) {
                // console.log(data);
                if(data.status == 'success'){
                    // console.log(data.message);
                    cartCount($("#btn_cart_list"));
                    Swal.fire({
                        icon: 'success',
                        text: data.message,
                        showConfirmButton: false,
                        timer: 1200
                    })
                } else {
                    if(data.status == 'not_logged_in'){
                        $("#login_modal_box").modal('show');
                    } else {
                        // console.log(data);
                    }
                }
            }
        });
    }
    function cartCount(context)
    {
        $.ajax({
            type: "post",
            url: "<?=base_url('get-item-count')?>",
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>'},
            dataType: "json",
            success: function (response) {
                if(response.status == 'success'){
                    $(context).find('span').html(response.total_count+" Items");
                } else {
                    // console.log(response);
                }
            }
        });
    }
    // -----------  Add to cart  -------------//
    // -----------  Product Details Popup  -------------//
    function productDetails(ctx,e)
    {
        e.preventDefault();
        $("#pd_modal_product_id").val("");
        $("#pd_modal_product_id").val($(ctx).data('product_id'));
    }
    // -----------  Product Details Popup  -------------//
    // -----------  Remove An item from cart  -------------//
    function removeFromCart(ctx,e)
    {
        e.preventDefault();
        var product_id = $(ctx).data('product_id');
        // alert(product_id);
        Swal.fire({
            title: "<?=display('Are you sure?')?>",
            text: "<?=display('You want to remove this product!')?>",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#D3D3D3',
            confirmButtonText: "<?=display('Yes')?>"
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    url: "<?=base_url('remove-from-cart')?>",
                    headers: {'X-Requested-With': 'XMLHttpRequest'},
                    data: {'<?=csrf_token()?>': '<?=csrf_hash()?>', product_id: product_id},
                    dataType: "json",
                    success: function (data) {
                        if(data.status == 'success'){
                            Swal.fire({
                                icon: 'success',
                                text: data.message,
                                showConfirmButton: false,
                                timer: 1000
                            })
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: data.message,
                                showConfirmButton: false,
                                timer: 1000
                            })
                        }
                        setTimeout(function(){ window.location.reload(); }, 1005);
                    }
                });
            }
        })
    }
    // -----------  Remove An item from cart  -------------//

    // -----------  Payment Details page Addres checkbox  -------------//
    function addressChecked(ctx,e)
    {
        if($(ctx).prop('checked'))
        {
            $("#ship_address_id").val($(ctx).val());
        }
    }
    // -----------  Payment Details page Addres checkbox  -------------//

    // -----------  Ajax Quoation List  -------------//
    function ajaxQuotations(page_url)
    {
        var quotation_no = $("#quote_no").val();
        var statu = $("#quote_status").val();
        var base_url = "<?=base_url('ajax-quotations')?>";
        if(page_url){
            base_url = page_url;
        }
        $.ajax({
            type: "post",
            url: base_url,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>',quotation_no: quotation_no,status: status},
            dataType: "json",
            success: function (response) {
                $("#ajax_quote_list").html(response.html);
                $("#quote_no").val(response.quotation_no);
                $("#quote_status").val(response.status);
            }
        });
    }
    // -----------  Ajax Quoation List  -------------//
    
    // -----------  Payment Details page submit  -------------//
    function validatePaymentForm(ctx,e)
    {
        // e.preventDefault();
        var address_id = $.trim(ctx.address_id.value);
        var payment_method = $.trim(ctx.payment_method.value);
        var html = '';
        var valid = true;
        if(address_id == '' || address_id == null)
        {
            html += "<?=display('Atleast One address should be selected')?><br />";
            valid = false;
        }
        if(payment_method == '' || payment_method == null)
        {
            html += "<span class='text-danger'><?=display('One of payment method should be selected')?></span><br />";
            valid = false;
        }
        // console.log(payment_method);
        if(!valid)
        {
            e.preventDefault();
            Swal.fire({
                title: 'Error!',
                html: html,
                width: 500,
                padding: '3em',
                background: '#fff',
            })
        }
        if(valid)
        {
            ctx.submit();
        }
    }
    // -----------  Payment Details page submit  -------------//

    // -----------  Ajax Orders List  -------------//
    function ajaxOrders(page_url)
    {
        var order_no = $.trim($("#order_no").val());
        var ord_status = $.trim($("#ord_status").val());
        var base_url = "<?=base_url('order-ajax')?>";
        if(page_url){
            base_url = page_url;
        }
        $.ajax({
            type: "post",
            url: base_url,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>',order_no: order_no, status: ord_status},
            dataType: "json",
            success: function (response) {
                $("#ajax_order_list").html(response.html);
                $("#order_no").val(response.order_no);
                $("#trn_status").val(response.status);
            }
        });
    }
    // -----------  Ajax Orders List  -------------//

        // -----------  Ajax Orders List  -------------//
    function ajaxReviews(page_url)
    {
        var base_url = "<?=base_url('reviews-ajax')?>";
        if(page_url){
            base_url = page_url;
        }
        $.ajax({
            type: "post",
            url: base_url,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>'},
            dataType: "json",
            success: function (response) {
                $("#ajax_review_list").html(response.html);
            }
        });
    }
    // -----------  Ajax Orders List  -------------//

    function ajaxAddress(page_url)
    {
        var base_url = "<?=base_url('address-ajax')?>";
        if(page_url){
            base_url = page_url;
        }
        
        $.ajax({
            type: "post",
            url: base_url,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>'},
            dataType: "json",
            success: function (response) {
                $("#ajax_address_list").html(response.html);
            }
        });
    }
    // -----------  Ajax Transaction List  -------------//
    function ajaxTransactionList(page_url)
    {
        var reference_no = $.trim($("#reference_no").val());
        var trn_quote_no = $.trim($("#trn_quote_no").val());
        var base_url = "<?=base_url('ajax-transaction')?>";
        if(page_url){
            base_url = page_url;
        }
        $.ajax({
            type: "post",
            url: base_url,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>',reference_no: reference_no, quote_no:trn_quote_no},
            dataType: "json",
            success: function (response) {
                $("#ajax_transaction_list").html(response.html);
                $("#reference_no").val(response.reference_no);
                $("#trn_quote_no").val(response.quotation_no);
            }
        });
    }
    // -----------  Ajax Transaction List  -------------//
    
    // -----------  Ajax Main Product list  -------------//
    function mainProductList(sub_category_id){
        // $("#main_product_list").html("");
        // alert();
        var search_text = $("#search_text").val();
        var category_id = $("#search_category").val();
        var base_url = "<?=base_url('product-dynamic')?>";
        // pageCount = $(".btn_product_count").find('.active').attr('data-product_count');
        
        if(page_url){
            base_url = page_url;
        }
        return $.ajax({
            type: "post",
            url: base_url,
            data: {'<?=csrf_token()?>': '<?=csrf_hash()?>',sub_category_id: sub_category_id,per_page:pageCount,search_text: search_text,category_id: category_id},
            dataType: "json",
        });
    }
    // -----------  Ajax Main Product list  -------------//


    // -----------  Main Product list style  -------------//
    function mainProductListStyle(ctx,e) {
        
        e.preventDefault();
        var sub_category_id = '<?=isset($sub_category_id)?$sub_category_id:0?>';
        pageCount = $(ctx).data('product_count');
        var productObj = mainProductList(sub_category_id);
        // alert('main product list style');
        productObj.done(function(res){
            // console.log(res);
            if(res.status == 'success')
            {
                $("#main_product_list").html(res.html);
                $("#prduct_list_counter").html("<?=display('Showing')?> "+res.count+" <?=display('of')?> "+res.total_count+" <?=display('results')?>");
                $("#search_text").val(res.search_text);
                $("#search_category").val(''+res.category_id);
                // $(".btn_product_count").find('.active').attr('data-product_count',res.per_page);
                $("#main_product_list").animate('slow');
                $('.shop_toolbar_btn > button').removeClass('active');
                ctx.addClass('active');
                
                var parentsDiv = $('.shop_wrapper');
                var viewMode = ctx.data('role');
                
                
                parentsDiv.removeClass('grid_3 grid_4 grid_5 grid_list').addClass(viewMode);

                if(viewMode == 'grid_3'){
                    parentsDiv.children().addClass('col-lg-4 col-md-4 col-sm-6').removeClass('col-lg-3 col-cust-5 col-12');
                }

                if(viewMode == 'grid_4'){
                    parentsDiv.children().addClass('col-lg-3 col-md-4 col-sm-6').removeClass('col-lg-4 col-cust-5 col-12');
                }
                
                if(viewMode == 'grid_list'){
                    parentsDiv.children().addClass('col-12').removeClass('col-lg-3 col-lg-4 col-md-4 col-sm-6 col-cust-5');
                }
            }
        }); 
	}
    // -----------  Main Product list style  -------------//
    // -----------  Main Product list Filter section  -------------//
    function mainProductListFilter(ctx,e) {
        
        e.preventDefault();
        var sub_category_id = ctx.data('sub_category_id');
        pageCount = $('.shop_toolbar_btn > button').find('.active').data('product_count');
        var productObj = mainProductList(sub_category_id);
        productObj.done(function(res){
            // console.log(res);
            if(res.status == 'success')
            {
                $("#main_product_list").html(res.html);
                $("#prduct_list_counter").html("<?=display('Showing')?> "+res.count+" <?=display('of')?> "+res.total_count+" <?=display('results')?>");
                $("#search_text").val(res.search_text);
                $("#search_category").val(''+res.category_id);
                // $(".btn_product_count").find('.active').attr('data-product_count',res.per_page);
                // $(".btn_product_count").find('.active').click();
            }
        }); 
	}

    $(function() {
        $(".btn-lang").click(function() { event.preventDefault();
            var lang = $(this).data('id');
            var token = "<?= csrf_hash() ?>";
            // alert(lang);
            $.ajax({
                      type:'POST',
                      url:'<?=base_url('language-toggle');?>', 
                      data:{lang: lang, csrf_stream_token:token},
          
              }).done(function(response) 
              {
                // console.log(response);
                window.location.reload();

              }).fail(function() 
              {
                  // alert(this.responseText);
              });

        });
    });
    // -----------  Main Product list Filter section  -------------//


</script>
</body>

</html>