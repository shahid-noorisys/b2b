    
    <!--brand newsletter area start-->
    <div class="brand_newsletter_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                   <!--  <div class="brand_container owl-carousel">
                        
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!--brand area end-->
    
    <!--footer area start-->
    <footer class="footer_widgets" style="background: #f3f3f3;">
        <div class="container">  
            <div class="footer_top">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="widgets_container contact_us">
                            <a href="<?=base_url()?>"><img src="<?php echo base_url('public/assets/img/logo/yahodehime.png')?>" alt=""></a>
                            <div class="footer_contact">
                                <ul>
                                    <li><i class="zmdi zmdi-home"></i><span>Addresss:</span> 2 Fauconberg Rd,Chiswick, London</li>
                                    <li><i class="zmdi zmdi-phone-setting"></i><span>Phone:</span><a href="tel:(+1) 866-540-3229">(+1) 866-540-3229</a> </li>
                                    <li><i class="zmdi zmdi-email"></i><span>Email:</span>  info@plazathemes.com</li>
                                </ul>
                            </div>
                             <div class="social_icone">
                                <ul>
                                    <li class="share"><a href="#" title="rss"><i class="fa fa-share-alt"></i></a>
                                        <div class="social_title">
                                            <p>Subscribe</p>
                                            <h3>Rss Feed</h3>
                                        </div> 
                                    </li>
                                    <li class="twitter"><a href="#" title="twitter"><i class="fa fa-twitter"></i></a>
                                        <div class="social_title">
                                            <p>Follow Us</p>
                                            <h3>Twitter</h3>
                                        </div> 
                                    </li>
                                    <li class="facebook"><a href="#" title="facebook"><i class="fa fa-facebook"></i></a>
                                        <div class="social_title">
                                            <p>Find Us</p>
                                            <h3>Facebook</h3>
                                        </div>
                                    </li>
                                    <li class="google_plus"><a href="#" title="google"><i class="fa fa-google-plus"></i></a>
                                        <div class="social_title">
                                            <p>Find Us</p>
                                            <h3>Google+</h3>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-7">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="widgets_container widget_menu">
                                    <h3>CUSTOMER SERVICE</h3>
                                    <div class="footer_menu">
                                        <ul>
                                            <li><a href="#">Shipping & Returns</a></li>
                                            <li><a href="#"> Secure Shopping</a></li>
                                            <li><a href="#">International Shipping</a></li>
                                            <li><a href="#"> Affiliates</a></li>
                                            <li><a href="contact.html"> Contact</a></li>
                                            <li><a href="#"> Travel</a></li>
                                            <li><a href="#">ecommerce</a></li>
                                            <li><a href="#"> Creative</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="widgets_container widget_menu">
                                    <h3>Information</h3>
                                    <div class="footer_menu">
                                        <ul>
                                            <li><a href="<?=base_url('about-us')?>"><?=display('About Us')?></a></li>
                                            <li><a href="<?=base_url('contact-us')?>"><?=display('Contact Us')?></a></li>
                                            <li><a href="<?=base_url('privacy-policy')?>"><?=display('Privacy policy')?></a></li>
                                            <li><a href="<?=base_url('terms')?>"><?=display('Terms & Conditions')?></a></li>
                                            <li><a href="<?=base_url('return-refund')?>"><?=display('Return & Refund')?></a></li>
                                            <li><a href="<?=base_url('faq')?>"> <?=display('Frequently Questions')?></a></li>
                                           
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="widgets_container">
                            <h3>Latest Posts</h3>
                            <div class="Latest_Posts_wrapper">
                                <div class="single_Latest_Posts">
                                    <div class="Latest_Posts_thumb">
                                        <a href="#"><img src="assets/img/category/post1.jpg" alt=""></a>
                                    </div>
                                    <div class="Latest_Posts_content">
                                        <h3><a href="#">Blog image post</a></h3>
                                        <span><i class="zmdi zmdi-card-sd"></i> 10 Mar, 2019</span>
                                    </div>
                                </div>
                                <div class="single_Latest_Posts">
                                    <div class="Latest_Posts_thumb">
                                        <a href="#"><img src="assets/img/category/post2.jpg" alt=""></a>
                                    </div>
                                    <div class="Latest_Posts_content">
                                        <h3><a href="#">Post with Gallery</a></h3>
                                        <span><i class="zmdi zmdi-card-sd"></i> 10 Mar, 2019</span>
                                    </div>
                                </div>
                                <div class="single_Latest_Posts">
                                    <div class="Latest_Posts_thumb">
                                        <a href="#"><img src="assets/img/category/post3.jpg" alt=""></a>
                                    </div>
                                    <div class="Latest_Posts_content">
                                        <h3><a href="#">Post with Audio</a></h3>
                                        <span><i class="zmdi zmdi-card-sd"></i> 10 Mar, 2019</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
      <!--   <div class="footer_tag">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="footer_tag_container">
                            <div class="footer_tag_menu">
                                <h3>Furniture :</h3>
                                <ul>
                                    <li><a href="#">bedroom</a></li>
                                    <li><a href="#">Livingroom</a></li>
                                    <li><a href="#">badroom</a></li>
                                    <li><a href="#">Sofa</a></li>
                                    <li><a href="#">Chair</a></li>
                                    <li><a href="#">Bed</a></li>
                                    <li><a href="#">Desk</a></li>
                                </ul>
                            </div>
                            <div class="footer_tag_menu">
                                <h3>Electronic :</h3>
                                <ul>
                                    <li><a href="#">Laptop</a></li>
                                    <li><a href="#">TV</a></li>
                                    <li><a href="#">Computer</a></li>
                                    <li><a href="#">Mobile</a></li>
                                    <li><a href="#">Tablet</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
         <div class="footer_bottom">      
            <div class="container">
               <div class="row">
                    <div class="col-lg-6 col-md-7">
                        <div class="copyright_area">
                            <p>Copyright &copy; 2020 <a href="#"> <?=display('yahodehime')?> </a>  All Right Reserved.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5">
                        <div class="footer_payment text-right">
                            <p><img src="assets/img/icon/payment.png" alt=""></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </footer>
    <!--footer area end-->
   
    <?=view('buyer/modals/login_modal')?>
    <?=view('buyer/modals/product_detail_modal')?>
    <?=view('buyer/modals/add_billing_address')?>
    <?=view('buyer/modals/review_rating_modal')?>
    <?=view('buyer/modals/edit_address_modal_box')?>
    <?=view('buyer/modals/add_company_profile')?>
    <?=view('buyer/modals/edit_company_profile')?>
    <?//view('buyer/modals/subscribe')?>
