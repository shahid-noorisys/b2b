<?php 
    $UserModel = model('App\Models\UserModel');
    if(!empty($_SESSION['user_details'])){
        $UserDetails = $UserModel->find(session()->get('user_details')->user_id);
    }
?>
    <!--header area start-->
    <header class="header_area">
        <!--header top start-->
        <div class="header_top">
            <div class="container">  
                <div class="row align-items-center">
                    <div class="col-lg-4">
                       <div class="welcome_text">
                           <p><?=display('Welcome to')?> <span><?=strtoupper(display('yahodehime'))?></span> </p>
                       </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="top_right text-right">
                            <ul>
                                <!-- <li class="currency"><a href="#"><i class="fa fa-dollar"></i> US Dollar <i class="zmdi zmdi-caret-down"></i></a>
                                    <ul class="dropdown_currency">
                                        <li><a href="#">EUR – Euro</a></li>
                                        <li><a href="#">GBP – British Pound</a></li>
                                        <li><a href="#">INR – India Rupee</a></li>
                                    </ul>
                                </li> -->
                                <?php if(! loggedIn()): ?>
                                <li class="top_linksss"><a href="<?=base_url('supplier/login')?>"><i class="fa fa-sign-in"></i> <?=display('Supplier Login') ?> </a>
                                </li>
                                <li class="top_linksss"><a href="<?=base_url('register-buyer')?>"><i class="fa fa-user-plus"></i> <?=display('Register') ?> </a>
                                </li>
                                <li class="top_linksss"><a href="#" data-toggle="modal" data-target="#login_modal_box"><i class="zmdi zmdi-account"></i> <?=display('Login') ?> </a>
                                </li>
                                <?php else: ?>
                                <li class="top_links"><a href="#"><i class="zmdi zmdi-account"></i> <?=$UserDetails->firstname.' '.$UserDetails->lastname?> <i class="zmdi zmdi-caret-down"></i></a>
                                    <ul class="dropdown_links">
                                        <!-- <li><a href="checkout.html">Checkout </a></li> -->
                                        <li><a href="<?=base_url('buyer-profile')?>">My Account </a></li>
                                        <!-- <li><a href="cart.html">Shopping Cart</a></li> -->
                                        <!-- <li><a href="wishlist.html">Wishlist</a></li> -->
                                    </ul>
                                </li>
                                <?php
                                $session = session();
                                $note_count = (isset($note_current_counter))?$note_current_counter:$session->get('user_details')->note_count;
                                ?>
                                <li class=""> <a href="<?=base_url('list-notification')?>"><i class="zmdi zmdi-notifications"></i> <?=display('Notification')?> <span class="badge badge-dark" id="note_counter"><?=$note_count?></span> </a>
                                </li>
                                <li class="top_linksss"><a href="<?=base_url('logout-buyer')?>"> <i class="fa fa-sign-out"></i> <?=display('Logout') ?> </a>
                                </li>
                                <?php endif; ?>

                                <?php 
                                  $text = '<img src="'.base_url('public/assets/img/english.png').'" alt=""> '.display('English');

                                  if(isset($_SESSION['lang']))
                                  {
                                  if($_SESSION['lang']=='fr'){ $text='<img src="'.base_url('public/assets/img/french.png').'" alt=""> '.display('French');} 
                                  else {  $text = '<img src="'.base_url('public/assets/img/english.png').'" alt=""> '.display('English');}
                                  } 
                                  else 
                                  { 
                                    $_SESSION['lang'] = "en"; 
                                    $text = '<img src="'.base_url('public/assets/img/english.png').'" alt=""> '.display('English'); 
                                  }
                                ?>
                                <li class="language"><a><?=$text;?><i class="ion-ios-arrow-down"></i></a>
                                    <ul class="dropdown_language">
                                      <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='fr')): ?>
                                        <li>
                                          <a href="#" class="btn-lang" style="cursor: pointer; color: black" data-id="fr"><img src="<?=base_url('public/assets/img/french.png')?>" alt=""> <?=display('French');?></a>
                                        </li>
                                      <?php endif; ?>
                                      <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='en')): ?>
                                        <li>
                                          <a href="#" class="btn-lang" style="cursor: pointer; color: black" data-id="en"><img src="<?=base_url('public/assets/img/english.png')?>" alt=""> <?=display('English');?></a>
                                        </li>
                                      <?php endif; ?>
                                    </ul>
                                </li>
                            </ul>
                        </div> 
                    </div>
                    
                </div>
            </div>
        </div>
        <!--header top start-->
        <!--header center area start-->
        <div class="header_middle">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="logo">
                            <a href="<?=base_url()?>"><img src="<?php echo base_url('public/assets/img/logo/yahodehime.png')?>" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="header_middle_inner">
                            <div class="search-container">
                                <?=form_open('#', ' id="search_form" '); ?>
                                    <div class="hover_category">
                                    <?php $categories = getAllCategories();
                                    $category_id = (isset($category_id))?$category_id:'0';
                                    if(!empty($categories)):
                                    ?>
                                        <select class="select_option" name="search_category" id="search_category">
                                            <option <?=($category_id == 0)?'selected':''?> value="0"><?=display('All Categories')?></option>
                                        <?php foreach ($categories as $v): ?>
                                            <option value="<?=$v->id?>" <?=($category_id == $v->id)?'selected':''?>><?=$v->name?></option>
                                        <?php endforeach; ?>
                                        </select>                      
                                    <?php endif; ?>
                                    </div>
                                    <div class="search_box">
                                        <input placeholder="Search product..." type="text" name="search_text" id="search_text" value="<?=(isset($search_text))?$search_text:''?>">
                                        <button type="submit" id="btn_search"><i class="zmdi zmdi-search"></i></button> 
                                    </div>
                                </form>
                            </div>
                            <div class="mini_cart_wrapper">
                                <a href="<?=base_url('cart-list')?>" id="btn_cart_list"><i class="zmdi zmdi-shopping-basket"></i> <span>0 items</span> </a>
                                <!--mini cart-->
                                <!-- <div class="mini_cart">
                                    <div class="cart_item">
                                       <div class="cart_img">
                                           <a href="#"><img src="assets/img/s-product/product.jpg" alt=""></a>
                                       </div>
                                        <div class="cart_info">
                                            <a href="#">Condimentum Watches</a>

                                            <span class="quantity">Qty: 1</span>
                                            <span class="price_cart">$60.00</span>

                                        </div>
                                        <div class="cart_remove">
                                            <a href="#"><i class="ion-android-close"></i></a>
                                        </div>
                                    </div>
                                    <div class="cart_item">
                                       <div class="cart_img">
                                           <a href="#"><img src="assets/img/s-product/product2.jpg" alt=""></a>
                                       </div>
                                        <div class="cart_info">
                                            <a href="#">Officiis debitis</a>
                                            <span class="quantity">Qty: 1</span>
                                            <span class="price_cart">$69.00</span>
                                        </div>
                                        <div class="cart_remove">
                                            <a href="#"><i class="ion-android-close"></i></a>
                                        </div>
                                    </div>
                                    <div class="mini_cart_table">
                                        <div class="cart_total">
                                            <span>Subtotal:</span>
                                            <span class="price">$138.00</span>
                                        </div>
                                    </div>

                                    <div class="mini_cart_footer">
                                       <div class="cart_button">
                                            <a href="cart.html">View cart</a>
                                            <a href="checkout.html">Checkout</a>
                                        </div>
                                    </div>

                                </div> -->
                                <!--mini cart end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--header center area end-->

<!-- ////////////////////////////////-------Header End-----/////////////////////////// -->