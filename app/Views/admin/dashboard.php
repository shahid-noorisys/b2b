<style type="text/css">
    sup {
    top: -0.1em;
}
</style>
<?php 
    $admin_id = session()->get('user_details')->admin_id;
    $order = model('App\Models\OrdersModel',false,$b);
    $allOrders = $order->findAll();    
    $allpendingOrders = $order->findAll();    
    $alldeliveredOrders = $order->findAll();    

    $Products = model('App\Models\ProductModel',false,$b);
    $allProducts = $Products->findAll();

    $quote = model('App\Models\QuotationModel',false,$b);
    $allAcceptedquote = $quote->where(['is_accepted'=>1,'is_rejected'=>2])->findAll();
    $allValidatequote = $quote->where(['is_rejected'=>2,'is_validated'=>1])->findAll();
?>
 <div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Monthly Report</h4>
                <div id="bar-chart" style="width:100%; height:400px;"></div>
            </div>
        </div>
    </div>
    <!-- column -->
</div>
<div class="row">
    <!-- Column -->
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?=strtoupper(display('Total Products'))?></h4>
                <div class="text-right"> <span class="text-muted"><?=display('Overall Products')?></span>
                    <h1 class="font-light"><sup><i class="ti-arrow-right text-success"></i></sup> <?=(!empty($allProducts))?count($allProducts):0?></h1>
                </div>
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 50%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?=strtoupper(display('Accepted Quotation'))?></h4>
                <div class="text-right"> <span class="text-muted"><?=display('Overall Quotations')?></span>
                    <h1 class="font-light"><sup><i class="ti-arrow-right text-primary"></i></sup> <?=(!empty($allAcceptedquote))?count($allAcceptedquote):0?></h1>
                </div>
                <div class="progress">
                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?=strtoupper(display('Validate Quotation'))?></h4>
                <div class="text-right"> <span class="text-muted"><?=display('Overall Quotations')?></span>
                    <h1 class="font-light"><sup><i class="ti-arrow-right text-info"></i></sup> <?=(!empty($allValidatequote))?count($allValidatequote):0?></h1>
                </div>
                <div class="progress">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?=strtoupper(display('Order Received'))?></h4>
                <div class="text-right"> <span class="text-muted"><?=display('Overall Orders')?></span>
                    <h1 class="font-light"><sup><i class="ti-arrow-right text-inverse"></i></sup> <?=(!empty($allOrders))?count($allOrders):0?></h1>
                </div>
                <div class="progress">
                    <div class="progress-bar bg-inverse" role="progressbar" style="width: 50%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">   
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?=display('Recent Quotations')?></h5>
                <div class="table-responsive m-t-30">
                    <table class="table product-overview">
                        <thead>
                            <tr>
                                <th><?=display('Quotation Number')?></th>
                                <th><?=display('Request From')?></th>
                                <th><?=display('Amount')?></th>
                                <th><?=display('Total Items')?></th>
                                <th><?=display('Accepted')?></th>
                                <th><?=display('Paid')?></th>
                                <th><?=display('Validate')?></th>
                                <th><?=display('Action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($quotation as $p): ?>
                                <?php $user = get_user_details_by_id($p->request_from,3); ?>
                                <tr>
                                    <td><?=$p->q_no?></td>
                                    <td><?=$user->firstname.' '.$user->lastname;?></td>
                                    <td><?=$p->amount?></td>
                                    <td><?=$p->total_items?></td>
                                    <td><?=($p->is_accepted == 'Yes')?'<span class="label label-success font-weight-100">'.display('Yes').'</span>':'<span class="label label-primary font-weight-100">'.display('No').'</span>'?> </td>
                                    <td><?=($p->is_paid == 'Yes')?'<span class="label label-success font-weight-100">'.display('Yes').'</span>':'<span class="label label-primary font-weight-100">'.display('No').'</span>'?> </td>
                                    <td><?=($p->is_validated == 'Yes')?'<span class="label label-success font-weight-100">'.display('Yes').'</span>':'<span class="label label-primary font-weight-100">'.display('No').'</span>'?> </td>
                                    <td>
                                        <a href="<?=base_url('admin/quotation/view/'.$p->id.'')?>" class="text-dark m-r-10" title="<?=display('View')?>" data-toggle="tooltip"  data-id="<?=$p->id?>"><i class="fas fa-eye"></i></a>

                                        <a href="<?=base_url('admin/messages/'.$p->id.'')?>" class="text-dark" title="<?=display('Message')?>" data-toggle="tooltip"  data-id="<?=$p->id?>"><i class="fas fa-paper-plane"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">   
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?=display('Recent Orders')?></h5>
                <div class="table-responsive m-t-30">
                    <table class="table product-overview">
                       <thead>
                            <tr>
                                <th>#</th>
                                <th><?=display('Order Number')?></th>
                                <th><?=display('Quotation Number')?></th>
                                <th><?=display('Request From')?></th>
                                <th><?=display('Amount')?></th>
                                <th><?=display('Total Items')?></th>
                                <th><?=display('Payment mode')?></th>
                                <th><?=display('Order status')?></th>
                                <th><?=display('Action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($orders as $key => $value): ?>
                                <?php $user = get_user_details_by_id($value->buyer_id,3); ?>
                                <tr>
                                    <td><?=$key+1?></td>
                                    <td><?=$value->order_no?></td>
                                    <td><?=$value->q_no?></td>
                                    <td><?=$user->firstname.' '.$user->lastname;?></td>
                                    <td><?=$value->amount?></td>
                                    <td><?=$value->total_items?></td>
                                    <td><?=($value->payment_mode == 'Online')?'<span class="label label-success font-weight-100">'.display('Online').'</span>':'<span class="label label-warning font-weight-100">'.display('Cash').'</span>'?> </td>
                                    <td>
                                        <?php if($value->order_status == 'pending'):?>
                                            <span class="label label-primary font-weight-100"><?=display('Pending')?></span>
                                        <?php endif;?>
                                        <?php if($value->order_status == 'in_progress'):?>
                                            <span class="label label-warning font-weight-100"><?=display('In-progress')?></span>
                                        <?php endif;?>
                                        <?php if($value->order_status == 'delivered'):?>
                                            <span class="label label-success font-weight-100"><?=display('Delivered')?></span>
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <a href="<?=base_url('admin/orders/view/'.$value->id)?>" class="text-dark m-r-10" title="<?=display('View')?>" data-toggle="tooltip"  data-id="<?=$value->id?>"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
