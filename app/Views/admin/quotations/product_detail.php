<div class="row">
   <div class="col-md-5">
        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators2" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="img-fluid" src="<?=($product ANd $product->image_1 != '')? base_url($product->image_1) :base_url('public/assets/img/no-image.jpg')?>" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?=($product ANd $product->image_2 != '')? base_url($product->image_2) :base_url('public/assets/img/no-image.jpg')?>" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?=($product ANd $product->image_3 != '')? base_url($product->image_3) :base_url('public/assets/img/no-image.jpg')?>" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?=($product ANd $product->image_4 != '')? base_url($product->image_4) :base_url('public/assets/img/no-image.jpg')?>" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="img-fluid" src="<?=($product ANd $product->image_5 != '')? base_url($product->image_5) :base_url('public/assets/img/no-image.jpg')?>" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
   </div>
   <div class="col-md-7">
        <h3 class="m-t-10"><?=$product->name?></h3>
        <h6 class="card-subtitle m-t-10">Category : <?=$product->category_name?></h6>
        <h6 class="card-subtitle m-t-10">Sub-Category : <?=$product->sub_category_name?></h6>
        <h4 class="box-title m-t-40">Product description</h4>
        <p><?=$product->description?></p>
        <h3 class="box-title m-t-40">Key Highlights</h3>
        <ul class="list-unstyled">
            <li> 
                <?php if ($product->is_published == 'Yes'):?>
                <i class="fa fa-check text-success"></i> Publish : Yes
                <?php else:?>
                <i class="fa fa-times text-danger"></i> Publish : NO
                <?php endif;?>
            </li>
            <li>
                <?php if ($product->status == 'Active'):?>
                <i class="fa fa-check text-success"></i> Status : Active
                <?php else:?>
                <i class="fa fa-times text-danger"></i> Status : Inactive
                <?php endif;?>
            </li>
        </ul>
        
   </div>
</div>