<style type="text/css">
    .w-30{
        width: 30%;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col text-left">
                        <a href="<?=base_url('admin/quotations')?>" class="btn btn-primary" href="#" role="button"> <i class="fa fa-arrow-left"></i> <?=display('Back')?> </a>
                    </div>
                    <?php if($quote->is_rejected !== 'Yes' && $quote->is_paid == 'No'):?>
                        <div class="col text-right">
                            <?php if($quote->is_validated !== 'Yes'):?>
                                <button id="print" class="btn waves-effect waves-light btn-outline-success btn-valid <?=($quote->amount<1500)?'disabled':''?>" type="button" data-quote ="<?=$quote->id?>"> <span><i class="fa fa-check"></i> <?=display('Validate')?></span> </button>
                                <b> | </b>
                            <?php endif; ?>
                            <button data-id="<?=$quote->id?>" class="btn waves-effect waves-light btn-outline-danger btn-reject" type="button"> <span><i class="fa fa-times"></i> <?=display('Reject')?></span> </button>                
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-body printableArea">
            <h3><b><?= strtoupper(display('Quotation')) ?></b> <span class="pull-right">#<?=$quote->q_no?></span></h3>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-left">
                        <address>
                            <h5><?= strtoupper(display('To'));?>,</h5>
                            <h4> &nbsp;<b class="font-bold"><?=($seller_company->company_name)??''?></b></h4>
                            <p class="text-muted m-l-5"><?=($seller_address->address)??''?>,
                                <br/> <?=($seller_address->city)??''?> - <?=($seller_address->zip_code)??''?>,
                                <br/> <?=($seller_address->country)??''?></p>
                        </address>
                    </div>
                    <div class="pull-right text-right">
                        <address>
                            <h5><?= strtoupper(display('From'));?>,</h5>
                            <h4 class="font-bold"><?=($buyer_company->company_name)??((!empty($buyer) AND isset($buyer->firstname))?$buyer->firstname.' '.$buyer->lastname:'')?>,</h4>
                            <p class="text-muted m-l-30"><?=($buyer_address->address)??''?>,
                                <br/> <?=($buyer_address->city)??''?> - <?=($buyer_address->zip_code)??''?>,
                                <br/> <?=($buyer_address->country)??''?></p>
                            
                            <p class="m-t-30"><b><?=display('Quotation Date')?> :</b> <i class="fa fa-calendar"></i> <?= date('d M Y',strtotime($quote->created_date)) ?></p>
                           
                            <p><b><?=display('Due Date')?> :</b> <i class="fa fa-calendar"></i> <?= date('d M Y',strtotime($quote->created_date)) ?></p>
                            
                            <p><b><?=display('Accepted')?> :</b> <?=($quote->is_accepted == 'Yes')?'<span class="label label-success label font-weight-500">'.display('Yes').'</span>':'<span class="label label-danger font-weight-500">'.display('No').'</span>'?></p>
                            
                            <p><b><?=display('Validate')?> :</b> <?=($quote->is_validated == 'Yes')?'<span class="label label-success font-weight-500">'.display('Yes').'</span>':'<span class="label label-danger font-weight-500">'.display('No').'</span>'?></p>
                        </address>
                    </div>
                </div>
                <div class="col-md-12">
                    <!-- <div class="table-responsive m-t-40" style="clear: both;"> -->
                        <table id="product" class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Image</th>
                                    <th>Description</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Unit Cost</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if(!empty($quote->product_json) && isset($quote->product_json)):
                                        $products_json = json_decode($quote->product_json,true);
                                    endif;
                                if (!empty($products_json)):
                                    foreach ($products_json as $key => $value):
                                       $productModel = model('App\Models\ProductModel');
                                       $products = $productModel->find($value['product_id']);
                                ?>
                                    <tr>
                                        <td class="text-center"><?=$key+1?></td>
                                        <td>
                                            <a href="javascript:void(0)" class="product-view" data-id="<?=$value['product_id']?>">
                                                <?=$products->name?>
                                            </a>
                                        </td>
                                        <td> <img src="<?=base_url($products->image_1)?>" alt="iMac" width="60" height="60"> </td>
                                        <td><?=character_limiter($products->description,80)?></td>
                                        <td class="text-center"><?=$value['quantity'];?></td>
                                       
                                        <td class="text-center product_amount"><?=($value['product_amount'])? number_format($value['product_amount'],2):0?></td>

                                        <td class="text-center product_total"><?= number_format((int)$value['quantity']*(int)$value['product_amount'],2) ?></td>

                                    </tr>
                                    <?php endforeach; ?> 
                                <?php endif; ?>
                            </tbody>
                        </table>
                    <!-- </div> -->
                </div>
                <div class="col-md-12">
                    <div class="pull-right m-t-30 text-right">
                        <!-- <p>Sub - Total amount: $13,848</p>
                        <p>vat (10%) : $138 </p> -->
                        <hr>
                        <h5><b>Total Amount :</b> <span id="final_total"><?=($quote->amount)?$quote->amount:0?></span></h5>
                    </div>
                    <div class="clearfix"></div>
                    <hr>                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="product_details" class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl modal-dialog-centered">
    <!-- <div class="modal-dialog modal-dialog-centered"> -->
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h4 class="modal-title" id="vcenter">Modal Heading</h4> -->
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body products-info">
             
            </div>
           
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    $('#product').DataTable({
         "lengthChange": false,
         "pageLength": 5,
    });
    $(".select2").select2();
    
    $(document).on('click','.product-view', function(e){
        var product_id = $(this).data('id');
        var token = "<?= csrf_hash() ?>";
        $.ajax({
             url: "<?php echo base_url('supplier/product-details'); ?>",
             type: 'POST',
             dataType: 'html',
             data: {product_id:product_id,csrf_stream_token:token},
        }).done(function(data) {
            // console.log(data);
            $('.products-info').html(data);
            $('#product_details').modal('show');
        });
    });

    $(document).on('click','.btn-reject', function(e){
        var quote_id = $(this).data('id');
        var token = "<?= csrf_hash() ?>";
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'mr-2 btn btn-danger'
          },
          buttonsStyling: false,
        })
        swalWithBootstrapButtons.fire({
          title: "<?= display('Are you sure'); ?>",
          text:"<?= display('You want to reject this quotation'); ?>",
          type: 'warning',
          // icon: 'warning',
          showCancelButton: true,
          confirmButtonText: "<?= display('Yes reject it'); ?>",
          cancelButtonText: "<?= display('No cancel it'); ?>",
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            $.ajax({
             url: "<?php echo base_url('admin/reject-quotation'); ?>",
             type: 'POST',
             dataType: 'json',
             data: {quote_id: quote_id, csrf_stream_token:token},
            }).done(function(data) {
                if (data.status == 'success') {
                    swalWithBootstrapButtons.fire({
                        title: "<?= display('Success'); ?>",
                        text:data.message,
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: "<?= display('Okay'); ?>",
                        reverseButtons: true
                    });
                   setTimeout(function() { window.location.reload(); }, 2000);
                } else {
                    swalWithBootstrapButtons.fire({
                        title: "<?= display('Sorry'); ?>",
                        text:data.message,
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonText: "<?= display('Okay'); ?>",
                        reverseButtons: true
                    });
                   setTimeout(function() { window.location.reload(); }, 2000);
                }
               
            });
          } 
        })
    });


    $(document).on('click','.btn-valid', function(e){
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'mr-2 btn btn-danger'
          },
          buttonsStyling: false,
        })
        var quote_id = $(this).data('quote');
        var token = "<?= csrf_hash() ?>";
        $(this).prop("disabled", true);
        
        $.ajax({
             url: "<?php echo base_url('admin/valid-quotation'); ?>",
             type: 'POST',
             dataType: 'json',
             data: {quote_id:quote_id,csrf_stream_token:token},
        }).done(function(data) {
            console.log(data);
            if (data.status == 'success') {
                    $(this).prop("disabled", false);
                    swalWithBootstrapButtons.fire({
                        title: "<?= display('Success'); ?>",
                        text:data.message,
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: "<?= display('Okay'); ?>",
                        reverseButtons: true
                    });
                   setTimeout(function() { window.location.reload(); }, 2000);
                } 
        });
    });


</script>
