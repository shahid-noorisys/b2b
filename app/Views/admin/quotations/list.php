<link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />
<style type="text/css">
input[type=radio], input[type=checkbox] {
    box-sizing: border-box;
    padding: 0;
    position: absolute;
    opacity: 0;
}
.btn-group>.btn {
    position: relative;
    flex: 1 1 auto;
    display: inline-block;
    border: 1px solid;
    cursor: pointer;
}
</style>
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open('admin/quotations','class="order-filter-form" autocomplete="off"'); ?>
                            <div class="row">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class=" far fa-lightbulb"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Quotation number/ Ref number')?>" aria-describedby="basic-addon1" name="quote_no" value ="<?=(isset($_SESSION['quote']['quote_no']) && $_SESSION['quote']['quote_no']!="")?$_SESSION['quote']['quote_no']:''?>">
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                   
                                  <?=form_dropdown('seller_id', $seller_list, (isset($_SESSION['quote']['seller_id']) && $_SESSION['quote']['seller_id']!="")?$_SESSION['quote']['seller_id']:'' ,' class="select2 form-control" ');?>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                   
                                  <?=form_dropdown('user_id', $users_list, (isset($_SESSION['quote']['user_id']) && $_SESSION['quote']['user_id']!="")?$_SESSION['quote']['user_id']:'' ,' class="select2 form-control" ');?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select From Date')?>" name="start" value="<?=(isset($_SESSION['quote']['start']) && $_SESSION['quote']['start']!="")?date('m/d/Y',strtotime($_SESSION['quote']['start'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select To Date')?>" name="end" value="<?=(isset($_SESSION['quote']['end']) && $_SESSION['quote']['end']!="")?date('m/d/Y',strtotime($_SESSION['quote']['end'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-md-12 pull-left">
                                    <label for="status"><?=display('Action')?> : &nbsp;</label>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['quote']['status']) && $_SESSION['quote']['status'] == 'Accepted')?'active':''?>">
                                            <input type="radio" name="status" id="accepted" value="Accepted"><?=strtoupper(display('Accepted'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['quote']['status']) && $_SESSION['quote']['status'] == 'Paid')?'active':''?>">
                                            <input type="radio" name="status" id="paid" value="Paid"><?=strtoupper(display('Paid'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['quote']['status']) && $_SESSION['quote']['status'] == 'Validate')?'active':''?>">
                                            <input type="radio" name="status" id="validate" value="Validate"><?=strtoupper(display('Validate'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-dark <?=(isset($_SESSION['quote']['status']) && $_SESSION['quote']['status'] == 'Rejected')?'active':''?>">
                                            <input type="radio" name="status" id="rejected" value="Rejected"><?=strtoupper(display('Rejected'))?>
                                        </label>
                                    </div>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['quote']['criteria']) && $_SESSION['quote']['criteria'] == 'All')?'active':''?>">
                                            <input type="radio" name="criteria" id="all" value="All"><?=strtoupper(display('All'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['quote']['criteria']) && $_SESSION['quote']['criteria'] == 'Yes')?'active':''?>">
                                            <input type="radio" name="criteria" id="Yes" value="Yes"><?=strtoupper(display('Yes'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['quote']['criteria']) && $_SESSION['quote']['criteria'] == 'No')?'active':''?>">
                                            <input type="radio" name="criteria" id="No" value="No"><?=strtoupper(display('No'))?>
                                        </label>
                                      
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12 text-left">
                                   <div class="row button-group">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-success" name="quote_filter" value="filter"><i class="fas fa-filter m-r-5"></i><?=display('Filter')?></button>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-danger" name="remove_filter" value="remove"><i class="fas fa-ban m-r-5"></i><?=display('Cancel')?></button>
                                        </div>
                                    </div>     
                                </div>  
                            </div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <?php if(!empty($quotation)): ?>
            <div class="table-responsive">
                <table id="product" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th><?=display('Quotation Number')?></th>
                            <th><?=display('Request From')?></th>
                            <th><?=display('Amount')?></th>
                            <th><?=display('Total Items')?></th>
                            <th><?=display('Accepted')?></th>
                            <th><?=display('Paid')?></th>
                            <th><?=display('Validate')?></th>
                            <th><?=display('Action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($quotation as $p): ?>
                            <?php $user = get_user_details_by_id($p->request_from,3); ?>
                            <tr>
                                <td><?=$p->q_no?></td>
                                <td><?=$user->firstname.' '.$user->lastname;?></td>
                                <td><?=$p->amount?></td>
                                <td><?=$p->total_items?></td>
                                <td><?=($p->is_accepted == 'Yes')?'<span class="label label-success font-weight-100">'.display('Yes').'</span>':'<span class="label label-primary font-weight-100">'.display('No').'</span>'?> </td>
                                <td><?=($p->is_paid == 'Yes')?'<span class="label label-success font-weight-100">'.display('Yes').'</span>':'<span class="label label-primary font-weight-100">'.display('No').'</span>'?> </td>
                                <td><?=($p->is_validated == 'Yes')?'<span class="label label-success font-weight-100">'.display('Yes').'</span>':'<span class="label label-primary font-weight-100">'.display('No').'</span>'?> </td>
                                <td>
                                    <a href="<?=base_url('admin/quotation/view/'.$p->id.'')?>" class="text-dark delete m-r-10" title="<?=display('View')?>" data-toggle="tooltip"  data-id="<?=$p->id?>"><i class="fas fa-eye"></i></a>

                                    <a href="<?=base_url('admin/messages/'.$p->id.'')?>" class="text-dark" title="<?=display('Message')?>" data-toggle="tooltip"  data-id="<?=$p->id?>"><i class="fas fa-paper-plane"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="alert-empty-table-wrapper">
                            <div class="alert-empty-table-icon"><i class="fas fa-archive"></i></div>
                            <div class="alert-empty-info alert-empty-bot-info">
                                <span><?=display('There is no quotation to display')?></span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- ============================================================== -->

<script type="text/javascript">
    $('#product').DataTable();
    $(".select2").select2();
    $('.daterange').daterangepicker();
    jQuery('.mydatepicker, #datepicker').datepicker();
</script>