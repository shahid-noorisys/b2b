<link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />
<style type="text/css">
input[type=radio], input[type=checkbox] {
    box-sizing: border-box;
    padding: 0;
    position: absolute;
    opacity: 0;
}
.btn-group>.btn {
    position: relative;
    flex: 1 1 auto;
    display: inline-block;
    border: 1px solid;
    cursor: pointer;
}
</style>
<!-- <div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open('supplier/reviews','class="order-filter-form"'); ?>
                            <div class="row">
                                <div class="input-group col-md-4 mb-3">
                                  <?=form_dropdown('user_id', $users_list, (isset($_SESSION['order']['user_id']) && $_SESSION['order']['user_id']!="")?$_SESSION['order']['user_id']:'' ,' class="select2 form-control" ');?>
                                </div>
                                <div class="col-md-8 pull-left">
                                    <label for="status"><?=display('Rating')?> : &nbsp;</label>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn waves-effect waves-light btn-outline-warning <?=(isset($_SESSION['order']['rating']) && $_SESSION['order']['rating'] == 1)?'active':''?>">
                                            <input type="radio" name="rating" id="pending" value="pending">1 <?=strtoupper(display('Star'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-warning <?=(isset($_SESSION['order']['rating']) && $_SESSION['order']['rating'] == 2)?'active':''?>">
                                            <input type="radio" name="rating" id="in_progress" value="in_progress">2 <?=strtoupper(display('Star'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-warning <?=(isset($_SESSION['order']['rating']) && $_SESSION['order']['rating'] == 3)?'active':''?>">
                                            <input type="radio" name="rating" id="delivered" value="delivered">3 <?=strtoupper(display('Star'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-warning <?=(isset($_SESSION['order']['rating']) && $_SESSION['order']['rating'] == 3)?'active':''?>">
                                            <input type="radio" name="rating" id="delivered" value="delivered">4 <?=strtoupper(display('Star'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-warning <?=(isset($_SESSION['order']['rating']) && $_SESSION['order']['rating'] == 3)?'active':''?>">
                                            <input type="radio" name="rating" id="delivered" value="delivered">5 <?=strtoupper(display('Star'))?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12 text-left">
                                   <div class="row button-group">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-success" name="review_filter" value="filter"><i class="fas fa-filter m-r-5"></i><?=display('Filter')?></button>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-danger" name="remove_filter" value="remove"><i class="fas fa-ban m-r-5"></i><?=display('Cancel')?></button>
                                        </div>
                                    </div>     
                                </div>  
                            </div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div id="ajax_reviews_list"></div>
<!-- ============================================================== -->
<script src="<?php echo base_url('public/assets/admin/dist/js/select2.full.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>