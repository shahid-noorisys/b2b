<style type="text/css">
.counter {
    color: #000000;
    font-size: 11px;
}
.topbar .top-navbar .navbar-nav>.nav-item>.nav-link {
    padding-left: 15px;
    padding-right: 15px;
    font-size: 18px;
    line-height: 27px !important;
}
.notify {
    top: -38px !important;
    right: -10px !important;
}
.badge.badge-pill {
    padding: 0.1em 0.5em !important;
}
</style>

<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="<?=base_url('admin/dashboard')?>">
                <!-- Logo icon --><b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="<?=base_url('public/assets/admin/images/logo-icon.png')?>" alt="homepage" class="dark-logo" />
                    <!-- Light Logo icon -->
                    <img src="<?=base_url('public/assets/admin/images/logo-light-icon.png')?>" alt="homepage" class="light-logo" />
                </b>
                <!--End Logo icon -->
                <!-- Logo text --><span>
                 <!-- dark Logo text -->
                 <img src="<?php echo base_url('public/assets/admin/images/logo-text.png')?>" alt="homepage" class="dark-logo" />
                 <!-- Light Logo text -->    
                 <img src="<?php echo base_url('public/assets/admin/images/logo-light-text.png')?>" class="light-logo" alt="homepage" /></span> </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                <!-- ============================================================== -->
                <!-- Search -->
                <!-- ============================================================== -->
                
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
                <?php 
                   $admin_id = session()->get('user_details')->admin_id;
                   $admin_role = session()->get('user_details')->user_role;
                   $notifications = get_unread_notification_by_user_id($admin_id,$admin_role);
                   $count = (!empty($notifications) && isset($notifications))?count($notifications):0;
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="top: 5px;"> 
                        <i class="ti-bell">
                        <?php  if(!empty($notifications)):?>
                            <div class="notify"> 
                                <span class="badge badge-pill badge-warning ml-auto counter"><?=$count?></span> 
                             </div>
                        <?php endif;?>
                        </i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                        <ul>
                            <li>
                                <div class="drop-title">You have <?=$count?> new Notifications</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <?php if(!empty($notifications)):?>
                                        <?php if(isset($notifications)):?>
                                            <?php foreach ($notifications as $key => $value):?>
                                            <a href="<?=base_url('notifications')?>">
                                                <div class="user-img"> <img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user" class="img-circle"> 
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5><?=$value->title?></h5> <span class="mail-desc"><?=$value->description?></span> <span class="time"><?= date('d M Y',strtotime($value->created_date)) ?> | <?= date('H:i A',strtotime($value->created_date)) ?></span> </div>
                                            </a>
                                        <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endif;?>
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center link" href="<?=base_url('notifications')?>"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown d-none">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="ti-email"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </a>
                    <div class="dropdown-menu mailbox dropdown-menu-right animated bounceInDown" aria-labelledby="2">
                        <ul>
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <!-- Message -->
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=base_url('public/assets/admin/images/users/1.jpg')?>" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=base_url('public/assets/admin/images/users/2.jpg')?>" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=base_url('public/assets/admin/images/users/3.jpg')?>" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>
                                    <!-- Message -->
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=base_url('public/assets/admin/images/users/4.jpg')?>" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center link" href="javascript:void(0);"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <!-- ============================================================== -->
                <!-- End Messages -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- mega menu -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="<?=base_url('admin/logout')?>" id="2" style="top: 5px;"> <i class="fa fa-power-off"></i>
                        
                    </a>
                </li>
                <?php 
                  $text = '<i class="flag-icon flag-icon-us">';

                  if(isset($_SESSION['lang']))
                  {
                  if($_SESSION['lang']=='fr'){ 
                    $text='<i class="flag-icon flag-icon-fr">';} 
                  else {  
                    $text = '<i class="flag-icon flag-icon-us">'; }
                  } 
                  else 
                  { 
                    $_SESSION['lang'] = "en"; 
                    $text = '<i class="flag-icon flag-icon-us">';
                  }
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='en')): ?>
                            <i class="flag-icon flag-icon-fr"></i>
                        <?php endif; ?>
                        <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='fr')): ?>
                            <i class="flag-icon flag-icon-us"></i>
                        <?php endif; ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right  animated bounceInDown" aria-labelledby="navbarDropdown2">
                    <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='en')): ?>
                        <a class="dropdown-item btn-lang" href="#" data-id='en'>
                            <i class="flag-icon flag-icon-us"></i> <?=display('English')?> 
                        </a>
                        <a class="dropdown-item btn-lang" href="#" data-id='fr'>
                            <i class="flag-icon flag-icon-fr"></i> <?=display('French')?>
                        </a>
                    <?php endif; ?>
                    <?php if(isset($_SESSION['lang'])&&($_SESSION['lang']!='fr')): ?>
                        <a class="dropdown-item btn-lang" href="#" data-id='en'>
                            <i class="flag-icon flag-icon-us"></i> <?=display('English')?> 
                        </a>
                        <a class="dropdown-item btn-lang" href="#" data-id='fr'>
                            <i class="flag-icon flag-icon-fr"></i> <?=display('French')?>
                        </a>
                    <?php endif; ?>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ==============================================================