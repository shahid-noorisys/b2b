
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<?php 
    $AdminModel = model('App\Models\AdminModel');
    $adminDetails = $AdminModel->find(session()->get('user_details')->admin_id);
?>
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div><img src="<?=base_url($adminDetails->thumbnail)?>" alt="user-img" class="img-circle"></div>
                <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$adminDetails->firstname.' '.$adminDetails->lastname?><span class="caret"></span></a>
                    <div class="dropdown-menu animated flipInY">
                        <a href="<?=base_url('admin/profile')?>" class="dropdown-item"><i class="ti-user"></i> <?=display('My Profile')?></a>
                        <a href="<?=base_url('change-password')?>" class="dropdown-item"><i class="ti-settings"></i> <?=display('Change Password')?></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- <li>
                    <div class="hide-menu text-center">
                        <div id="eco-spark"></div>
                        <small>TOTAL EARNINGS - JUNE 2019</small>
                        <h4>$2,478.00</h4>
                    </div>
                </li> -->
                <li class="nav-small-cap">---</li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/dashboard')?>" aria-expanded="false"><i class="icon-speedometer"></i><span class="hide-menu"><?=display('Dashboard')?></span></a></li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu"><?=display('Masters')?></span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?=base_url('admin/categories');?>">Categories </a></li>
                        <li><a href="<?=base_url('admin/sub_categories');?>">Sub-Categories</a></li>
                    </ul>
                </li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-user"></i><span class="hide-menu">Manage Users</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?=base_url('admin/suppliers');?>">Supplier </a></li>
                        <li><a href="<?=base_url('admin/buyers');?>">Buyer</a></li>
                    </ul>
                </li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/products')?>" aria-expanded="false"><i class="ti-layout-grid2"></i><span class="hide-menu"><?=display('Products')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/messages')?>" aria-expanded="false"><i class="icon-bubbles"></i><span class="hide-menu"><?=display('Messages')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/quotations')?>" aria-expanded="false"><i class="icon-tag"></i><span class="hide-menu"><?=display('Quotations')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/orders')?>" aria-expanded="false"><i class="icon-basket"></i><span class="hide-menu"><?=display('Order History')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/payment-history')?>" aria-expanded="false"><i class="icon-wallet"></i><span class="hide-menu"><?=display('Payment History')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/commision')?>" aria-expanded="false"><i class="icon-layers"></i><span class="hide-menu"><?=display('Platform Fee')?></span></a></li>
                <li> <a class="waves-effect waves-dark" href="<?=base_url('admin/reviews')?>" aria-expanded="false"><i class="icon-bubbles"></i><span class="hide-menu"><?=display('Reviews & Rating')?></span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ==============================================================