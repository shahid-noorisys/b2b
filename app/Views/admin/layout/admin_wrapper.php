<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('public/assets/img/logo/favicon.png')?>">
    <title>YAHODEHIME</title>
    <!-- chartist CSS -->
    <link href="<?php echo base_url('public/assets/admin/dist/css/morris.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/ecommerce.css')?>" rel="stylesheet">
    <!-- Custom CSS -->

    

    <link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url('public/assets/admin/icons/font-awesome/css/fontawesome.min.css')?>" rel="stylesheet">


    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/icon-page.css')?>" rel="stylesheet">
    
    <link href="<?php echo base_url('public/assets/admin/dist/css/sweetalert2.min.css')?>" rel="stylesheet">

    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/chat-app-page.css')?>" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/admin/dist/css/dataTables.bootstrap4.css')?>">
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/admin/dist/css/responsive.dataTables.min.css')?>">

    <link href="<?php echo base_url('public/assets/admin/dist/css/style.min.css')?>" rel="stylesheet">
    <!-- CSS -->
    <link href="<?php echo base_url('public/assets/admin/dist/css/Custom.css')?>" rel="stylesheet">

    <link href="<?php echo base_url('public/assets/admin/dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/bootstrap-timepicker.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/daterangepicker.css')?>" rel="stylesheet">
    <!-- JS -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/sweetalert2.all.min.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery-3.2.1.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/select2.full.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery.dataTables.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/dataTables.responsive.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/ecom-dashboard.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/prism.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/moment.js')?>">"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/daterangepicker.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/bootstrap-datepicker.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/bootstrap-timepicker.min.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/raphael-min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/morris.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/morris-data.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/echarts-all.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/echarts-init.js')?>"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">YAHODEHIME</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">

    <!-- ============================================================== -->
    <?php echo view('admin/layout/admin_header'); ?>
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <?php echo view('admin/layout/admin_sidebar'); ?>
    <!-- ============================================================== -->

        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <?php if($title != "no_breadcrum"){ ?>
                    <div class="row page-titles">
                        <div class="col-md-5 align-self-center">
                            <h4 class="text-themecolor"><?=$title?></h4>
                        </div>
                        <div class="col-md-7 align-self-center text-right">
                            <div class="d-flex justify-content-end align-items-center">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=$path?>"><?=$controller_name?></a></li>
                                    <li class="breadcrumb-item active"><?=$title?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <?php if(session()->getFlashData('message') != null): ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin: 20px 20px 20px 20px;">
                        <strong><?=display('Success')?>!</strong> <?php $errors = session()->getFlashData('message');
                        if(is_array($errors)){
                            foreach ($errors as $value) {
                                echo $value.'<br>';
                            }
                        } else { echo $errors; }
                        ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif; ?>
                <?php if(session()->getFlashData('exception') != null): ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin: 20px 20px 20px 20px;">
                        <strong><?=display('Sorry')?>!</strong> <?php $errors = session()->getFlashData('exception');
                        if(is_array($errors)){
                            foreach ($errors as $value) {
                                echo $value.'<br>';
                            }
                        } else { echo $errors; }
                        ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif; ?>
                <!-- ============================================================== -->
                <?php echo ($content)?$content:''; ?>
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
         <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <?php echo view('admin/layout/admin_footer'); ?>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/pages/chat.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/popper.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/bootstrap.min.js')?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/perfect-scrollbar.jquery.min.js')?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/waves.js')?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/sidebarmenu.js')?>"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/sticky-kit.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery.sparkline.min.js')?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/custom.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery.sparkline.min.js')?>"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->

    <!--Custom JavaScript -->
    <script type="text/javascript">


        $(function () {
            // For select 2
            $(".select2").select2();
            ajaxadminReviewsList(page_url = false);
            $(document).on('click','.reviews_list .pagination li a', function(e){
                e.preventDefault();
                var page_url = $(this).attr('href');
                // alert(page_url);
                ajaxadminReviewsList(page_url);
            });

            function ajaxadminReviewsList(page_url)
            {
                var base_url = "<?=base_url('admin/reviews-ajax')?>";
                if(page_url){
                    base_url = page_url;
                }
                // alert(page_url);
                $.ajax({
                    type: "post",
                    url: base_url,
                    headers: {'X-Requested-With': 'XMLHttpRequest'},
                    data: {'<?=csrf_token()?>': '<?=csrf_hash()?>'},
                    dataType: "json",
                    success: function (response) {
                        $("#ajax_reviews_list").html(response.html);
                    }
                });
            }
        });

        // ----------  Detect form change plugin code ----------------//
        $.fn.extend({
            trackChanges: function() {
                $(":input",this).change(function() {
                    $(this.form).data("changed", true);
                });
            }
            ,
            isChanged: function() { 
            return this.data("changed"); 
            } 
        });

        $(function() {
            $(".btn-lang").click(function() { event.preventDefault();
                var lang = $(this).data('id');
                var token = "<?= csrf_hash() ?>";
                // alert(lang);
                $.ajax({
                          type:'POST',
                          url:'<?=base_url('language-toggle');?>', 
                          data:{lang: lang, csrf_stream_token:token},
              
                  }).done(function(response) 
                  {
                    // console.log(response);
                    window.location.reload();

                  }).fail(function() 
                  {
                      // alert(this.responseText);
                  });

            });
        });
        // ----------  Detect form change plugin code ----------------//
    </script>
</body>

</html>