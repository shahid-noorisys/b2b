<?php 
    foreach ($messages as $key => $value) { 
        if ($value->send_by !== 'Admin') { 
            $receiver = get_user_details_by_id($sender_id,$user_role);
?>
            <li>
                <div class="chat-img"><img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user" /></div>
                <div class="chat-content">
                    <h5><?=$receiver->firstname.' '.$receiver->lastname;?></h5>
                    <div class="box bg-light-info"><?=$value->text_msg?></div>
                    <div class="chat-time"><?= date('H:i A',strtotime($value->created_date));?>    |    <?= date('M d',strtotime($value->created_date));?></div>
                </div>
            </li>
<?php   } else { 
            $sender = get_user_details_by_id($admin_id,$admin_role);
?>
            <li class="reverse">
                <div class="chat-content">
                    <h5><?=$sender->firstname.' '.$sender->lastname;?></h5>
                    <div class="box bg-light-info"><?=$value->text_msg?></div>
                    <div class="chat-time"><?= date('H:i A',strtotime($value->created_date));?>    |    <?= date('M d',strtotime($value->created_date));?></div>
                </div>
                <div class="chat-img"><img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user" /></div>
            </li>
<?php   } 
    
    }

?>
