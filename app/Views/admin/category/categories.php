<style type="text/css">
.image-form-body input[type="file"]{
    display: none;
}
.image-form-body button{
    margin-top: 20px;
    width: 100%;
}
.img-thumbnail {
    height: 75px;
    width: 75px;
    margin-top: -9px;
}
</style>

<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                      <?php echo form_open_multipart('admin/category_add', 'class ="category-add"'); ?>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label"><?=display('Category Name')?></label>
                                            <input type="text" id="category_add" name="category_name" class="form-control" placeholder="<?=display('Enter Category Name Here')?>"> 
                                            <span class="error text-danger" id="error_category_add"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <label class="control-label"><?=display('Category icon')?> </label>
                                        <input type="file" class="form-control" class="icon_add" name="avatar">
                                        <small class="text-danger">icon should be 24*24 pixels /.png / and transparent</small>
                                        <span class="error text-danger" id="error_category_add"></span>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn waves-effect waves-light btn-outline-success category_add_btn"> <i class="fa fa-check"></i> <?=display('Save')?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                         <?php echo form_open_multipart('admin/category_edit', 'class ="category-edit"'); ?>
                            <input type="hidden" name="id" id="category_id" value="">
                            <input type="hidden" name="old_icon_url" id="old_icon_url" value="">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label"><?=display('Category Name')?></label>
                                            <input type="text" id="category_upd" name="category_name" class="form-control"> 
                                            <span class="error text-danger" id="error_category_upd"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label"><?=display('Category icon')?> </label>
                                            <input type="file" class="form-control" class="icon_upd" name="avatar" onchange="document.getElementById('icon_preview').src = window.URL.createObjectURL(this.files[0])">
                                            <small class="text-danger">icon should be 24*24 pixels /.png / and transparent</small>
                                            <span class="error text-danger" id="error_category_upd"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12">
                                         <img src="" alt="..." id="icon_preview" class="img-thumbnail">
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn waves-effect waves-light btn-outline-info category_upd_btn"> <i class="fas fa-pencil-alt"></i> <?=display('Update')?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="table-responsive m-t-40">
            <table id="categories" class="table table-bordered table-striped">
                 <thead>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Category Name</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($categories)) { ?>
                        <?php foreach ($categories as $key => $value) { ?>
                            <tr>
                                <td><?=$key+1?></td>
                                <td><?=$value->name?></td>
                                <td><?=($value->status == 'Active')?'Active':'Inactive'?></td>
                                <td>
                                    <a href="javascript:void(0)" class="text-dark p-r-10 edit" data-toggle="tooltip" title="<?=display('Edit')?>" data-id="<?=$value->id?>"><i class="fas fa-pencil-alt"> </i></a> 
                                    <?php if($value->status == 'Active'): ?>
                                        <a href="javascript:void(0)" class="text-danger btn-deactive m-l-5" title="Inactivate" data-toggle="tooltip" id="<?=$value->id?>"><i class="fas fa-times"></i></a>
                                    <?php else: ?>
                                        <a href="javascript:void(0)" class="text-success btn-active m-l-5" title="Activate" data-toggle="tooltip" id="<?=$value->id?>"><i class="fas fas fa-check"></i></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<script type="text/javascript">

    $(function () {
        $(".select2").select2();
        $('#categories').DataTable();
    });

    $(function () {
        $(".category-edit").trackChanges();

        $('.category-edit').hide();
        $(document).on('click','#categories .edit', function(e){
            e.preventDefault();
            $(window).scrollTop(0);
            $('.category-edit').show();
            $('.category-add').hide();
            var token = "<?= csrf_hash() ?>";
            var name = "<?= csrf_token() ?>";
            var category_id = $(this).data('id');
            $.ajax({
                 url: "<?php echo base_url('admin/get_category'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {category_id: category_id, csrf_stream_token:token},
              })
              .done(function(data) {
                // console.log(data);
                $('#category_id').val(data.category.id);
                $('#category_upd').val(data.category.name);
                $('#old_icon_url').val(data.category.icon_url);
                if (data.category.icon_url !== null) {
                  $(".img-thumbnail").attr('src',"<?=base_url()?>/"+data.category.icon_url);
                } else {
                  $(".img-thumbnail").attr('src',"<?=base_url('public/assets/img/no-image.jpg')?>");
                }
              });
        });

        $(document).on('click','.delete', function(e){
            e.preventDefault();
            var token = "<?= csrf_hash() ?>";
            var id = $(this).data('id');
            
            const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'mr-2 btn btn-danger'
              },
              buttonsStyling: false,
            })
            swalWithBootstrapButtons.fire({
              title: "<?= display('Are you sure'); ?>",
              text:"<?= display('This category will be deleted'); ?>",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: "<?= display('Yes delete it'); ?>",
              cancelButtonText: "<?= display('No cancel it'); ?>",
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                $.ajax({
                 url: "<?php echo base_url('admin/delete_category'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {category_id: id, csrf_stream_token:token},
                }).done(function(data) {
                    if (data.status == 'success') {
                        swalWithBootstrapButtons.fire(
                        "<?= display('Success'); ?>",
                        data.message,
                        'success'
                       )
                       setTimeout(function() { window.location.reload(); }, 2000);
                    } else {
                        swalWithBootstrapButtons.fire(
                        "<?= display('Sorry'); ?>",
                        data.message,
                        'error'
                       )
                       setTimeout(function() { window.location.reload(); }, 2000);
                    }
                   
                });
              } 
            })    
        });

        $(".category_add_btn").on('click', function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#category_add").val()) == '')
            {
              $("#error_category_add").html("<?php echo display('Please Enter Category Name'); ?>");
              valid = false;
            }
            if(valid){
              $(".category-add").get(0).submit();  
            }
        });

        $(".category-edit").submit(function (e){
            e.preventDefault();
        });

        $(".category_upd_btn").on('click', function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#category_upd").val()) == '')
            {
              $("#error_category_upd").html("<?php echo display('Please Enter Category Name'); ?>");
              valid = false;
            }

            if(valid){
                if ($(".category-edit").isChanged()) {
                    $(".category-edit").get(0).submit();
                } else {
                    Swal.fire({
                        icon: 'error',
                        text: "<?=display('Nothing is changed')?>",
                        showConfirmButton: false,
                        timer: 1000
                    })
                } 
            }
        });
    });

    $(document).on("click",".btn-active", function () {
        var id = this.id;
        var token = "<?= csrf_hash() ?>";
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        })
        swalWithBootstrapButtons.fire({
            title: "<?= display('Are you sure'); ?>",
            text:"<?= display('You want to activate this category'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "<?= display('Yes activate it'); ?>",
            cancelButtonText: "<?= display('No cancel it'); ?>",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                 url: "<?php echo base_url('admin/category_activate'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {category_id: id, csrf_stream_token:token},
                }).done(function(data) {
                    if (data.status == 'success') {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Success'); ?>",
                            text:data.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                        });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    } else {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Sorry'); ?>",
                            text:data.message,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    }
                   
                });
            }
        })
    });

    $(document).on('click', ".btn-deactive",function () {
        var id = this.id;
        var token = "<?= csrf_hash() ?>";
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        })
        swalWithBootstrapButtons.fire({
            title: "<?= display('Are you sure'); ?>",
            text:"<?= display('You want to inactivate this category'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "<?= display('Yes inactivate it'); ?>",
            cancelButtonText: "<?= display('No cancel it'); ?>",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                 url: "<?php echo base_url('admin/category_deactivate'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {category_id: id, csrf_stream_token:token},
                }).done(function(data) {
                    if (data.status == 'success') {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Success'); ?>",
                            text:data.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    } else {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Sorry'); ?>",
                            text:data.message,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    }
                   
                });
            }
        })
    });
</script>