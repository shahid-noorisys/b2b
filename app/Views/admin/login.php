<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('public/assets/img/logo/favicon.png')?>">
    <title>YAHODEHIME</title>
    
    <!-- page css -->
    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/login-register-lock.css')?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('public/assets/admin/dist/css/style.min.css')?>" rel="stylesheet">
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">YAHODEHIME</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->

    <section id="wrapper">
        <div class="login-register" style="background-image:url(<?php echo base_url('public/assets/admin/images/background/login-register.jpg');?>)">
            <div class="login-box card">
                <div class="card-body">
                    <?php if(session()->getFlashData('message') != null): ?>
                        <p class="text-success"><?=session()->getFlashData('message')?></p>
                    <?php endif; ?>
                    <?php if(session()->getFlashData('exception') != null): ?>
                        <p class="text-danger"><?=session()->getFlashData('exception')?></p>
                    <?php endif; ?>
                    <?php echo form_open('admin/login', 'class ="form-horizontal form-material loginform" autocomplete="off"'); ?>
                        <h3 class="text-center m-b-20"><?=display('Sign In')?></h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="<?=display('Email')?>" name="email" id="email"  autocomplete="off"> 
                                <span class="error text-danger" id="error_email"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="<?=display('Password')?>" name="password" id="password"  autocomplete="off">
                                <span class="error text-danger" id="error_password"></span> 
                            </div>
                        </div>
                      
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button type="button" class="btn btn-lg btn-block btn-outline-primary login"><?=display('Log In')?></button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
    
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery-3.2.1.min.js')?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/popper.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/bootstrap.min.js')?>"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });

        $(".login").on('click', function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#email").val()) == '')
            {
              $("#error_email").html("<?php echo display('Please Enter Your Email'); ?>");
              valid = false;
            }
            if($.trim($("#password").val()) == '')
            {
              $("#error_password").html("<?php echo display('Please Enter Your Password'); ?>");
              valid = false;
            }
            if(valid){
              $(".loginform").get(0).submit();  
            }
        });
    </script>
    
</body>

</html>