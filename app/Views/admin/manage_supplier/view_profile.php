<link href="<?=base_url('public/assets/admin/dist/css/user-card.css')?>" rel="stylesheet">
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> 
                    <img src="<?=(!empty($profile->thumbnail))?base_url($profile->thumbnail):base_url('public/assets/admin/images/nophoto_user_icon.png')?>" class="img-circle" width="150" />
                    <h4 class="card-title m-t-10"><?=$profile->firstname.' '.$profile->lastname?></h4>
                    
                    <div class="row text-center justify-content-md-center">
                        <div class="col-6">
                            <i class="icon-people"></i> <font class="font-medium">Seller</font>
                        </div>
                        <div class="col-6">
                                <i class="icon-tag"></i> 
                                <?php if($profile->status == 'Active'): ?>
                                    <font class="font-medium text-success">Active</font>
                                <?php else: ?>
                                    <font class="font-medium text-danger">Inactive</font>
                                <?php endif; ?>
                        </div>
                    </div>
                </center>
            </div>
            <div class="button-group text-right m-r-10">
                <?php if($profile->status == 'Active'): ?>
                    <a href="javascript:void(0)" class="btn waves-effect waves-light btn-outline-danger btn-deactive" title="Inactivate" data-toggle="tooltip" id="<?=$profile->id?>">Inactive</a>
                <?php else: ?>
                    <a href="javascript:void(0)" class="btn waves-effect waves-light btn-outline-success btn-active" title="Activate" data-toggle="tooltip" id="<?=$profile->id?>">Active</a>
                <?php endif; ?>
            </div>
            <div>
                <hr> 
            </div>
            <div class="card-body"> <small class="text-muted">Email address </small>
                <h6><?=$profile->email?></h6> 
                <small class="text-muted p-t-30 db">Phone</small>
                <h6><?=$profile->mobile?></h6> 
                <small class="text-muted p-t-30 db">Gender</small>
                <h6><?=($profile->gender == 'Male')?'Male':'Female'?></h6>
                <small class="text-muted p-t-30 db">Company Name</small>
                <h6><?=$company->company_name?></h6>
                <small class="text-muted p-t-30 db">Company Email</small>
                <h6><?=$company->company_email?></h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
           
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile Details</a> </li>
                 <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#password" role="tab">Change Password</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#notification" role="tab">Notification</a> </li>
            </ul>
     
            <div class="tab-content">
                <!-- <div class="tab-pane active" id="home" role="tabpanel">
                    <div class="card-body">
                        <div class="profiletimeline">
                            <div class="sl-item">
                                <div class="sl-left"> <img src="<?=base_url('public/assets/admin/images/users/3.jpg')?>" alt="user" class="img-circle" /> </div>
                                <div class="sl-right">
                                    <div><a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                        <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                    </div>
                                    <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                </div>
                            </div>
                            <hr>
                            <div class="sl-item">
                                <div class="sl-left"> <img src="<?=base_url('public/assets/admin/images/users/4.jpg')?>" alt="user" class="img-circle" /> </div>
                                <div class="sl-right">
                                    <div><a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                        <blockquote class="m-t-10">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="tab-pane active" id="profile" role="tabpanel">
                   <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                    <a href="<?=base_url($profile->manger_document)?>" target="_blank">
                                        <img class="card-img-top img-responsive" src="<?=base_url('public/assets/admin/images/files.png')?>" alt="Card image cap">
                                        <p class="card-text m-t-10 text-center">Manager Document</p>
                                    </a>
                            </div>  
                            <div class="col-lg-9 col-md-9">
                                <div class="card">
                                    <div class="card-header text-center">
                                        Personal Information
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Name  : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$profile->firstname.' '.$profile->lastname?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Email  : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$profile->email?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Gender : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$profile->gender?></h6> 
                                            </div>
                                        </div>
                                        <?php if ($profile->designation == "Other"):?>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span class="text-muted">Manager Name : </span>
                                                </div>
                                                <div class="col-md-6 text-left">
                                                    <h6 class="font-bold"><?=$profile->manager_firstName.' '.$profile->manager_lastName?></h6> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <span class="text-muted">Manager Gender : </span>
                                                </div>
                                                <div class="col-md-6 text-left">
                                                    <h6 class="font-bold"><?=$profile->manger_gender?></h6> 
                                                </div>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                    <a href="<?=base_url($company->thumbnail)?>" target="_blank">
                                        <img class="card-img-top img-responsive" src="<?=base_url('public/assets/admin/images/files.png')?>" alt="Card image cap">
                                        <p class="card-text m-t-10 text-center">Company Document</p>
                                    </a>
                            </div>  
                            <div class="col-lg-9 col-md-9">
                                <div class="card">
                                    <div class="card-header text-center">
                                        Company Information
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Company Name: </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$company->company_name?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Company Email  : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$company->company_email?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Mobile : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$company->contact?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Country : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$company->country?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">City : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$company->city?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Address : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$company->address?></h6> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                    <a href="<?=base_url($bank->document)?>" target="_blank">
                                        <img class="card-img-top img-responsive" src="<?=base_url('public/assets/admin/images/files.png')?>" alt="Card image cap">
                                        <p class="card-text m-t-10 text-center">Bank Document</p>
                                    </a>
                            </div>  
                            <div class="col-lg-9 col-md-9">
                                <div class="card">
                                    <div class="card-header text-center">
                                        Bank Information
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Name  : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$bank->bank_name?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">Address : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$bank->address?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">city : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$bank->city?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">IBAN : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$bank->IBAN?></h6> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <span class="text-muted">BIC : </span>
                                            </div>
                                            <div class="col-md-6 text-left">
                                                <h6 class="font-bold"><?=$bank->BIC?></h6> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                   </div>
                </div>
                <div class="tab-pane" id="password" role="tabpanel">
                   <div class="card-body">
                        <?php echo form_open('admin/supplier_credential', ' class="form-horizontal form-material sup_change_pswd_form" autocomplete="off" '); ?>
                            <input type="hidden" name="id" value="<?=$profile->id?>">
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">New Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line" name="new_pswd" id="new_pswd">
                                    <span class="error text-danger" id="error_new_pswd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Confirm Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line" name="confrim_pswd" id="confrim_pswd">
                                    <span class="error text-danger" id="error_confrim_pswd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="button-group">
                                    <button type="submit" class="btn waves-effect waves-light btn-outline-primary upd_password">Update Credentials</button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                   </div>
                </div>
                <div class="tab-pane" id="notification" role="tabpanel">
                   <div class="card-body">
                        <?php echo form_open('admin/seller-notify', ' class="form-horizontal sup_notification_form" autocomplete="off" '); ?>
                            <input type="hidden" name="user_id" value="<?=$profile->id?>">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="title" id="title" placeholder="Title">
                                    <span class="error text-danger" id="error_title"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="10" name="description" id="description" placeholder="Description"></textarea>
                                    <span class="error text-danger" id="error_description"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="button-group">
                                    <button type="submit" class="btn waves-effect waves-light btn-outline-success btn-notify"><i class="fas fa-paper-plane m-r-15"></i>Send Notification</button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<script src="<?=base_url('public/assets/admin/dist/js/jquery.magnific-popup.min.js')?>"></script>
<script src="<?=base_url('public/assets/admin/dist/js/jquery.magnific-popup-init.js')?>"></script>
<script type="text/javascript">

    $(".upd_password").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#confrim_pswd").val()) == '')
        {
          $("#error_confrim_pswd").html("<?php echo display('Please Enter Confirm Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) == '')
        {
          $("#error_new_pswd").html("<?php echo display('Please Enter New Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) !== $.trim($("#confrim_pswd").val()))
        {
          $("#error_confrim_pswd").html("<?php echo display('Password Not Match.....'); ?>");
          valid = false;
        }
        if(valid){
          $(".sup_change_pswd_form").get(0).submit();  
        }
    });

    $(".btn-notify").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#title").val()) == '')
        {
          $("#error_title").html("<?php echo display('Please Enter the title'); ?>");
          valid = false;
        }
        if($.trim($("#description").val()) == '')
        {
          $("#error_description").html("<?php echo display('Please Enter the description'); ?>");
          valid = false;
        }
       
        if(valid){
          $(".sup_notification_form").get(0).submit();  
        }
    });

    $(".btn-active").click(function () {
        var id = this.id;
        var token = "<?= csrf_hash() ?>";
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        })
        swalWithBootstrapButtons.fire({
            title: "<?= display('Are you sure'); ?>",
            text:"<?= display('User will be activated'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "<?= display('Yes activate it'); ?>",
            cancelButtonText: "<?= display('No cancel it'); ?>",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                 url: "<?php echo base_url('admin/supplier_activate'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {user_id: id, csrf_stream_token:token},
                }).done(function(data) {
                    if (data.status == 'success') {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Success'); ?>",
                            text:data.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                        });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    } else {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Sorry'); ?>",
                            text:data.message,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    }
                   
                });
            }
        })
    });

    $(".btn-deactive").click(function () {
        var id = this.id;
        var token = "<?= csrf_hash() ?>";
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        })
        swalWithBootstrapButtons.fire({
            title: "<?= display('Are you sure'); ?>",
            text:"<?= display('User will be inactivated'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "<?= display('Yes inactivate it'); ?>",
            cancelButtonText: "<?= display('No cancel it'); ?>",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                 url: "<?php echo base_url('admin/supplier_deactivate'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {user_id: id, csrf_stream_token:token},
                }).done(function(data) {
                    if (data.status == 'success') {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Success'); ?>",
                            text:data.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    } else {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Sorry'); ?>",
                            text:data.message,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    }
                   
                });
            }
        })
    });
</script>