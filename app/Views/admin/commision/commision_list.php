<!-- ============================================================== -->
<style type="text/css">
    .has-error{
        border: 1px solid #dd4814 !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                      <?php echo form_open_multipart('admin/commision_add', 'class ="add-commision-form"'); ?>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="input-group col-md-6 mb-3">
                                    <input type="text" class="form-control" placeholder="<?=display('Platform Commision Percentage')?>" aria-describedby="basic-addon1" name="add_commision_percent" id="add_commision_percent">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-percent"></i></span>
                                    </div>
                                </div>
                                <div class="input-group col-md-2 mb-3">
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn waves-effect waves-light btn-outline-success add-commision"> <i class="fa fa-check"></i> <?=display('Save')?></button>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        <?php echo form_close(); ?>
                        <?php echo form_open_multipart('admin/commision_edit', 'class ="update-commision-form"'); ?>
                            <div class="row">
                                <input type="hidden" name="commision_id" id="commision_id" value="">
                                <div class="col-md-2"></div>
                                <div class="input-group col-md-6 mb-3">
                                    <input type="text" class="form-control" placeholder="<?=display('Platform Commision Percentage')?>" aria-describedby="basic-addon1" name="upd_commision_percent" id="upd_commision_percent">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-percent"></i></span>
                                    </div>
                                </div>
                                <div class="input-group col-md-2 mb-3">
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn waves-effect waves-light btn-outline-info edit-commision"> <i class="fas fa-pencil-alt"></i> <?=display('Update')?></button>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="table-responsive m-t-40">
            <table id="commisions" class="table table-bordered table-striped">
                 <thead>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Platform Commision</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($commisions)) { ?>
                        <?php foreach ($commisions as $key => $value) { ?>
                            <tr>
                                <td><?=$key+1?></td>
                                <td><?=$value->commission_charge?></td>
                                <td><?=($value->status == 'Active')?'Active':'Inactive'?></td>
                                <td>
                                    <a href="javascript:void(0)" class="text-dark p-r-10 edit" data-toggle="tooltip" title="Edit" data-id="<?=$value->id?>"><i class="fas fa-pencil-alt"></i></a> 
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<script type="text/javascript">

    $(function () {
        $(".select2").select2();
        $('#commisions').DataTable();
    });

    $(function () {

        $('.update-commision-form').hide();
        // $('.add-commision-form').hide();
        

        $("#commisions").on('click','.edit', function(e){
            e.preventDefault();
            $('.update-commision-form').show();
            $('.add-commision-form').hide();
            var token = "<?= csrf_hash() ?>";
            var commision_id = $(this).data('id');
            $.ajax({
                 url: "<?php echo base_url('admin/get_commision_info'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {commision_id: commision_id, csrf_stream_token:token},
              })
              .done(function(data) {
                // console.log(data);
                $('#commision_id').val(data.commision.id);
                $('#upd_commision_percent').val(data.commision.commission_charge);
                
              });
        });

        $(document).on('click','.add-commision', function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#add_commision_percent").val()) == '')
            {
              $("#add_commision_percent").addClass('has-error'); 
              valid = false;
            }
            if(valid){
              $("#add_commision_percent").removeClass('has-error'); 
              $(".add-commision-form").get(0).submit();  
            }
        });

        $(document).on('click','.edit-commision', function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#upd_commision_percent").val()) == '')
            {
              $("#upd_commision_percent").addClass('has-error'); 
              valid = false;
            }
            if(valid){
              $("#upd_commision_percent").removeClass('has-error'); 
              $(".update-commision-form").get(0).submit();  
            }
        });

       
    });
</script>