<link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />
<style type="text/css">
input[type=radio], input[type=checkbox] {
    box-sizing: border-box;
    padding: 0;
    position: absolute;
    opacity: 0;
}
.btn-group>.btn {
    position: relative;
    flex: 1 1 auto;
    display: inline-block;
    border: 1px solid;
    cursor: pointer;
}
</style>
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open('admin/products','class="order-filter-form" autocomplete="off"'); ?>
                            <div class="row">
                                <div class="input-group col-md-4 mb-3">
                                   <?=form_dropdown('user_id', $seller_list, (isset($_SESSION['product']['user_id']) && $_SESSION['product']['user_id']!="")?$_SESSION['product']['user_id']:'' ,' class="select2 form-control" ');?>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                   <?=form_dropdown('category', $categories, (isset($_SESSION['product']['category']) && $_SESSION['product']['category']!="")?$_SESSION['product']['category']:'' ,' class="select2 form-control" ');?>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                 <?=form_dropdown('sub_category', $sub_categories, (isset($_SESSION['product']['sub_category']) && $_SESSION['product']['sub_category']!="")?$_SESSION['product']['sub_category']:'' ,' class="select2 form-control" ');?>
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select From Date')?>" name="start" value="<?=(isset($_SESSION['product']['start']) && $_SESSION['product']['start']!="")?date('m/d/Y',strtotime($_SESSION['product']['start'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select To Date')?>" name="end" value="<?=(isset($_SESSION['product']['end']) && $_SESSION['product']['end']!="")?date('m/d/Y',strtotime($_SESSION['product']['end'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 pull-left">
                                    <label for="status"><?=display('Publish')?> : &nbsp;</label>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['product']['publish']) && $_SESSION['product']['publish'] == 'Yes')?'active':''?>">
                                            <input type="radio" name="publish" id="Yes" value="Yes" <?=(isset($_SESSION['product']['publish']) && $_SESSION['product']['publish'] == 'Yes')?'checked':''?>><?=strtoupper(display('Publish'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['product']['publish']) && $_SESSION['product']['publish'] == 'No')?'active':''?>">
                                            <input type="radio" name="publish" id="No" value="No" <?=(isset($_SESSION['product']['publish']) && $_SESSION['product']['publish'] == 'No')?'checked':''?>><?=strtoupper(display('Not Publish'))?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-md-12 pull-left">
                                    <label for="status"><?=display('Status')?> : &nbsp;</label> 
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn waves-effect waves-light btn-outline-success <?=(isset($_SESSION['product']['status']) && $_SESSION['product']['status'] == 'Active')?'active':''?>">
                                            <input type="radio" name="status" id="active" value="Active" <?=(isset($_SESSION['product']['status']) && $_SESSION['product']['status'] == 'Active')?'checked':''?>><?=strtoupper(display('Active'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-success <?=(isset($_SESSION['product']['status']) && $_SESSION['product']['status'] == 'Inactive')?'active':''?>">
                                            <input type="radio" name="status" id="inactive" value="Inactive" <?=(isset($_SESSION['product']['status']) && $_SESSION['product']['status'] == 'Inactive')?'checked':''?>><?=strtoupper(display('Inactive'))?>
                                        </label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12 text-left">
                                   <div class="row button-group">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-success" name="product_filter" value="filter"><i class="fas fa-filter m-r-5"></i><?=display('Filter')?></button>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-danger" name="remove_filter" value="remove"><i class="fas fa-ban m-r-5"></i><?=display('Cancel')?></button>
                                        </div>
                                    </div>     
                                </div>  
                            </div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <?php if(!empty($products)): ?>
           <div class="table-responsive">
                <table id="product" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                            <th>#</th>
                            <th><?=display('Product Name')?></th>
                            <th><?=display('Image')?></th>
                            <!-- <th><?=display('Description')?></th> -->
                            <th><?=display('Category')?></th>
                            <th><?=display('Sub Category')?></th>
                            <th><?=display('Created By')?></th>
                            <th><?=display('Order Counter')?></th>
                            <th><?=display('Publish')?></th>
                            <th><?=display('Created Date')?></th>
                            <th><?=display('Status')?></th>
                            <th><?=display('Action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $key => $p): ?>
                        <?php $seller = get_user_details_by_id($p->user_id,2); ?>
                        <tr>
                            <td><?=$key+1?></td>
                            <td><?=$p->name?></td>
                            <td> <img src="<?=base_url($p->image_1)?>" alt="iMac" width="60" height="60"> </td>
                            <!-- <td><?=character_limiter($p->description,80)?></td> -->
                            <td><?=$p->category_name?></td>
                            <td><?=$p->sub_category_name?></td>
                            <td><?=$seller->firstname.' '.$seller->lastname;?></td>
                            <td><?=($p->order_counter)??'0'?></td>
                            <td><?=($p->is_published == 'Yes')?'Yes':'No'?></td>
                            <td><?=$p->created_date?></td>
                            <td> <span class="label label-success font-weight-100"><?=$p->status?></span> </td>
                            <td class="text-center"><a href="<?=base_url('admin/'.$p->id.'/product-edit')?>" class="text-dark p-r-10" data-toggle="tooltip" title="<?=display('Edit')?>"><i class="fas fa-pencil-alt"></i></a> </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="alert-empty-table-wrapper">
                            <div class="alert-empty-table-icon"><i class="fas fa-archive"></i></div>
                            <div class="alert-empty-info alert-empty-bot-info">
                                <span><?=display('There is no product to display')?></span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- ============================================================== -->

<script type="text/javascript">
    $('#product').DataTable();
    $(".select2").select2();
    $('.daterange').daterangepicker();
    jQuery('.mydatepicker, #datepicker').datepicker();
</script>