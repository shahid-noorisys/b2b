<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="offset-md-3 col-md-6">
				    <div class="card">
				        <div class="card-body">
				            <?php echo form_open('change-password', 'class ="pswd_upd_form"'); ?>
				                <div class="form-body">
				                    <div class="row">
				                        <div class="col-md-12">
				                            <div class="form-group">
				                                <input type="password" id="old_pswd" name="old_pswd" class="form-control" placeholder="<?=display('Old Password')?>"> 
				                                <span class="error text-danger" id="error_old_pswd"></span>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row">
				                        <div class="col-md-12">
				                            <div class="form-group">
				                                <input type="password" id="new_pswd" name="new_pswd" class="form-control" placeholder="<?=display('New Password')?>"> 
				                                <span class="error text-danger" id="error_new_pswd"></span>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row">
				                        <div class="col-md-12">
				                            <div class="form-group">
				                                <input type="password" id="confrim_pswd" name="confrim_pswd" class="form-control" placeholder="<?=display('Confirm Password')?>"> 
				                                <span class="error text-danger" id="error_confrim_pswd"></span>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row">
					                    <div class="col-md-12">
					                        <div class="form-actions text-right">
		                                        <button type="submit" class="btn waves-effect waves-light btn-outline-info pswd_upd"> <i class="fas fa-pencil-alt"></i> <?=display('Update')?></button>
		                                    </div>
					                    </div>
				                	</div>
				                </div>
				            <?php echo form_close(); ?>
				        </div>
				    </div>
				</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	    
	    $(".pswd_upd").on('click', function(e){
            e.preventDefault();
            var valid = true;
            $(".error").html('');
            if($.trim($("#confrim_pswd").val()) == '')
            {
              $("#error_confrim_pswd").html("<?php echo display('Please Enter Confirm Password'); ?>");
              valid = false;
            }
            if($.trim($("#new_pswd").val()) == '')
            {
              $("#error_new_pswd").html("<?php echo display('Please Enter New Password'); ?>");
              valid = false;
            }
            if($.trim($("#old_pswd").val()) == '')
            {
              $("#error_old_pswd").html("<?php echo display('Please Enter Old Password'); ?>");
              valid = false;
            }
            if($.trim($("#new_pswd").val()) !== $.trim($("#confrim_pswd").val()))
            {
              $("#error_confrim_pswd").html("<?php echo display('Password Not Match.....'); ?>");
              valid = false;
            }
            if(valid){
              $(".pswd_upd_form").get(0).submit();  
            }
        });

</script>
