<link href="<?php echo base_url('public/assets/admin/dist/css/select2.min.css')?>" rel="stylesheet" type="text/css" />
<style type="text/css">
input[type=radio], input[type=checkbox] {
    box-sizing: border-box;
    padding: 0;
    position: absolute;
    opacity: 0;
}
.btn-group>.btn {
    position: relative;
    flex: 1 1 auto;
    display: inline-block;
    border: 1px solid;
    cursor: pointer;
}
</style>
<!-- ============================================================== -->
<div class="row">
    <div class="col-md-12">
        <div class="card border-dark">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo form_open('admin/buyers','class="order-filter-form" autocomplete="off"'); ?>
                            <div class="row">
                                <div class="input-group col-md-4 mb-3">
                                    <?=form_dropdown('country_id',$countries, (isset($_SESSION['buyer']['country_id']) && $_SESSION['buyer']['country_id']!="")?$_SESSION['buyer']['country_id']:'' ,' class="select2 form-control" ');?>
                                 
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class=" far fa-lightbulb"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Search by state...')?>" aria-describedby="basic-addon1" name="state" value ="<?=(isset($_SESSION['buyer']['state']) && $_SESSION['buyer']['state']!="")?$_SESSION['buyer']['state']:''?>">
                                  
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class=" far fa-lightbulb"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="<?=display('Search by city...')?>" aria-describedby="basic-addon1" name="city" value ="<?=(isset($_SESSION['buyer']['city']) && $_SESSION['buyer']['city']!="")?$_SESSION['buyer']['city']:''?>">
                                </div>
                            </div>
                            <div class="row m-t-20">
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select From Date')?>" name="start" value="<?=(isset($_SESSION['buyer']['start']) && $_SESSION['buyer']['start']!="")?date('m/d/Y',strtotime($_SESSION['buyer']['start'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group col-md-4 mb-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="<?=display('Select To Date')?>" name="end" value="<?=(isset($_SESSION['buyer']['end']) && $_SESSION['buyer']['end']!="")?date('m/d/Y',strtotime($_SESSION['buyer']['end'])):'';?>" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 pull-left">
                                    <label for="status"><?=display('Gender')?> : &nbsp;</label>
                                    <div class="btn-group" data-toggle="buttons">
                                       <!--  <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['buyer']['gender']) && $_SESSION['buyer']['gender'] == 'All')?'active':''?>">
                                            <input type="radio" name="gender" id="All" value="All" <?=(isset($_SESSION['buyer']['gender']) && $_SESSION['buyer']['gender'] == 'All')?'checked':''?>><?=strtoupper(display('All'))?>
                                        </label> -->
                                        <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['buyer']['gender']) && $_SESSION['buyer']['gender'] == 'Male')?'active':''?>">
                                            <input type="radio" name="gender" id="Male" value="Male" <?=(isset($_SESSION['buyer']['gender']) && $_SESSION['buyer']['gender'] == 'Male')?'checked':''?>><?=strtoupper(display('Male'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-info <?=(isset($_SESSION['buyer']['gender']) && $_SESSION['buyer']['gender'] == 'Female')?'active':''?>">
                                            <input type="radio" name="gender" id="Female" value="Female" <?=(isset($_SESSION['buyer']['gender']) && $_SESSION['buyer']['gender'] == 'Female')?'checked':''?>><?=strtoupper(display('Female'))?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-md-12 pull-left">
                                    <label for="status"><?=display('Status')?> : &nbsp;</label> 
                                    <div class="btn-group" data-toggle="buttons">
                                        <!-- <label class="btn waves-effect waves-light btn-outline-success <?=(isset($_SESSION['buyer']['status']) && $_SESSION['buyer']['status'] == 'All')?'active':''?>">
                                            <input type="radio" name="status" id="all" value="All" <?=(isset($_SESSION['buyer']['status']) && $_SESSION['buyer']['status'] == 'All')?'checked':''?>><?=strtoupper(display('All'))?>
                                        </label> -->
                                        <label class="btn waves-effect waves-light btn-outline-success <?=(isset($_SESSION['buyer']['status']) && $_SESSION['buyer']['status'] == 'Active')?'active':''?>">
                                            <input type="radio" name="status" id="active" value="Active" <?=(isset($_SESSION['buyer']['status']) && $_SESSION['buyer']['status'] == 'Active')?'checked':''?>><?=strtoupper(display('Active'))?>
                                        </label>
                                        <label class="btn waves-effect waves-light btn-outline-success <?=(isset($_SESSION['buyer']['status']) && $_SESSION['buyer']['status'] == 'Inactive')?'active':''?>">
                                            <input type="radio" name="status" id="inactive" value="Inactive" <?=(isset($_SESSION['buyer']['status']) && $_SESSION['buyer']['status'] == 'Inactive')?'checked':''?>><?=strtoupper(display('Inactive'))?>
                                        </label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12 text-left">
                                   <div class="row button-group">
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-success" name="buyer_filter" value="filter"><i class="fas fa-filter m-r-5"></i><?=display('Filter')?></button>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <button type="submit" class="btn waves-effect waves-light btn-block btn-danger" name="remove_filter" value="remove"><i class="fas fa-ban m-r-5"></i><?=display('Cancel')?></button>
                                        </div>
                                    </div>     
                                </div>  
                            </div>
                        <?php echo form_close(); ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <?php if (!empty($buyers)): ?>
            <div class="table-responsive m-t-40">
                <table id="product" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                            <th>#</th>
                            <th><?=display('Name')?></th>
                            <th><?=display('Email')?></th>
                            <th><?=display('Mobile')?></th>
                            <th><?=display('Status')?></th>
                            <th><?=display('Action')?></th>
                        </tr>
                    </thead>
                    <tbody>
                            <?php foreach ($buyers as $key => $value): ?>
                                <tr>
                                    <td><?=$key+1?></td>
                                    <td><?=$value->firstname.' '.$value->lastname;?></td>
                                    <td><?=$value->email;?></td>
                                    <td><?=$value->mobile;?></td>
                                    <td> 
                                        <?php if($value->status == 'Active'): ?>
                                            <span class="label label-success font-weight-100">Active</span> 
                                        <?php else: ?>
                                            <span class="label label-danger font-weight-100">Inactive</span> 
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?=base_url()?>/admin/view_buyer/<?=$value->id?>" class="text-primary p-r-10 view" data-toggle="tooltip" title="view profile" data-id=""><i class="fas fas fa-eye"></i></a> 
                                        <?php if($value->status == 'Active'): ?>
                                           <a href="javascript:void(0)" class="text-danger btn-deactive" title="Deactivate" data-toggle="tooltip" id="<?=$value->id?>"><i class="fas  fas  fas fas fa-user-times"></i></a>
                                        <?php else: ?>
                                            <a href="javascript:void(0)" class="text-success btn-active" title="Activate" data-toggle="tooltip" id="<?=$value->id?>"><i class="fas  fas  fas fa-user-plus"></i></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="alert-empty-table-wrapper">
                            <div class="alert-empty-table-icon"><i class="fas fa-users"></i></div>
                            <div class="alert-empty-info alert-empty-bot-info">
                                <span><?=display('There is no user to display')?></span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- ============================================================== -->
<script src="<?php echo base_url('public/assets/admin/dist/js/select2.full.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $('#product').DataTable();
    $(".select2").select2();

     $('.daterange').daterangepicker();
    jQuery('.mydatepicker, #datepicker').datepicker();

    $(document).on('click',".btn-active", function () {
        var id = this.id;
        var token = "<?= csrf_hash() ?>";
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        })
        swalWithBootstrapButtons.fire({
            title: "<?= display('Are you sure'); ?>",
            text:"<?= display('User will be activated'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "<?= display('Yes activate it'); ?>",
            cancelButtonText: "<?= display('No cancel it'); ?>",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                 url: "<?php echo base_url('admin/buyer_activate'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {user_id: id, csrf_stream_token:token},
                }).done(function(data) {
                    if (data.status == 'success') {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Success'); ?>",
                            text:data.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                        });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    } else {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Sorry'); ?>",
                            text:data.message,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    }
                   
                });
            }
        })
    });

    $(document).on('click', ".btn-deactive", function () {
        var id = this.id;
        var token = "<?= csrf_hash() ?>";
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'mr-2 btn btn-danger'
            },
            buttonsStyling: false,
        })
        swalWithBootstrapButtons.fire({
            title: "<?= display('Are you sure'); ?>",
            text:"<?= display('User will be inactivated'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "<?= display('Yes inactivate it'); ?>",
            cancelButtonText: "<?= display('No cancel it'); ?>",
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                 url: "<?php echo base_url('admin/buyer_deactivate'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {user_id: id, csrf_stream_token:token},
                }).done(function(data) {
                    if (data.status == 'success') {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Success'); ?>",
                            text:data.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    } else {
                        swalWithBootstrapButtons.fire({
                            title: "<?= display('Sorry'); ?>",
                            text:data.message,
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: "<?= display('Okay'); ?>",
                            reverseButtons: true
                       });
                       setTimeout(function() { window.location.reload(); }, 2000);
                    }
                   
                });
            }
        })
    });
</script>