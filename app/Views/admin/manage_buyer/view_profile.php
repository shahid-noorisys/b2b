<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> <img src="<?=(!empty($profile->thumbnail))?base_url($profile->thumbnail):base_url('public/assets/admin/images/nophoto_user_icon.png')?>" class="img-circle" width="150" />
                    <h4 class="card-title m-t-10"><?=$profile->firstname.' '.$profile->lastname?></h4>
                    
                    <div class="row text-center justify-content-md-center">
                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">Buyer</font></a></div>
                        <div class="col-4">
                            <a href="javascript:void(0)" class="link">
                                <i class="icon-tag"></i> 
                                <?php if($profile->status == 'Active'): ?>
                                    <font class="font-medium text-success">Active</font>
                                <?php else: ?>
                                    <font class="font-medium text-danger">Inactive</font>
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>
                </center>
            </div>
            <div>
                <hr> 
            </div>
            <div class="card-body"> <small class="text-muted">Email address </small>
                <h6><?=$profile->email?></h6> <small class="text-muted p-t-30 db">Phone</small>
                <h6>+<?=$address->country_code.' '.$profile->mobile?></h6> 
                <small class="text-muted p-t-30 db">Address</small>
                <h6><?=$address->address?></h6>
               <!--  <small class="text-muted p-t-30 db">Company Name</small>
                <h6><?=$profile->company_name?></h6>
                <small class="text-muted p-t-30 db">Company Email</small>
                <h6><?=$profile->company_email?></h6> -->
                <small class="text-muted p-t-30 db">Gender</small>
                <h6><?=($profile->gender == 'Male')?'Male':'Female'?></h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <!-- <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Timeline</a> </li> -->
               <!--  <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li> -->
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Change Password</a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#notification" role="tab">Notification</a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- <div class="tab-pane active" id="home" role="tabpanel">
                    <div class="card-body">
                        <div class="profiletimeline">
                            <div class="sl-item">
                                <div class="sl-left"> <img src="<?=base_url('public/assets/admin/images/users/3.jpg')?>" alt="user" class="img-circle" /> </div>
                                <div class="sl-right">
                                    <div><a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                        <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                    </div>
                                    <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                </div>
                            </div>
                            <hr>
                            <div class="sl-item">
                                <div class="sl-left"> <img src="<?=base_url('public/assets/admin/images/users/4.jpg')?>" alt="user" class="img-circle" /> </div>
                                <div class="sl-right">
                                    <div><a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                        <blockquote class="m-t-10">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!--second tab-->
                <div class="tab-pane active" id="profile" role="tabpanel">
                   <div class="card-body">
                        <?php echo form_open('admin/buyer_credential', ' class="form-horizontal form-material sup_change_pswd_form" autocomplete="off" '); ?>
                            <input type="hidden" name="id" value="<?=$profile->id?>">
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">New Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line" name="new_pswd" id="new_pswd">
                                    <span class="error text-danger" id="error_new_pswd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Confirm Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control form-control-line" name="confrim_pswd" id="confrim_pswd">
                                    <span class="error text-danger" id="error_confrim_pswd"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="button-group">
                                    <button type="submit" class="btn waves-effect waves-light btn-outline-primary upd_password">Update Credentials</button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                   </div>
                </div>
                <div class="tab-pane" id="notification" role="tabpanel">
                   <div class="card-body">
                        <?php echo form_open('admin/buyer-notify', ' class="form-horizontal sup_notification_form" autocomplete="off" '); ?>
                            <input type="hidden" name="user_id" value="<?=$profile->id?>">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="title" id="title" placeholder="Title">
                                    <span class="error text-danger" id="error_title"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="10" name="description" id="description" placeholder="Description"></textarea>
                                    <span class="error text-danger" id="error_description"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="button-group">
                                    <button type="submit" class="btn waves-effect waves-light btn-outline-success btn-notify"><i class="fas fa-paper-plane m-r-15"></i>Send Notification</button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<script type="text/javascript">

    $(".upd_password").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#confrim_pswd").val()) == '')
        {
          $("#error_confrim_pswd").html("<?php echo display('Please Enter Confirm Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) == '')
        {
          $("#error_new_pswd").html("<?php echo display('Please Enter New Password'); ?>");
          valid = false;
        }
        if($.trim($("#new_pswd").val()) !== $.trim($("#confrim_pswd").val()))
        {
          $("#error_confrim_pswd").html("<?php echo display('Password Not Match.....'); ?>");
          valid = false;
        }
        if(valid){
          $(".sup_change_pswd_form").get(0).submit();  
        }
    });

    $(".btn-notify").on('click', function(e){
        e.preventDefault();
        var valid = true;
        $(".error").html('');
        if($.trim($("#title").val()) == '')
        {
          $("#error_title").html("<?php echo display('Please Enter the title'); ?>");
          valid = false;
        }
        if($.trim($("#description").val()) == '')
        {
          $("#error_description").html("<?php echo display('Please Enter the description'); ?>");
          valid = false;
        }
       
        if(valid){
          $(".sup_notification_form").get(0).submit();  
        }
    });

</script>