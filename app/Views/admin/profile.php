<style type="text/css">
.text-author{
  border: 1px solid rgba(0,0,0,.05);
  max-width: 100%;
  margin: 0 auto;
  padding: 2em;
}
.text-author h3{
  margin-bottom: 0;
}

.avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 10px auto;
}
.avatar-edit {
        position: absolute;
        right: 2px;
        z-index: 1;
        top: 127px; 
}
.avatar-edit input {
    display: none;
}
.avatar-edit label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    font-weight: normal;
    transition: all .2s ease-in-out;
}
.avatar-edit label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
}
.avatar-edit label:after {
    content: "\f030";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 7px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
}
.avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
}
.avatar-preview > div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
}
hr {
    box-sizing: content-box;
    height: 0;
    margin-top: 0.7rem;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, .1);
}
</style>
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> 
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" class="avatar" accept=".png, .jpg, .jpeg" />
                            <input type="hidden" id="old_image" value="<?=$profile->thumbnail?>">
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview"  style="background-image: url(<?=base_url($profile->thumbnail)?>);">
                            </div>
                        </div>
                    </div>
                    <h4 class="card-title m-t-10"><?=$profile->firstname.' '.$profile->lastname?></h4>
                    <div class="row text-center justify-content-md-center">
                        <div class="col-6">
                            <i class="icon-people"></i> <font class="font-medium">Owner</font>
                        </div>
                        <div class="col-6">
                                <i class="icon-tag"></i> 
                                <?php if($profile->status == 'Active'): ?>
                                    <font class="font-medium text-success">Active</font>
                                <?php else: ?>
                                    <font class="font-medium text-danger">Inactive</font>
                                <?php endif; ?>
                        </div>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <div class="card-body"> 
                <div class="row">
                    <div class="col-md-3">
                        <span class="text-muted">Full Name  : </span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6 class="font-bold"><?=$profile->firstname.' '.$profile->lastname?></h6> 
                    </div>
                </div>
                <div><hr> </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="text-muted">Email address  : </span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6 class="font-bold"><?=$profile->email?></h6> 
                    </div>
                </div>
                <div><hr> </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="text-muted">Mobile  : </span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6 class="font-bold">+<?=$profile->country_code.' '.$profile->mobile?></h6> 
                    </div>
                </div>
                <div><hr> </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="text-muted">Address  : </span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6 class="font-bold"><?=$profile->address?></h6> 
                    </div>
                </div>
                <div><hr> </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="text-muted">City  : </span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6 class="font-bold"><?=$profile->city?></h6> 
                    </div>
                </div>
                <div><hr> </div>
                <div class="row">
                    <div class="col-md-3">
                        <span class="text-muted">Country  : </span>
                    </div>
                    <div class="col-md-9 text-left">
                        <h6 class="font-bold"><?=$profile->country?></h6> 
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    function UploadProfilePicture()
    {
        $(document).on('change','.avatar', function() {
            var formData = new FormData();
            var old_pic = $('#old_image').val();
            var file = $('.avatar').prop('files')[0];
            var token = "<?= csrf_hash() ?>";
            formData.append("profile_pic",file);
            formData.append("old_image",old_pic);
            formData.append("csrf_stream_token",token);
            $.ajax({
                 url: "<?php echo base_url('admin/profile-image'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: formData,
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
            }).done(function(data) {
                // console.log(data);
                 window.location.reload();
               // setTimeout(function() { window.location.reload(); }, 1000);
            });
        });
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
           
            reader.readAsDataURL(input.files[0]);
            UploadProfilePicture();
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });
</script>