<?php if (!empty($reviews)):?>
    <div class="card">
        <div class="card-body">
            <div class="profiletimeline">
                <?php foreach ($reviews as $key => $value):
                    $UserModel = model('App\Models\UserModel');
                    $customer = $UserModel->find($value->buyer_id);
                    $OrdersModel = model('App\Models\OrdersModel');
                    $order = $OrdersModel->find($value->order_id);
                ?>
                    <div class="sl-item">
                        <div class="sl-left"> <img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user" class="img-circle" /> </div>
                        <div class="sl-right">
                            <div><a href="javascript:void(0)" class="link"><?=$customer->firstname.' '.$customer->lastname?></a> <span class="sl-date"> </span>
                                <p class="m-t-10"> <?=$value->review_msg?> </p>
                            </div>
                            <div class="like-comm m-t-20"> <a href="<?=base_url('admin/orders/view/'.$order->id)?>" class="link m-r-10">Order Number : <span class="m-r-10">#<?=$order->order_no?></span> | </a> 
                                <a href="javascript:void(0)" class="link m-r-10">
                                    <?php for ($i=0; $i < $value->rating ; $i++):?>
                                        <i class="fas fa-star text-warning"></i> 
                                    <?php endfor; ?>
                                </a> 
                            </div>
                        </div>
                    </div>
                    <hr>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="col-md-12 reviews_list">
      <?=$pagination?>
    </div>
   <!--  <div class="row">
        <div class="col-md-12">
            <div class="card border-dark">
                <div class="card-body p-b-0 p-t-15">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div> -->
<?php else: ?>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <div class="alert-empty-table-wrapper">
                <div class="alert-empty-table-icon"><i class="far fa-comments"></i></div>
                <div class="alert-empty-info alert-empty-bot-info">
                    <span><?=display('There is no review & rating to display')?></span>
                </div>
            </div>
        </div>
    </div>      
</div>
<?php endif; ?>