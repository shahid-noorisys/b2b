<style type="text/css">
#partitioned {
  padding-left: 6px;
  letter-spacing: 40px;
  border: 0;
  background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
  background-position: bottom;
  background-size: 50px 1px;
  background-repeat: repeat-x;
  background-position-x: 35px;
  width: 300px;
}

@import url('https://fonts.googleapis.com/css?family=Raleway:200');

$BaseBG: #0f0f1a;

body, html {
  height: 100%;
  margin: 0;
  font-family: 'Raleway', sans-serif;
  font-weight: 200;
}

body {
  background-color: $BaseBG;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
}

.digit {
    width: 40px;
    height: 51px;
    background-color: lighten($BaseBG, 5%);
    border-bottom: 1px solid #000;
    border-top: none;
    border-left: none;
    border-right: none;
    line-height: 50px;
    text-align: center;
    font-size: 24px;
    font-weight: 200;
    color: #040404;
    margin: 0 2px;
}

.splitter {
  padding: 0 5px;
  color: #151515;
  font-size: 24px;
}
.prompt {
  margin-bottom: 20px;
  font-size: 20px;
  color: white;
}
</style>

<div class="login-register">
    <div class="login-box card">
        <div class="card-body">
            <?php echo form_open('', ' class="form-horizontal otp-verification digit-group" '); ?>
                <h3 class="box-title m-b-20 text-center"><?=display('OTP verification')?></h3>
                <div class="form-group text-center">
                   <!--  <div class="col-xs-12 ">
                       <input class="otp_code" name="otp" id="partitioned" type="text" maxlength="6" />
                    </div>
                      <span class="error text-danger" id="error_otp"></span> -->
                    <input type="text" id="digit-1" class="digit" name="digit-1" data-next="digit-2" />
                    <input type="text" id="digit-2" class="digit" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                    <input type="text" id="digit-3" class="digit" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                    <span class="splitter">&ndash;</span>
                    <input type="text" id="digit-4" class="digit" name="digit-4" data-next="digit-5" data-previous="digit-3" />
                    <input type="text" id="digit-5" class="digit" name="digit-5" data-next="digit-6" data-previous="digit-4" />
                    <input type="text" id="digit-6" class="digit" name="digit-6" data-previous="digit-5" />
                </div>
                <div class="form-group text-center p-b-20">
                    <div class="col-xs-12">
                        <button type="submit" id="verify_otp" class="btn waves-effect waves-light btn-rounded btn-outline-info"><?=display('Verify')?></button>
                    </div>
                </div>

                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <?=display('Did not receive the OTP ?')?> <b id="timer"></b> 
                        <a href="javascript:void(0)" class="text-info m-l-5 resend-otp"><?=display('Resend OTP')?></a>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<link href="<?php echo base_url('public/assets/admin/dist/css/sweetalert2.min.css')?>" rel="stylesheet">
<script src="<?php echo base_url('public/assets/admin/dist/js/sweetalert2.all.min.js')?>"></script>
<script type="text/javascript">

    let timerOn = true;
    function timer(remaining) {
      var m = Math.floor(remaining / 60);
      var s = remaining % 60;
      
      m = m < 10 ? '0' + m : m;
      s = s < 10 ? '0' + s : s;
      document.getElementById('timer').innerHTML = m + ':' + s;
      remaining -= 1;
      if(remaining >= 0 && timerOn) {
        setTimeout(function() {
            timer(remaining);
        }, 1000);
        return;
      }

      if(!timerOn) {
        // Do validate stuff here
        return;
      }

      $('#timer').hide();
      $('.resend-otp').show();

      var token = "<?= csrf_hash() ?>";
      $.ajax({
             url: "<?php echo base_url('verification-otp-reset'); ?>",
             type: 'POST',
             dataType: 'json',
             data: {
                    csrf_stream_token:token
                   },
        }).done(function(data) {
            console.log(data);
            if (data.status == 'success') {
               console.log(data.message);
            } else {
               console.log(data.message);
            }
        });
    }

    $(function() {
        $(".preloader").fadeOut();
        $('.resend-otp').hide();
        timer(20);
        $("#verify_otp").on('click', function(e){
          e.preventDefault();
          var valid = true;
          $(".error").html('');
          var OTP = $('#digit-1').val()+''+$('#digit-2').val()+''+$('#digit-3').val()+''+$('#digit-4').val()+''+$('#digit-5').val()+''+$('#digit-6').val();
          var token = "<?= csrf_hash() ?>";
            $.ajax({
                 url: "<?php echo base_url('otp-verification'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {
                        OTP:OTP,
                        csrf_stream_token:token
                       },
            }).done(function(data) {
                if (data.status == 'success') {
                    $(".alert-msg").html("<div class='alert alert-success alert-dismissible fade show' role='alert' style='margin: 20px 20px 20px 20px;'><strong><?=display('Success')?>!</strong> "+data.message+"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                   setTimeout(function() { window.location.href ="<?=base_url('supplier/dashboard')?>"; }, 2000);
                } else {
                   $(".alert-msg").html("<div class='alert alert-danger alert-dismissible fade show' role='alert' style='margin: 20px 20px 20px 20px;'><strong><?=display('Sorry')?>!</strong> "+data.message+"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                }
            });
        });

        $('.resend-otp').on('click', function(e){
          e.preventDefault();
          var token = "<?= csrf_hash() ?>";
          $.ajax({
                 url: "<?php echo base_url('otp-resend'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {
                        csrf_stream_token:token
                       },
            }).done(function(data) {
                console.log(data);
                if (data.status == 'success') {
                  $(".alert-msg").html("<div class='alert alert-success alert-dismissible fade show' role='alert' style='margin: 20px 20px 20px 20px;'><strong><?=display('Success')?>!</strong> "+data.message+"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                    $('.resend-otp').hide();
                    $('#timer').show();
                    timer(20);
                } else {
                    $(".alert-msg").html("<div class='alert alert-danger alert-dismissible fade show' role='alert' style='margin: 20px 20px 20px 20px;'><strong><?=display('Sorry')?>!</strong> "+data.message+"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
                      $('.resend-otp').show();
                      $('#timer').hide();
                }
            });
        });
    }); 

    $('.digit-group').find('input').each(function() {
      $(this).attr('maxlength', 1);
      $(this).on('keyup', function(e) {
        var parent = $($(this).parent());
        
        if(e.keyCode === 8 || e.keyCode === 37) {
          var prev = parent.find('input#' + $(this).data('previous'));
          
          if(prev.length) {
            $(prev).select();
          }
        } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
          var next = parent.find('input#' + $(this).data('next'));
          
          if(next.length) {
            $(next).select();
          } else {
            if(parent.data('autosubmit')) {
              parent.submit();
            }
          }
        }
      });
    });
</script>


