<div class="row">
    <div class="col-md-12">
        <div class="card card-body printableArea">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive m-t-40" style="clear: both;">
                        <table id="notification" class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-left">Title</th>
                                    <th>Description</th>
                                    <th class="text-center">Time</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var token = "<?=csrf_hash()?>";
    $('#notification').DataTable({
        "lengthChange": false,
        "processing" : false,
        "serverSide" : true,
        "ajax" : {
           url:"<?=base_url('notification-list')?>",
           type:"POST",
           data:{csrf_stream_token:token},
        },
        'columns': [
            { data: 'id' },
            { data: 'title'},
            { data: 'description' },
            { data: 'created_date' },
            { data: 'read_status' },
        ]
    });
    $(document).on('click','.page-link, .previous , .next', function(e){
        var token = "<?= csrf_hash() ?>";
        $.ajax({
             url: "<?php echo base_url('get-notification-counter'); ?>",
             type: 'POST',
             dataType: 'json',
             data: {csrf_stream_token:token},
        }).done(function(data) {
           if(data.counter !== 0 ) {
               $('.counter').text(data.counter);
               $('.drop-title').text('You have '+data.counter+' new Notifications');
               $('.message-center').empty().append(data.list);
           } else {
               $('.counter').text('');
               $('.drop-title').text('You have '+data.counter+' new Notifications');
               $('.message-center').empty().append('');
           }
        });
    });
</script>