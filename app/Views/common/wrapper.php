<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('public/assets/img/logo/favicon.png')?>">
    <title><?=display('yahodehime')?></title>
    
    <!-- page css -->
    
    <link href="<?php echo base_url('public/assets/admin/dist/css/morris.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/ecommerce.css')?>" rel="stylesheet">

    <link href="<?php echo base_url('public/assets/admin/icons/font-awesome/css/fontawesome.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/icon-page.css')?>" rel="stylesheet">

    <link href="<?php echo base_url('public/assets/admin/dist/css/Custom.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/sweetalert2.min.css')?>" rel="stylesheet">

    <link href="<?php echo base_url('public/assets/admin/dist/css/sweetalert2.min.css')?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('public/assets/admin/dist/css/style.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('public/assets/admin/dist/css/pages/login-register-lock.css')?>" rel="stylesheet">
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery-3.2.1.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/sweetalert2.all.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/ecom-dashboard.js')?>"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
    @media (min-width: 1024px) {
        .footer, .page-wrapper {
             margin-left: 0px !important; 
        }
    }
    @media (min-width: 768px){
        .mini-sidebar .footer, .mini-sidebar .page-wrapper {
             margin-left: 0px; 
        }
    }
    .container-fluid {
        padding: 0 25px;
    }
    .wrapper {
        padding :20px 0 0 0;
    }
    /*.register-details {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        height: 100%;
        width: 100%;
        padding: 0% 5%;
        position: fixed;
        overflow: scroll;
    }*/
    @media (min-width:360px) {
        .register-details {
           overflow: scroll;
           padding: 5% 0%;
        }
    }
    .form-top {
        overflow: hidden;
        padding: 0px 25px 0px 25px;
        background: #fff;
        border-radius: 4px 4px 0 0;
        text-align: left;
    }
    .form-top-left {
        float: left;
        width: 75%;
        padding-top: 25px;
    }
    .form-top-right {
        float: left;
        width: 25%;
        padding-top: 5px;
        font-size: 66px;
        color: #e6e6e6;
        line-height: 100px;
        text-align: right;
    }
    .white-box {
        background: #fff;
        padding: 25px;
        margin-bottom: 15px;
    }  
    .alert-empty-table-wrapper {
        padding: 0;
        clear: both;
        overflow: hidden;
    }
    .alert-empty-table-icon {
        margin: 10px 0;
        text-align: center;
    }
    .alert-empty-table-icon i {
        font-size: 90px;
        color: #EDF1F5;
    }
    /*.alert-empty-bot-info {
        margin-top: 50px;
    }*/
    .alert-empty-info {
        text-align: center;
        text-transform: uppercase;
        font-size: 22px;
        color: #c9d2de;
    } 
</style>
</head>

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label"><?=display('yahodehime')?></p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->

    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?=base_url();?>">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?=base_url('public/assets/admin/images/logo-icon.png')?>" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?=base_url('public/assets/admin/images/logo-light-icon.png')?>" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url('public/assets/admin/images/logo-text.png')?>" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url('public/assets/admin/images/logo-light-text.png')?>" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="page-wrapper">
            <div class="container-fluid">
                <section class="wrapper">
                    <div class="row">
                        <div class="offset-md-2 col-md-8 col-sm-12 col-xs-12 alert-msg">
                            <?php if(session()->getFlashData('message') != null): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin: 20px 20px 20px 20px;">
                                    <strong><?=display('Success')?>!</strong> <?php $errors = session()->getFlashData('message');
                                    if(is_array($errors)){
                                        foreach ($errors as $value) {
                                            echo $value.'<br>';
                                        }
                                    } else { echo $errors; }
                                    ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                            <?php if(session()->getFlashData('exception') != null): ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin: 20px 20px 20px 20px;">
                                    <strong><?=display('Sorry')?>!</strong> <?php $errors = session()->getFlashData('exception');
                                    if(is_array($errors)){
                                        foreach ($errors as $value) {
                                            echo $value.'<br>';
                                        }
                                    } else { echo $errors; }
                                    ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <?php echo ($content)?$content:''; ?>
                    <!-- ============================================================== -->
                </section>
            </div>
        </div>
        <footer class="footer text-center">
            © 2020 <?= strtoupper(display('yahodehime'));?>
        </footer>
    </div>
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery-3.2.1.min.js')?>"></script>
    
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('public/assets/admin/dist/js/popper.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/perfect-scrollbar.jquery.min.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/waves.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/sidebarmenu.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/sticky-kit.min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/jquery.sparkline.min.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/custom.min.js')?>"></script>

    <script src="<?php echo base_url('public/assets/admin/dist/js/raphael-min.js')?>"></script>
    <script src="<?php echo base_url('public/assets/admin/dist/js/morris.min.js')?>"></script>

    <script type="text/javascript">        

        $(function() {
            $(".preloader").fadeOut();
            $('[data-toggle="tooltip"]').tooltip()

        });
    </script>
    
</body>

</html>