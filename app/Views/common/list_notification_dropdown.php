 <?php if(!empty($notifications)):?>
    <?php if(isset($notifications)):?>
        <?php foreach ($notifications as $key => $value):?>
        <a href="<?=base_url('supplier/notifications')?>">
            <div class="user-img"> <img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="user" class="img-circle"> 
            </div>
            <div class="mail-contnet">
                <h5><?=$value->title?></h5> <span class="mail-desc"><?=$value->description?></span> <span class="time"><?= date('d M Y',strtotime($value->created_date)) ?> | <?= date('H:i A',strtotime($value->created_date)) ?></span> </div>
        </a>
    <?php endforeach;?>
    <?php endif;?>
<?php endif;?>