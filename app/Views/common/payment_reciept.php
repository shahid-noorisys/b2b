<!DOCTYPE html>
<html lang="en">

<head>
    
<style type="text/css">
    @media (min-width: 1024px) {
        .footer, .page-wrapper {
             margin-left: 0px !important; 
        }
    }
    @media (min-width: 768px){
        .mini-sidebar .footer, .mini-sidebar .page-wrapper {
             margin-left: 0px; 
        }
    }
    .container-fluid {
        padding: 0 25px;
    }
    .wrapper {
        padding :20px 0 0 0;
    }
    /*.register-details {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        height: 100%;
        width: 100%;
        padding: 0% 5%;
        position: fixed;
        overflow: scroll;
    }*/
    @media (min-width:360px) {
        .register-details {
           overflow: scroll;
           padding: 5% 0%;
        }
    }
    .form-top {
        overflow: hidden;
        padding: 0px 25px 0px 25px;
        background: #fff;
        border-radius: 4px 4px 0 0;
        text-align: left;
    }
    .form-top-left {
        float: left;
        width: 75%;
        padding-top: 25px;
    }
    .form-top-right {
        float: left;
        width: 25%;
        padding-top: 5px;
        font-size: 66px;
        color: #e6e6e6;
        line-height: 100px;
        text-align: right;
    }
    .white-box {
        background: #fff;
        padding: 25px;
        margin-bottom: 15px;
    }  
    .alert-empty-table-wrapper {
        padding: 0;
        clear: both;
        overflow: hidden;
    }
    .alert-empty-table-icon {
        margin: 10px 0;
        text-align: center;
    }
    .alert-empty-table-icon i {
        font-size: 90px;
        color: #EDF1F5;
    }
    /*.alert-empty-bot-info {
        margin-top: 50px;
    }*/
    .alert-empty-info {
        text-align: center;
        text-transform: uppercase;
        font-size: 22px;
        color: #c9d2de;
    } 
</style>
</head>

<body class="skin-default card-no-border">
    <div id="main-wrapper">
        <div class="page-wrapper">
            <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Main Content Here -->
                    <?php 
                        $seller_company = get_company_by_user_id($receipt->paid_to);
                        $buyer_company = get_company_by_user_id($receipt->paid_by);
                        $AdderssModel = model('App\Models\UserAddressModel');
                        $address_details = $AdderssModel->where(['user_id'=>$receipt->paid_by,'is_primary'=>'Yes'])->first();
                    ?>
                    <!-- ============================================================== -->
                   <div class="row">
                        <div class="col-md-12">
                            <div class="card card-body printableArea">
                                <h3><b><?= strtoupper(display('Payment Receipt')) ?></b> <span class="pull-right">#<?=$receipt->receipt_id?></span></h3>
                                <hr>
                              
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="pull-left">
                                            <address>
                                                <h5><?= strtoupper(display('From'));?>,</h5>
                                                <h4> &nbsp;<b class="font-bold"><?=($seller_company->company_name)??''?></b></h4>
                                                <p class="text-muted m-l-5"><?=($seller_company->address)??''?>,
                                                    <br/> <?=($seller_company->city)??''?>  <?=($seller_company->zip_code)??''?>,
                                                    <br/> <?=($seller_company->country)?get_country_by_id($seller_company->country)->name:''?></p>
                                            </address>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pull-right text-right">
                                            <address>
                                                <h5><?= strtoupper(display('To'));?>,</h5>
                                                <h4 class="font-bold"><?=($buyer_company->company_name)??''?>,</h4>
                                                <p class="text-muted m-l-30"><?=($buyer_company->address)??''?>,
                                                    <br/> <?=($buyer_company->city)??''?>  <?=($buyer_company->zip_code)??''?>,
                                                    <br/> <?=(!empty($buyer_company) AND $buyer_company->country)?get_country_by_id($buyer_company->country)->name:''?>
                                                </p> 
                                                
                                            </address>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4" style="padding: 0px 30px 0px 30px;">
                                        <div class="pull-right text-left">
                                            <address>
                                                <h6 class="font-bold"><?= display('Shipping Address');?>,</h6>
                                                <h6><?=($receipt->address)??''?>,</h6>
                                                <p class="text-muted"><?=($receipt->city)??''?>  (<?=($receipt->state)??''?>),
                                                    <br/> <?=($receipt->country)??''?>
                                                </p> 
                                            </address>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding: 0px 30px 0px 30px;">
                                        <div class="pull-right text-left">
                                            <address>
                                                <h6 class="font-bold"><?= display('Billing Address');?>,</h6>
                                                <h6><?=($address_details->address)??''?>,</h6>
                                                <p class="text-muted"><?=($address_details->city)??''?>  (<?=($address_details->state)??''?>),
                                                    <br/> <?=($address_details->country)??''?>
                                                </p> 
                                            </address>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding: 0px 30px 0px 30px;">
                                        <div class="pull-right text-right">
                                            <address>
                                                <h6 class="font-bold"><?= display('Payment Details');?>,</h6>
                                                <h6><b><?=display('Payment Mode')?> : </b> <span><?= $receipt->payment_mode?></span></h6>
                                                <h6><b><?=display('Payment Method')?> : </b> <span><?= strtoupper($receipt->payment_method) ?></span></h6>
                                                <h6><b><?=display('Payment Date')?> : </b> <span><?= date('d F Y',strtotime($receipt->created_date)) ?></span></h6>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive m-t-40" style="clear: both;">
                                            <table id="product" class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th class="text-left">Name</th>
                                                        <!-- <th>Description</th> -->
                                                        <th class="text-center">Quantity</th>
                                                        <th class="text-center">Unit Cost</th>
                                                        <th class="text-center">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        if(!empty($receipt->product_json) && isset($receipt->product_json)):
                                                            $products_json = json_decode($receipt->product_json,true);
                                                        endif;
                                                    if (!empty($products_json)):
                                                        foreach ($products_json as $key => $value):
                                                           $productModel = model('App\Models\ProductModel');
                                                           $products = $productModel->find($value['product_id']);
                                                    ?>
                                                        <tr>
                                                            <td class="text-center"><?=$key+1?></td>
                                                            <td>
                                                                <a href="javascript:void(0)" class="product-view" data-id="<?=$value['product_id']?>">
                                                                    <?=$products->name?>
                                                                </a>
                                                            </td>
                                                            <!-- <td><?=character_limiter($products->description,80)?></td> -->
                                                            <td class="text-center"><?=$value['quantity'];?></td>
                                                           
                                                            <td class="text-center product_amount"><?=($value['product_amount'])? number_format($value['product_amount'],2):0?></td>

                                                            <td class="text-center product_total"><?= number_format((int)$value['quantity']*(int)$value['product_amount'],2) ?></td>

                                                        </tr>
                                                        <?php endforeach; ?> 
                                                    <?php endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-right m-t-30 text-right">
                                            <p class="font-bold"><b><span class="text-dark">Sub - Total amount :</span> € <?= number_format($receipt->order_amount,2); ?></b></p>
                                            <p class="font-bold"><b>VAT (8%) : € <?= number_format((int)$receipt->order_amount*(8/100),2); ?> </b></p>
                                            <hr>
                                            <h4 class="font-bold"><b>Total Amount :</b> <span id="final_total">€ <?= number_format((int)$receipt->order_amount+(int)$receipt->order_amount*(8/100),2); ?></span></h4>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

</body>

</html>