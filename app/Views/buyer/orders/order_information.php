<style type="text/css">
.invoice {
    padding: 30px;
}

.invoice h2 {
    margin-top: 0px;
    line-height: 0.8em;
}

.invoice .small {
    font-weight: 300;
}

.invoice hr {
    margin-top: 10px;
    border-color: #ddd;
}

.invoice .table tr.line {
    border-bottom: 1px solid #ccc;
}

.invoice .table td {
    border: none;
}

.invoice .identity {
    margin-top: 10px;
    font-size: 1.1em;
    font-weight: 300;
}

.invoice .identity strong {
    font-weight: 600;
}


.grid {
    position: relative;
    width: 100%;
    background: #fff;
    color: #666666;
    border-radius: 2px;
    margin-bottom: 25px;
    box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.1);
}
h4 {
    font-size: 18px !important;
    color: #000 !important;
}
.order-summary{
    padding: 50px 0px 10px 0px;
}
.order-summary h3 {
    font-size: 25px;
    line-height: 30px;
    margin-bottom: 25px;
}
.m-t-30 {
    margin-top: 50px;
}
.order-date{
    font-size: 15px;
    font-weight: 500;
    color: #000;
}

.order-status{
    font-size: 15px;
    font-weight: 500;
    color: #000;
}
.coupon_area {
     margin-bottom: 5px; 
}
</style>
<?php 
    $company = model('App\Models\UserCompanyModel');
    $seller_company =  $company->where('user_id',$order->supplier_id)->first();
    $userModel = model('App\Models\UserModel');
    $buyer =  $userModel->find($order->buyer_id);
?>
<!--contact area start-->
<div class="contact_area" style="margin-top: 25px;">
    <div class="container">   
        <div class="row">
            <div class="col-md-12">
                <div class="grid invoice">
                    <div class="grid-body">
                        <div class="invoice-title">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>ORDER SUMMARY<br>
                                    <span class="small">Order #<?=$order->order_no?></span></h3>
                                </div>
                                <div class="col-md-6 text-right">
                                    <h6 class="order-date"><b><?=display('Order Date')?> : </b> <span><?= date('d M Y',strtotime($order->created_date)) ?></span></h6>

                                <p class="order-status m-l-30"><b><?=display('Payment Method')?> :</b> 
                                    <?php
                                        if($order->payment_method == 'mtn'){
                                            echo '<span class="label label-success font-bold m-b-5">'.display('MTN').'</span>';
                                        }
                                        else if($order->payment_method == 'orange'){
                                            echo '<span class="label label-warning label-rounded font-bold m-b-5">'.display('Orange Mobile').'</span>';
                                        }
                                        else if($order->payment_method == 'togo'){
                                            echo '<span class="label label-info label-rounded font-bold m-b-5">'.display('Togo Cell').'</span>';
                                        }
                                    ?>

                                    <br/> <b><?=display('Order Status')?> :</b> 
                                    <?php if($order->order_status == 'pending'):?>
                                        <span class="label label-primary font-bold"><?=display('Pending')?></span>
                                    <?php endif;?>
                                    <?php if($order->order_status == 'in_progress'):?>
                                        <span class="label label-warning font-bold"><?=display('In-progress')?></span>
                                    <?php endif;?>
                                    <?php if($order->order_status == 'delivered'):?>
                                        <span class="label label-success font-bold"><?=display('Delivered')?></span>
                                    <?php endif;?>
                                </p> 
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <address>
                                    <h4><?=strtoupper(display('From')) ?>,</h4>
                                    <h4><?=($seller_company->company_name)??$order->seller_name?></h4>
                                    <?=($seller_company->address)??''?><br>
                                    <?=($seller_company->city)??''?><br>
                                    <?=(!empty($seller_company) AND $seller_company->country)?get_country_by_id($seller_company->country)->name:''?>
                                </address>
                            </div>
                            <div class="col-md-4 text-left">
                                <address>
                                    <h4><?=strtoupper(display('To')) ?>,</h4>
                                    <h4><?=$buyer->firstname.' '.$buyer->lastname?></h4>
                                    <h6><?=($order->address)??''?>,</h6>
                                    <p class="text-muted m-l-30"><?=($order->city)??''?>  (<?=($order->state)??''?>),
                                        <br/> <?=($order->country)??''?>
                                    </p> 
                                </address>
                            </div>
                            <div class="col-md-4 text-left">
                                <address>
                                        <h4 class="font-bold"><?= display('Shipping Address');?>,</h4>
                                        <h6><?=($order->address)??''?>,</h6>
                                        <p class="text-muted m-l-30"><?=($order->city)??''?>  (<?=($order->state)??''?>),
                                            <br/> <?=($order->country)??''?>
                                        </p> 
                                </address>
                            </div>
                        </div>
                        <hr>
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right text-right">
                                    <address>
                                        <h4 class="font-bold"><?= display('Shipping Address');?>,</h4>
                                        <h6><?=($order->address)??''?>,</h6>
                                        <p class="text-muted m-l-30"><?=($order->city)??''?>  (<?=($order->state)??''?>),
                                            <br/> <?=($order->country)??''?>
                                        </p> 
                                    </address>
                                </div>
                            </div>
                        </div> -->
                        <div class="row order-summary">
                            <div class="col-md-12">
                                <h3>PRODUCT DETAILS</h3>
                                <div class="table_desc">
                                    <div class="cart_page table-responsive">
                                        <table>
                                            <thead>
                                                <tr>
                                                  <th class="product_name">Product</th>
                                                  <th class="product_thumb"><?=display('Product Image')?></th>
                                                  <th class="product_quantity">Quantity</th>
                                                  <th class="product-price">Price</th>
                                                  <th class="product_total">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    if(!empty($order->product_json) && isset($order->product_json)):
                                                    $products_json = json_decode($order->product_json,true);
                                                    endif;
                                                    if (!empty($products_json)):
                                                        foreach ($products_json as $key => $value):
                                                           $productModel = model('App\Models\ProductModel');
                                                           $products = $productModel->find($value['product_id']);
                                                ?>
                                                    <tr>
                                                        <td>
                                                            <a href="javascript:void(0)" class="product-view" data-id="<?=$value['product_id']?>">
                                                                <?=$products->name?>
                                                            </a>
                                                        </td>
                                                        <td> 
                                                            <img src="<?=base_url($products->image_1)?>" alt="iMac" width="60" height="60"> 
                                                        </td>
                                                       
                                                        <td class="text-center"><?=$value['quantity'];?></td>
                                                       
                                                        <td class="text-center product_amount"><?=($value['product_amount'])? number_format($value['product_amount'],2):0?></td>

                                                        <td class="text-center product_total"><?= number_format((int)$value['quantity']*(int)$value['product_amount'],2) ?></td>

                                                    </tr>
                                                    <?php endforeach; ?> 
                                                <?php endif; ?>
                                            </tbody>
                                        </table> 
                                    </div>  
                                </div>
                            </div>    
                        </div>                                  
                    </div>
                    <div class="coupon_area">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="coupon_code right">
                                    <h3>Amount Detail</h3>
                                    <div class="coupon_inner">
                                        <div class="cart_subtotal" style="border-bottom: 2px solid #000; margin-top: 10px;">
                                           <p>SUB-TOTAL</p>
                                           <p class="cart_amount">€ <?= number_format($order->amount,2); ?></p>
                                        </div>
                                        <div class="cart_subtotal" style="border-bottom: 2px solid #000; margin-top: 10px;">
                                             <p>VAT INCLUDED (8%)</p>
                                             <p class="cart_amount">€ <?= number_format((int)$order->amount*(8/100),2); ?></p>
                                        </div>
                                        <div class="cart_subtotal" style="margin-top: 15px;">
                                         <p>TOTAL AMOUNT</p>
                                         <p class="cart_amount">€ <?= number_format((int)$order->amount+(int)$order->amount*(8/100),2); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
<!--contact area end-->