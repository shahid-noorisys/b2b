<style>
    .widget_list form button {
        height: 30px;
        line-height: 30px;
        padding: 0 20px;
        text-transform: capitalize;
        color: #ffffff;
        background: #262626;
        border: 0;
        border-radius: 30px;
        -webkit-transition: 0.3s;
        transition: 0.3s;
    }
    .widget_list form button:hover {
        background: #ea000d;
    }
    .widget_list form {
        padding: 10px 20px 10px 20px;
    }
    .widget_list form input {
        font-size: 0.75rem;
    }
    .widget_list form p {
        margin: 2px 2px;
    }
    .widget_list h2 {
        margin-bottom: 0px;
    }
    .shop_toolbar.t_bottom {
        justify-content: center;
        margin-bottom: 0;
        margin-top: 0px;
    }
    .table-responsive table tbody tr td{
        min-width: 140px;
    }
</style>
<!-- Quotation  area start-->
<div class="shop_area shop_sidebar mt-50 mb-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <!--sidebar widget start-->
                <aside class="sidebar_widget">
                    <div class="widget_list widget_categories mt-50">
                        <h2><?=display('Filters')?></h2>
                        <form action="#">
                            <div class="form-group">
                                <p><?=display('Order Number')?></p>
                                <input type="text" name="" id="" class="form-control" placeholder="<?=display('Order Number')?>">
                            </div>
                            <div class="form-group">
                                <p><?=display('Status')?></p>
                                <input type="text" name="" id="" class="form-control" placeholder="<?=display('Status')?>">
                            </div>
                            <button type="submit"><?=display('Submit')?></button>
                            <button type="submit" class="clear_filter_btn"><?=display('Clear')?></button>
                        </form>
                    </div>
                    
                </aside>
                <!--sidebar widget end-->
            </div>
            
            <div class="col-lg-9 col-md-12">
                <!--shop wrapper start-->
                <!--shop toolbar start-->
                <div class="shop_title">
                    <h1><?=display('Orders')?></h1>
                </div>
                <div id="ajax_order_list"></div>
                <!--shop wrapper end-->
            </div>
        </div>
    </div>
</div>
<!-- Quotation  area end-->