<style>
    .checkout_form label.righ_0 {
        margin-bottom: 10px;
    }
    .checkout_form input {
        width: unset !important;
        height: unset !important;
    }
    .checkout_form input {
        border: 1px solid #e5e5e5;
        background: none;
        height: 40px;
        width: 100%;
        padding: 0 20px;
        color: #262626;
    }
</style>
<!--Checkout page section-->
<div class="Checkout_section mt-60">
    <div class="container">
        <div class="checkout_form">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <!-- <form action="#"> -->
                    <?=form_open(base_url('create-order'),'onsubmit="return validatePaymentForm(this,event)" class="payment_form"')?>
                    <h3><?=display('Billing Details')?></h3>
                        <div class="row">
                            <?php if(! empty($primary_address)): ?>
                            <div class="col-12 mb-20">
                                <div class="card text-left">
                                  <div class="card-body">
                                    <input type="radio" name="address_id" value="<?=$primary_address->id?>" class="address_checkbox" onchange="addressChecked(this,event)" checked >     
                                    <label> <?=$primary_address->full_name;?> </label>
                                    <p> <?=$primary_address->address.', '.$primary_address->city.', '.$primary_address->state.', '.$primary_address->country?> </p>
                                  </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <!-- collapse for extra address -->
                            <div class="col-12 mb-20">
                                <label class="righ_0" for="address" data-toggle="collapse" data-target="#collapsetwo" role="button" aria-expanded="false" aria-controls="collapseOne"><?=display('Ship to a different address?')?></label>
                                <label class="righ_0" data-toggle="modal" data-target="#new_address_modal_box"><?=display('Add new shipping addresss')?></label>

                                <div id="collapsetwo" class="collapse row">
                                    <?php if(! empty($u_address)): ?>
                                    <?php foreach($u_address as $v): ?>
                                    <div class="col-12 mb-20">
                                        <div class="card text-left">
                                        <div class="card-body">
                                            <input type="radio" name="address_id" value="<?=$v->id?>" class="address_checkbox" onchange="addressChecked(this,event)" >     
                                            <label> <?=$v->full_name;?> </label>
                                            <p class="card-text"> <?=$v->address.', '.$v->city.', '.$v->state.', '.$v->country?> </p>
                                        </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                        <div class="card text-left">
                                          <div class="card-body">
                                            <p class="card-text"><?=display('No more Addresses found')?></p>
                                          </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- collapse for extra address -->
                        </div>
                    <!-- </form>     -->
                </div>
                <div class="col-lg-6 col-md-6">
                        <!-- <input type="hidden" name="ship_address_id" id="ship_address_id" value="<?//$primary_address->id?>"> -->
                        <input type="hidden" name="quotation_id" id="quotation_id" value="<?=$quotation->id?>">
                        <h3><?=display('Your order')?></h3> 
                        <label for=""><?=display('Quotation Number')?> : <?=($quotation->q_no)??'QT#' ?></label>
                        <div class="order_table table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?=display('Product')?></th>
                                        <th><?=display('Quantity')?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $product_array = json_decode($quotation->product_json); ?>
                                    <?php foreach ($product_array as $k => $p): ?>
                                    <?php
                                    $pModel = model('App\Models\ProductModel');
                                    $product = $pModel->find($p->product_id); 
                                    ?>
                                    <tr>
                                        <td><?=$product->name?></td>
                                        <td><?=$p->quantity?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>     
                        </div>

                        <div class="order-notes">
                            <label for="order_note"><?=display('Order Notes')?></label>
                            <textarea id="order_note" name="order_note" placeholder="<?=display('Notes about your order, e.g. special notes for delivery.')?>"></textarea>
                        </div>    

                        <div class="order-notes">
                            <span class="col-6"><?=display('Quotation Amount')?></span>
                            <span class="col-6"><strong><?=$quotation->amount?></strong></span>
                            <span class="col-6"><?=display('Shipping')?></span>
                            <span class="col-6"><strong>$5.00</strong></span>
                            <span class="col-6"><?=display('Order Total')?></span>
                            <span class="col-6"><strong>$220.00</strong></span>
                        </div>    

                        <div class="payment_method" style="margin-top: 15px;">
                    <!--         <div class="panel-default">
                                <input id="payment_defult" name="payment_method" class="payment_mode" type="radio" value="mtn" />
                                <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" role="button" aria-expanded="false" aria-controls="collapsedefult"><?=display('MTN')?> <img src="<?=base_url('public/assets/img/icon/mtn.png')?>" alt=""></label>

                                <div id="collapsedefult" class="collapse">
                                    <div class="card-body1">
                                        <p>Pay via MTN; you can pay with your credit card if you don’t have an MTN account.</p> 
                                    </div>
                                </div>
                            </div>
                            <div class="panel-default">
                                <input id="payment_togo_cell" name="payment_method" type="radio" class="payment_mode" value="togo" />
                                <label for="payment_togo_cell" data-toggle="collapse" data-target="#collapse_togo" role="button" aria-expanded="false" aria-controls="collapse_togo"><?=display('Togo Cell')?> <img src="<?=base_url('public/assets/img/icon/togocell.png')?>" alt=""></label>

                                <div id="collapse_togo" class="collapse">
                                    <div class="card-body1">
                                        <p>Pay via TogoCell.</p> 
                                    </div>
                                </div>
                            </div>
                            <div class="panel-default">
                                <input id="payment_orange" name="payment_method" type="radio" class="payment_mode" value="orange" />
                                <label for="payment_orange" data-toggle="collapse" data-target="#collapse_orange" role="button" aria-expanded="false" aria-controls="collapse_orange"><?=display('Orange')?> <img src="<?=base_url('public/assets/img/icon/orange.png')?>" alt="" ></label>

                                <div id="collapse_orange" class="collapse">
                                    <div class="card-body1">
                                        <p>Pay via Orange Mobile Money.</p> 
                                    </div>
                                </div>
                            </div>  -->
                            <div class="panel-default">
                                <input id="payment_paypal" name="payment_method" class="payment_mode" type="radio" value="PayPal" />
                                <label for="payment_defult" data-toggle="collapse" data-target="#collapsedefult" role="button" aria-expanded="false" aria-controls="collapsedefult"><?=display('PayPal')?> <img src="<?=base_url('public/assets/img/icon/papyel.png')?>" alt="" style="margin-left: 5px;"></label>
                                
                                <div id="collapsedefult" class="collapse">
                                    <div class="card-body1">
                                        <p>Pay via PayPal; you can pay with your credit card if you don’t have an MTN account.</p> 
                                    </div>
                                </div>
                            </div>
                            <div class="panel-default">
                                <input id="payment_defult" name="payment_method" type="radio" class="payment_mode" value="bank" />
                                <label for="payment_orange" data-toggle="collapse" data-target="#collapse_bank" role="button" aria-expanded="false" aria-controls="collapse_bank"><?=display('By Bank')?></label>

                                <div id="collapse_bank" class="collapse">
                                    <div class="card-body1">
                                         <div class="col-lg-6 mb-20">
                                            <label>Transaction Number <span>*</span></label>
                                            <input type="text" name="transaction_number" class="checkout_form" style="height: 40px !important;width: 100% !important;">    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="order_button">
                                <button class="btn-click btn-order" type="submit"><?=display('Confirm Order')?></button> 
                            </div>  
                            <div class="paypal-btn" style="width : 30%;">
                                
                            </div>  
                        </div> 
                    </form> 
                </div>
            </div> 
        </div> 
    </div>       
</div>
<!--Checkout page section end-->
<script type="text/javascript">
    $(".paypal-btn").hide();  
    $(".btn-order").hide();  
    $(document).ready(function () {
    });
    $('.btn-click').on('click',function(){
        if($('.payment_mode').val() == 'PayPal'){
            $(".PayPal_form").get(0).submit();  
        }else {

            $(".payment_form").get(0).submit();  
        }
    });    
    $(document).on('change','.payment_mode',function(){
        if($('input[name="payment_method"]:checked').val() == 'PayPal'){
            // alert($('input[name="payment_method"]:checked').val());
            // $(".PayPal_form").get(0).submit();  
            $(".btn-click").hide();  
            $(".paypal-btn").show();  
        }else {
            // alert($('input[name="payment_method"]:checked').val());
            $(".paypal-btn").hide();  
            $(".btn-click").show();  
            // $(".payment_form").get(0).submit();  
        }
    });
</script>
<script src="https://www.paypal.com/sdk/js?client-id=ATtslgoq5RFeVot_YRy238V2W50xvXjsluqvE_v_IeXMRlcFfe0q7e7NDFYgJP5LKc8DzA03NQIWcZVI&currency=USD"></script>
<script type="text/javascript">
var amount = 1;
var payment_method = 'paypal';
var address_id = $('input[name="address_id"]:checked').val();
var order_note = $('#order_note').val();
var quotation_id = $('#quotation_id').val();
document.paypal_action=null;
        
        paypal.Buttons({

          style: {
            color        : 'black',
            shape        : 'rect',
            label        : '',
            height       : 40,
            size         : 'responsive',
            layout       : 'horizontal',
            tagline      : false,
            fundingicons : false,

          },

            // Set up the transaction
            createOrder: function(data, actions) {
              document.paypal_action=actions;
              return actions.order.create({
                purchase_units: [{
                  amount: {
                    value: amount
                  }
                }]
              });
            },

            // Finalize the transaction
            onApprove: function(data, actions) {
              return actions.order.capture().then(function(details) {
                    // Show a success message to the buyer
                    console.log(details); 
                    if(details.status == "COMPLETED"){
                        var token = "<?= csrf_hash() ?>";
                        
                        $.ajax({
                                    url: '<?=base_url("create-order");?>',
                                    type: 'POST',
                                    dataType: 'json',
                                    data: {
                                            detail:details,
                                            amount : amount,
                                            payment_method : payment_method,
                                            address_id : address_id,
                                            order_note : order_note,
                                            quotation_id : quotation_id,
                                            csrf_stream_token:token,
                                          },
                              }).done(function(response) 
                                {
                                  console.log(response);
                                    if(response.status !== 'failed'){
                                            Swal.fire({
                                                icon: 'success',
                                                text: response.message,
                                                showConfirmButton: true,
                                            }).then(function(){   
                                                window.location.href = response.redirect;  
                                            });
                                        }
                                        else {
                                            Swal.fire({
                                                icon: 'success',
                                                text: response.message,
                                                showConfirmButton: true,
                                            }).then(function(){   
                                                window.location.reload();  
                                            });
                                        }

                                }).fail(function(response) 
                                {
                                   console.log(response);
                                    // alert(this.responseText);
                                });
                    } 
                  });
            },
            // onInit is called when the button first renders
            onInit: function(data, actions) {
            // Disable the buttons
            },
            // onClick is called when the button is clicked
            onClick: function() {
            // Show a validation error if the checkbox is not checked
          }
        }).render('.paypal-btn');

</script>