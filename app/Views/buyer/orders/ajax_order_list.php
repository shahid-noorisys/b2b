<!--shop toolbar end-->
<div class="row">
    <div class="col-12">
        <div class="cart_page table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th><?=display('Order Number')?></th>
                        <th><?=display('Total Items')?></th>
                        <th><?=display('Amount')?></th>
                        <th><?=display('Status')?></th>
                        <th><?=display('Ordered')?></th>
                        <th><?=display('Action')?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if(!empty($orders)):?>
                <?php foreach ($orders as $v): ?>
                    <tr>
                        <td class="product_name"><?=$v->order_no?></td>
                        <td class="product_name"><?=$v->total_items?></td>
                        <td class="product-price"><?=$v->amount?></td>
                        <td class="product_quantity"><?php
                        $msg = display('Pending');
                        if($v->order_status == 'in_progress'){
                            $msg = display('Processing');
                        }
                        else if($v->order_status == 'delivered'){
                            $msg = display('Delivered');
                        }
                        echo $msg;
                        ?></td>
                        <td class="product_total"><?php
                        $time = CodeIgniter\I18n\Time::parse($v->created_date);
                        echo $time->humanize();
                        ?></td>
                        <td class="product_total">
                            <a href="<?=base_url('order-info/'.$v->id)?>" title="view details"><i class="zmdi zmdi-info-outline" style="font-size: 20px; margin-right: 10px;"></i></a>
                            <?php if($v->order_status == 'delivered') { ?>
                                <?php
                                    $ReviewModel = model('App\Models\ReviewandRatingModel');
                                    $Review = $ReviewModel->where('order_id',$v->id)->first();
                                    if (empty($Review)) {
                                ?>
                                <a href="#" title="Review & Rating" class="reviews" data-toggle="modal" data-order = "<?=$v->id?>"><i class="zmdi zmdi-comments" style="font-size: 18px;"></i></a>
                                <?php } ?>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>   
        </div>
        </div>
</div>
<div class="order_list">
<?=$pagination?>
</div>
<!--shop toolbar end-->
<script type="text/javascript">
    $(document).on('click','.reviews',function(){
        var order_id = $(this).data('order');
        $('#order_id').val(order_id);
        $('#review_rating').modal('show');
    });
</script>