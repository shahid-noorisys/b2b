<!--shop toolbar end-->
<div class="row">
    <div class="col-12">
        <div class="cart_page table-responsive">
            <table class="table">
                <?php if(!empty($transactions)):?>
                <thead>
                    <tr>
                        <th><?=display('Reference Number')?></th>
                        <th><?=display('Quotation Number')?></th>
                        <th><?=display('Amount')?></th>
                        <th><?=display('Status')?></th>
                        <th><?=display('Ordered')?></th>
                        <th><?=display('Action')?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($transactions as $v): ?>
                    <tr>
                        <td class="product_name"><?=$v->transaction_id?></td>
                        <td class="product_name"><?=$v->q_no?></td>
                        <td class="product-price"><?=$v->transaction_amount?></td>
                        <td class="product_quantity"><?=$v->status?></td>
                        <td class="product_quantity"><?php
                        $time = CodeIgniter\I18n\Time::parse($v->created_date);
                        echo $time->humanize();
                        ?></td>
                        <td class="product_total">
                            <a href="#"><?=display('View Details')?></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <?php else: ?>
                    <tr><?=display('No records found')?></tr>
                <?php endif; ?>
            </table>   
        </div>
        </div>
</div>
<div class="transaction_list">
<?=$pagination?>
</div>
<!--shop toolbar end-->