<style type="text/css">
.product_d_info {
    margin-bottom: 50px;
    background: #f6f6f6;
    padding: 50px 0;
}
.product_d_inner {
    padding: 20px 30px 50px;
    background: #ffffff;
}
.product_info_button {
    border-bottom: 1px solid #f0f0f0;
    padding-bottom: 15px;
    margin-bottom: 40px;
}
.product_info_button ul {
    justify-content: center;
}
.nav>li {
    position: relative;
    display: block;
}
.product_info_button ul li:last-child a {
    margin-right: 0;
}
.product_info_button ul li a.active {
    color: #333333;
}
.product_info_button ul li a {
    display: block;
    float: left;
    text-transform: capitalize;
    font-size: 20px;
    /*color: #a9a9a9;*/
    font-weight: 600;
    margin-right: 35px;
    line-height: 26px;
    position: relative;
    padding-bottom: 10px;
}
.tab-content > .tab-pane.active {
    /*display: block;*/
    height: auto;
    opacity: 1;
    overflow: visible;
}
.h3, h3 {
    font-size: 24px;
}
.h1, .h2, .h3, h1, h2, h3 {
    margin-top: 20px;
    margin-bottom: 10px;
}

ul {
    margin-top: 0;
    margin-bottom: 10px;
    list-style: none;
}
</style>
<div class="product_d_info">
    <div class="container">   
        <div class="row">
            <div class="col-12">
                <div class="product_d_inner">   
                    <div class="product_info_button">    
                        <ul class="nav" role="tablist">
                            <li >
                                <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false"><?=display('Terms & Conditions')?></a>
                            </li>
                            
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="info">
                            <div class="">
                                <h3>What is Lorem Ipsum?</h3>
                                   <!--  <li>SARL MECA-TROUVE </li>
                                    <li>106 rue des Fusains </li>
                                    <li>34750 Villeneuve-lès-Maguelone</li> -->
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta impedit velit maiores nemo perferendis facere a assumenda in sint illo provident pariatur ullam voluptates id eveniet optio neque perspiciatis dolores quod, quisquam! Repellendus alias laudantium nesciunt nostrum magnam debitis quidem aut temporibus expedita accusantium, illum ipsam eos, eveniet explicabo assumenda, laboriosam modi fugiat dolores dolor sit.
                                    </p>
                                
                                <h3>Why do we use it?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta impedit velit maiores nemo perferendis facere a assumenda in sint illo provident pariatur ullam voluptates id eveniet optio neque perspiciatis dolores quod, quisquam! Repellendus alias laudantium nesciunt nostrum magnam debitis quidem aut temporibus expedita accusantium, illum ipsam eos, eveniet explicabo assumenda, laboriosam modi fugiat dolores dolor sit.
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta impedit velit maiores nemo perferendis facere a assumenda in sint illo provident pariatur ullam voluptates id eveniet optio neque perspiciatis dolores quod, quisquam! Repellendus alias laudantium nesciunt nostrum magnam debitis quidem aut temporibus expedita accusantium, illum ipsam eos, eveniet explicabo assumenda, laboriosam modi fugiat dolores dolor sit.
                                </p>
                            </div>    
                        </div>            
                    </div>
                </div>     
            </div>
        </div>
    </div>
</div>