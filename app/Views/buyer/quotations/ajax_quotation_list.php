<!--shop toolbar end-->
<div class="row">
    <div class="col-12">
        <div class="cart_page table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th><?=display('Quotation Number')?></th>
                        <th><?=display('Total Items')?></th>
                        <th><?=display('Amount')?></th>
                        <th><?=display('Status')?></th>
                        <th><?=display('Updated')?></th>
                        <th><?=display('Action')?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if(!empty($quotations)):?>
                <?php foreach ($quotations as $v): ?>
                    <tr>
                        <td class="product_name"><?=$v->q_no?></td>
                        <td class="product_name"><?=$v->total_items?></td>
                        <td class="product-price"><?=$v->amount?></td>
                        <td class="product_quantity"><?php
                        $msg = display('Pending');
                        if($v->is_paid == 'Yes'){
                            $msg = display('Paid');
                        }
                        else if($v->is_accepted == 'Yes' AND $v->is_validated == 'Yes' AND $v->is_rejected == 'No'){
                            $msg = display('Accepted & Validated');
                        }
                        else if($v->is_accepted == 'Yes' AND $v->is_rejected == 'No'){
                            $msg = display('Accepted');
                        }
                        else if($v->is_rejected == 'Yes')
                        {
                            $msg = display('Rejected');
                        }
                        echo $msg;
                        ?></td>
                        <td class="product_total"><?php
                        $time = CodeIgniter\I18n\Time::parse($v->updated_date);
                        echo $time->humanize();
                        ?></td>
                        <td class="product_total">
                        <?php if($v->is_accepted == 'Yes' && $v->is_validated == 'Yes'): ?>
                            <?php if($v->is_paid == 'No'): ?>
                            <a href="<?=base_url('order-details/'.$v->id)?>"><i class="fa fa-money"></i></a>
                            <?php endif; ?>
                             &nbsp;
                            <a href="<?=base_url('messages/'.$v->id)?>"><i class="fa fa-paper-plane"></i></a>
                        <?php endif; ?>
                         &nbsp;
                          <a href="<?=base_url('quotations-info/'.$v->id)?>" title="view details"><i class="zmdi zmdi-info" style="font-size: 20px; margin-right: 10px;"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>   
        </div>
    </div>
</div>
<div class="quotation_list">
<?=$pagination?>
</div>