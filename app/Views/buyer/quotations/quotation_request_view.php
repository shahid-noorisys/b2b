<!--shopping cart area start -->
<div class="shopping_cart_area mt-60">
    <div class="container">  
        <?=form_open(base_url('buyer/submit-quote'),'')?>
            <div class="row">
                <div class="col-12">
                    <h3>Quotation Request</h3>
                    <div class="table_desc">
                        <div class="cart_page table-responsive">
                            <table>
                            <thead>
                                <tr>
                                    <th class="product_thumb"><?=display('Image')?></th>
                                    <th class="product_name"><?=display('Product Name')?></th>
                                    <th class="product_quantity"><?=display('Quantity')?></th>
                                    <th class="product_total"><?=display('Action')?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($product)): ?>
                                <input type="hidden" name="product_id" value="<?=$product->id?>">
                                <tr>
                                    <td class="product_image"><img src="<?=base_url($product->image_1)?>" alt="" class="img-fluid" style="width:60px; height: 60px;"></td>
                                    <td class="product_name"><?=$product->name?></td>
                                    <td class="product_quantity"><label><?=display('Quantity')?></label> <input min="1" max="100" value="1" type="number" name="quatity"></td>
                                    <td class="product_thumb"><a href="#"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>   
                        </div>  
                        <div class="cart_submit">
                            <button type="submit"><?=display('Submit')?></button>
                        </div>      
                    </div>
                    </div>
                </div>
            <!--coupon code area start-->
            <!-- <div class="coupon_area">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code left">
                            <h3>Coupon</h3>
                            <div class="coupon_inner">   
                                <p>Enter your coupon code if you have one.</p>                                
                                <input placeholder="Coupon code" type="text">
                                <button type="submit">Apply coupon</button>
                            </div>    
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code right">
                            <h3>Cart Totals</h3>
                            <div class="coupon_inner">
                                <div class="cart_subtotal">
                                    <p>Subtotal</p>
                                    <p class="cart_amount">£215.00</p>
                                </div>
                                <div class="cart_subtotal ">
                                    <p>Shipping</p>
                                    <p class="cart_amount"><span>Flat Rate:</span> £255.00</p>
                                </div>
                                <a href="#">Calculate shipping</a>

                                <div class="cart_subtotal">
                                    <p>Total</p>
                                    <p class="cart_amount">£215.00</p>
                                </div>
                                <div class="checkout_btn">
                                    <a href="#">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!--coupon code area end-->
        </form> 
    </div>     
</div>
<!--shopping cart area end -->