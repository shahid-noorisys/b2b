<?php if(!empty($reviews)):?>
    <?php foreach ($reviews as $v): ?>
        <?php $customer = get_user_details_by_id($v->buyer_id,3); ?>
        <div class="comments_box">
            <div class="comment_list">
                <div class="comment_thumb">
                    <img src="<?=(!empty($customer->thumbnail))?base_url($customer->thumbnail):base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="">
                </div>
                <div class="comment_content">
                    <div class="comment_meta">
                        <h5><a href="#"><?=$customer->firstname.' '.$customer->lastname?></a></h5>
                    </div>
                    <span><?=$time = CodeIgniter\I18n\Time::parse($v->created_date);?></span> 
                    <p><?=$v->review_msg?> </p>
                    <div class="comment_reply">
                        <?php $counter = $v->rating; ?>
                        <?php for ($i = 1 ; $i <= 5 ; $i++) { 
                            if ($counter >= $i) { ?>
                                <i class="zmdi zmdi-star text-warning"></i>
                            <?php } else { ?>
                                <i class="zmdi zmdi-star"></i>
                        <?php  }
                            } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="review_list">
<?=$pagination?>
</div>