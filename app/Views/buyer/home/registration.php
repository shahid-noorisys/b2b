<!-- Buyer Registration start -->
<div class="customer_login mt-60">
    <div class="container">
        <div class="row" style="justify-content: center;">
            <div class="account_form register" style="width: 70%">
                <h2><?=display('Register')?></h2>
                <?=form_open('onboard-buyer',' id="buyer_reg_form" onsubmit="return false;" ');?>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="firstname"><?=display('First Name')?></label>
                            <input type="text" name="firstname" class="form-control" id="firstname" placeholder="<?=display('First Name')?>">
                            <p class="text-danger error"></p>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="lastname"><?=display('Last Name')?></label>
                            <input type="text" name="lastname" class="form-control" id="lastname" placeholder="<?=display('Last Name')?>">
                            <p class="text-danger error"></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email"><?=display('Email')?></label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="<?=display('Valid Email ID')?>">
                            <p class="text-danger error"></p>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="mobile"><?=display('Mobile')?></label>
                            <input type="text" name="mobile" class="form-control mobile" id="mobile" placeholder="<?=display('Valid Mobile Number')?>">
                            <p class="text-danger error"></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="password"><?=display('Password')?></label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="<?=display('Password')?>">
                            <p class="text-danger error"></p>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="conf_password"><?=display('Confirm Password')?></label>
                            <input type="password" name="conf_password" class="form-control" id="conf_password" placeholder="<?=display('Confirm Password')?>">
                            <p class="text-danger error"></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="address"><?=display('Address')?></label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="<?=display('Full Address')?>">
                            <p class="text-danger error"></p>
                        </div>
                        <div class="form-group col-md-4">
                        <?php
                        $gender = [
                            ' ' => display('Select Gender'),
                            '1' => display('Male'),
                            '2' => display('Female')
                        ];
                        ?>
                            <label for="address"><?=display('Gender')?></label>
                            <?=form_dropdown('gender',$gender,'',' id="id_gender" class="form-control" ');?>
                            <p class="text-danger error"></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="city"><?=display('City')?></label>
                            <input type="text" name="city" class="form-control" id="city" placeholder="<?=display('City')?>">
                            <p class="text-danger error"></p>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="state"><?=display('State')?></label>
                            <input type="text" name="state" class="form-control" id="state" placeholder="<?=display('State')?>">
                            <p class="text-danger error"></p>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="country"><?=display('Country')?></label>
                            <?=form_dropdown('country', $countries, '','class="form-control" id="country" ');?>
                            <p class="text-danger error"></p>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="zip_code"><?=display('Zip Code')?></label>
                            <input type="text" name='zip_code' class="form-control" id="zip_code" placeholder="<?=display('Zip Code')?>">
                            <p class="text-danger error"></p>
                        </div>
                    </div>
                    <div class="login_submit">
                        <button type="submit" id="btn_reg"> <?=display('Register')?> </button>
                    </div>
                <?=form_close()?>
            </div>    
        </div>
    </div>    
</div>
<!-- Buyer Registration end -->

<script type="text/javascript">
$(document).ready(function () {
    $("#buyer_reg_form").submit(function (e) { 
        e.preventDefault(e);
    });

    $("#btn_reg").click(function (e) { 
        var firstname = $("#firstname");
        var lastname = $("#lastname");
        var email = $("#email");
        var mobile = $("#mobile");
        var password = $("#password");
        var conf_password = $("#conf_password");
        var address = $("#address");
        var city = $("#city");
        var state = $("#state");
        var country = $("#country");
        var zip_code = $("#zip_code");
        var gender = $("#id_gender");
        var valid = true;
        $(".error").html('');

        if($.trim(firstname.val()) == ''){
            firstname.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(lastname.val()) == ''){
            lastname.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(email.val()) == ''){
            email.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(mobile.val()) == ''){
            mobile.next().html("<?=display('Required')?>");
            valid = false;
        }
        else if(! validMobileNumber(mobile.val())){
            mobile.next().html("<?=display('Enter Valid mobile number')?>");
            valid = false;
        }
        if($.trim(password.val()) == ''){
            password.next().html("<?=display('Required')?>");
            valid = false;
        }
        else if(!validPasswordLength(password.val())){
            password.next().html("<?=display('Mininmum 8 chars required')?>");
            valid = false;
        }
        if($.trim(conf_password.val()) == ''){
            conf_password.next().html("<?=display('Required')?>");
            valid = false;
        }
        else if(!validConfirmPassword(password.val(),conf_password.val())){
            conf_password.next().html("<?=display('Confirm password not matched with password')?>");
            valid = false;
        }
        if($.trim(address.val()) == ''){
            address.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(city.val()) == ''){
            city.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(state.val()) == ''){
            state.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(country.find(":selected").val()) == ''){
            country.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(gender.find(":selected").val()) == ''){
            gender.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(zip_code.val()) == ''){
            zip_code.next().html("<?=display('Required')?>");
            valid = false;
        }
        if(valid){ $("#buyer_reg_form").get(0).submit(); }
    });
});
</script>