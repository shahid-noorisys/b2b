<!--contact area start-->
<div class="contact_area" style="margin-top: 25px;">
    <div class="container">   
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="contact_message content">
                    <h3>contact us</h3>    
                        <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram anteposuerit litterarum formas human. qui sequitur mutationem consuetudium lectorum. Mirum est notare</p>
                    <ul>
                        <li><i class="fa fa-fax"></i>  Address : No 40 Baria Sreet 133/2 NewYork City</li>
                        <li><i class="fa fa-phone"></i> <a href="#">Infor@roadthemes.com</a></li>
                        <li><i class="fa fa-envelope-o"></i> 0(1234) 567 890</li>
                    </ul>             
                </div> 
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="contact_message form">
                    <h3><?=display('Tell us your queries')?></h3>
                    <?= form_open('',' id="contact-form" ')?>
                        <p>  
                            <label> <?=display('Your Name (required)')?></label>
                            <input name="name" id="name" placeholder="<?=display('Name')?> *" type="text"> 
                            <span class="error text-danger"></span>
                        </p>
                        <p>       
                            <label>  <?=display('Your Email (required)')?></label>
                            <input name="email" id="email" placeholder="<?=display('Email')?> *" type="email">
                            <span class="error text-danger"></span>
                        </p>
                        <p>          
                            <label>  <?=display('Subject')?></label>
                            <input name="subject" id="subject" placeholder="<?=display('Subject')?> *" type="text">
                            <span class="error text-danger"></span>
                        </p>    
                        <div class="contact_textarea">
                            <label>  <?=display('Your Message')?></label>
                            <textarea placeholder="<?=display('Message')?> *" name="message" id="message" class="form-control2" ></textarea>
                            <span class="error text-danger"></span>
                        </div>   
                        <button type="submit" id="btn_send"> <?=display('Send')?></button>  
                        <p class="form-messege"></p>
                    </form> 

                </div> 
            </div>
        </div>
    </div>    
</div>
<!--contact area end-->

<script>
    $(document).ready(function () {
        $("#contact-form").submit(function (e) { 
            e.preventDefault();
        });
        $("#btn_send").click(function (e) { 
            e.preventDefault();
            var name = $("#name");
            var email = $("#email");
            var subject = $("#subject");
            var message = $("#message");
            $(".error").html("");
            var valid = true;
            if($.trim(name.val()) == '')
            {
                name.next().html("<?=display('Required')?>");
                valid = false;
            }
            if($.trim(email.val()) == '')
            {
                email.next().html("<?=display('Required')?>");
                valid = false;
            }
            if($.trim(subject.val()) == '')
            {
                subject.next().html("<?=display('Required')?>");
                valid = false;
            }
            if($.trim(message.val()) == '')
            {
                message.next().html("<?=display('Required')?>");
                valid = false;
            }
            if(valid){ $("#contact-form").get(0).submit();}
        });
    });
</script>