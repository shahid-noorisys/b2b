<div class="row no-gutters shop_wrapper grid_list">
    <?php foreach ($products as $k => $p): ?>
    <div class=" col-12 ">
        <div class="single_product">
            <div class="product_thumb">
                <a href="<?=base_url('product-view/'.$p->id)?>"><img src="<?=base_url($p->image_1)?>" alt="" style="height:238px;"></a>
                <!-- <div class="label_product">
                    <span class="label_sale">sale</span>
                </div> -->
                <div class="quick_button">
                    <a href="<?=base_url('product-view/'.$p->id)?>" data-toggle="modal" data-target="#modal_box"  title="quick view" onclick="productDetails(this,event)" data-product_id="<?=$p->id?>"> <i class="zmdi zmdi-eye"></i></a>
                </div>
            </div>	
            <div class="product_content grid_content">
                <div class="product_name">
                    <h3><a href="#"><?=$p->name?></a></h3>
                </div>
                <!-- <div class="product_rating">
                    <ul>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    </ul>
                </div> -->
                <div class="price_box">
                    <span class="current_price"></span>
                    <span class="old_price"></span>   
                </div>
                <div class="action_links">
                    <ul>
                        <!-- <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li> -->
                        <li class="add_to_cart"><a href="" onclick="addToCart(this,event)" title="add to cart" data-product_id="<?=$p->id?>"><i class="zmdi zmdi-shopping-cart-plus"></i> add to cart</a></li>
                        <!-- <li class="compare"><a href="#" title="compare"><i class="zmdi zmdi-swap"></i></a></li> -->
                    </ul>
                </div>
            </div>
            <div class="product_content list_content">
                <div class="product_name">
                    <h3><a href="#"><?=$p->name?></a></h3>
                </div>
                <!-- <div class="product_rating">
                    <ul>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    </ul>
                </div> -->
                    <div class="price_box">
                    <span class="current_price"></span>
                    <span class="old_price"></span>   
                </div>
                <div class="action_links">
                    <ul>
                        <!-- <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li> -->
                        <li class="add_to_cart"><a href="" onclick="addToCart(this,event)" title="add to cart" data-product_id="<?=$p->id?>"><i class="zmdi zmdi-shopping-cart-plus"></i> <?=display('add to cart')?></a></li>
                        <!-- <li class="compare"><a href="#" title="compare"><i class="zmdi zmdi-swap"></i></a></li> -->
                    </ul>
                </div>

                <div class="product_desc">
                    <p><?=$p->description?></p>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>

<div class="main_product_pagination">
    <?=$pagination?>
</div>
<!--shop toolbar end-->
<!--shop wrapper end-->