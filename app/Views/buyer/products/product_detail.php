 <!--product details start-->
<div class="product_details mt-50 mb-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
               <div class="product-details-tab">
                    <div id="img-1" class="zoomWrapper single-zoom">
                        <a href="#">
                            <img id="zoom1" src="<?=base_url($product->image_1)?>" data-zoom-image="<?=base_url($product->image_1)?>" alt="big-1">
                        </a>
                    </div>
                    <div class="single-zoom-thumb">
                        <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
                            <li>
                                <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?=base_url($product->image_1)?>" data-zoom-image="<?=base_url($product->image_1)?>">
                                    <img src="<?=base_url($product->image_1)?>" alt="zo-th-1"/>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?=base_url($product->image_2)?>" data-zoom-image="<?=base_url($product->image_2)?>">
                                    <img src="<?=base_url($product->image_2)?>" alt="zo-th-1"/>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?=base_url($product->image_3)?>" data-zoom-image="<?=base_url($product->image_3)?>">
                                    <img src="<?=base_url($product->image_3)?>" alt="zo-th-1"/>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?=base_url($product->image_4)?>" data-zoom-image="<?=base_url($product->image_4)?>">
                                    <img src="<?=base_url($product->image_4)?>" alt="zo-th-1"/>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?=base_url($product->image_5)?>" data-zoom-image="<?=base_url($product->image_5)?>">
                                    <img src="<?=base_url($product->image_5)?>" alt="zo-th-1"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product_d_right">
                   <form action="#">
                       
                        <h1><?=$product->name?></h1>
                       <!--  <div class="product_nav">
                            <ul>
                                <li class="prev"><a href="product-details.html"><i class="fa fa-angle-left"></i></a></li>
                                <li class="next"><a href="product-grouped.html"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div> -->
                        <div class="product_rating">
                            <ul>
                                <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                            </ul>
                        </div>
                        <div class="price_box">
                            <span class="current_price">$70.00</span>
                            <span class="old_price">$80.00</span>
                            
                        </div>
                        <div class="product_desc">
                            <p><?=$product->description?></p>
                        </div>
                        <div class="action_links">
                            <ul>
                                <li class="add_to_cart"><a href="#" title="add to cart" onclick="addToCart(this,event)" data-product_id="<?=$product->id?>"><i class="zmdi zmdi-shopping-cart-plus"></i> <?=display('Add to Cart')?></a></li>
                                <li class="add_to_cart">
                                    <a href="<?=base_url('buyer/quotation-request/'.$product->id)?>" title="Request a quote"><i class="zmdi zmdi-shopping-cart-plus"></i> <?=display('Request to Quote')?></a>
                                </li>
                            </ul>
                        </div>
                        <!-- <div class="product_meta">
                            <span>Category: <a href="#">Clothing</a></span>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>    
</div>
<!--product details end-->
<script type="text/javascript">
  $(document).ready(function() {
      $('.single-product-active').owlCarousel({
        autoplay: true,
        loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 4,
        margin:15,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
        responsive:{
            0:{
            items:1,
          },
                320:{
            items:2,
          },
                992:{
            items:3,
          },
                1200:{
            items:4,
          },
         }
     });  
  });
  $("#zoom1").elevateZoom({
        gallery:'gallery_01', 
        responsive : true,
        cursor: 'crosshair',
        zoomType : 'inner'
    
    });  
</script>