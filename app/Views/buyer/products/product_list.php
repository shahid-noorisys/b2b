<style>
    .sub_category_filter:hover {
        color: red;
    }
</style>
<!--shop  area start-->
<div class="shop_area shop_reverse mt-50 mb-50">
    <div class="container">
        <div class="row">
            <!-- <div class="col-lg-3 col-md-12">
                <aside class="sidebar_widget">
                    <div class="widget_list widget_categories">
                        <h2><?= display('Filters') ?></h2>
                        <div id="accordion" class="card__accordion">
                            <?php if(!empty($categories)): ?>
                            <?php foreach ($categories as $i => $c):  
                                $subCatModel = model("App\Models\admin\Subcategories_model");
                                $subCategories = $subCatModel->where('category_id',$c->id)->where('status','Active')->where('deleted','No')->findAll();
                            ?>
                            <div class="card card_dipult" style="margin-bottom: 0 !important;">
                                <div class="card-header card_accor" id="heading_<?=($i+1)?>" style="background: none !important;">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_<?=($i+1)?>" aria-expanded="true" aria-controls="collapse_<?=($i+1)?>" style="border: none !important;background: none !important;">
                                    <?= $c->name.' ('.COUNT($subCategories).')' ?>
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>

                                    </button>

                                </div>
                                
                                <div id="collapse_<?=($i+1)?>" class="collapse" aria-labelledby="heading_<?=($i+1)?>" data-parent="#accordion">
                                <?php if(!empty($subCategories)): ?>
                                <?php foreach ($subCategories as $si => $sc): ?>
                                <div class="card-body" style="padding: 2px 0px 2px 20px !important;">
                                    <a href="<?= base_url('list-product/'.$sc->id) ?>" class="sub_category_filter" data-sub_category_id="<?=$sc->id?>"><?= $sc->name ?></a>
                                </div>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                </aside>
            </div> -->
            <div class="col-lg-12 col-md-12">
                <!--shop wrapper start-->
                <!--shop toolbar start-->
                <div class="shop_title">
                    <h1><?=display('Product List')?></h1>
                </div>
                <div class="shop_toolbar_wrapper">
                    <div class="shop_toolbar_btn">

                        <button data-role="grid_3" type="button" class=" btn-grid-3 btn_product_count" data-toggle="tooltip" title="3" data-product_count="12" onclick="mainProductListStyle($(this),event)"></button>

                        <button data-role="grid_4" type="button"  class=" btn-grid-4 btn_product_count" data-toggle="tooltip" title="4" data-product_count="16" onclick="mainProductListStyle($(this),event)"></button>

                        <button data-role="grid_list" type="button"  class="active btn-list btn_product_count" data-toggle="tooltip" title="List" data-product_count="4" onclick="mainProductListStyle($(this),event)"></button>

                    </div>
                    <!-- <div class=" niceselect_option">

                        <form class="select_option" action="#">
                            <select name="orderby" id="short">

                                <option selected value="1">Sort by average rating</option>
                                <option  value="2">Sort by popularity</option>
                                <option value="3">Sort by newness</option>
                                <option value="4">Sort by price: low to high</option>
                                <option value="5">Sort by price: high to low</option>
                                <option value="6">Product Name: Z</option>
                            </select>
                        </form>


                    </div> -->
                    <div class="page_amount">
                        <p id="prduct_list_counter">Showing 1–9 of 21 results</p>
                    </div>
                </div>
                <!--shop toolbar end-->

                <div id="main_product_list"></div>
            </div>
        </div>
    </div>
</div>
<!--shop  area end-->
<script>
    
</script>