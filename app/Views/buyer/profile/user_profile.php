<style>
    .account_form p{
        margin-bottom: 8px;
    }
    .account_form .form-group{
        margin-bottom: 0;
    }
    .account_form .form-group input{
        margin-bottom: 0;
    }
    .add-btn button {
        background: #ea000d;
        border: 0;
        color: #ffffff;
        display: inline-block;
        font-size: 12px;
        font-weight: 700;
        height: 34px;
        line-height: 21px;
        padding: 5px 20px;
        text-transform: uppercase;
        cursor: pointer;
        -webkit-transition: 0.3s;
        transition: 0.3s;
        border-radius: 20px;
    }
    .comment_thumb img{
        max-width: 40px;
        border-radius: 50%;
    }
    .comment_list .comment_content {
        margin-left: 55px !important;
        position: relative;
        border: 1px solid #ebebeb;
        border-radius: 3px;
        padding: 15px;
    }
    .comments_box {
        margin-bottom: 20px !important;
    }
    .comment_reply {
        position: absolute;
        top: 18px !important;
        right: 12px !important;
        font-size: 19px !important;
    }
    .edit{
        float: right !important;
    }
    .edit i {
        font-size: 20px;
        margin-right: 10px;
    }
    .pagination ul li.current a {
        background: #ea000d;
        color: #ffffff;
    }
    .pagination ul li a {
        color: #000000;
    }
</style>
<!-- my account start  -->
<section class="main_content_area">
    <div class="container">   
        <div class="account_dashboard">
            <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <!-- Nav tabs -->
                    <div class="dashboard_tab_button">
                        <ul role="tablist" class="nav flex-column dashboard-list">
                            <li><a href="#dashboard" data-toggle="tab" class="nav-link active"><?=display('Profile')?></a></li>
                            <li><a href="#account-details" data-toggle="tab" class="nav-link"><?=display('Edit Profile')?></a></li>
                            <li><a href="#password-change" data-toggle="tab" class="nav-link"><?=display('Change Password')?></a></li>
                            <li><a href="#company-profile" data-toggle="tab" class="nav-link"><?=display('Company Profile')?></a></li>
                            <li><a href="#bill_address" data-toggle="tab" class="nav-link"><?=display('Addresses')?></a></li>
                            <li> <a href="#orders" data-toggle="tab" class="nav-link"><?=display('Orders')?></a></li>
                            <li> <a href="#transactions" data-toggle="tab" class="nav-link"><?=display('Transactions')?></a></li>
                            <li> <a href="#reviews" data-toggle="tab" class="nav-link"><?=display('Review & Ratings')?></a></li>
                            <li><a href="<?=base_url('logout-buyer')?>" class="nav-link"><?=display('Logout')?></a></li>
                        </ul>
                    </div>    
                </div>
                <div class="col-sm-12 col-md-9 col-lg-9">
                    <!-- Tab panes -->
                    <div class="tab-content dashboard_content">
                        <div class="tab-pane fade show active" id="dashboard">
                            <div class="row">
                            <?php
                            $img = ($user AND $user->thumbnail != null) ? base_url($user->thumbnail) : base_url('public/assets/img/no-image.jpg');?>
                                <div class="col-md-4 col-lg-4 col-sm-5">
                                    <img class="rounded mx-auto img-thumbnail" src="<?=$img?>" style="max-height: 225px;">
                                <?=form_open_multipart(base_url('buyer/profile-image-update'),' class="account_form mt-10" ')?>
                                <input type="hidden" name="user_id" value="<?=$user->id?>">
                                <input type="hidden" name="old_file_name" value="<?=$user->thumbnail?>">
                                    <input type="file" name="thumbnail" id="thumbnail" style="border: none;padding: 0;">
                                    <button type="submit" style="margin: 0;width:85%"><?=display('Upload')?></button>
                                <?=form_close()?>
                                </div>
                                <div class="col-md-8 col-lg-8 col-sm-7">
                                    <p><strong><?=display('Email')?> : </strong> <?=($user->email) ?? ''?> </p>
                                    <p><strong><?=display('Mobile')?> : </strong> <?=($user->mobile) ?? ''?> </p>
                                    <p><strong><?=display('Gender')?> : </strong> <?=($user->gender) ?? 'N/A' ?></p>
                                    <p><strong><?=display('City')?> : </strong> <?=($user->city) ?? 'N/A' ?></p>
                                    <p><strong><?=display('State')?> : </strong> <?=($user->state) ?? 'N/A' ?></p>
                                    <p><strong><?=display('Country')?> : </strong> <?=($user->country) ?? 'N/A' ?></p>
                                    <!-- <p><strong><?//display('Primary Address')?> : </strong>
                                        <span class="tags"></span> 
                                        <span class="tags">css3</span>
                                        <span class="tags">jquery</span>
                                        <span class="tags">bootstrap3</span>
                                    </p> -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="account-details">
                            <?=form_open(base_url('buyer/profile-update'),' class="account_form" id="profileForm" ')?>
                            <input type="hidden" name="user_id" value="<?=$user->id?>">
                                <div class="form-group row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-radio">
                                            <span class="custom-radio"><input type="radio" value="1" name="id_gender" class="id_gender" <?=($user AND $user->gender == 'Male') ? 'checked' : ''; ?>> <?=display('Mr.')?></span>
                                            <span class="custom-radio"><input type="radio" value="2" name="id_gender" class="id_gender" <?=($user AND $user->gender == 'Female') ? 'checked' : ''; ?>> <?=display('Mrs.')?></span>
                                        </div>
                                        <p class="error text-danger"></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4 col-sm-6">
                                        <p><?=display('First Name')?></p>
                                        <input type="text" class="form-control" name="firstname" id="firstname" value="<?=$user->firstname ?? ''?>" placeholder="<?=display('First Name')?>">
                                        <p class="error text-danger"></p>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <p><?=display('Last Name')?></p>
                                        <input type="text" class="form-control" name="lastname" id="lastname" value="<?=$user->lastname ?? ''?>" placeholder="<?=display('Last Name')?>" >
                                        <p class="error text-danger"></p>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <p><?=display('City')?></p>
                                        <input type="text" class="form-control" name="city" id="city" value="<?=$user->city ?? ''?>" placeholder="<?=display('City')?>">
                                        <p class="error text-danger"></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-4 col-sm-6">
                                        <p><?=display('State')?></p>
                                        <input type="text" class="form-control" name="state" id="state" value="<?=$user->state ?? ''?>" placeholder="<?=display('State')?>">
                                        <p class="error text-danger"></p>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <p><?=display('Country')?></p>
                                        <?=form_dropdown('country',$countries,($user->country_id ?? ''),' class="form-control" id="country" ');?>
                                        <p class="error text-danger"></p>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <p><?=display('Zip Code')?></p>
                                        <input type="text" class="form-control" name="zip_code" id="zip_code" value="<?=$user->zip_code ?? ''?>" placeholder="<?=display('Zip Code')?>">
                                        <p class="error text-danger"></p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6 col-xs-6">
                                        <p><?=display('Mobile')?></p>
                                        <input type="text" class="form-control" name="mobile" id="mobile" value="<?=$user->mobile?>" placeholder="<?=display('Valid Mobile Number')?>">
                                        <p class="error text-danger"></p>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <div class="login_submit" style="margin-top: 40px;float: left;">
                                            <button type="submit" class="buttonsfdsdfds" id="btn_update"> <?=display('Save')?> </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane fade" id="company-profile">
                            <h3><?=display('Company Profile')?></h3>
                            <?php if(!empty($company)):?>
                                <form action="" class="ord_form account_form mb-20">
                                    <div class="form-group row text-right">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <button type="button" data-user="<?=$company->user_id?>" data-company="<?=$company->id?>" class="company-edit" id="company_btn_submit"><?=display('Edit company profile')?></button>
                                        </div>
                                    </div>
                                </form> 
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card" style="padding: 10px;">
                                            <p><strong><?=display('Company Name')?> : </strong> <?=($company->company_name) ?? ''?></p>
                                            <p><strong><?=display('Company Email')?> : </strong> <?=($company->company_email) ?? ''?> </p>
                                            <p><strong><?=display('Mobile')?> : </strong> <?=($company->contact) ?? ''?> </p>
                                            <p><strong><?=display('City')?> : </strong> <?=($company->city) ?? 'N/A' ?></p>
                                            <p><strong><?=display('Country')?> : </strong> <?=($company->country) ?? 'N/A' ?></p>
                                            <p><strong><?=display('Address')?> : </strong> <?=($company->address) ?? 'N/A' ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php else:?>
                                <form action="" class="ord_form account_form mb-20">
                                    <div class="form-group row text-right">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <button type="button" data-toggle="modal" data-target="#new_company_profile_modal_box" id="company_btn_submit"><?=display('Add company profile')?></button>
                                        </div>
                                    </div>
                                </form>
                            <?php endif;?>
                        </div>

                        <div class="tab-pane fade" id="password-change">
                            <div class="login">
                                <div class="login_form_container">
                                    <div class="account_login_form col-md-6">
                                        <?=form_open(base_url('buyer/profile-password-change'),' class="account_form" id="changePasswordForm" ')?>
                                        <input type="hidden" name="user_id" value="<?=$user->id?>">
                                            <div class="form-group">
                                                <label><?=display('Old Password')?></label>
                                                <input type="password" name="old_password" id="old_password" class="form-control" placeholder="<?=display('Old Password')?>">
                                                <p class="error text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <label><?=display('New Password')?></label>
                                                <input type="password" name="new_password" id="new_password" class="form-control" placeholder="<?=display('New Password')?>">
                                                <p class="error text-danger"></p>
                                            </div>
                                            <div class="form-group">
                                                <label><?=display('Confirm Password')?></label>
                                                <input type="password" name="conf_password" id="conf_password" class="form-control" placeholder="<?=display('Confirm Password')?>">
                                                <p class="error text-danger"></p>
                                            </div>
                                            <div class="login_submit">
                                                <button type="submit" id="btn_change_pass"><?=display('Save')?></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="bill_address">
                            <h3><?=display('Billing address')?></h3>
                             <form action="" class="ord_form account_form mb-20">
                                <div class="form-group row text-right">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <button type="button" data-toggle="modal" data-target="#new_address_modal_box" id="ord_btn_submit"><?=display('Add new shipping addresss')?></button>
                                    </div>
                                </div>
                            </form>
                            <div id="ajax_address_list">
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="orders">
                            <h4><?=display('Search')?></h4>
                            <form action="" class="ord_form account_form mb-20">
                                <input type="hidden" name="ord_page_no" id="ord_page_no">
                                <div class="form-group row">
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <!-- <p><?//display('Reference Number')?></p> -->
                                        <input type="text" name="order_no" id="order_no" class="form-control" placeholder="<?=display('By').' '.display('Order Number')?>">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <!-- <p><?//display('Status')?></p> -->
                                        <input type="text" name="ord_status" id="ord_status" class="form-control" placeholder="<?=display('By').' '.display('Status')?>">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <button type="submit" id="ord_btn_submit"><?=display('Submit')?></button>
                                        <button type="submit" class="clear_filter_btn" id="ord_btn_clear_filter"><?=display('Clear')?></button>
                                    </div>
                                </div>
                            </form>
                            <div id="ajax_order_list"></div>
                        </div>
                        <div class="tab-pane fade" id="transactions">
                            <h4><?=display('Search')?></h4>
                            <form action="" class="trn_form account_form mb-20">
                                <input type="hidden" name="trn_page_no" id="trn_page_no">
                                <div class="form-group row">
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <!-- <p><?//display('Reference Number')?></p> -->
                                        <input type="text" name="order_no" id="reference_no" class="form-control" placeholder="<?=display('By').' '.display('Reference Number')?>">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <!-- <p><?//display('Status')?></p> -->
                                        <input type="text" name="trn_quote_no" id="trn_quote_no" class="form-control" placeholder="<?=display('By').' '.display('Quotation Number')?>">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <button type="submit" id="trn_btn_submit"><?=display('Submit')?></button>
                                        <button type="submit" class="clear_filter_btn" id="trn_btn_clear_filter"><?=display('Clear')?></button>
                                    </div>
                                </div>
                            </form>
                            <div id="ajax_transaction_list"></div>
                        </div>

                        <div class="tab-pane fade" id="reviews">
                            <h4 style="margin-bottom: 25px;"><?=display('Review & Ratings')?></h4>
                            <div id="ajax_review_list"></div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>        	
</section>			
<!-- my account end   --> 
<script>
$(function () {
    // $('#company_profile_add_form').hide();
    // $('#company_profile_edit_form').hide();
    // $("#btn_company").on('click', function(e){
    //     $('#company_profile_add_form').show();
    //     $('.confirmation').hide();
    // });
    // $("#edit_btn_company").on('click', function(e){
    //     $('#company_profile_edit_form').show();
    //     $('.company-profile').hide();
    // });
   $("#profileForm").submit(function (e) {
       e.preventDefault();
   });
   $("#changePasswordForm").submit(function (e) {
       e.preventDefault();
   });
   $("#btn_update").click(function (e) {
       e.preventDefault();
       var firstname = $("#firstname");
        var lastname = $("#lastname");
        var mobile = $("#mobile");
        var city = $("#city");
        var state = $("#state");
        var country = $("#country");
        var zip_code = $("#zip_code");
        var valid = true;
        $(".error").html('');

        if($.trim(firstname.val()) == ''){
            firstname.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(lastname.val()) == ''){
            lastname.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(mobile.val()) == ''){
            mobile.next().html("<?=display('Required')?>");
            valid = false;
        }
        else if(! validMobileNumber(mobile.val())){
            mobile.next().html("<?=display('Enter Valid mobile number')?>");
            valid = false;
        }
        if($.trim(city.val()) == ''){
            city.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(state.val()) == ''){
            state.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(country.find(":selected").val()) == ''){
            country.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(zip_code.val()) == ''){
            zip_code.next().html("<?=display('Required')?>");
            valid = false;
        }
        if(valid){ $("#profileForm").get(0).submit(); }
   });
   $("#btn_change_pass").click(function (e){
       e.preventDefault();
       var old_pass = $("#old_password");
       var new_pass = $("#new_password");
       var conf_pass = $("#conf_password");

       var valid = true;
       $(".error").html('');

       if($.trim(old_pass.val()) == ''){
            old_pass.next().html("<?=display('Required')?>");
            valid = false;
        }
       if($.trim(new_pass.val()) == ''){
            new_pass.next().html("<?=display('Required')?>");
            valid = false;
        }
        else if(!validPasswordLength(new_pass.val())){
            new_pass.next().html("<?=display('Mininmum 8 chars required')?>");
            valid = false;
        }
        if($.trim(conf_pass.val()) == ''){
            conf_pass.next().html("<?=display('Required')?>");
            valid = false;
        }
        else if(!validConfirmPassword(new_pass.val(),conf_pass.val())){
            conf_pass.next().html("<?=display('Confirm password not matched with password')?>");
            valid = false;
        }
        if(valid){ $("#changePasswordForm").get(0).submit(); }
   });
});
</script>