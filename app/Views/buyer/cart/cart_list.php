<!--shopping cart area start -->
<div class="shopping_cart_area mt-60">
    <div class="container">  
        <?=form_open(base_url('update-cart'),'')?>
            <div class="row">
                <div class="col-12">
                    <div class="shop_title">
                        <h1>Cart List</h1>
                    </div>
                    <div class="table_desc">
                        <div class="cart_page table-responsive">
                        <table>
                        <thead>
                            <tr>
                                <th class="product_remove"><?=display('Remove')?></th>
                                <th class="product_thumb"><?=display('Product Image')?></th>
                                <th class="product_name"><?=display('Product Name')?></th>
                                <!-- <th class="product-price">Price</th> -->
                                <th class="product-price"><?=display('Supplier Name')?></th>
                                <th class="product_quantity"><?=display('Quantity')?></th>
                                <!-- <th class="product_total"><?=display("Total")?></th> -->
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($items)): ?>
                        <input type="hidden" name="temp_id" value="<?=$items['temp_id']?>">
                        <?php foreach (json_decode($items['product_json']) as $item): ?>
                        <?php $productModel = model('App\Models\ProductModel');
                        $product = $productModel->find($item->product_id);
                        $userModel = model('App\Models\UserModel');
                        $supplier =  $userModel->find($items['supplier_id']);
                        // print_r($product);exit;
                        ?>
                            <tr>
                                <td clas="product_remove"><a href="#" data-product_id="<?=$item->product_id?>" onclick="removeFromCart(this,event)" id="remove_from_cart"><i class="fa fa-trash-o"></i></a></td>
                                <td class="product_thumb"><a href="#"><img src="<?=base_url($product->image_1)?>" alt=""></a></td>
                                <td class="product_name"><a href="#"><?=$product->name?></a></td>
                                <!-- <td class="product-price">£65.00</td> -->
                                <td class="product-price"><?=(! empty($supplier))?$supplier->firstname.' '.$supplier->lastname:''?></td>
                                <td class="product_quantity"><label><?=display('Quantity')?></label> <input min="1" max="100" value="<?=$item->quantity?>" type="number" name="quantity[]">
                                <input type="hidden" name="product_id[]" value="<?=$item->product_id?>">
                                </td>
                                <!-- <td class="product_total">£130.00</td> -->
                            </tr>
                        <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                        </table> 
                        </div>  
                        <div class="cart_submit">
                            <button type="submit" id="update_cart"><?=display('Update Cart')?></button>
                            <button type="button" id="request_to_quote"><?=display('Request to Quote')?></button>
                        </div>     
                    </div>
                    </div>
                </div>
                <!--coupon code area start-->
            <!-- <div class="coupon_area">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code left">
                            <h3>Coupon</h3>
                            <div class="coupon_inner">   
                                <p>Enter your coupon code if you have one.</p>                                
                                <input placeholder="Coupon code" type="text">
                                <button type="submit">Apply coupon</button>
                            </div>    
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code right">
                            <h3>Cart Totals</h3>
                            <div class="coupon_inner">
                                <div class="cart_subtotal">
                                    <p>Subtotal</p>
                                    <p class="cart_amount">£215.00</p>
                                </div>
                                <div class="cart_subtotal ">
                                    <p>Shipping</p>
                                    <p class="cart_amount"><span>Flat Rate:</span> £255.00</p>
                                </div>
                                <a href="#">Calculate shipping</a>

                                <div class="cart_subtotal">
                                    <p>Total</p>
                                    <p class="cart_amount">£215.00</p>
                                </div>
                                <div class="checkout_btn">
                                    <a href="#">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!--coupon code area end-->
        </form>
        <?=form_open(base_url('add-to-quote'),'class="form_quote"');?>
        <input type="hidden" name="temp_id" value="<?=(isset($items['temp_id']) AND $items['temp_id'] != '')?$items['temp_id']:''?>">
        <?=form_close();?>
    </div>     
</div>
<!--shopping cart area end -->

<script>
    $(document).ready(function () {
        $("#request_to_quote").click(function(e){
            e.preventDefault();
            $(".form_quote").get(0).submit();
        });
    });
</script>