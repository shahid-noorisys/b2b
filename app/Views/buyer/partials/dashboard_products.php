<div class="deals_product_list" id="latest_product_list">
    <?php if(!empty($products)): ?>
    <?php foreach($products as $i => $p): ?>
    <div class="single_deals_product">
        <div class="product_thumb">
            <a href="<?=base_url('product-view/'.$p->id)?>" target="_blank"><img src="<?=($p->image_1 AND file_exists($p->image_1)) ? base_url($p->image_1) : NO_IMAGE?>" alt=""></a>
            <!-- <div class="label_product">
                <span class="label_sale">sale</span>
            </div> -->
            <div class="quick_button">
                <a href="" data-toggle="modal" data-target="#modal_box"  title="quick view" onclick="productDetails(this,event)" data-product_id="<?=$p->id?>"> <i class="zmdi zmdi-eye"></i></a>
            </div>
            <!-- <div class="product_timing">
                <div data-countdown="2030/12/15"></div>
            </div> -->
        </div>
        <div class="product_content">
            <div class="product_name">
                <h3><a href="#"><?=$p->name?></a></h3>
            </div>
            <!-- <div class="product_rating">
                <ul>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                </ul>
            </div> -->
                <!-- <div class="price_box">
                <span class="current_price">$65.00</span>
                <span class="old_price">$70.00</span>    
            </div> -->
            <div class="action_links">
                <ul>
                    <!-- <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li> -->
                    <li class="add_to_cart"><a href="#" title="add to cart" onclick="addToCart(this,event)" data-product_id="<?=$p->id?>"><i class="zmdi zmdi-shopping-cart-plus"></i> <?=display('Add to Cart')?></a></li>
                    <li class="add_to_cart"><a href="<?=base_url('buyer/quotation-request/'.$p->id)?>" title="add to cart"><i class="zmdi zmdi-shopping-cart-plus"></i> <?=display('Request to Quote')?></a></li>
                    <!-- <li class="compare"><a href="#" title="compare"><i class="zmdi zmdi-swap"></i></a></li> -->
                </ul>
            </div>
        </div>
    </div>
    <!-- <div class="single_deals_product">
        <div class="product_thumb">
            <a href="product-details.html"><img src="public/assets/img/product/product2.jpg" alt=""></a>
            <div class="label_product">
                <span class="label_sale">sale</span>
            </div>
            <div class="quick_button">
                <a href="#" data-toggle="modal" data-target="#modal_box"  title="quick view"> <i class="zmdi zmdi-eye"></i></a>
            </div>
            <div class="product_timing">
                <div data-countdown="2030/12/15"></div>
            </div>
        </div>
        <div class="product_content">
            <div class="product_name">
                <h3><a href="product-details.html">Driven Backpack	</a></h3>
            </div>
            <div class="product_rating">
                <ul>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                </ul>
            </div>
                <div class="price_box">
                <span class="current_price">$65.00</span>
                <span class="old_price">$70.00</span>   
            </div>
            <div class="action_links">
                <ul>
                    <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                    <li class="add_to_cart"><a href="cart.html" title="add to cart"><i class="zmdi zmdi-shopping-cart-plus"></i> add to cart</a></li>
                    <li class="compare"><a href="#" title="compare"><i class="zmdi zmdi-swap"></i></a></li>
                </ul>
            </div>
        </div>
    </div> -->
    <?php endforeach; ?>
    <?php endif; ?>
</div>