<?php if(!empty($products)): ?>
    <?php foreach($products as $key => $p): ?>
        <div class="single_product">
            <div class="product_thumb">
                <a href="<?=base_url('product-view/'.$p->id)?>" target="_blank"><img src="<?=($p->image_1 AND file_exists($p->image_1)) ? base_url($p->image_1) : NO_IMAGE?>" alt=""></a>
                <!-- <div class="label_product">
                    <span class="label_sale">sale</span>
                </div> -->
                <div class="quick_button">
                    <a href="" data-toggle="modal" data-target="#modal_box"  title="quick view" onclick="productDetails(this,event)" data-product_id="<?=$p->id?>"> <i class="zmdi zmdi-eye"></i></a>
                </div>
            </div>
            <div class="product_content">
                <div class="product_name">
                    <h3><a href="#"><?=$p->name?></a></h3>
                </div>
                <!-- <div class="product_rating">
                    <ul>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-star-outline"></i></a></li>
                    </ul>
                </div>
                    <div class="price_box">
                    <span class="current_price">$65.00</span>
                    <span class="old_price">$70.00</span>   
                </div> -->
                <div class="action_links">
                    <ul>
                        <li class="add_to_cart"><a href="#" title="add to cart" onclick="addToCart(this,event)" data-product_id="<?=$p->id?>"><i class="zmdi zmdi-shopping-cart-plus"></i> <?=display('Add to Cart')?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

