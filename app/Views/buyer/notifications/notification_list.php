<div class="container" style="margin-top: 20px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3><?=display('Notifications')?></h3>
            <?php if(!empty($notifications)): ?>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?=display('Title')?></th>
                            <th><?=display('Description')?></th>
                            <th><?=display('Time')?></th>
                            <th><?=display('Read Status')?></th>	 	 	 	
                        </tr>
                    </thead>
                    <tbody>
                    <?php $sl = 1; ?>
                    <?php foreach ($notifications as $note): ?>
                        <tr>
                            <td><?=$sl?></td>
                            <td><?=$note->title?></td>
                            <td><?=$note->description?></td>
                            <td><?php
                            $time = CodeIgniter\I18n\Time::parse($note->created_date);
                            echo $time->humanize();
                            ?></td>
                            <td>
                            <?php if($note->read_status == 'Unread'): ?>
                            <span class="badge badge-danger"><?=$note->read_status?></span></td>
                            <?php else: ?>
                            <span class="badge badge-success"><?=$note->read_status?></span></td>
                            <?php endif; ?>
                        </tr>
                    <?php $sl++; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <?php else: ?>
            <div class="font-weight-bold"><?=display('No records found')?></div>
            <?php endif; ?>
        </div>
    </div>
    <div class=""><?=$pagination?></div>
</div>