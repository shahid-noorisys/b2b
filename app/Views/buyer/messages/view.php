<link rel="stylesheet" href="<?= base_url('public/assets/css/reset.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('public/assets/css/chat.css') ?>">
<style type="text/css">
  #frame #sidepanel #contacts ul li.contact .wrap span {
    position: absolute;
    /* top: -3px; */
    left: 269px;
    margin: -2px 0 0 -2px;
    width: 27px;
    height: 27px;
    border-radius: 50%;
    border: 2px solid #2c3e50;
    background: #2ec46f;
    color: #fff;
    font-size: 10px;
    padding-top: 6px;
}
</style>
<?php 
        $uri = service('uri');
        $quotation_id = $uri->getSegment(2);
        $user_role = session()->get('user_details')->user_role;
?>
<div class="container" style="margin: 30px auto;">
  <div id="frame">
    <div id="sidepanel">
      <div id="profile">
        <div class="wrap">
          <img id="profile-img" src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" class="online" alt="" />
          <p><?=session()->get('user_details')->fullname?></p>
        </div>
      </div>
      <div id="search">
        <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
        <input type="text" onkeyup="searchQuotation()" id="searchquote" placeholder="Search quotation..."  autocomplete="off" />
      </div>
      <div id="contacts">
        <ul class="chatonline" id="chat_list">
          <?php 
              foreach ($quotation as $key => $value) { 
              $active = ($quotation_id == $value->id)?'active':'';
              $quote = model('App\Models\QuotationModel');
              $quotation = $quote->find($value->id);

              $message = model('App\Models\MessageModel');
              $response = $message->where('q_id',$value->id)->orderBy('created_date','DESC')->limit(1)->find();
              $msg_count = $message->where(['q_id'=>$value->id,'read_status'=>'No','user_role'=>$user_role])->findAll();
          ?>
          <li class="contact get-message <?=$active?>" data-quotation="<?=$value->id;?>" data-sender ="<?=session()->get('user_details')->user_id?>" id="get_message_<?=$value->id;?>">
              <div class="wrap">
                <img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="" />
                <div class="meta">
                  <p class="name"><?=$quotation->q_no?> <?=(!empty($msg_count))?'<span class="badge badge-danger ml-auto counter">'.count($msg_count).'</span>':''?></p>
                  <?php if (!empty($response)):?>
                    <?php foreach ($response as $value): ?>
                      <p class="preview"><?=($value->send_by == 'Buyer')?display('You'):display('yahodehime')?> : <?=$value->text_msg?></p>
                    <?php endforeach;?>
                  <?php else:?>
                    <p class="preview"><?=display('There is no message to display')?></p>
                  <?php endif;?>
                </div>
              </div>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
    <div class="content">
      <div class="contact-profile">
        <img src="<?=base_url('public/assets/admin/images/nophoto_user_icon.png')?>" alt="" />
        <p>Chat Message</p>
        <div class="social-media">
        </div>
      </div>
      <div class="messages">
        <ul class="msg_history">
         
        </ul>
      </div>
      <div class="message-input">
          <?php echo form_open('', 'class ="form-horizontal" autocomplete="off"'); ?>
            <div class="wrap">
              <input type="hidden" name="quotesion_id" id="quotesion_id" value="">
              <div class="row">
                <div class="col-md-10">
                  <input type="text" placeholder="Write your message..." class="message" />
                </div>
                <div class="col-md-1">
                  <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                    <input type="file" name="avatar" class="upload avatar" onchange="attachement_file_name(event);" />
                </div>
                <div class="col-md-1">
                  <button type="button" class="btn-message"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function attachement_file_name(e) { 
        $(".attached_file").addClass('text-success').text(e.target.files[0].name); 
    }

    function searchQuotation() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("searchquote");
        filter = input.value.toUpperCase();
        ul = document.getElementById("chat_list");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("div")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }

    function getmessagelist(){
        var selected_quotation = $('.chatonline').find('li.active').attr('id');
        var quotation_id = $('#'+selected_quotation).data('quotation');
        var supplier_id = $('#'+selected_quotation).data('supplier');
        var token = "<?= csrf_hash() ?>";
        // alert(quotation_id);
        $.ajax({
             url: "<?php echo base_url('get-messages'); ?>",
             type: 'POST',
             dataType: 'html',
             data: {quotation_id:quotation_id,supplier_id:supplier_id,csrf_stream_token:token},
        }).done(function(data) {
            // console.log(data);
            $('.msg_history').html(data);
            $('#quotesion_id').val(quotation_id);
        });
        
    }


    $(function () {

        getmessagelist();
        $(".attachment").on('click', function(e) { e.preventDefault(); 
            $(".avatar").trigger('click'); 
        });

        function validate_message(message) {
            if(message=="") { return; }
            var file = $('.avatar').prop('files')[0];
            var formData = new FormData();
            var token = "<?= csrf_hash() ?>";
            var quotation_id = $('#quotesion_id').val();
            
            formData.append("message",message);
            formData.append("quotation_id",quotation_id);
            formData.append("file_name",file);
            formData.append("csrf_stream_token",token);
    
            $.ajax({
                 url: "<?php echo base_url('send-message'); ?>",
                 type: 'POST',
                 dataType: 'html',
                 data: formData,
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
            }).done(function(data) {
                // console.log(data);
                $('.msg_history').empty().append(data);
                $( "#chat_list" ).load(window.location.href + " #chat_list" );
                 
            });
        };

        $('.btn-message').click(function(e) {
            var message = $('.message').val();
            $(".message").val('');
            return validate_message(message);  
        });

        $(document).on('click','.get-message',function(e) {
            
            var selected_quotation = $('.chatonline').find('li.active').attr('id');
            $('#'+selected_quotation).removeClass('active');
            
            var quotation_id = $(this).data('quotation');
            var sender = $(this).data('sender');
            var token = "<?= csrf_hash() ?>";

            $.ajax({
                 url: "<?php echo base_url('get-messages'); ?>",
                 type: 'POST',
                 dataType: 'html',
                 data: {quotation_id:quotation_id,sender:sender,csrf_stream_token:token},
            }).done(function(data) {
                // console.log(data);
                $('.msg_history').html(data);
                $('#quotesion_id').val(quotation_id);
                $('#get_message_'+quotation_id).addClass('active');
                $( "#chat_list" ).load(window.location.href + " #chat_list" );
            });

        });
    });
</script>