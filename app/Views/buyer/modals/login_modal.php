<style type="text/css">
#partitioned {
  padding-left: 6px;
  letter-spacing: 40px;
  border: 0;
  border-bottom: 1px solid #000;
  /*background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);*/
  background-position: bottom;
  background-size: 50px 1px;
  background-repeat: repeat-x;
  background-position-x: 35px;
  width: 310px;
  margin-bottom: 20px;
}
.splitter {
  padding: 0 5px;
  color: #151515;
  font-size: 24px;
}
.prompt {
  margin-bottom: 20px;
  font-size: 20px;
  color: white;
}
</style>
<!-- login modal area start-->
<div class="modal fade" id="login_modal_box" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="left:91%">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body">
                <div class="container">
                <p class="text-success hidden" id="alert_msg"></p>
                    <div class="row" id="login_popup">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="account_form">
                                <h2><?=display('Login')?></h2>
                                <!-- <form action="<?//base_url('login-buyer')?>" method="post" id="login_form">
                                <?//csrf_field()?> -->
                                    <p>   
                                        <label> <?=display('Email')?> <span>*</span></label>
                                        <input type="text" name="email" id="login_email">
                                        <span class="text-danger error"></span>
                                    </p>
                                    <p>   
                                        <label> <?=display('Password')?> <span>*</span></label>
                                        <input type="password" name="password" id="login_password">
                                        <span class="text-danger error"></span>
                                    </p>   
                                    <div class="login_submit">
                                    <a href="#" id="forget_password_link"><?=display('Lost your password?')?></a>
                                        <label for="remember">
                                            <input id="remember" type="checkbox">
                                            <?=display('Remember me')?>
                                        </label>
                                        <button type="submit" id="btn_login"><?=display('Login')?></button>
                                        
                                    </div>

                                <!-- </form> -->
                            </div>    
                        </div>    
                    </div>
                    <!-- Forgot Password -->
                    <div class="row" id="forget_password_popup">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="account_form">
                                <h2><?=display('Forgot Password')?></h2>
                                <p class="text-success hidden" id="alert_msg" role="alert"></p>
                                <!-- <form action="<?//base_url('login-buyer')?>" method="post" id="login_form">
                                <?//csrf_field()?> -->
                                    <p>   
                                        <label> <?=display('Email')?> <span>*</span></label>
                                        <input type="email" name="email" id="forgot_pass_email" placeholder="<?=display('Enter Email Address to get password')?>">
                                        <span class="text-danger error"></span>
                                    </p>  
                                    <div class="login_submit">
                                    <a href="#" id="login_link"><?=display('Login').'?'?></a>
                                        <button type="submit" id="btn_forgot_pass_submit"><?=display('Submit')?></button>
                                        
                                    </div>

                                <!-- </form> -->
                            </div>    
                        </div>    
                    </div>
                    <div class="row" id="otp_popup">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="account_form digit-group text-center">
                                <h2><?=display('OTP verification')?></h2>
                                <div class="col-xs-12 ">
                                   <input class="otp_code" name="otp" id="partitioned" type="text" maxlength="6" />
                                </div>
                                <span class="error text-danger" id="error_otp"></span>
                                <div class="login_submit text-center">
                                    <button type="submit" id="btn_otp"><?=display('Verify')?></button>
                                </div>
                                <p class="text-center" style="margin-top: 20px;">   
                                    <?=display('Did not receive the OTP ?')?> <b id="timer"></b> 
                                    <a href="javascript:void(0)" class="text-info m-l-5 resend-otp"><?=display('Resend OTP')?></a>
                                </p>   
                            </div>    
                        </div>    
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
<!-- login modal area end-->
<script type="text/javascript">

    let timerOn = true;
    function timer(remaining) {
      var m = Math.floor(remaining / 60);
      var s = remaining % 60;
      
      m = m < 10 ? '0' + m : m;
      s = s < 10 ? '0' + s : s;
      document.getElementById('timer').innerHTML = m + ':' + s;
      remaining -= 1;
      if(remaining >= 0 && timerOn) {
        setTimeout(function() {
            timer(remaining);
        }, 1000);
        return;
      }

      if(!timerOn) {
        return;
      }

      $('#timer').hide();
      $('.resend-otp').show();
      var token = "<?= csrf_hash() ?>";
      $.ajax({
             url: "<?php echo base_url('otp-reset'); ?>",
             type: 'POST',
             dataType: 'json',
             data: {
                    csrf_stream_token:token
                   },
        }).done(function(data) {
            console.log(data);
            if (data.status == 'success') {
               console.log(data.message);
            } else {
               console.log(data.message);
            }
        });
    }

$(document).ready(function () {

    $('.resend-otp').hide();

    $(document).on("click", "#btn_login", function (e) { 
        $("#alert_msg").addClass('hidden');
        $("#alert_msg").removeAttr('class');
        $("#alert_msg").html('');
        e.preventDefault();
        var email = $.trim($("#login_email").val());
        var password = $("#login_password").val();
        var valid = true;
        $(".error").html('');
        if(email == ''){
            $("#login_email").next().html("<?=display('Required')?>");
            valid = false;
        }
        if(password == ''){
            $("#login_password").next().html("<?=display('Required')?>");
            valid = false;
        }

        if(valid) { 
            // alert(password+'---------'+email);
            // $("#login_form").get(0).submit(); 
            $.ajax({
                url: "<?=base_url('login-buyer')?>",
                type: "POST",
                dataType: "json",
                data: {'<?=csrf_token()?>': '<?= csrf_hash() ?>',email:email,password:password},
                success: function (data) {
                    // console.log(data);
                    if(data.status == 'success'){
                        $("#alert_msg").html(data.message);
                        $("#alert_msg").addClass('text-success');
                        $("#alert_msg").removeClass('hidden');
                        $("#login_popup").hide("slow","swing");
                        $("#otp_popup").show();
                        timer(90);
                    } else {
                        $("#alert_msg").html(data.message);
                        $("#alert_msg").addClass('text-danger');
                        $("#alert_msg").removeClass('hidden');                       
                    }
                },
                error: function(data) {
                    $("#alert_msg").html("Action not allowed");
                    $("#alert_msg").addClass('text-danger');
                    $("#alert_msg").removeClass('hidden');
                    
                }
            });
        }

    });

    $(document).on("click", "#btn_forgot_pass_submit", function (e){
        $("#alert_msg").addClass('hidden');
        $("#alert_msg").removeAttr('class');
        $("#alert_msg").html('');
        var email = $("#forgot_pass_email");
        var valid = true;
        $(".error").html('');
        if($.trim(email.val()) == '')
        {
            email.next().html("The email field is required");
            valid = false;
        }
        else if(! validEmail($.trim(email.val())))
        {
            email.next().html("The email is not valid email");
            valid = false;
        }
        if(valid)
        {
            $.ajax({
                type: "post",
                url: "<?=base_url('password-forgot')?>",
                data: {email: $.trim(email.val()), '<?=csrf_token()?>': '<?=csrf_hash()?>'},
                dataType: "json",
                success: function (data) {
                    // console.log(data);
                    if(data.status == 'success'){
                        $("#alert_msg").html(data.message);
                        $("#alert_msg").addClass('text-success');
                        $("#alert_msg").removeClass('hidden');
                        setTimeout(function(){
                            window.location.reload();
                        }, 1500);
                    } else {
                        $("#alert_msg").html(data.message);
                        $("#alert_msg").addClass('text-danger');
                        $("#alert_msg").removeClass('hidden');
                        // setTimeout(function(){
                        //     window.location.reload();
                        // }, 1000);
                    }
                },
                error: function(data) {
                    $("#alert_msg").html("<?=display('Action not allowed')?>");
                    $("#alert_msg").addClass('text-danger');
                    $("#alert_msg").removeClass('hidden');
                    // setTimeout(function(){
                    //     window.location.reload();
                    // }, 1000);
                    // console.log(data.message);
                }
            });
        }
    });

    $(document).on("click", "#btn_otp", function(e){
        $("#alert_msg").addClass('hidden');
        $("#alert_msg").removeAttr('class');
        $("#alert_msg").html('');
        var otp = $(".otp_code");
        var valid = true;
        $(".error").html('');
        if($.trim($(".otp_code").val()) == '')
        {
            $('#error_otp').html("Please enter OTP");
            valid = false;
        }
        if(valid)
        {
            $.ajax({
                type: "post",
                url: "<?=base_url('verification-otp')?>",
                data: {OTP: $.trim($(".otp_code").val()), '<?=csrf_token()?>': '<?=csrf_hash()?>'},
                dataType: "json",
                success: function (data) {
                    // console.log(data);
                    if(data.status == 'success'){
                        $("#alert_msg").html(data.message);
                        $("#alert_msg").addClass('text-success');
                        $("#alert_msg").removeClass('hidden');
                        setTimeout(function(){
                            window.location.reload();
                        }, 2000);
                    } else {
                        $("#alert_msg").html(data.message);
                        $("#alert_msg").addClass('text-danger');
                        $("#alert_msg").removeClass('hidden');
                    }
                },
                error: function(data) {
                    $("#alert_msg").html("<?=display('Action not allowed')?>");
                    $("#alert_msg").addClass('text-danger');
                    $("#alert_msg").removeClass('hidden');
                }
            });
        }
    });
    
    $('.resend-otp').on('click', function(e){
          e.preventDefault();
          var token = "<?= csrf_hash() ?>";
          $.ajax({
                 url: "<?php echo base_url('customer-otp-resend'); ?>",
                 type: 'POST',
                 dataType: 'json',
                 data: {csrf_stream_token:token},
            }).done(function(data) {
                // console.log(data);
                if (data.status == 'success') {
                    $("#alert_msg").html(data.message);
                    $("#alert_msg").addClass('text-success');
                    $("#alert_msg").removeClass('hidden');
                    $('.resend-otp').hide();
                    $('#timer').show();
                    timer(90);

                } else {
                   $('.resend-otp').show();
                    $("#alert_msg").html(data.message);
                    $("#alert_msg").addClass('text-danger');
                    $("#alert_msg").removeClass('hidden');
                }
            });
    });
});

   
</script>