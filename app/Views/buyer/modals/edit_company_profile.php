<!-- add billing address modal area start-->
<div class="modal fade" id="edit_company_profile_modal_box" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="left:91%">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body">
                <div class="container">
                <p class="text-success hidden" id="alert_msg"></p>
                    <div class="row" id="login_popup">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="account_form">
                                <h2><?=display('Edit company profile')?></h2>
                                <?=form_open(base_url('buyer/company-profile-edit'),' id="company_profile_edit_form" ')?>
                                        <input type="hidden" name="user_id" id="user_id" value="">
                                        <input type="hidden" name="company_id" id="company_id" value="">
                                        <label> <?=display('Company Name')?> <span>*</span></label>
                                        <input type="text" class="form-control" name="company_name" id="edit_company_name" placeholder="<?=display('Company Name')?>">
                                        <p class="error text-danger"></p>
                                    
                                        <label> <?=display('Company Email')?> <span>*</span></label>
                                        <input type="text" class="form-control" name="company_email" id="edit_company_email" placeholder="<?=display('Company Email')?>" >
                                        <p class="error text-danger"></p>
                                    
                                        <label><?=display('Mobile')?> <span>*</span></label>
                                        <input type="text" class="form-control" name="mobile" id="com_mobile" placeholder="<?=display('Valid Mobile Number')?>">
                                        <p class="error text-danger"></p>

                                        <!-- <label> <?=display('Country')?> <span>*</span></label>
                                        <?php
                                        $country = model('App\Models\Countries_model');
                                        $country_list = $country->findAll();
                                        $list[' '] = display('Select Country');
                                        foreach ($country_list as $v) {
                                            $list[$v->id] = $v->name;
                                        }
                                        echo 
                                        form_dropdown('company_country', $list, '',' class="form-control" id="company_country" ');
                                        ?>
                                        <p class="text-danger error" id="err_address_country"></p> -->
                                    
                                        <label> <?=display('City')?> <span>*</span></label>
                                        <input type="text" class="form-control" name="city" id="com_city" placeholder="<?=display('City')?>">
                                        <p class="error text-danger"></p>

                                        <label> <?=display('Address')?> <span>*</span></label>
                                        
                                        <textarea name="company_address" id="edit_company_address" rows="2" class="form-control" placeholder="<?=display('Home,street,landmark and Area')?>"></textarea>
                                        <p class="error text-danger"></p>
                                    
                                    <div class="login_submit">
                                        <button type="submit" id="edit_company_btn"><?=display('Submit')?></button>
                                    </div>

                                </form>
                            </div>    
                        </div>    
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
<!-- add billing address modal area end-->

<script type="text/javascript">
$(document).ready(function () {
    $("#address_form").submit(function (e){
        e.preventDefault();
    });
    $(document).on("click", "#edit_company_btn", function (e) { 
        e.preventDefault();
        var company_name = $("#edit_company_name");
        var company_email = $("#edit_company_email");
        var mobile = $("#com_mobile");
        var city = $("#com_city");
        // var company_country = $("#company_country");
        var company_address = $("#edit_company_address");
        var valid = true;
        $(".error").html('');
        if($.trim(company_name.val()) == ''){
            company_name.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(company_email.val()) == ''){
            company_email.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(mobile.val()) == ''){
            mobile.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(city.val()) == ''){
            city.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(company_address.val()) == ''){
            company_address.next().html("<?=display('Required')?>");
            valid = false;
        }
        // if($.trim(company_country.find(":selected").val()) == ''){
        //     $("#err_address_country").html("<?=display('Required')?>");
        //     valid = false;
        // }
        if(valid){
            $("#company_profile_edit_form").get(0).submit();
        }
    });
});
</script>