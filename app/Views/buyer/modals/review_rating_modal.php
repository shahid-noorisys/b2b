<style type="text/css">
.star-rating {
direction: rtl;
}
.star-rating input[type=radio] {
    display: none
}
.star-rating label {
    color: #bbb;
    cursor: pointer;
    font-size: 30px;
    margin-bottom: 0px;
    padding: 0;
    -webkit-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out
}
.star-rating label:hover,
.star-rating label:hover ~ label,
.star-rating input[type=radio]:checked ~ label {
    color: #f2b600
}
.m-t-10 {
    margin-top: 20px;
}
</style>

<div class="modal fade" id="review_rating" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="left:91%">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body">
                <div class="container">
                <p class="text-success hidden" id="alert_msg"></p>
                    <div class="row" id="login_popup">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="account_form">
                                <h2><?=display('Add Review & Ratings')?></h2>
                                <?=form_open(base_url('review-add'),' id="reviews_form" ')?>
                                        <input type="hidden" name="order_id" id="order_id" value="">
                                        <input type="hidden" name="order_rating" id="order_rating" value="0">
                                        <div class="form-group">
                                            <label><?=display('Over all Ratings')?></label>
                                            <div class="star-rating rate">
                                                <input class="rates" id="star5" type="radio" name="rates" value="5">
                                                <label for="star5" title="5 stars">
                                                    <i class="active zmdi zmdi-star"></i>
                                                </label>
                                                <input class="rates" id="star4" type="radio" name="rates" value="4">
                                                <label for="star4" title="4 stars">
                                                    <i class="active zmdi zmdi-star"></i>
                                                </label>
                                                <input class="rates" id="star3" type="radio" name="rates" value="3">
                                                <label for="star3" title="3 stars">
                                                    <i class="active zmdi zmdi-star"></i>
                                                </label>
                                                <input class="rates" id="star2" type="radio" name="rates" value="2">
                                                <label for="star2" title="2 stars">
                                                    <i class="active zmdi zmdi-star"></i>
                                                </label>
                                                <input class="rates" id="star1" type="radio" name="rates" value="1">
                                                <label for="star1" title="1 star">
                                                    <i class="active zmdi zmdi-star"></i>
                                                </label>
                                            <p class="text-danger error_rating error"></p>
                                            </div>
                                        </div>
                                    
                                        <label class="m-t-20"> <?=display('Your Review')?> <span>*</span></label>
                                        <textarea name="review" id="review" rows="5" class="form-control" placeholder="<?=display('Enter Your Review Here......')?>"></textarea>
                                        <p class="text-danger error" id="err_review"></p>
                                        <div class="login_submit m-t-10 text-center">
                                            <button type="submit" id="review_rate_btn"><?=display('Submit')?></button>
                                        </div>
                                </form>
                            </div>    
                        </div>    
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#reviews_form").submit(function (e){
            e.preventDefault();
        });
        $(document).on("click", "#review_rate_btn", function (e) { 
            e.preventDefault();
            var full_name = $("#full_name");
            var review = $("#review");
            var order_rating = $("#order_rating");
            var valid = true;
            $(".error").html('');
            
            if($.trim(review.val()) == ''){
                review.next().html("<?=display('Required')?>");
                valid = false;
            }  
            if($.trim(order_rating.val()) == 0){
                $('.error_rating').html("<?=display('Required')?>");
                valid = false;
            }
            
            if(valid){
                $("#reviews_form").get(0).submit();
            }
        });

        $(document).on("click",".rates",function(){
           $('#order_rating').val($("input[name = 'rates']:checked").val());
        });
    });
</script>