<!-- add billing address modal area start-->
<div class="modal fade" id="edit_address_modal_box" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="left:91%">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal_body">
                <div class="container">
                <p class="text-success hidden" id="alert_msg"></p>
                    <div class="row" id="login_popup">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="account_form">
                                <h2><?=display('Edit Billing Address')?></h2>
                                <?=form_open(base_url('address-edit'),' id="edit_address_form" ')?>
                                        <input type="text" name="user_id" id="edit_user_id" value="" class="d-none">
                                        <input type="text" name="address_id" id="edit_address_id" value="" class="d-none">
                                        <label> <?=display('Name')?> <span>*</span></label>
                                        <input type="text" name="full_name" id="edit_full_name" class="form-control" placeholder="<?=display('Full Name')?>">
                                        <p class="text-danger error" id="err_full_name" value=""></p>
                                    
                                        <label> <?=display('Address')?> <span>*</span></label>
                                        <!-- <input type="text" name="new_address" id="login_password"> -->
                                        <textarea name="new_address" id="edit_new_address" rows="2" class="form-control" placeholder="<?=display('Home,street,landmark and Area')?>" value=""></textarea>
                                        <p class="text-danger error" id="err_new_address"></p>
                                    
                                        <label> <?=display('City')?> <span>*</span></label>
                                        <input type="text" name="address_city" id="edit_address_city" class="form-control" placeholder="<?=display('City')?>" value="">
                                        <p class="text-danger error" id="err_address_city"></p>
                                    
                                        <label> <?=display('State')?> <span>*</span></label>
                                        <input type="text" name="address_state" id="edit_address_state" class="form-control" placeholder="<?=display('State')?>" value="">
                                        <p class="text-danger error" id="err_address_state"></p>

                                        <!-- <label> <?=display('Country')?> <span>*</span></label> -->
<!--                                         <?php
                                        $country = model('App\Models\Countries_model');
                                        $country_list = $country->findAll();
                                        $list[' '] = display('Select Country');
                                        foreach ($country_list as $v) {
                                            $list[$v->id] = $v->name;
                                        }
                                        echo 
                                        form_dropdown('address_country', $list, '',' class="form-control" id="address_country" ');
                                        ?> -->
                                        <!-- <p class="text-danger error" id="err_address_country"></p> -->
                                    
                                        <label> <?=display('Zip Code')?> <span>*</span></label>
                                        <input type="text" name="address_zip_code" id="edit_address_zip_code" class="form-control select2" placeholder="<?=display('Zip Code')?>" value="">
                                        <p class="text-danger error" id="err_address_zip_code"></p>
                                    <!-- </p> -->
                                    <div class="login_submit">
                                        <button type="submit" id="edit_address_btn"><?=display('Submit')?></button>
                                    </div>

                                </form>
                            </div>    
                        </div>    
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>
<!-- add billing address modal area end-->

<script type="text/javascript">
$(document).ready(function () {
    $("#address_form").submit(function (e){
        e.preventDefault();
    });
    $(document).on("click", "#edit_address_btn", function (e) { 
        e.preventDefault();
        var full_name = $("#edit_full_name");
        var new_address = $("#edit_new_address");
        var address_city = $("#edit_address_city");
        var address_state = $("#edit_address_state");
        // var address_country = $("#edit_address_country");
        var address_zip_code = $("#edit_address_zip_code");
        var valid = true;
        $(".error").html('');
        if($.trim(full_name.val()) == ''){
            full_name.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(new_address.val()) == ''){
            new_address.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(address_city.val()) == ''){
            address_city.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(address_state.val()) == ''){
            address_state.next().html("<?=display('Required')?>");
            valid = false;
        }
        if($.trim(address_zip_code.val()) == ''){
            address_zip_code.next().html("<?=display('Required')?>");
            valid = false;
        }
        // if($.trim(address_country.find(":selected").val()) == ''){
        //     $("#err_address_country").html("<?=display('Required')?>");
        //     valid = false;
        // }
        if(valid){
            $("#edit_address_form").get(0).submit();
        }
    });
});
</script>