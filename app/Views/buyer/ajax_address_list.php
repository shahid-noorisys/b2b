<div class="row">
    <?php if(!empty($address)):?>
        <?php foreach ($address as $v): ?>
            <?php $customer = get_user_details_by_id($v->user_id,3); ?>
            <div class="col-lg-4 col-md-6">
                <div class="single_services">
                    <div class="services_thumb">
                        <img src="<?=base_url('public/assets/img/navigation-map.jpg')?>" alt="">
                    </div>
                    <?php if ($v->is_primary == 'Yes'):?>
                        <div class="label_product">
                            <span class="label_sale">Primary</span>
                        </div>
                    <?php endif; ?>
                    <div class="services_content">
                           <h3><?=$v->full_name?> <a class="edit edit-address" data-address="<?=$v->id?>" data-user="<?=$user_id?>"><i class="zmdi zmdi-edit"></i></a>  </h3>
                            <address>
                                <?=$v->address?><br>
                                <?=$v->city.' ('.$v->zip_code.')'?><br>
                                <?=$v->state?> <br>
                                <?=$v->country?> <br>
                            </address>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<div class="address_list">
    <?=$pagination?>
</div>