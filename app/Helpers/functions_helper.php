<?php

if(! function_exists('display')){
    function display($str = null)
    {
        if($str == null){ return null; }
        return lang('Validation.'.$str);
    }
}

if(!function_exists('get_category_by_id')) {
    function get_category_by_id($category_id){
        $db = Config\Database::connect();
        $category = model('App\Models\admin\Categories_model',false,$b);
        return $category->find($category_id);
    }
}

if(!function_exists('get_user_details_by_id')) {
    function get_user_details_by_id($admin_id,$user_role){
        $db = Config\Database::connect();
        if ($user_role == 1 ) {
            $admin = model('App\Models\AdminModel',false,$b);
            return $admin->find($admin_id);    
        } else {
            $user = model('App\Models\UserModel',false,$b);
            return $user->find($admin_id);
        }
    }
}

if(!function_exists('get_country_by_id')) {
    function get_country_by_id($country_id){
        $db = Config\Database::connect();
        $country = model('App\Models\Countries_model',false,$b);
        return $country->find($country_id);
    }
}

if(!function_exists('get_sub_category_by_id')) {
    function get_sub_category_by_id($sub_category_id){
        $db = Config\Database::connect();
        $category = model('App\Models\admin\Subcategories_model',false,$b);
        return $category->find($sub_category_id);
    }
}
if(!function_exists('get_sub_category_by_category_id')) {
    function get_sub_category_by_category_id($category_id){
        $db = Config\Database::connect();
        $category = model('App\Models\admin\Subcategories_model',false,$b);
        return $category->where('category_id',$category_id)->where('status','Active')->findAll();
    }
}

if(!function_exists('get_company_by_user_id')) {
    function get_company_by_user_id($user_id){
        $db = Config\Database::connect();
        $company = model('App\Models\UserCompanyModel',false,$b);
        return $company->where('user_id',$user_id)->first();
    }
}

if(!function_exists('get_last_message_by_quotation')) {
    function get_last_message_by_quotation($quotation_id){
        $db = Config\Database::connect();
        $message = model('App\Models\MessageModel');
        return $message->where('q_id',$quotation_id)->orderBy('created_date','DESC')->limit(1)->find();
    }
}

if(!function_exists('get_unread_notification_by_user_id')) {
    function get_unread_notification_by_user_id($admin_id,$admin_role){
        $db = Config\Database::connect();
        $notificationModel = model('App\Models\NotificationModel');
        return $notificationModel->where(['user_id'=>$admin_id,'user_role'=>$admin_role,'read_status'=>2])->limit(5)->findAll();
    }
}