<?php

use Config\Services;

if(! function_exists('loggedIn')){
    function loggedIn(){
        $session = Services::session();
        $user = $session->get('user_details') ?? null;
        if($user AND $user->isLoggedIn){ return true; }
        return false; 
    }
}
if(! function_exists('allowEdit')){
    function allowEdit($field){
        $session = Services::session();
        $user = $session->get('user_details') ?? null;
        if($user AND ($user->email === $field OR $user->user_id === $field)){ return true; }
        return false; 
    }
}