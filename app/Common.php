<?php

use App\Libraries\Invoice;

/**
 * The goal of this file is to allow developers a location
 * where they can overwrite core procedural functions and
 * replace them with their own. This file is loaded during
 * the bootstrap process and is called during the frameworks
 * execution.
 *
 * This can be looked at as a `master helper` file that is
 * loaded early on, and may also contain additional functions
 * that you'd like to use throughout your entire application
 *
 * @link: https://codeigniter4.github.io/CodeIgniter4/
 */
	
if (! function_exists('cart'))
{
    /**
	 * Grabs a cart library and returns it to the user.
	 *
	 * If $getShared === false then a new cart instance will be provided,
	 * otherwise it will all calls will return the same instance.
	 *
	 * @param boolean $getShared
	 *
	 * @return \app\Libraries\Cart
	 */
    function cart(bool $getShared = true)
    {
        return \Config\Services::cart($getShared);
    }
}

if (! function_exists('getCounter'))
{
	/**
	 * A convenience method that provides access to the Cache
	 * object. If no parameter is provided, will return the object,
	 * otherwise, will attempt to return the cached value.
	 *
	 * Examples:
	 *    cache()->save('foo', 'bar');
	 *    $foo = cache('bar');
	 *
	 * @param string|null $type
	 *
	 * @return string|null
	 */
	function getCounter(string $type = null)
	{
		if (is_null($type))
		{
			return null;
        }
        $db = Config\Database::connect();
        $counterModel = model('App\Models\PlatformCounterModel',false,$b);
        $db->transStart();
        $counter = $counterModel->getWhere(['type' => strtoupper($type)])->getRow();
        $c = $counter->counter_value+1;
        $counterModel->where('type',strtoupper($type))->set(['counter_value' => $c])->update();
        $db->transComplete();
        return $counter->prefix.str_pad($c,$counter->min_length,$counter->symb_to_append,STR_PAD_LEFT);
	}
}


if (! function_exists('getReceiptInvoice'))
{
	/**
	 * A convenience method that provides access to the Cache
	 * object. If no parameter is provided, will return the object,
	 * otherwise, will attempt to return the cached value.
	 *
	 * Examples:
	 *    cache()->save('foo', 'bar');
	 *    $foo = cache('bar');
	 *
	 * @param string|null $type
	 *
	 * @return string|null
	 */
	function getReceiptInvoice(array $data )
	{
		if (!is_array($data))
		{
			return null;
        }
        $db = Config\Database::connect();
        $counterModel = model('App\Models\ReceiptModel',false,$b);
        $db->transStart();
	       	$invoice = new Invoice(); 
	       	$invoice->InvoicePDF($data);
        $db->transComplete();
        
	}
}

if (! function_exists('getAllCategories'))
{
	/**
	 * A convenience method that provides all the sub categories list
	 * from the database
	 *
	 * @param void
	 *
	 * @return array|null
	 */
	function getAllCategories()
	{
        $db = Config\Database::connect();
        $counterModel = model('App\Models\admin\Categories_model',false,$b);
		return $counterModel->findAll();
	}
}