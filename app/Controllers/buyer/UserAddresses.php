<?php
namespace App\Controllers\buyer;

use App\Models\Countries_model;
use App\Models\UserAddressModel;
use App\Controllers\BaseController;

class UserAddresses extends BaseController
{
    private $userAddrModel = null;
    private $country = null;
    public function __construct()
    {
        $this->userAddrModel = new UserAddressModel();
        $this->country = new Countries_model();
    }
    public function ajaxAddressList()
    {
        $pageNo = $this->request->getGet('page');
        $data['user_id'] = $user_id = $this->session->get('user_details')->user_id;

        if($pageNo == '' || $pageNo == null){ $pageNo = 1;}

        $data['address'] = $this->userAddrModel->where('user_id',$user_id)->orderBy('created_date','DESC')->paginate(3);
        $total = $this->userAddrModel->where('user_id',$user_id)->countAllResults();
        
        $pager = $this->userAddrModel->pager;
        $data['pagination'] = $pager->makeLinks($pageNo,3,$total,'buyer_pager');
        $response = ['html' => view('buyer/ajax_address_list',$data),'page_no' => $pageNo];
        echo json_encode($response);
    }
    public function getAddressdetails()
    {
        if($this->request->getMethod() == 'post')
        {
            $user_id = $this->request->getPost('user_id');
            if(! allowEdit($user_id)){
                $this->session->setFlashData('exception',display('You are not logged in'));
                return redirect()->back();
            }
            $address_id = $this->request->getPost('address_id');
            $data['address'] = $this->userAddrModel->find($address_id);
            echo json_encode($data);

            
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
            return redirect()->back();
        }
    }
    public function addNewAddress()
    {
        if($this->request->getMethod() == 'post')
        {
            // echo json_encode($_POST);die();
            $user_id = '';
            if (!empty($this->request->getPost('user_id'))) {
                $user_id = $this->request->getPost('user_id');
            } else {
               $user_id = $this->session->get('user_details')->user_id;
            }
            if(! allowEdit($user_id)){
                $this->session->setFlashData('exception',display('You are not logged in'));
                return redirect()->back();
            }
            
            $country_id = $this->request->getPost('address_country');

            $this->db->transStart();
            $address_data = [
                'user_id' => $user_id,
                'full_name' =>$this->request->getPost('full_name'),
				'address' => $this->request->getPost('new_address'),
				'city' => $this->request->getPost('address_city'),
				'state' => $this->request->getPost('address_state'),
				'country' => $this->country->find($country_id)->name,
				'country_code' => $this->country->find($country_id)->dial_code,
				'currency_code' => $this->country->find($country_id)->currency_code,
				'zip_code' => $this->request->getPost('address_zip_code'),
				'is_primary' => 2,
			];
			if(!$this->userAddrModel->insert($address_data))
			{
				$this->session->setFlashData('exception',$this->userAddrModel->errors());	
				return redirect()->back();
			}
			$this->db->transComplete();
			if($this->db->transStatus() !== FALSE)
			{
                $this->session->setFlashData('message',display('Address added successfully'));
                return redirect()->back();
            } else {
                $this->session->setFlashData('exception',display('Error occured while adding the address'));
                return redirect()->back();
            }
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
            return redirect()->back();
        }
    }    

    public function editAddressdetails()
    {
        if($this->request->getMethod() == 'post')
        {
            // echo json_encode($_POST);exit;
            $user_id = $this->request->getPost('user_id');
            if(! allowEdit($user_id)){
                $this->session->setFlashData('exception',display('You are not logged in'));
                return redirect()->back();
            }
            
            // $country_id = $this->request->getPost('address_country');
            $address_id = $this->request->getPost('address_id');

            $this->db->transStart();
            $address_data = [
                'full_name' =>$this->request->getPost('full_name'),
                'address' => $this->request->getPost('new_address'),
                'city' => $this->request->getPost('address_city'),
                'state' => $this->request->getPost('address_state'),
                // 'country' => $this->country->find($country_id)->name,
                // 'country_code' => $this->country->find($country_id)->dial_code,
                // 'currency_code' => $this->country->find($country_id)->currency_code,
                'zip_code' => $this->request->getPost('address_zip_code'),
                'is_primary' => 2,
            ];
            if(!$this->userAddrModel->update(['id'=> $address_id,'user_id'=> $user_id],$address_data))
            {
                $this->session->setFlashData('exception',$this->userAddrModel->errors());   
                return redirect()->back();
            }
            $this->db->transComplete();
            if($this->db->transStatus() !== FALSE)
            {
                $this->session->setFlashData('message',display('Address edited successfully'));
                return redirect()->back();
            } else {
                $this->session->setFlashData('exception',display('Error occured while editing the address'));
                return redirect()->back();
            }
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
            return redirect()->back();
        }
    }
}