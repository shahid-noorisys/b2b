<?php
namespace App\Controllers\buyer;

use App\Controllers\BaseController;
use App\Models\BuyerTransactionModel;

class Transactions extends BaseController {

    private $buyerTransModel = null;
    private $buyerTransModel2 = null;
    public function __construct()
    {
        $this->buyerTransModel = new BuyerTransactionModel();
        $this->buyerTransModel2 = new BuyerTransactionModel();
    }

    public function transactionList()
    {
        $data['title'] = display('Transactions');
	    $data['path'] = base_url();
        $data['controller_name'] = display('Transactions');
        // echo env('pager.perPage');
		$data['content'] = view('buyer/transactions/list_view',$data);
    	return view('layouts/buyer_wrapper', $data);
    }

    public function ajaxTransactionList()
    {
        $reference_no = $this->request->getPost('reference_no');
        $quotation_no = $this->request->getPost('quote_no');
        $user_id = $this->session->get('user_details')->user_id;
        $pageNo = $this->request->getGet('page');
        if($pageNo == '' || $pageNo == null){ $pageNo = 1;}
        if($reference_no != '' AND $reference_no != null){
            $this->buyerTransModel->like('transaction_id',$reference_no);
            $this->buyerTransModel2->like('transaction_id',$reference_no);
        }
        if($quotation_no != '' AND $quotation_no != null){
            $this->buyerTransModel->like('q_no',$quotation_no);
            $this->buyerTransModel2->like('q_no',$quotation_no);
        }
        $data['transactions'] = $this->buyerTransModel->where('paid_by',$user_id)->orderBy('created_date','DESC')->paginate();
        // echo json_encode($data['transactions']);exit;
        $total = $this->buyerTransModel2->where('paid_by',$user_id)->countAllResults();
        $pager = $this->buyerTransModel->pager;
        // echo env('pager.perPage');
        $data['pagination'] = $pager->makeLinks($pageNo,env('pager.perPage'),$total,'buyer_pager');
        $response = ['html' => view('buyer/transactions/ajax_transaction_list',$data),'reference_no' => $reference_no, 'quotation_no' => $quotation_no, 'page_no' => $pageNo];
        echo json_encode($response);
    }

}

/* End of file Transactions.php */
