<?php
namespace App\Controllers\buyer;

use App\Models\OrdersModel;
use App\Models\ProductModel;
use App\Models\ReceiptModel;
use App\Models\QuotationModel;
use App\Models\UserAddressModel;
use App\Models\NotificationModel;
use App\Controllers\BaseController;
use App\Models\AdminTransactionModel;
use App\Models\BuyerTransactionModel;
use App\Models\SupplierTransactionModel;

class Orders extends BaseController
{
    private $orderModel = null;
    private $quoteModel = null;
    private $userAddress = null;
    private $productModel = null;
    private $notifyModel = null;
    private $buyerTransaction = null;
    private $adminTransaction = null;
    private $supplierTransaction = null;
    private $receiptModel = null;
    private $orderFilterModel = null;
    public function __construct()
    {
        $this->orderModel = new OrdersModel();
        $this->quoteModel = new QuotationModel();
        $this->userAddress = new UserAddressModel();
        $this->productModel = new ProductModel();
        $this->notifyModel = new NotificationModel();
        $this->buyerTransaction = new BuyerTransactionModel();
        $this->adminTransaction = new AdminTransactionModel();
        $this->supplierTransaction = new SupplierTransactionModel();
        $this->receiptModel = new ReceiptModel();
        $this->orderFilterModel = new OrdersModel();
    }

    public function orderList()
    {
        $data['title'] = display('Orders');
	    $data['path'] = base_url();
        $data['controller_name'] = display('Orders');
        // echo json_encode($data['quotations']);exit;
		$data['content'] = view('buyer/orders/list_view',$data);
    	return view('layouts/buyer_wrapper', $data);
    }

    public function ajaxOrderList()
    {
        $order_no = $this->request->getPost('order_no');
        $status = ucfirst($this->request->getPost('ord_status'));
        $pageNo = $this->request->getGet('page');
        $user_id = $this->session->get('user_details')->user_id;

        if($pageNo == '' || $pageNo == null){ $pageNo = 1;}

        if(! empty(trim($order_no))){
            $this->orderModel->like('order_no',$order_no);
            $this->orderFilterModel->like('order_no',$order_no);
        }
        if(! empty(trim($status))){
            $this->orderModel->where('status',$status);
            $this->orderFilterModel->where('status',$status);
        }
        $data['orders'] = $this->orderModel->where('buyer_id',$user_id)->orderBy('created_date','DESC')->paginate();
        $total = $this->orderFilterModel->where('buyer_id',$user_id)->countAllResults();
        
        $pager = $this->orderModel->pager;
        $data['pagination'] = $pager->makeLinks($pageNo,env('pager.perPage'),$total,'buyer_pager');
        $response = ['html' => view('buyer/orders/ajax_order_list',$data),'order_no' => $order_no, 'status' => $status, 'page_no' => $pageNo];
        echo json_encode($response);
        // return view('buyer/orders/ajax_order_list',$data);
    }

    public function orderDetails($quotation_id = null)
    {
        if($quotation_id == null){
            $this->session->setFlashData('exception', display('Invalid Quotation ID'));
            return redirect()->back();
        }
        if($this->request->getMethod() == 'get')
        {
            // echo json_encode($quotation_id);
            if(! $quoteDetails = $this->quoteModel->find($quotation_id))
            {
                $this->session->setFlashData('exception', display('Invalid Quotation ID'));
                return redirect()->back();
            }
            else
            {
                // Valid quotation id
                // echo json_encode($quoteDetails);exit;
                $data['title'] = display('Make Order');
                $data['path'] = base_url();
                $data['controller_name'] = display('Orders');
                $user_id = $this->session->get('user_details')->user_id;
                $data['primary_address'] = $this->userAddress->where('user_id',$user_id)->where('is_primary','Yes')->get()->getRowObject();
                $data['u_address'] = $this->userAddress->where('user_id',$user_id)->where('is_primary','No')->get()->getResult();
                // echo json_encode($data['user_address']);exit;
                $data['user_details'] = $this->session->get('user_details');
                // print_r($data['user_details']);exit;
                $data['quotation'] = $quoteDetails;
                $data['content'] = view('buyer/orders/order_payment_page',$data);
                return view('layouts/buyer_wrapper', $data);
            }
        }
    }


    public function orderInformation($order_id = null)
    {
        if($order_id == null){
            $this->session->setFlashData('exception', display('Invalid Order ID'));
            return redirect()->back();
        }
        if($this->request->getMethod() == 'get')
        {
            // echo json_encode($order_id);
            if(! $order_details = $this->orderModel->find($order_id))
            {
                $this->session->setFlashData('exception', display('Invalid Order ID'));
                return redirect()->back();
            }
            else
            {
                // Valid quotation id
                // echo json_encode($order_details);exit;
                $data['title'] = display('Make Order');
                $data['path'] = base_url();
                $data['controller_name'] = display('Orders');
                $user_id = $this->session->get('user_details')->user_id;
                // $data['primary_address'] = $this->userAddress->where('user_id',$user_id)->where('is_primary','Yes')->get()->getRowObject();
                // $data['u_address'] = $this->userAddress->where('user_id',$user_id)->where('is_primary','No')->get()->getResult();

                $builder = $this->db->table('orders AS ord');
                $builder->select("ord.*,quote.id AS quote_id,quote.amount AS quote_amounts,add.address,add.city,add.state,add.country,CONCAT(u.firstname,' ',u.lastname) AS buyer_name,CONCAT(sup.firstname,' ',sup.lastname) AS seller_name");
                $builder->join('quotations AS quote', 'quote.id = ord.q_id','left');
                $builder->join('user_addresses AS add', 'add.id = ord.address_id','left');
                $builder->join('application_users AS u','u.id = ord.buyer_id','left');
                $builder->join('application_users AS sup','sup.id = ord.supplier_id','left');
                $builder->where('ord.id', $order_id);
                $builder->orderBy('ord.id','DESC');
                $data['order']  = $builder->get()->getRow();
                $data['user_details'] = $this->session->get('user_details');

                // print_r($data['user_details']);exit;
                $data['content'] = view('buyer/orders/order_information',$data);
                return view('layouts/buyer_wrapper', $data);
            }
        }
    }

    public function createOrder()
    {
        if($this->request->getMethod() == 'post')
        {
            // echo json_encode($_POST);exit;
            $response = $this->request->getPost('detail');
            $address_id = $this->request->getPost('address_id');
            $quotation_id = $this->request->getPost('quotation_id');
            $order_note = $this->request->getPost('order_note');
            $payment_method = $this->request->getPost('payment_method');
            $transaction_number =($payment_method == 'paypal')?$response['id']:$this->request->getPost('transaction_number');
            $quotation = $this->quoteModel->where('id',$quotation_id)->where('is_paid = ',2)->where('is_rejected = ',2)->get()->getRow();
            $user_id = $this->session->get('user_details')->user_id;
            $product_array = json_decode($quotation->product_json);
            // echo $this->db->getLastQuery();exit;
            // echo json_encode($quotation);exit;
            if(empty($quotation)){
                $this->session->setFlashData('exception',display('Invalid Quotation OR Quotation is reject OR Already Paid'));
                return redirect()->back();
            }
            $this->db->transStart();
            $order_number = getCounter('order');
            $insert_data = [
                'order_no' => $order_number,
                'q_id' => $quotation->id,
                'q_no' => $quotation->q_no,
                'product_ids' => $quotation->product_ids,
                'buyer_id' => $quotation->request_from,
                'supplier_id' => $quotation->request_to,
                'address_id' => $address_id,
                'amount' => $quotation->amount,
                'total_items' => $quotation->total_items,
                'product_json' => $quotation->product_json,
                'order_note' => $order_note,
                'payment_method' => $payment_method,
                'transaction_number' => $transaction_number,
                'payment_mode' => 'Online',
                'order_status' => 'pending',
            ];
            // print_r($insert_data);exit;
            $this->orderModel->insert($insert_data);
            $order_id = $this->orderModel->insertID();
            foreach ($product_array as $p) {
                $prodDetails = $this->productModel->find($p->product_id);
                $this->productModel->update($p->product_id,['order_counter' => (($prodDetails->order_counter != null)?$prodDetails->order_counter++:1)]);
            }
            $this->quoteModel->update($quotation->id,['is_paid' => 'Yes']);
            $notification = array(
                array(
                    'title' => display('Order placed and payment done'),
                    'description' => display("Payment done for the quotation number:").' '.$quotation->q_no.' '.display('By').' '.$this->session->get('user_details')->fullname,
                    'user_id' => $quotation->request_to,
                    'user_role' => 2,
                    'read_status' => 'Unread',
                    'created_date' => date('Y-m-d H:i:s')
                ),
                array(
                    'title' => display('Order placed and payment done'),
                    'description' => display("Payment done for the quotation number:").' '.$quotation->q_no.' '.display('By').' '.$this->session->get('user_details')->fullname,
                    'user_id' => 1,
                    'user_role' => 1,
                    'read_status' => 'Unread',
                    'created_date' => date('Y-m-d H:i:s')
                ),
                array(
                    'title' => display('Order placed and payment done'),
                    'description' => display("Payment done for the quotation number:").' '.$quotation->q_no.' '.display('By').' '.$this->session->get('user_details')->fullname,
                    'user_id' => $this->session->get('user_details')->user_id,
                    'user_role' => 3,
                    'read_status' => 'Unread',
                    'created_date' => date('Y-m-d H:i:s')
                ),
            );
            $this->notifyModel->insertBatch($notification);
            //----------- Order Receipt  ----------------//
            //----------- Order Receipt  ----------------//
            $receipt_id = getCounter('receipt');
            $receipt = [
                'receipt_id' => $receipt_id,
                'q_id' => $quotation->id,
                'q_no' => $quotation->q_no,
                'order_id' => $order_id,
                'order_no' => $order_number,
                'paid_by' => $user_id,
                'paid_to' => $quotation->request_to,
                'receipt_id' => $receipt_id,
                'order_amount' => $quotation->amount,
                'currency_code' => 'EUR',
                'product_json' => $quotation->product_json,
                'pdf_url' => '',
                'payment_method' => $payment_method,
            ];
            $this->receiptModel->insert($receipt);
            //----------- Manage Transactions on order  ----------------//
            $global_commission = 10;//in percentage
            //------------ Debit order amount from buyer --------------//
            $transaction = [
                'transaction_id' => getCounter('br_transaction'),
                'q_id' => $quotation->id,
                'q_no' => $quotation->q_no,
                'order_id' => $order_id,
                'order_no' => $order_number,
                'paid_by' => $user_id,
                'paid_to' => $quotation->request_to,
                'receipt_id' => $receipt_id,
                'transaction_amount' => $quotation->amount,
                'currency_code' => 'EUR',
                'type' => 'Dr',
                'payment_method' => $payment_method,
                'reason' => 'order',
                'status' => 'success',
                'created_date' => date('Y-m-d H:i:s'),
            ];
            $this->buyerTransaction->insert($transaction);
            //------------ Debit order amount from buyer --------------//
            $admin_commission = ($quotation->amount*($global_commission/100));
            $supplier_amount = $quotation->amount - ($quotation->amount*($global_commission/100));
            //------------ Credit commission amount to Admin --------------//
            $transaction = [
                'transaction_id' => getCounter('ad_transaction'),
                'q_id' => $quotation->id,
                'q_no' => $quotation->q_no,
                'order_id' => $order_id,
                'order_no' => $order_number,
                'paid_by' => $user_id,
                'paid_to' => 1,
                'receipt_id' => $receipt_id,
                'transaction_amount' => $admin_commission,
                'currency_code' => 'EUR',
                'type' => 'Cr',
                'payment_method' => $payment_method,
                'reason' => 'commission',
                'status' => 'success',
                'created_date' => date('Y-m-d H:i:s'),
            ];
            $this->adminTransaction->insert($transaction);
            //------------ Credit commission amount to Admin --------------//

            //------------ Credit order amount to Supplier --------------//
            // amount will be credited after deducting commission
            $transaction = [
                'transaction_id' => getCounter('sr_transaction'),
                'q_id' => $quotation->id,
                'q_no' => $quotation->q_no,
                'order_id' => $order_id,
                'order_no' => $order_number,
                'paid_by' => $user_id,
                'paid_to' => $quotation->request_to,
                'receipt_id' => $receipt_id,
                'transaction_amount' => $supplier_amount,
                'currency_code' => 'EUR',
                'type' => 'Cr',
                'payment_method' => $payment_method,
                'reason' => 'order',
                'status' => 'success',
                'created_date' => date('Y-m-d H:i:s'),
            ];
            $this->supplierTransaction->insert($transaction);
            //------------ Credit order amount to Supplier --------------//

            //------------ Commission amount paid to admin by supplier --------------//
            // $transaction = [
            //     'transaction_id' => getCounter('sr_transaction'),
            //     'q_id' => $quotation->id,
            //     'q_no' => $quotation->q_no,
            //     'order_id' => $order_id,
            //     'order_no' => $order_number,
            //     'paid_by' => $user_id,
            //     'paid_to' => 1,
            //     'receipt_id' => $receipt_id,
            //     'transaction_amount' => $admin_commission,
            //     'currency_code' => 'EUR',
            //     'type' => 'Dr',
            //     'payment_method' => $payment_method,
            //     'reason' => 'commission',
            //     'status' => 'success',
            //     'created_date' => date('Y-m-d H:i:s'),
            // ];
            // $this->supplierTransaction->insert($transaction);
            //------------ Commission amount paid to admin by supplier --------------//
            //------------ Commission amount received to admin from supplier --------------//
            // $transaction = [
            //     'transaction_id' => getCounter('ad_transaction'),
            //     'q_id' => $quotation->id,
            //     'q_no' => $quotation->q_no,
            //     'order_id' => $order_id,
            //     'order_no' => $order_number,
            //     'paid_by' => $user_id,
            //     // 'paid_to' => $quotation->request_to,
            //     'receipt_id' => $receipt_id,
            //     'transaction_amount' => $admin_commission,
            //     'currency_code' => 'EUR',
            //     'type' => 'Dr',
            //     'payment_method' => $payment_method,
            //     'reason' => 'commission',
            //     'status' => 'success',
            //     'created_date' => date('Y-m-d H:i:s'),
            // ];
            // $this->supplierTransaction->insert($transaction);
            //------------ Commission amount received to admin from supplier --------------//
            
            //----------- Manage Transactions on order  ----------------//
            // echo json_encode($transaction);exit;
            $this->db->transComplete();
            if($this->db->transStatus() !== FALSE){
                if($payment_method == 'paypal'){
                    echo json_encode(array(['status'=>'success','message'=>display('Payment Done Successfully'),'redirect' => base_url('order-info/'.$order_id)]));
                } else {
                    $this->session->setFlashData('message',display('Payment Done Successfully'));
                    return redirect()->to(base_url('order-info/'.$order_id));
                }
            } else {
                if($payment_method == 'paypal'){
                    echo json_encode(array(['failed'=>'success','message'=>display('Something went wrong. Payment failed!')]));
                } else{
                    $this->session->setFlashData('exception',display('Something went wrong. Payment failed!'));
                    return redirect()->back();
                }
            }
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
            return redirect()->back();
        }
    }
}