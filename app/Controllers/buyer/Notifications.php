<?php

namespace App\Controllers\buyer;

use CodeIgniter\Events\Events;
use App\Models\NotificationModel;
use App\Controllers\BaseController;

class Notifications extends BaseController
{
    private $notify = null;
    public function __construct()
    {
        $this->notify = new NotificationModel();
    }

    public function noteList()
    {
        $data['title'] = display('Inapp Notification');
        $data['path'] = base_url();
        $data['controller_name'] = display('Notifications');
        $user_id = $this->session->get('user_details')->user_id;
        $data['notifications'] = $note_data = $this->notify->where('user_role',3)->where('user_id',$user_id)->orderBy('created_date','DESC')->paginate();
        $pager = $this->notify->pager;
        Events::trigger('update_buyer_notification',$note_data,$this->notify);
        Events::trigger('get_current_note_counter',$this->session,$this->notify);
        $data['note_current_counter'] = $this->session->get('user_details')->note_count;
        // print_r($data['notifications']);exit;
        $data['pagination'] = $pager->links('default','buyer_pager');
        $data['content'] = view('buyer/notifications/notification_list',$data);
        return view('layouts/buyer_wrapper', $data);
    }
}