<?php
namespace App\Controllers\buyer;

use App\Models\ProductModel;
use App\Controllers\BaseController;

class Carts extends BaseController
{
    private $productModel = null;
    private $user_id = null;
    public function __construct()
    {
        $this->productModel = new ProductModel();
    }
    
    public function cartList()
    {
        $data['title'] = display('Cart List');
	    $data['path'] = base_url();
        $data['controller_name'] = display('Carts');
        // $user_id = (isset($this->session->user_details->user_id))?$this->session->user_details->user_id:null;
        $cart = cart();
        $data['items'] = $items = $cart->getItems();
        if(!$items){ 
            $this->session->setFlashData('exception',display('Your Cart is empty'));
            // return redirect()->to(base_url());
        }
        // if(empty($items = $cart->getItems())){
        //     $arr = ['user_id' => $user_id, 'supplier_id' => 1, 'temp_id' => 'TQ00002', 'product_id' => 9, 'quantity' => 1, 'product_amount' => ""];
        //     $cart->insert($arr);
        // }
        // echo ($items['product_json']);
        // print_r(json_decode($items['product_json']));exit;
        // print_r($items);exit;
        // echo json_encode($data['quotations']);exit;
		$data['content'] = view('buyer/cart/cart_list',$data);
    	return view('layouts/buyer_wrapper', $data);
    }

    public function addToCart()
    {
        $product_id = $this->request->getPost('product_id');
        $productDetails = $this->productModel->find($product_id);
        $supplier_id = $productDetails->user_id;
        $product_id = $productDetails->id;
        $this->user_id = (isset($this->session->get('user_details')->user_id))?$this->session->get('user_details')->user_id:null;
        // echo json_encode($productDetails);exit;
        $cart = cart();
        // echo json_encode($cart->totalProductIds());exit;
        $product_arr = [
            'product_id' => $product_id,
            'quantity' => "1",
            'product_amount' => "",
        ];
        $arr = [
            'supplier_id' => $supplier_id,
            'user_id' => $this->user_id,
            // 'product_json' => $product_arr,
            'created_date' => date('Y-m-d H:i:s')
        ];
        // echo json_encode($arr);exit;
        if($cart->initialize($arr))
        {
            if( empty($items = $cart->getItems()) )
            {
                if($cart->insert($product_arr)){
                    $response = ['status' => 'success', 'message' => display('Product Added to Cart')];
                } else {
                    $response = ['status' => 'failed', 'message' => display('Error occured while adding product to cart')];
                }
            } else {
                // print_r($items);exit;
                // if(! in_array($product_id,$cart->totalProductIds()) ){
                    if(isset($items['supplier_id']) && $supplier_id == $items['supplier_id'])
                    {
                        if($cart->update($product_arr)){
                            $response = ['status' => 'success', 'message' => display('Product Added to Cart')];
                        } else {
                            $response = ['status' => 'failed', 'message' => display('Error occured while adding product to cart')];
                        }
                    } else {
                        $response = ['status' => 'failed', 'message' => display("You already have the products into cart from different Supplier. Kindly remove products from cart OR proceed with <b>'Request to Quote'<b>")];
                    }
                // } else {
                //     $response = ['status' => 'failed', 'message' => display('Product is already exist in the cart')];
                // }
            }
        } else {
            $response = ['status' => 'failed', 'message' => display('Cart is not initialized.')];
        }
        echo json_encode($response);
    }

    public function updateCart()
    {
        if($this->request->getMethod() == 'post')
        {
            $product_ids = $this->request->getPost('product_id');
            $quantity = $this->request->getPost('quantity');
            // print_r($quantity);exit;
            $product_json = array();
            foreach ($product_ids as $i => $p) {
                $product_json[] = ['product_id' => $p,'quantity' => $quantity[$i],'product_amount' => ''];
            }
            $cart = cart();
            $items['total_products'] = COUNT($product_ids);
            $items['product_json'] = $product_json;
            if($cart->updateAll($items)){
                $this->session->setFlashData('message',display('Cart Updated Successfully'));
            } else {
                $this->session->setFlashData('exception', display('Error Occured while updating the cart'));
            }
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
        }
        return redirect()->back();
    }

    public function getTotalCartCount()
    {
        $cart = cart();
        if($cart->totalItems()){
            echo json_encode(['status' => 'success', 'total_count' => $cart->totalItems()]);exit;
        }
        echo json_encode(['status' => 'failed', 'total_count' => 0]);
    }

    public function removeFromCart()
    {
        if($this->request->getMethod() == 'post')
        {
            $product_id = $this->request->getPost('product_id');
            $cart = cart();
            if($cart->removeItem($product_id)){
                $response = ['status' => 'success', 'message' => display('An Item is removed from your cart')];
            } else {
                $response = ['status' => 'failed', 'message' => display('Error occured while removing item from cart')];
            }
        } else {
            $response = ['status' => 'failed', 'message' => display('Form is not submitted')];
        }
        echo json_encode($response);
    }
}