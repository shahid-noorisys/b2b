<?php

namespace App\Controllers\buyer;

use App\Models\ProductModel;
use App\Controllers\BaseController;
use App\Models\admin\Categories_model;

class Products extends BaseController
{
    private $productModel = null;
    private $productFilterModel = null;
    private $categoriesModel = null;
    public function __construct()
    {
        $this->productModel = new ProductModel();
        $this->productFilterModel = new ProductModel();
        $this->categoriesModel = new Categories_model();
    }

    public function productList($category_id,$sub_category)
    {
        $data['title'] = display('Product List');
        $data['path'] = base_url();
        $data['controller_name'] = display('Products');
        $data['user_details'] = $this->session->get('user_details');
        $search_text = '';
        if($this->request->getMethod() == 'post'){
            // echo json_encode($_POST);exit;
            $search_text = $this->request->getPost('search_text');
        }
        $data['sub_category_id'] = $sub_category;
        $data['category_id'] = $category_id;
        $data['search_text'] = $search_text;
        // $data['categories'] = $this->categoriesModel->where('status','Active')->where('deleted','No')->findAll();
        // echo json_encode($data);exit;
		$data['content'] = view('buyer/products/product_list',$data);
    	return view('layouts/buyer_wrapper', $data);
    }

    public function ajaxProductList()
    {
        $sub_category = $this->request->getPost('sub_category_id');
        $perPage = $this->request->getPost('per_page');
        $pageNo = $this->request->getGet('page');
        $search_text = $this->request->getPost('search_text');
        $category_id = $this->request->getPost('category_id');
        if($pageNo == '' || $pageNo == null){ $pageNo = 1;}
        if(!empty($sub_category) AND !empty($category_id)){
            $this->productModel->where('sub_category_id',$sub_category);
            $this->productModel->where('category_id',$category_id);
            $this->productFilterModel->where('sub_category_id',$sub_category);
            $this->productFilterModel->where('category_id',$category_id);
        }
        else if(!empty($sub_category) AND empty($category_id)){
            $this->productModel->where('sub_category_id',$sub_category);
            $this->productFilterModel->where('sub_category_id',$sub_category);
        }
        else if(empty($sub_category) AND !empty($category_id)){
            $this->productModel->where('category_id',$category_id);
            $this->productFilterModel->where('category_id',$category_id);
        }
        if(!empty($search_text)){
            // $this->productModel->where('category_id',$category_id);
            // $this->productModel->where('sub_category_id',$sub_category);
            $this->productModel->groupStart();
            $this->productModel->like('name',$search_text);
            $this->productModel->orLike('description',$search_text);
            $this->productModel->groupEnd();
            // $this->productFilterModel->where('category_id',$category_id);
            // $this->productFilterModel->where('sub_category_id',$sub_category);
            $this->productFilterModel->groupStart();
            $this->productFilterModel->like('name',$search_text);
            $this->productFilterModel->orLike('description',$search_text);
            $this->productFilterModel->groupEnd();
        }
        $data['products'] = $this->productModel->paginate($perPage);
        // echo $this->db->getLastQuery();exit;
        // $response['query'] = (string) $this->db->getLastQuery();
        $total = $this->productFilterModel->countAllResults();

        $pager = $this->productModel->pager;
        $data['pagination'] = $pager->makeLinks($pageNo,$perPage,$total,'buyer_pager');
        $response['html'] = view('buyer/products/ajax_product_list',$data);
        $response['status'] = 'success';
        $response['count'] = COUNT($data['products']);
        $response['total_count'] = $total;
        $response['search_text'] = $search_text;
        $response['category_id'] = $category_id;
        $response['sub_category_id'] = $sub_category;
        $response['per_page'] = $perPage;
        echo json_encode($response);
    }
}