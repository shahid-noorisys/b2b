<?php
namespace App\Controllers\buyer;

use App\Controllers\BaseController;


class StaticPages extends BaseController
{
    public function about_us()
    {
        $data['title'] = display('Product List');
        $data['path'] = base_url();
        $data['controller_name'] = display('Products');
        $data['content'] = view('buyer/static_pages/about_us',$data);
    	return view('layouts/buyer_wrapper', $data);
    }

    public function faq()
    {
        $data['title'] = display('Product List');
        $data['path'] = base_url();
        $data['controller_name'] = display('Products');
        $data['content'] = view('buyer/static_pages/faq',$data);
    	return view('layouts/buyer_wrapper', $data);
    }    

    public function privacy_policy()
    {
        $data['title'] = display('Product List');
        $data['path'] = base_url();
        $data['controller_name'] = display('Products');
        $data['content'] = view('buyer/static_pages/privacy_policy',$data);
    	return view('layouts/buyer_wrapper', $data);
    }   

    public function terms_condition()
    {
        $data['title'] = display('Product List');
        $data['path'] = base_url();
        $data['controller_name'] = display('Products');
        $data['content'] = view('buyer/static_pages/terms_condition',$data);
    	return view('layouts/buyer_wrapper', $data);
    }    

    public function return_refund()
    {
        $data['title'] = display('Product List');
        $data['path'] = base_url();
        $data['controller_name'] = display('Products');
        $data['content'] = view('buyer/static_pages/return_refund',$data);
    	return view('layouts/buyer_wrapper', $data);
    }
}