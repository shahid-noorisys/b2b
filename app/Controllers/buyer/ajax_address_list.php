<?php if(!empty($address)):?>
    <?php foreach ($address as $v): ?>
        <?php $customer = get_user_details_by_id($v->buyer_id,3); ?>
        <div class="col-lg-4 col-md-6">
            <div class="single_services">
                <div class="services_thumb">
                    <img src="<?=base_url('public/assets/img/navigation-map.jpg')?>" alt="">
                </div>
                <div class="services_content">
                       <h3>Address Details <a href="" class="edit"><i class="zmdi zmdi-edit"></i></a></h3>
                        <address>
                            House #15<br>
                            Road #1<br>
                            Block #C <br>
                            Banasree <br>
                            Dhaka <br>
                            1212
                        </address>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<div class="order_list">
<?=$pagination?>
</div>