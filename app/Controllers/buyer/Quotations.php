<?php

namespace App\Controllers\buyer;

use App\Models\ProductModel;
use App\Models\QuotationModel;
use App\Controllers\BaseController;
use App\Models\QuotationProductModel;
use App\Models\UserAddressModel;

class Quotations extends BaseController
{
    private $productModel = null;
    private $quoteModel = null;
    private $quoteFilterModel = null;
    private $qprodModel = null;
    private $userAddress = null;

    public function __construct()
    {
        $this->productModel = new ProductModel();
        $this->quoteModel = new QuotationModel();
        $this->userAddress = new UserAddressModel();
        $this->quoteFilterModel = new QuotationModel();
        $this->qprodModel = new QuotationProductModel();
    }
    public function quotationList()
    {
        $data['title'] = display('Quotations');
	    $data['path'] = base_url();
        $data['controller_name'] = display('Quotations');
        // 
		$data['content'] = view('buyer/quotations/list_view',$data);
    	return view('layouts/buyer_wrapper', $data);
    }

    public function ajaxQuotationList()
    {
        $quotation_no = $this->request->getPost('quotation_no');
        $status = $this->request->getPost('status');
        $pageNo = $this->request->getGet('page');
        $user_id = $this->session->get('user_details')->user_id;

        if($pageNo == '' || $pageNo == null){ $pageNo = 1;}

        if(! empty(trim($quotation_no))){
            $this->quoteModel->like('q_no',$quotation_no);
            $this->quoteFilterModel->like('q_no',$quotation_no);
        }
        if(! empty(trim($status))){
            $this->quoteModel->where('status',$status);
            $this->quoteFilterModel->where('status',$status);
        }
        $data['quotations'] = $this->quoteModel->where('request_from',$user_id)->orderBy('created_date','DESC')->paginate();
        $total = $this->quoteFilterModel->where('request_from',$user_id)->countAllResults();
        
        $pager = $this->quoteModel->pager;
        $data['pagination'] = $pager->makeLinks($pageNo,env('pager.perPage'),$total,'buyer_pager');
        $response = ['html' => view('buyer/quotations/ajax_quotation_list',$data),'quotation_no' => $quotation_no, 'status' => $status, 'page_no' => $pageNo];
        // echo json_encode($data['quotations']);exit;
        $pager = $this->quoteModel->pager;
        echo json_encode($response);
    }

    public function requestToQuote($prod_id)
    {
        // echo $prod_id;exit;
        if($prod_id == null || $prod_id == ''){ return redirect()->back(); }
        $data['title'] = display('Quotation Request');
	    $data['path'] = base_url();
        $data['controller_name'] = display('Quotations');
        $prodDetails = $this->productModel->find($prod_id);
        
        $data['product'] = $prodDetails;
		$data['content'] = view('buyer/quotations/quotation_request_view',$data);
    	return view('layouts/buyer_wrapper', $data);
    }

    public function submitQuoteRequest()
    {
        // echo json_encode($_POST);exit;
        // $v = '{"0":{"product_id":"3","quantity":"1","product_amount":""}}';
        // print_r(json_decode($v));exit;
        if($this->request->getMethod() == 'post')
        {
            $prod_id = $this->request->getPost('product_id');
            $quantity = $this->request->getPost('quatity');
            $this->db->transStart();
            $prodDetails = $this->productModel->find($prod_id);
            $qprodData = array(
                array(
                    'product_id' => $prod_id,
                    'quantity' => $quantity,
                    'product_amount' => '',
                )
            );
            $quoteData = [
                'q_no' => getCounter('quotation'),
                'product_ids' => $prod_id,
                'request_from' => $this->session->get('user_details')->user_id,
                'request_to' => $prodDetails->user_id,
                'product_json' => json_encode($qprodData),
                'total_items' => 1,//count($prodDetails),
            ];
            
            $q_id = $this->quoteModel->insert($quoteData);
            $this->productModel->update($prod_id,['quote_counter' => (($prodDetails->quote_counter != null)?$prodDetails->quote_counter++:1)]);
            // echo json_encode($this->productModel->find($prod_id));exit;
            $this->db->transComplete();
            if($this->db->transStatus() !== FALSE){
                $this->session->setFlashData('message',display('Quotation Submitted successfully'));
                return redirect()->to(base_url('buyer/quotations'));
            } else {
                $this->session->setFlashData('exception',display('Error occured while submitting quotation'));
                return redirect()->back();
            }
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
            return redirect()->back();
        }
    }

    public function addToQuote()
    {
        // echo json_encode($_POST);exit;
        // $v = '{"0":{"product_id":"3","quantity":"1","product_amount":""}}';
        // print_r(json_decode($v));exit;
        if($this->request->getMethod() == 'post')
        {
            $temp_id = $this->request->getPost('temp_id');
            $cart = cart();
            $cart_products = $cart->getItems();
            $user_id = $cart_products['user_id'];
            $supplier_id = $cart_products['supplier_id'];
            $product_array = json_decode($cart_products['product_json']);
            $prod_ids = array();
            foreach ($product_array as $v) {
                $prod_ids[] = $v->product_id;
            }
            // echo json_encode($product_array);exit;
            // $quantity = $this->request->getPost('quatity');
            if(! allowEdit($user_id)){
                $this->session->setFlashData('exception',display('You are not Authorized to submit the request.'));
                return redirect()->back();
            }
            // print_r($_POST);exit;
            $this->db->transStart();
            // $prodDetails = $this->productModel->find($prod_id);
            // $qprodData = array(
            //     array(
            //         'product_id' => $prod_id,
            //         'quantity' => $quantity,
            //         'product_amount' => '',
            //     )
            // );
            $quoteData = [
                'q_no' => getCounter('quotation'),
                'product_ids' => implode(",",$prod_ids),
                'request_from' => $user_id,
                'request_to' => $supplier_id,
                'total_items' => $cart_products['total_products'],
                'product_json' => $cart_products['product_json'],
            ];
            // print_r($quoteData);exit;
            $q_id = $this->quoteModel->insert($quoteData);
            foreach ($product_array as $p) {
                $prodDetails = $this->productModel->find($p->product_id);
                $this->productModel->update($p->product_id,['quote_counter' => (($prodDetails->quote_counter != null)?$prodDetails->quote_counter++:1)]);
            }
            $cart->destroy();
            // echo json_encode($this->productModel->find($prod_id));exit;
            $this->db->transComplete();
            if($this->db->transStatus() !== FALSE){
                $this->session->setFlashData('message',display('Quotation Submitted successfully'));
                return redirect()->to(base_url('buyer/quotations'));
            } else {
                $this->session->setFlashData('exception',display('Error occured while submitting quotation'));
                return redirect()->back();
            }
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
            return redirect()->back();
        }
    }

    public function getQuotatationdetails($quote_id)
    {
        $data['title'] = display('Quotation Details');
        $data['path'] = base_url();
        $data['controller_name'] = display('Quotations');
        $user_id = $this->session->get('user_details')->user_id;
       
        $builder = $this->db->table('quotations AS quote');
        $builder->select("quote.*,CONCAT(u.firstname,' ',u.lastname) AS buyer_name,CONCAT(sup.firstname,' ',sup.lastname) AS seller_name");
        $builder->join('application_users AS u','u.id = quote.request_from','left');
        $builder->join('application_users AS sup','sup.id = quote.request_to','left');
        $builder->where('quote.id', $quote_id);
        $data['quotation']  = $builder->get()->getRow();
        $data['user_details'] = $this->session->get('user_details');
        $data['user_address'] = $this->userAddress->where(['user_id'=>$user_id,'is_primary'=>1])->first();

        // echo $this->userAddress->getLastQuery();die();

        // print_r($data['user_address']);exit;
        $data['content'] = view('buyer/quotations/quotation_info',$data);
        return view('layouts/buyer_wrapper', $data);
    }
}