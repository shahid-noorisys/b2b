<?php 
namespace App\Controllers\buyer;

use App\Models\supplier\Supplier_model;
use App\Models\UserModel;
use App\Models\MessageModel;
use App\Models\AdminModel;
use App\Models\QuotationModel;
use App\Controllers\BaseController;

class UserMessages extends BaseController
{
	
	private $user = null;
	private $country = null;
	private $address = null;
	private $quote = null;
	

	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->admin = new AdminModel();
		$this->message = new MessageModel();
		$this->quote = new QuotationModel();
		$this->admin_id = 1;
		
	}
	
	public function message_view()
	{
	  
	    $admin_id = session()->get('user_details')->user_id;
	    // $data['quotation'] = $this->message->where(['sender_id'=>$admin_id])->orWhere(['receiver_id'=>$admin_id])->groupBy('q_id')->findAll();

	    $data['quotation'] = $this->quote->where(['request_from'=>$admin_id])->orderBy('created_date')->findAll();

		$data['title'] = "Chat";
	    $data['path'] = base_url();
	    $data['controller_name'] = "User Messages";
	    $data['content'] = view('buyer/messages/view',$data);
    	return view('layouts/buyer_wrapper', $data);
	}

	public function send_message()
	{
		// echo json_encode($_POST);die();
		// $name = $file->getClientMimeType();
		$file = $this->request->getFile('file_name');
		if (!empty($file)) {
			if (! $file->isValid()) {
	            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
	        }
	        if ($file->isValid() && ! $file->hasMoved()) {
	            $path = 'public/assets/uploads/images/message_attachment/';
	            $newName = $file->getRandomName();
	            $file->move($path,$newName);

				$data['attachment_url'] = $path.$newName; 
				$data['attachment_file_type'] = $file->getClientExtension();
	        }
		} 
		
		$data['text_msg'] = $this->request->getPost('message'); 
		$data['q_id'] = $quotation_id = $this->request->getPost('quotation_id');
		$data['sender_id'] = $this->session->get('user_details')->user_id;
		$data['receiver_id'] = $this->admin->find($this->admin_id)->id;
		$data['send_by'] = 'Buyer';
		$data['user_role'] = $this->session->get('user_details')->user_role;
		$data['read_status'] = 'No';
		
		$this->message->insert($data);
		if ($inserted_id = $this->message->getInsertID()) {
			// $response = array('status' => 'success','message'=>'message send successfully');
			$data['user_id'] = $user_id = $this->session->get('user_details')->user_id;
			$data['user_role'] = $user_role = $this->session->get('user_details')->user_role;

			$data['messages'] =  $data['messages'] = $this->message->where(['user_role'=>$user_role,'q_id'=>$quotation_id])->orderBy('created_date','ASC')->findAll();
			// echo json_encode($data);die();
			$message_list =  view('buyer/messages/list',$data);

			echo $message_list;
		} 

	}

	public function get_quotation_messages()
	{
		$quotation_id = $this->request->getPost('quotation_id'); 
		$sender_id = $this->request->getPost('sender'); 
		// logged-in user_id
		$data['user_id'] = $this->session->get('user_details')->user_id;
		$data['user_role'] = $user_role = $this->session->get('user_details')->user_role;

		$data['messages'] =  $data['messages'] = $this->message->where(['user_role'=>$user_role,'q_id'=>$quotation_id])->orderBy('created_date','ASC')->findAll();

		
		$builder = $this->db->table('user_quotation_chatrooms');
		foreach ($data['messages'] as $key => $value):
			$response = $builder->where(['q_id' => $value->q_id,'user_role'=>$user_role])->update(['read_status'=>'Yes','read_date'=>date('Y-,-d H:i:s')]);
		endforeach;
		
		// echo $this->message->getLastQuery();
	 //    echo json_encode($data['messages']);die();
		$message_list = view('buyer/messages/list',$data);

		echo $message_list;
	}

	//--------------------------------------------------------------------

}
