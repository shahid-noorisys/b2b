<?php
namespace App\Controllers\buyer;

use App\Controllers\BaseController;


class Contact extends BaseController
{
    public function showForm()
    {
        if($this->request->getMethod() == 'post')
        {
            // echo json_encode($_POST);exit;
            $mail['data']=display('Hello').', '.$this->request->getPost('message'). "\r\n";

			$email = \Config\Services::email();

			$email->setTo($this->request->getPost('email'));
			$email->setFrom($this->request->getPost('name'));
			
			$msg = view('common/email_template',$mail);
			$email->setSubject($this->request->getPost('subject'));
			$email->setMessage($msg);

			if ($email->send()) 
			{
                $this->session->setFlashData('message',display('Your query is sent. We will contact you shortly.'));
			} 
			else 
			{
			    $this->session->setFlashData('exception', display('Error occured while sending mail'));
			}
        }
        $data['title'] = display('Product List');
        $data['path'] = base_url();
        $data['controller_name'] = display('Products');
        $data['content'] = view('buyer/contact/contact',$data);
    	return view('layouts/buyer_wrapper', $data);
    }
}