<?php
namespace App\Controllers\buyer;

use Config\Services;
use App\Models\UserModel;
use App\Models\Countries_model;
use App\Models\UserAddressModel;
use App\Models\UserCompanyModel;
use App\Controllers\BaseController;

class UserProfile extends BaseController
{
    private $userModel = null;
    private $userAddress = null;
    private $countries = null;
    private $company = null;
    private $country = null;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->userAddress = new UserAddressModel();
        $this->countries = new Countries_model();
        $this->company = new UserCompanyModel();
        $this->country = new Countries_model();
    }
    
    public function index()
    {
        echo "hello from profile";
    }
    public function home()
    {
        $data['title'] = display('Profile');
		$data['path'] = base_url();
        $data['controller_name'] = display('User Profile');
        $user_id = $this->session->get('user_details')->user_id;
        $data['company'] = $this->company->where('user_id',$user_id)->first();
        $data['user'] = $this->userModel->find($user_id);
        $data['user_addresses'] = $this->userAddress->where('user_id', $user_id)->findAll();
        $country_data = $this->countries->findAll();
		$country_list[' '] = display('Select Country');
		foreach ($country_data as $v) {
			$country_list[$v->id] = $v->name;
        }
        $data['countries'] = $country_list;
		$data['content'] = view('buyer/profile/user_profile',$data);
		return view('layouts/buyer_wrapper', $data);
    }
    public function updateProfile(){
        $user_id = $this->request->getPost('user_id');
        if(! allowEdit($user_id)){
            $this->session->setFlashData('exception',display('You are not logged in'));
            return redirect()->to(base_url('buyer-profile'));
        }
        $data = [
            'firstname' => $this->request->getPost('firstname'),
            'lastname' => $this->request->getPost('lastname'),
            'gender' => $this->request->getPost('id_gender'),
            'city' => $this->request->getPost('city'),
            'state' => $this->request->getPost('state'),
            'country_id' => $this->request->getPost('country'),
            'zip_code' => $this->request->getPost('zip_code'),
        ];
        $profileUpdate = $this->db->table('application_users');
        if($profileUpdate->update($data,['id' => $user_id])){
            $this->session->setFlashData('message',display('Profile updated successfully'));
        } else {
            $this->session->setFlashData('exception',$this->userModel->errors());
        }
        return redirect()->to(base_url('buyer-profile'));
    }

    public function CompanyprofileAdd()
    {
        // echo json_encode($_POST);exit;
        $user_id = $this->session->get('user_details')->user_id;
        $data = [
                    'user_id' => $user_id,
                    'company_name' => $this->request->getPost('company_name'),
                    'company_email' => $this->request->getPost('company_email'),
                    'contact' => $this->request->getPost('mobile'),
                    'address' => $this->request->getPost('company_address'),
                    'city' => $this->request->getPost('city'),
                    'country' => $this->request->getPost('company_country'),
                    'country_code' => $this->country->find($this->request->getPost('company_country'))->dial_code,
                ];  
        $this->company->insert($data);
        if ($this->company->getInsertID()) {
                $this->session->setFlashData('message',display('Your company profile details successfully added'));
        } else {
            $this->session->setFlashData('exception',display('Sorry, Unable to add company profile details'));
        } 

        return redirect()->to(base_url('buyer-profile'));       

    }

    public function getCompanydetails()
    {
        if($this->request->getMethod() == 'post')
        {
            $user_id = $this->request->getPost('user_id');
            if(! allowEdit($user_id)){
                $this->session->setFlashData('exception',display('You are not logged in'));
                return redirect()->back();
            }
            $company_id = $this->request->getPost('company_id');
            $data['company'] = $this->company->find($company_id);
            echo json_encode($data);

            
        } else {
            $this->session->setFlashData('exception',display('Form is not submitted'));
            return redirect()->back();
        }
    }

    public function CompanyprofileEdit()
    {
        $user_id =  $this->request->getPost('user_id');  
        $company_id = $this->request->getPost('company_id');  
        // echo json_encode($_POST);die();  
        $data = [
                    // 'user_id' => $user_id,
                    'company_name' => $this->request->getPost('company_name'),
                    'company_email' => $this->request->getPost('company_email'),
                    'contact' => $this->request->getPost('mobile'),
                    'address' => $this->request->getPost('company_address'),
                    'city' => $this->request->getPost('city'),
                    // 'country' => $this->request->getPost('company_country'),
                    // 'country_code' => $this->country->find($this->request->getPost('company_country'))->dial_code,
                ];  
        $this->company->update(['id'=>$company_id],$data);
        if ($this->company->affectedRows()) {
                $this->session->setFlashData('message',display('Your company profile details successfully updated'));
                
        } else {
            $this->session->setFlashData('exception',display('Sorry, Unable to update company profile details'));
            
        }
        return redirect()->to(base_url('buyer-profile')); 

    }

    public function updateProfileImage(){
        $user_id = $this->request->getPost('user_id');
        if(! allowEdit($user_id)){
            $this->session->setFlashData('exception',display('You are not logged in'));
            return redirect()->to(base_url('buyer-profile'));
        }
        $file = $this->request->getFile('thumbnail');
        $old_file = $this->request->getPost('old_file_name');
        if (! $file->isValid())
        {
            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
        }
        if ($file->isValid() && ! $file->hasMoved())
        {
            $path = 'public/assets/uploads/images/users/';
            $newName = $file->getRandomName();
            $file->move($path,$newName);
            // dd($file);
            $builder = $this->db->table('application_users');
            if($builder->update(['thumbnail' => $path.$newName],['id' => $user_id])){
                if(file_exists($old_file)){ unlink($old_file); }
                $this->session->setFlashData('message',display('Profile Image updated successfully'));
            } else {
                if(file_exists($path.$newName)){ unlink($path.$newName); }
                $this->session->setFlashData('message',display('Profile Image updated successfully'));
            }
        }
        else {
            $this->session->setFlashData('exception',display('You are not selected any file to upload'));
        }
        return redirect()->to(base_url('buyer-profile'));
    }
    public function updatePassword(){
        $user_id = $this->request->getPost('user_id');
        if(! allowEdit($user_id)){
            $this->session->setFlashData('exception',display('You are not logged in'));
            return redirect()->to(base_url('buyer-profile'));
        }
        $data = [
            'old_password' => $this->request->getPost('old_password'),
            'password' => $this->request->getPost('new_password'),
            'conf_password' => $this->request->getPost('conf_password'),
        ];
        $validation = Services::validation();
        $validation->setRules(['old_password'     => 'required|verify',//verify is custom rule in App\Validation\VerifyPassword
        ]);
        if($validation->run($data))
        {
            if($this->userModel->update($user_id,$data)){
                $this->session->setFlashData('message',display('Password updated successfully'));
            } else {
                $this->session->setFlashData('exception',$this->userModel->errors());
            }
        } else {
            $this->session->setFlashData('exception',$validation->getError('old_password'));
        }
        return redirect()->to(base_url('buyer-profile'));
    }
}