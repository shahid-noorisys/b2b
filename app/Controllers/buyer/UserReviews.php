<?php
namespace App\Controllers\buyer;

use App\Models\Countries_model;
use App\Models\ReviewandRatingModel;
use App\Models\OrdersModel;
use App\Controllers\BaseController;

class UserReviews extends BaseController
{
    private $userReviewModel = null;
    private $country = null;
    private $ordersModel = null;
    public function __construct()
    {
        $this->userReviewModel = new ReviewandRatingModel();
        $this->ordersModel = new OrdersModel();
        $this->country = new Countries_model();
    }
    public function ajaxReviewsList()
    {
        $pageNo = $this->request->getGet('page');
        $user_id = $this->session->get('user_details')->user_id;

        if($pageNo == '' || $pageNo == null){ $pageNo = 1;}

        $data['reviews'] = $this->userReviewModel->where('buyer_id',$user_id)->orderBy('created_date','DESC')->paginate(5);
        $total = $this->userReviewModel->where('buyer_id',$user_id)->countAllResults();
        
        $pager = $this->userReviewModel->pager;
        $data['pagination'] = $pager->makeLinks($pageNo,5,$total,'buyer_pager');
        $response = ['html' => view('buyer/ajax_reviews_list',$data),'page_no' => $pageNo];
        echo json_encode($response);
    }
    public function addNewReviews()
    {
        if($this->request->getMethod() == 'post')
        {
            $user_id = $this->session->get('user_details')->user_id;
            if(! allowEdit($user_id)){
                $this->session->setFlashData('exception',display('You are not logged in'));
                return redirect()->back();
            }
            
            $order_id = $this->request->getPost('order_id');
            $order = $this->ordersModel->find($order_id);

            $this->db->transStart();
            $review_data = [
                'review_msg' =>$this->request->getPost('review'),
				'rating' => $this->request->getPost('order_rating'),
				'buyer_id' => $order->buyer_id,
				'supplier_id' => $order->supplier_id,
				'order_id' => $order->id,
				'q_id' => $order->q_id,
			];
			if(!$this->userReviewModel->insert($review_data))
			{
				$this->session->setFlashData('exception',$this->userReviewModel->errors());	
				return redirect()->back();
			}
			$this->db->transComplete();
			if($this->db->transStatus() !== FALSE)
			{
                $this->session->setFlashData('message',display('Review & Rating added successfully'));
                return redirect()->back();
            } else {
                $this->session->setFlashData('exception',display('Error occured while adding the review & rating'));
                return redirect()->back();
            }
        } 
    }
}