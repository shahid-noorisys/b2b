<?php 
namespace App\Controllers;

use App\Models\supplier\Supplier_model;
use App\Models\UserModel;
use App\Controllers\BaseController;

class Cron extends BaseController
{
	
	private $user = null;
	private $supplier = null;

	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->supplier = new Supplier_model();
		
	}
	
	public function password_reset()
	{

		$seller = $this->supplier->where('deleted',2)
								 ->where('user_role',2)
								 ->where('reset_password_date <',date('Y-m-d H:i:s',strtotime('- 90 days')))
								 ->findAll();
		if (!empty($seller)) {
			if (isset($seller)) {
				foreach ($seller as $key => $value) {
					$last_password_change_date = date('Y-m-d',strtotime($value->reset_password_date));
					$current_date = date('Y-m-d');

					$diffrence = (strtotime($current_date) - strtotime($last_password_change_date));
					$days = abs(round($diffrence / 86400)); 
					if ($days >= 90 ) {
					
						helper('text');
					    $password = random_string('alnum',10);
					    $data['password'] = $password;
					    $data['link_send_date'] = date('Y-m-d H:i:s');
					    // $data['token'] = random_string('alnum',20);
					    if($this->supplier->update($value->id,$data))
						{
							$mail['data']=display('Hello').', '.$value->firstname.' '.$value->lastname.','. "\r\n";
							$mail['data'].=display('Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>').$data['password'].'</b>'."\r\n";
							$mail['data'].='<br>'.display('Please Update your password.');
							$mail['data'].='<br>'.display('Thanks & Regards');

							$email = \Config\Services::email();

							$email->setTo($value->email);
							$email->setFrom('Forgot Password');
							
							$msg = view('common/email_template',$mail);
							$email->setSubject('Test subject');
							$email->setMessage($msg);

							if (!$email->send()) 
							{
								$data = $email->printDebugger(['headers']);
								print_r($data);
							} 
						}
					}
				}
			}
		}
	}

	//--------------------------------------------------------------------

}
