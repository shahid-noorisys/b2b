<?php 
namespace App\Controllers\admin;

use App\Models\UserModel;
use App\Models\AdminModel;
use App\Models\MessageModel;
use App\Models\QuotationModel;
use App\Models\UserAddressModel;
use App\Models\UserCompanyModel;
use App\Models\ProductModel;
use App\Models\QuotationProductModel;
use App\Models\NotificationModel;
use App\Controllers\BaseController;

class Quotations extends BaseController
{
	
	private $user = null;
	private $admin = null;
	private $address = null;
	private $supplier = null;
	private $quote = null;
	private $company = null;
	private $product = null;
	private $quote_product = null;
	private $notifiy = null;

	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->message = new MessageModel();
		$this->admin = new AdminModel();
		$this->address = new UserAddressModel();
		$this->company = new UserCompanyModel();
		$this->quote = new QuotationModel();
		$this->quote_product = new QuotationProductModel();
		$this->product = new ProductModel();
		$this->notifiy = new NotificationModel();
		$this->admin_id = 1;
		
	}
	
	public function quotations_list()
	{
	  
	    $admin_id = session()->get('user_details')->admin_id;

	    $_SESSION['quote']['quote_no'] = '';
        $_SESSION['quote']['user_id'] = '';
        $_SESSION['quote']['status'] = '';
	    $_SESSION['quote']['criteria'] = '';
	    $_SESSION['quote']['start'] = '';
        $_SESSION['quote']['end'] = '';
        $_SESSION['quote']['seller_id'] = '';

        if(!empty($_POST['quote_filter']))
        {
            $_SESSION['quote']['criteria'] =(isset($_POST['criteria']) && !empty($_POST['criteria']))?trim($_POST['criteria']):'';
            $_SESSION['quote']['quote_no'] =(isset($_POST['quote_no']) && !empty($_POST['quote_no']))?trim($_POST['quote_no']):'';
            $_SESSION['quote']['user_id'] =(isset($_POST['user_id']) && !empty($_POST['user_id']))?trim($_POST['user_id']):'';
            $_SESSION['quote']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['quote']['seller_id'] =(isset($_POST['seller_id']) && !empty($_POST['seller_id']))?trim($_POST['seller_id']):'';
            $_SESSION['quote']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['quote']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
            $_SESSION['quote']['criteria'] ='';
            $_SESSION['quote']['quote_no'] = '';
            $_SESSION['quote']['user_id'] = '';
            $_SESSION['quote']['status'] = '';
            $_SESSION['quote']['start'] = '';
	        $_SESSION['quote']['end'] = '';
	        $_SESSION['quote']['seller_id'] = '';
        }

	    $builder = $this->db->table('quotations');
		$builder->select('quotations.request_from,user.firstname,user.lastname');
		$builder->distinct('request_from');
		$builder->join('application_users AS user', 'user.id = quotations.request_from');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select Customer');
		if(!empty($users)){
			foreach ($users as $customer) {
				$users_list[$customer->request_from] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$builder = $this->db->table('quotations');
		$builder->select('quotations.request_to,user.firstname,user.lastname');
		$builder->distinct('request_to');
		$builder->join('application_users AS user', 'user.id = quotations.request_to');
		$seller  = $builder->get()->getResult();

		$seller_list[' '] = display('Please Select Seller');
		if(!empty($seller)){
			foreach ($seller as $selles) {
				$seller_list[$selles->request_to] = $selles->firstname.' '.$selles->lastname;
			}
		}

		$data['users_list'] = $users_list;
		$data['seller_list'] = $seller_list;
	    $data['quotation'] = $this->quote->getalladminQuotationList($_SESSION['quote'],$admin_id);
		$data['title'] = display('Quotations');
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = display('Home');
	    $data['content'] = view('admin/quotations/list',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function quotation_details($quotation_id)
	{
	    $admin_id = session()->get('user_details')->admin_id;

	    $data['quote'] = $this->quote->find($quotation_id);
	    $data['products'] = $this->quote_product->where(['quotation_id'=>$quotation_id])->findAll();

	    // echo json_encode($data['quote']);die();
	    $data['seller_address'] = $this->address->where(['user_id'=>$data['quote']->request_to,'is_primary'=>1])->first();
	    $data['buyer_address'] = $this->address->where(['user_id'=>$data['quote']->request_from,'is_primary'=>1])->first();
	    $data['seller_company'] = $this->company->where(['user_id'=>$data['quote']->request_to])->first();
		$data['buyer_company'] = $this->company->where(['user_id'=>$data['quote']->request_from])->first();
		$data['buyer'] = $this->user->find($data['quote']->request_from);

		$data['title'] = display('Quotations details');
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = display('Home');
	    $data['content'] = view('admin/quotations/view',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function product_details()
	{
		$product_id = $this->request->getPost('product_id'); 
		$data['product'] = $this->product->find($product_id);
		$product_info =  view('supplier/quotation/product_detail',$data);
		echo $product_info;
	}	

	public function update_quotation_amount()
	{
		$product_amount = $this->request->getPost('product_amount'); 
		$quotation_amount = $this->request->getPost('quotation_amount'); 
		$id = $this->request->getPost('id'); 
		$quotation_id= $this->request->getPost('quotation_id'); 
		$this->quote->update($quotation_id,['amount'=> $quotation_amount]);
		if($this->quote->affectedRows()){
			$this->quote_product->update($id,['product_amount'=> $product_amount]);
			if(!$this->quote_product->affectedRows()){
				$this->response = array('status'=>'failed','message'=>'sorry, unable to update....');
			} else {
				$this->response = array('status'=>'success','message'=>'successfully update....');
			}
		} else {
			$this->response = array('status'=>'failed','message'=>'sorry, unable to update....');
		}
	
		echo json_encode($this->response);
	}

	public function reject_quotation()
	{		
		$quote_id= $this->request->getPost('quote_id'); 
		$this->quote->update($quote_id,['is_rejected'=> 1]);
		$quote_detail = $this->quote->where(['id' => $quote_id])->first();
		if($this->quote->affectedRows()){

			$notification = array(	
								array( 
									'title' => 'Quotation Reject',
									'description' => 'Quotation number '.$quote_detail->q_no.' has been rejected by the admin',
									'user_id' => $quote_detail->request_to,
									'user_role' => 2,
									'read_status' => 'Unread',
									'created_date' => date('Y-m-d H:i:s'),
								 ),
								array( 
									'title' => 'Quotation Reject',
									'description' => 'Quotation number '.$quote_detail->q_no.' has been rejected by the admin',
									'user_id' => $quote_detail->request_from,
									'user_role' => 3,
									'read_status' => 'Unread',
									'created_date' => date('Y-m-d H:i:s'),
								 )
							);
			$db      = \Config\Database::connect();
			$builder = $db->table('platform_notifications');
			$builder->insertBatch($notification);
			$this->response = array('status'=>'success','message'=>display('Quotation has been rejected successfully'));
		} else {
			$this->response = array('status'=>'failed','message'=>'Sorry, something goes wrong, so quotation cant not reject');
		}
	
		echo json_encode($this->response);
	}

	public function valid_quotation()
	{		
		$quote_id= $this->request->getPost('quote_id');
		$quotation = $this->quote->find($quote_id);
		if (!empty($quotation)) {
			if (isset($quotation)) {
				$admin = get_user_details_by_id($this->admin_id,1);
				$buyer = get_user_details_by_id($quotation->request_from,3);
				$data['is_accepted'] = 1;
				$data['is_validated'] = 1;
				$emails = array($admin->email,$buyer->email);
			
				if($this->quotation_send_email($quotation,$emails,$data))
				{
						$notification = array(	
								array( 
									'title' => 'Quotation Validated',
									'description' => 'Quotation number '.$quotation->q_no.' has been validate by the admin',
									'user_id' => $quotation->request_to,
									'user_role' => 2,
									'read_status' => 'Unread',
									'created_date' => date('Y-m-d H:i:s'),
								 ),
								array( 
									'title' => 'Quotation Validated',
									'description' => 'Quotation number '.$quotation->q_no.' has been validate by the admin',
									'user_id' => $quotation->request_from,
									'user_role' => 3,
									'read_status' => 'Unread',
									'created_date' => date('Y-m-d H:i:s'),
								 )
							);	
						$db      = \Config\Database::connect();
						$builder = $db->table('platform_notifications');
						$builder->insertBatch($notification);

					$this->response = array('status'=>'success','message'=>display('Quotation has been validated successfully'));

				} else {
					$this->response = array('status'=>'failed','message'=>'Sorry, something goes wrong, so quotation cant not be validate');
				}
				$this->quote->update($quote_id,$data);
			}
		} else{
			
			$this->response = array('status'=>'failed','message'=>'Sorry, something goes wrong, so quotation cant not be validate');
		}
		echo json_encode($this->response);
	} 

	public function quotation_send_email($quotation,$emails,$data)
	{

		if($this->quote->update($quotation->id,$data))
		{
		    $mail['data']  = '<p style="float:right;">'.display('From').'<br>';
			$mail['data'] .= 'Jhon Doe</p>';
			$mail['data'] .= '<div style="padding:50px 0px;"></div>';

			$mail['data'] .= '<p>'.display('To').'<br>';
			$mail['data'] .= 'Mirza Mohsin</p>';

			$mail['data'] .= '<p>This letter is with reference to quote number [123456] which I received from you on [5 July 2015].</p>';
			$mail['data'] .= '<p>After a close and comprehensive study of your quote, the management has given their approval. Enclosed with this letter is the purchase order corresponding to your quotation. Please review our order and do the necessary to proceed with it.</p>';
			$mail['data'] .= '<p>We look forward to doing business with you.</p>';
			$mail['data'] .= '<p>Regards</p>';

			$email = \Config\Services::email();

			$email->setTo($emails);
			$email->setFrom('YAHODIHIME');
			
			$msg = view('common/quotation_template',$mail);
			$email->setSubject('Quotation validated');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			}
			return true;
		} else {
				return false;
		}
	}
	//--------------------------------------------------------------------

}
