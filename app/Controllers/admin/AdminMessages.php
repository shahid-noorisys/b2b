<?php 
namespace App\Controllers\admin;

use App\Models\supplier\Supplier_model;
use App\Models\UserModel;
use App\Models\MessageModel;
use App\Models\AdminModel;
use App\Models\QuotationModel;
use App\Controllers\BaseController;

class AdminMessages extends BaseController
{
	
	private $user = null;
	private $country = null;
	private $address = null;
	private $quote = null;
	

	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->admin = new AdminModel();
		$this->message = new MessageModel();
		$this->quote = new QuotationModel();
		$this->admin_id = 1;
		
	}
	
	public function messages()
	{
	  
	    $admin_id = session()->get('user_details')->admin_id;
	    
	    // $data['quotation'] = $this->message->where(['sender_id'=>$admin_id])->orWhere(['receiver_id'=>$admin_id])->groupBy('q_id')->findAll();

	    $data['quotation'] = $this->quote->orderBy('created_date')->findAll();

		$data['title'] = "Chat";
	    $data['path'] = base_url();
	    $data['controller_name'] = "Messages";
	    $data['content'] = view('admin/messages/view',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}


	public function send_message()
	{

		// $name = $file->getClientMimeType();
		$file = $this->request->getFile('file_name');
		if (!empty($file)) {
			if (! $file->isValid()) {
	            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
	        }
	        if ($file->isValid() && ! $file->hasMoved()) {
	            $path = 'public/assets/uploads/images/message_attachment/';
	            $newName = $file->getRandomName();
	            $file->move($path,$newName);

				$message_data['attachment_url'] = $path.$newName; 
				$message_data['attachment_file_type'] = $file->getClientExtension();
	        }
		} 
		
		$message_data['text_msg'] = $this->request->getPost('message'); 
		$message_data['q_id'] = $quotation_id = $this->request->getPost('quotation_id');
		$message_data['sender_id'] = $this->session->get('user_details')->admin_id;
		$message_data['receiver_id'] = $receiver_id = $this->request->getPost('reciever_id');
		$message_data['user_role'] = $this->request->getPost('reciever_role');
		$message_data['send_by'] = 1;
		$message_data['read_status'] = 'No';
		$this->message->insert($message_data);
		
		if ($inserted_id = $this->message->getInsertID()) {
			
			$data['admin_id'] = $admin_id = $this->session->get('user_details')->admin_id;
			$data['admin_role'] = $admin_role = $this->session->get('user_details')->user_role;

			$data['user_role'] = $reciever_role = $this->request->getPost('reciever_role');
			$data['sender_id'] = $sender_id = $this->request->getPost('reciever_id');

			// admin sent messages to user
			$data['messages'] = $this->message->where(['q_id'=>$quotation_id,'user_role'=>$reciever_role])->orderBy('created_date','ASC')->findAll();

			// echo json_encode($data);die();
			$message_list = view('admin/messages/message_list',$data);
			echo $message_list;
		} 

	}

	public function get_messages()
	{
		$quotation_id = $this->request->getPost('quotation_id'); 
		$data['sender_id'] = $sender_id = $this->request->getPost('sender_id'); //id of user who send message
		// echo json_encode($sender_id);die();
		$response['user_details'] = $this->user->find($sender_id);
		$data['user_role'] = $user_role = $this->request->getPost('user_role'); // for seller / buyer identification who send message

		// logged-in user details
		$data['admin_id'] = $admin_id = $this->session->get('user_details')->admin_id;
		$data['admin_role'] = $admin_role = $this->session->get('user_details')->user_role;

		// admin sent messages to user
		$data['messages'] = $this->message->where(['q_id'=>$quotation_id,'user_role'=>$user_role])->orderBy('created_date','ASC')->findAll();

		$response['message_list'] = view('admin/messages/message_list',$data);

		echo json_encode($response);
	}

	//--------------------------------------------------------------------

}
