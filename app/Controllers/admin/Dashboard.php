<?php 
namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\admin\Subcategories_model;
use App\Models\AdminModel;
use App\Models\Countries_model;

class Dashboard extends BaseController
{

	private $sub_categories = null;
	private $categories = null;
	private $country = null;
	private $admin = null;

	public function __construct()
	{
		$this->country = new Countries_model();
		$this->admin = new AdminModel();
	}

	public function dashboard()
	{
		$db      = \Config\Database::connect();
		$order = $db->table('orders');
		$order->select('*');
		$order->orderBy('id','DESC');
		$order->limit(5);
		$data['orders'] = $order->get()->getResult();

		$quote = $db->table('quotations');
		$quote->select('*');
		$quote->orderBy('id','DESC');
		$quote->limit(5);
		$data['quotation'] = $quote->get()->getResult();

		$data['title'] = "Dashboard";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/dashboard',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}	

	public function login()
	{
		if($this->request->getMethod() == 'post')
		{
			$email = $this->request->getPost('email');
			$password = $this->request->getPost('password');
			$details = $this->admin->authenticate($this->request->getPost());
			if(!empty($details))
	        {
               	$session_data = (object)["fullname" => $details->firstname.' '.$details->lastname, "admin_id" => $details->id, "user_role" => $details->user_role, "email" =>$details->email, "is_logged_in" =>true];
				$this->session->set('user_details',$session_data);
                $this->session->setFlashData('message',display('Welcome to the admin dashboard'));
				return redirect()->to(base_url('admin/dashboard'));
	        } else {
					$this->session->setFlashData('exception',display('Incorrect Email OR Password'));
					return redirect()->to(base_url('admin/login'))->withInput();
	        }
		}else {
			return view('admin/login');
		}
	}

	public function change_password()
	{
		if($this->request->getMethod() == 'post')
		{
			$old_pswd = $this->request->getPost('old_pswd');
			$new_pswd = $this->request->getPost('new_pswd');
			$confrim_pswd = $this->request->getPost('confrim_pswd');
			$user = $this->session->get('user_details') ?? null;
			$details = $this->admin->find($user->admin_id);	
			if(!empty($details))
	        {
	            $verify = password_verify($old_pswd, $details->password);
	            if($verify)
	            {
					$this->admin->update($user->admin_id, ['password' => $confrim_pswd]);
					// echo json_encode($verify);die();
					if ($this->admin->affectedRows()) {
		                $this->session->setFlashData('message',display('Your credential updated Successfully'));
						return redirect()->to(base_url('admin/dashboard'));
					}
	            } else {
	            	$this->session->setFlashData('exception',display('Unable to match your current credential'));
					return redirect()->to(base_url('change-password'));
	            }

	        } else {
					$this->session->setFlashData('exception',$this->admin->errors());
					return redirect()->to(base_url('admin/login'))->withInput();
	        }
			
		} else {
			$data['title'] = "Change Password";
		    $data['path'] = base_url('admin/dashboard');
		    $data['controller_name'] = "Home";
		    $data['content'] = view('admin/change_password',$data);
	    	return view('admin/layout/admin_wrapper', $data);
		}
	}

	public function logout(){
		$this->session->remove('user_details');//some session data name for user details
		$this->session->setFlashData('message',display('You are logged out Successfully'));
		return redirect()->to(base_url('admin/login'));
		// set flash data
		// redirect to home
	}	

	public function profile()
	{
		$user = $this->session->get('user_details');
		$data['profile'] = $this->admin->find($user->admin_id);
		$data['title'] = "Profile";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/profile',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function profile_image()
	{
		// echo json_encode('File Upload');die();
		$user_id = $this->session->get('user_details')->admin_id;	

        $file = $this->request->getFile('profile_pic');
        $old_file = $this->request->getPost('old_image');
        if (! $file->isValid())
        {
            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
        }
        if ($file->isValid() && ! $file->hasMoved())
        {
            $path = 'public/assets/uploads/images/users/';
            $newName = $file->getRandomName();
            $file->move($path,$newName);

            $builder = $this->db->table('platform_users');
            if($builder->update(['thumbnail' => $path.$newName],['id' => $user_id])){
                if(file_exists($old_file)){ unlink($old_file); }
                $data = array('status' => 'success','message'=> display('Profile Image updated successfully'));
            } else {
                if(file_exists($path.$newName)){ unlink($path.$newName); }
                $data = array('status' => 'failed','message'=> display('Profile Image updated successfully')); 
            }
        }
        else {
            $data = array('status' => 'failed','message'=> display('You are not selected any file to upload'));
        }
        echo json_encode($data);
	}



	//--------------------------------------------------------------------

}
