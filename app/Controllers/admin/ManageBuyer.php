<?php 
namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\Countries_model;
use App\Models\UserAddressModel;
use App\Models\UserModel;
use App\Models\supplier\Supplier_model;
use App\Models\UserCompanyModel;
use App\Models\NotificationModel;

class ManageBuyer extends BaseController
{
	private $country = null;
	private $admin = null;
	private $user = null;
	private $company = null;
	private $notifiy = null;

	public function __construct()
	{
		$this->country = new Countries_model();
		$this->admin = new AdminModel();
		$this->supplier = new Supplier_model();
		$this->address = new UserAddressModel();
		$this->company = new UserCompanyModel();
		$this->notifiy = new NotificationModel();
	}

	public function buyer_list()
	{
		// echo json_encode($_POST);die();

		$_SESSION['buyer']['country_id'] = '';
        $_SESSION['buyer']['state'] = '';
        $_SESSION['buyer']['city'] = '';
        $_SESSION['buyer']['status'] = '';
        $_SESSION['buyer']['gender'] = '';
        $_SESSION['buyer']['start'] = '';
        $_SESSION['buyer']['end'] = '';

        if(!empty($_POST['buyer_filter']))
        {
            $_SESSION['buyer']['country_id'] =(isset($_POST['country_id']) && !empty($_POST['country_id']))?trim($_POST['country_id']):'';
            $_SESSION['buyer']['state'] =(isset($_POST['state']) && !empty($_POST['state']))?trim($_POST['state']):'';
            $_SESSION['buyer']['city'] =(isset($_POST['city']) && !empty($_POST['city']))?trim($_POST['city']):'';
            $_SESSION['buyer']['gender'] =(isset($_POST['gender']) && !empty($_POST['gender']))?trim($_POST['gender']):'';
            $_SESSION['buyer']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['buyer']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['buyer']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
            $_SESSION['buyer']['country_id'] ='';
            $_SESSION['buyer']['state'] = '';
            $_SESSION['buyer']['city'] = '';
            $_SESSION['buyer']['status'] = '';
            $_SESSION['buyer']['gender'] = '';
            $_SESSION['buyer']['start'] = '';
            $_SESSION['buyer']['end'] = '';
        }

		$data['buyers'] = $this->supplier->getallCustomerList($_SESSION['buyer']);
		// $data['buyers'] = $this->supplier->where('deleted',2)->where('user_role',3)->findAll();

	    $countries = $this->country->findAll();
		$country_list[' '] = display('Select Country');
		foreach ($countries as $v) {
			$country_list[$v->id] = $v->name;
		}
		$data['countries'] = $country_list;
		$data['title'] = "Manage Buyers";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";

	    $data['content'] = view('admin/manage_buyer/buyer_list',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}


	public function view_buyer_profile()
	{
		$uri = service('uri');
		$user_id = $uri->getSegment(3);
		$data['profile'] = $this->supplier->where('deleted',2)->where('user_role',3)->find($user_id);
		$data['address'] = $this->address->where('user_id',$user_id)->where('is_primary',1)->first();
		
		$data['title'] = "Manage Buyers";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/manage_buyer/view_profile',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}


	public function buyer_activate()
	{
		$user_id = $this->request->getPost('user_id');
		$this->supplier->allowCallbacks(false);
		$isExist = $this->supplier->getWhere(['id' => $user_id])->getRow();

		if($this->supplier->update($user_id,['status' => 'Active'])){
			if($this->sendAccountactivate($isExist->id,$isExist)){
				$data = array('status' => 'success','message'=> display('User has been activated'));
			} else {
				$data = array('status' => 'failed','message'=> display('Sorry, Unable to activated the user'));
			}
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to activated the user'));
		}
		echo json_encode($data);
	}	

	public function buyer_deactivate()
	{
		$user_id = $this->request->getPost('user_id');
		$isExist = $this->supplier->getWhere(['id' => $user_id])->getRow();
		$this->supplier->allowCallbacks(false);
		if($this->supplier->update($user_id,['status' => 'Inactive'])){
			if($this->sendAccountdeactivate($isExist->id,$isExist))
			{
				$data = array('status' => 'success','message'=> display('User has been inactivated'));
			} else {

				$data = array('status' => 'failed','message'=> display('Sorry, Unable to inactivated the user'));
			}
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to inactivated the user'));
		}
		echo json_encode($data);
	}

	public function buyer_credential()
	{
		if($this->request->getMethod() == 'post')
		{
			$user_id = $this->request->getPost('id');
			$new_pswd = $this->request->getPost('new_pswd');
			$confrim_pswd = $this->request->getPost('confrim_pswd');
			$user = $this->supplier->getWhere(['id' => $user_id])->getRow();

			$this->supplier->update($user_id, ['password' => $confrim_pswd]);
			if ($this->supplier->affectedRows()) {

				$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
				$mail['data'].=display('Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>').$confrim_pswd.'</b>'."\r\n";
				$mail['data'].='<br>'.display('Please Update your password.');
				$mail['data'].='<br>'.display('Thanks & Regards');

				$email = \Config\Services::email();

				$email->setTo($user->email);
				$email->setFrom('Forgot Password');
				
				$msg = view('common/email_template',$mail);
				$email->setSubject('Test subject');
				$email->setMessage($msg);

				if (!$email->send()) 
				{
				    $data = $email->printDebugger(['headers']);
				    print_r($data);
				}

                $this->session->setFlashData('message',display('Buyer credential updated Successfully'));
				return redirect()->to(base_url('admin/view_buyer/'.$user_id));
			} else {
				$this->session->setFlashData('exception',display('Sorry, Unable to update buyer credential'));
				return redirect()->to(base_url('admin/view_buyer/'.$user_id));
	        }
			
		} 
	}

	private function sendAccountactivate($user_id,$user){
		helper('text');
		$password = random_string('alnum',10);
        $data['password'] = $password;
		if($this->supplier->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Thanks for contacting regarding to account activation,This Is Your login credential <br><b>Email : </b> <b>').$user->email.'<b>'.display('<br><b>Password : </b><b>').$password.'</b>'."\r\n";
			$mail['data'].='<br>'.display('Please Update your password.');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('YAHODIHIME');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Account Activation');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}

	private function sendAccountdeactivate($user_id,$user){
		helper('text');
		$password = random_string('alnum',10);
        $data['password'] = $password;
		if($this->supplier->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Sorry, Your account is Deactivated due to some reason...!')."\r\n";
			// $mail['data'].='<br>'.display('Please Update your password.');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('YAHODIHIME');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Account De-activated');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}

	public function Send_notification()
	{
		$user_id = $this->request->getPost('user_id');
		$isExist = $this->supplier->getWhere(['id' => $user_id])->getRow();
		if (!empty($isExist)) {
			$notification = array(
									'title' => $this->request->getPost('title'),
									'description' => $this->request->getPost('description'),
									'user_id' => $isExist->id,
									'user_role' => $isExist->user_role,
									'read_status' => 'Unread',
								 );
			$this->notifiy->insert($notification);
			$inserted_id = $this->notifiy->getInsertID();
            $this->session->setFlashData('message',display('Notification send to supplier Successfully'));
			return redirect()->to(base_url('admin/view_buyer/'.$user_id));
		} else {
			$this->session->setFlashData('exception',display('Unable to send notification to supplier'));
			return redirect()->to(base_url('admin/view_buyer/'.$user_id));
        }

	}

	//--------------------------------------------------------------------

}
