<?php 
namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\Countries_model;
use App\Models\admin\Subcategories_model;
use App\Models\admin\Categories_model;

class Sub_Categories extends BaseController
{
	private $country = null;
	private $admin = null;

	public function __construct()
	{
		$this->country = new Countries_model();
		$this->admin = new AdminModel();
		$this->sub_categories = new Subcategories_model();
		$this->categories = new Categories_model();
	}

	public function index()
	{
		
	}

	public function sub_category_list()
	{
		$categories_items = $this->categories->where('deleted',2)->findAll();
		$category_list[' '] = display('Select Category');
		if(!empty($categories_items)){
			foreach ($categories_items as $c) {
				$category_list[$c->id] = $c->name;
			}
		}
		$data['categories'] = $category_list;
		$data['sub_categories'] = $this->sub_categories->where('deleted',2)->findAll();
		$data['title'] = "Sub-Categories";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/sub_category/sub_categories',$data);
    	return view('admin/layout/admin_wrapper', $data);
		
	}

	public function subcategory_add()
	{
		$file = $this->request->getFile('avatar');
		if (! $file->isValid())
        {
            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
        }

        $db      = \Config\Database::connect();
        $builder = $db->table('categories');
        $validated = $this->validate([
            'avatar' => [
                'max_dims[avatar,24,24]',
                'mime_in[avatar,image/png]',
                'max_size[avatar,24]',
            ],
            
        ]);
        if ($validated) {
        	if ($file->isValid() && ! $file->hasMoved())
	        {
	            $path = 'public/assets/uploads/images/subcategories_icon/';
	            $newName = $file->getRandomName();
	            $file->move($path,$newName);
				
				$data =array(
								'name' => $this->request->getPost('category_name'),
								'category_id' => $this->request->getPost('category_id'),
								'user_id' => $this->session->get('user_details')->admin_id,
								'icon_url' => $path.$newName,
								'created_by' => $this->session->get('user_details')->user_role,
							);
				$this->db->transStart();
				$this->sub_categories->insert($data);
				if(!$inserted_id = $this->sub_categories->getInsertID()){
					$this->session->setFlashData('exception',display('Sorry, Unable to add Sub-Category'));
					return redirect()->to(base_url('admin/sub_categories'));
				} 
				$this->db->transComplete();
				$this->session->setFlashData('message',display('Sub-Category Added Successfully'));
				return redirect()->to(base_url('admin/sub_categories'));
			}
		}
		return redirect()->to(base_url('admin/sub_categories'))->with('exception','please upload valid icon file...');
		

	}

	public function get_subcategory_details()
	{
		$category_id = $this->request->getPost('category_id');
		$category = $this->sub_categories->asObject()->where('id', $category_id)->first();
		if (!empty($category)) {
			$data = array('status' => 'success','category'=>$category);
		} else{
			$data = array('status' => 'failed','category'=>'');
		}
		echo json_encode($data);
	}

	public function delete_subcategory()
	{
		$category_id = $this->request->getPost('category_id');
		$category = $this->sub_categories->find($category_id);
		$category->deleted = 1;
		if($this->sub_categories->save($category)){
			$data = array('status' => 'success','message'=> display('Sub-Category Deleted Successfully'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry , Unable to delete sub-category'));
		}
		echo json_encode($data);
		
	}	

	public function subcategory_edit()
	{		
		$id = $this->request->getPost('id');
		$file = $this->request->getFile('avatar');
		$old_icon_url = $this->request->getPost('old_icon_url');
        $data['category_id'] = $this->request->getPost('category_id_upd');
        $data['name'] = $this->request->getPost('category_name');
		if (! $file->isValid())
        {
            // throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
            $data['icon_url'] = $this->request->getPost('old_icon_url');

        } else {    	
	        $db      = \Config\Database::connect();
	        $builder = $db->table('categories');
	        $validated = $this->validate([
	            'avatar' => [
	                'max_dims[avatar,24,24]',
	                'mime_in[avatar,image/png]',
	                'max_size[avatar,24]',
	            ],
	            
	        ]);
	        if ($validated) {
	        	if ($file->isValid() && ! $file->hasMoved())
		        {
		            $path = 'public/assets/uploads/images/subcategories_icon/';
		            $newName = $file->getRandomName();
		            $file->move($path,$newName);

		            $data['icon_url'] = $path.$newName;
		            if(file_exists($old_icon_url)){ unlink($old_icon_url); }
				}
			} else{
				return redirect()->to(base_url('admin/sub_categories'))->with('exception','please upload valid icon file...');		
			}
        }

        $this->db->transStart();
		$this->sub_categories->update($id,$data);
		if(!$this->sub_categories->affectedRows()){
			if(file_exists($path.$newName)){ unlink($path.$newName); }
			$this->session->setFlashData('exception',$this->sub_categories->errors());
			return redirect()->to(base_url('admin/sub_categories'));
		} 
		$this->db->transComplete();
		$this->session->setFlashData('message',display('Sub-Category Updated Successfully'));
		return redirect()->to(base_url('admin/sub_categories'));
	}	


	public function sub_category_activate()
	{
		$category_id = $this->request->getPost('category_id');		
		if($this->sub_categories->update($category_id,['status' => 'Active'])){
			$data = array('status' => 'success','message'=> display('Sub-Category has been activated'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to activated the sub-category'));
		}
		echo json_encode($data);
	}	

	public function sub_category_deactivate()
	{
		$category_id = $this->request->getPost('category_id');		
		if($this->sub_categories->update($category_id,['status' => 'Inactive'])){
			$data = array('status' => 'success','message'=> display('Sub-Category has been inactivated'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to inactivated the category'));
		}
		echo json_encode($data);
		
	}

	//--------------------------------------------------------------------

}
