<?php 
namespace App\Controllers\admin;

use App\Models\supplier\Supplier_model;
use App\Models\AdminModel;
use App\Models\QuotationModel;
use App\Models\OrdersModel;
use App\Models\AdminTransactionModel;
use App\Models\ReceiptModel;
use App\Controllers\BaseController;

class PaymentHistorty extends BaseController
{
	
	private $admin = null;
	private $supplier = null;
	private $quote = null;
	private $order = null;
	private $transaction = null;
	private $receipt = null;

	public function __construct()
	{
		helper('text');
		$this->admin = new AdminModel();
		$this->transaction = new AdminTransactionModel();
		$this->supplier = new Supplier_model();
		$this->quote = new QuotationModel();
		$this->order = new OrdersModel();
		$this->receipt = new ReceiptModel();
		
	}
	
	public function payment_historty_list()
	{
	  	// echo json_encode($_POST);die();
	    $admin_id = session()->get('user_details')->admin_id;
	    $_SESSION['transaction']['transaction_no'] = '';
	    $_SESSION['transaction']['order_no'] = '';
	    $_SESSION['transaction']['quote_no'] = '';
        $_SESSION['transaction']['user_id'] = '';
        $_SESSION['transaction']['method'] = '';
        $_SESSION['transaction']['type'] = '';
        $_SESSION['transaction']['start'] = '';
        $_SESSION['transaction']['end'] = '';
        $_SESSION['transaction']['seller_id'] = '';
       
        if(!empty($_POST['transaction_filter']))
        {
            $_SESSION['transaction']['transaction_no'] =(isset($_POST['transaction_no']) && !empty($_POST['transaction_no']))?trim($_POST['transaction_no']):'';
            $_SESSION['transaction']['order_no'] =(isset($_POST['order_no']) && !empty($_POST['order_no']))?trim($_POST['order_no']):'';
            $_SESSION['transaction']['quote_no'] =(isset($_POST['quote_no']) && !empty($_POST['quote_no']))?trim($_POST['quote_no']):'';
            $_SESSION['transaction']['user_id'] =(isset($_POST['user_id']) && !empty($_POST['user_id']))?trim($_POST['user_id']):'';
            $_SESSION['transaction']['method'] =(isset($_POST['method']) && !empty($_POST['method']))?trim($_POST['method']):'';
            $_SESSION['transaction']['type'] =(isset($_POST['type']) && !empty($_POST['type']))?trim($_POST['type']):'';
            $_SESSION['transaction']['seller_id'] =(isset($_POST['seller_id']) && !empty($_POST['seller_id']))?trim($_POST['seller_id']):'';
            $_SESSION['transaction']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['transaction']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
       	    $_SESSION['transaction']['transaction_no'] = '';
		    $_SESSION['transaction']['order_no'] = '';
		    $_SESSION['transaction']['quote_no'] = '';
	        $_SESSION['transaction']['user_id'] = '';
	        $_SESSION['transaction']['method'] = '';
	        $_SESSION['transaction']['type'] = '';
	        $_SESSION['transaction']['start'] = '';
            $_SESSION['transaction']['end'] = '';
	        $_SESSION['transaction']['seller_id'] = '';

        }

		$data['transaction'] = $this->transaction->getallAdmintransactionList($_SESSION['transaction'],$admin_id);
	    // echo $this->transaction->getLastQuery();die();
	    $db      = \Config\Database::connect();
		$builder = $db->table('admin_transactions');
		$builder->select('admin_transactions.paid_by,user.firstname,user.lastname');
		$builder->distinct('paid_by');
		$builder->join('application_users AS user', 'user.id = admin_transactions.paid_by');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select Customer');
		if(!empty($users)){
			foreach ($users as $customer) {
				$users_list[$customer->paid_by] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$builder = $db->table('admin_transactions');
		$builder->select('admin_transactions.paid_to,user.firstname,user.lastname');
		$builder->distinct('paid_to');
		$builder->join('application_users AS user', 'user.id = admin_transactions.paid_to');
		$sellers  = $builder->get()->getResult();

		$seller_list[' '] = display('Please Select Seller');
		if(!empty($sellers)){
			foreach ($sellers as $seler) {
				$seller_list[$seler->paid_to] = $seler->firstname.' '.$seler->lastname;
			}
		}
		
		$payment_method[' '] = display('Please Select Payment Method');
		$payment_method['mtn'] = 'MTN';
		$payment_method['togo'] = 'Togo Cell';
		$payment_method['orange'] = 'Orange';

		// echo json_encode($data);die();
		$data['users_list'] = $users_list;
		$data['seller_list'] = $seller_list;
		$data['payment_method'] = $payment_method;
		$data['title'] = "Payment Historty";
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/payment_historty',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function download_receipt($receipt_no)
	{
	    $admin_id = session()->get('user_details')->admin_id;
		// $data['receipt'] = $this->receipt->find($receipt_no);
		// echo json_encode($data);die();
		// getReceiptInvoice($data);
		$db      = \Config\Database::connect();

		$builder = $db->table('receipts');
		$builder->select('receipts.*,to.address_id,to.payment_mode,quote.id AS quote_id,quote.amount AS quote_amounts,add.address,add.city,add.state,add.country');
		$builder->join('orders AS to', 'receipts.order_id = to.id');
		$builder->join('quotations AS quote', 'quote.id = to.q_id');
		$builder->join('user_addresses AS add', 'add.id = to.address_id');
		$builder->where('receipts.receipt_id', $receipt_no);
		$builder->where('receipts.paid_to', $admin_id);
		$data['receipt']  = $builder->get()->getRow();
		// echo json_encode($data);die();
		return view('common/payment_reciept', $data);
		// $pdf_file = 'public/assets/uploads/images/invoices/invoices_'. strtotime(date("Y-m-d H:i:s"));
  //     	$pdf_path = $pdf_file.'.pdf';
  //   	$mpdf = new \Mpdf\Mpdf();
  //       $html = view('common/payment_reciept', $data);
  //       $mpdf->WriteHTML($html);
  //       $mpdf->Output(UPLOAD_PATH.$pdf_path, "F");
	}


	//--------------------------------------------------------------------
}
