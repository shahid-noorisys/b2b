<?php 
namespace App\Controllers\admin;

use App\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\Countries_model;
use App\Models\supplier\Supplier_model;
use App\Models\UserAddressModel;
use App\Models\UserCompanyModel;
use App\Models\NotificationModel;
use App\Models\UserBankModel;


class ManageSupplier extends BaseController
{
	private $country = null;
	private $admin = null;
	private $supplier = null;
	private $company = null;
	private $notifiy = null;
	private $bank = null;

	public function __construct()
	{
		$this->country = new Countries_model();
		$this->admin = new AdminModel();
		$this->supplier = new Supplier_model();
		$this->address = new UserAddressModel();
		$this->company = new UserCompanyModel();
		$this->notifiy = new NotificationModel();
		$this->bank = new UserBankModel();
	}

	public function supplier_list()
	{
		$_SESSION['seller']['country_id'] = '';
        $_SESSION['seller']['state'] = '';
        $_SESSION['seller']['city'] = '';
        $_SESSION['seller']['status'] = '';
        $_SESSION['seller']['gender'] = '';
        $_SESSION['seller']['start'] = '';
        $_SESSION['seller']['end'] = '';

        if(!empty($_POST['seller_filter']))
        {
            $_SESSION['seller']['country_id'] =(isset($_POST['country_id']) && !empty($_POST['country_id']))?trim($_POST['country_id']):'';
            $_SESSION['seller']['state'] =(isset($_POST['state']) && !empty($_POST['state']))?trim($_POST['state']):'';
            $_SESSION['seller']['city'] =(isset($_POST['city']) && !empty($_POST['city']))?trim($_POST['city']):'';
            $_SESSION['seller']['gender'] =(isset($_POST['gender']) && !empty($_POST['gender']))?trim($_POST['gender']):'';
            $_SESSION['seller']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['seller']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['seller']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
            $_SESSION['seller']['country_id'] ='';
            $_SESSION['seller']['state'] = '';
            $_SESSION['seller']['city'] = '';
            $_SESSION['seller']['status'] = '';
            $_SESSION['seller']['gender'] = '';
            $_SESSION['seller']['start'] = '';
            $_SESSION['seller']['end'] = '';
        }

		$data['sellers'] = $this->supplier->getallSellerList($_SESSION['seller']);

		$countries = $this->country->findAll();
		$country_list[' '] = display('Select Country');
		foreach ($countries as $v) {
			$country_list[$v->id] = $v->name;
		}
		$data['countries'] = $country_list;

		$data['title'] = "Manage Suppliers";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/manage_supplier/supplier_list',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}	
	public function view_supplier_profile()
	{
		$uri = service('uri');
		$user_id = $uri->getSegment(3);
		$data['profile'] = $this->supplier->where('deleted',2)->where('user_role',2)->find($user_id);
		$data['address'] = $this->address->where('user_id',$user_id)->where('is_primary',1)->first();
		$data['company'] = $this->company->where('user_id',$user_id)->first();
		$data['bank'] = $this->bank->where('user_id',$user_id)->first();
		// echo json_encode($data['company']);die();
		$data['title'] = "Manage Suppliers";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/manage_supplier/view_profile',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function supplier_activate()
	{
		$user_id = $this->request->getPost('user_id');
		$isExist = $this->supplier->getWhere(['id' => $user_id])->getRow();
		$this->supplier->allowCallbacks(false);
		if($this->supplier->update($user_id,['status' => 'Active'])){
			if($this->sendAccountactivate($isExist->id,$isExist))
			{
				$data = array('status' => 'success','message'=> display('User has been activated'));
			} else {
				
				$data = array('status' => 'failed','message'=> display('Sorry, Unable to activated the user'));
			}
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to activated the user'));
		}
		echo json_encode($data);
	}	

	public function supplier_deactivate()
	{
		$user_id = $this->request->getPost('user_id');
		$isExist = $this->supplier->getWhere(['id' => $user_id])->getRow();
		$this->supplier->allowCallbacks(false);
		if($this->supplier->update($user_id,['status' => 'Inactive'])){
			if($this->sendAccountdeactivate($isExist->id,$isExist))
			{
				$data = array('status' => 'success','message'=> display('User has been inactivated'));
			} else {

				$data = array('status' => 'failed','message'=> display('Sorry, Unable to inactivated the user'));
			}
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to inactivated the user'));
		}
		echo json_encode($data);
		
	}

	public function supplier_credential()
	{
		if($this->request->getMethod() == 'post')
		{
			$user_id = $this->request->getPost('id');
			$new_pswd = $this->request->getPost('new_pswd');
			$confrim_pswd = $this->request->getPost('confrim_pswd');
			$user = $this->supplier->getWhere(['id' => $user_id])->getRow();
			
			$this->supplier->update($user_id, ['password' => $confrim_pswd]);
			if ($this->supplier->affectedRows()) {

				$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
				$mail['data'].=display('Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>').$confrim_pswd.'</b>'."\r\n";
				$mail['data'].='<br>'.display('Please Update your password.');
				$mail['data'].='<br>'.display('Thanks & Regards');

				$email = \Config\Services::email();

				$email->setTo($user->email);
				$email->setFrom('Forgot Password');
				
				$msg = view('common/email_template',$mail);
				$email->setSubject('Test subject');
				$email->setMessage($msg);

				if (!$email->send()) 
				{
				    $data = $email->printDebugger(['headers']);
				    print_r($data);
				}
                $this->session->setFlashData('message',display('supplier credential updated Successfully'));
				return redirect()->to(base_url('admin/view_supplier/'.$user_id));
			} else {
				$this->session->setFlashData('exception',display('Sorry, Unable to update supplier credential'));
				return redirect()->to(base_url('admin/view_supplier/'.$user_id));
	        }
			
		} 
	}

	private function sendPassword($user_id,$user){
		helper('text');
		$password = random_string('alnum',10);
        $data['password'] = $password;
		if($this->supplier->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>').$password.'</b>'."\r\n";
			$mail['data'].='<br>'.display('Please Update your password.');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('Forgot Password');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Test subject');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}

	private function sendAccountactivate($user_id,$user){
		helper('text');
		$password = random_string('alnum',10);
        $data['password'] = $password;
		if($this->supplier->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Thanks for contacting regarding to account activation,This Is Your login credential <br><b>Email : </b> <b>').$user->email.'<b>'.display('<br><b>Password : </b><b>').$password.'</b>'."\r\n";
			$mail['data'].='<br>'.display('Please Update your password.');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('YAHODIHIME');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Account Activation');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}

	private function sendAccountdeactivate($user_id,$user){
		helper('text');
		$password = random_string('alnum',10);
        $data['password'] = $password;
		if($this->supplier->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Sorry, Your account is Deactivated due to some reason...!')."\r\n";
			// $mail['data'].='<br>'.display('Please Update your password.');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('YAHODIHIME');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Account De-activated');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}

	public function Send_notification()
	{
		$user_id = $this->request->getPost('user_id');
		$isExist = $this->supplier->getWhere(['id' => $user_id])->getRow();
		if (!empty($isExist)) {
			$notification = array(
									'title' => $this->request->getPost('title'),
									'description' => $this->request->getPost('description'),
									'user_id' => $isExist->id,
									'user_role' => $isExist->user_role,
									'read_status' => 'Unread',
								 );
			$this->notifiy->insert($notification);
			$inserted_id = $this->notifiy->getInsertID();
            $this->session->setFlashData('message',display('Notification send to supplier Successfully'));
			return redirect()->to(base_url('admin/view_supplier/'.$user_id));
		} else {
			$this->session->setFlashData('exception',display('Unable to send notification to supplier'));
			return redirect()->to(base_url('admin/view_supplier/'.$user_id));
        }

	}
	//--------------------------------------------------------------------

}
