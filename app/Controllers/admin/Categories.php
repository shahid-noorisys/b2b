<?php 
namespace App\Controllers\admin;

use Config\Services;
use App\Controllers\BaseController;
use App\Models\AdminModel;
use App\Models\Countries_model;
use App\Models\admin\Categories_model;

class Categories extends BaseController
{
	private $country = null;
	private $admin = null;

	public function __construct()
	{
		$this->country = new Countries_model();
		$this->admin = new AdminModel();
		$this->categories = new Categories_model();
	}

	public function index()
	{
		
	}

	public function category_list()
	{

		$data['categories'] = $this->categories->where('deleted',2)->findAll();
		$data['title'] = "Categories";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/category/categories',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function category_add()
	{
		$file = $this->request->getFile('avatar');
		if (! $file->isValid())
        {
            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
        }

        $db      = \Config\Database::connect();
        $builder = $db->table('categories');
        $validated = $this->validate([
            'avatar' => [
                'max_dims[avatar,24,24]',
                'mime_in[avatar,image/png]',
                'max_size[avatar,24]',
                'errors' => [
                 'max_dims' => 'icon should be 24*24 in dimension',
		         'mime_in' => 'icon type should be .png',
            	]
            ],
            
        ]);

        if ($validated) {
	        if ($file->isValid() && ! $file->hasMoved())
	        {
	            $path = 'public/assets/uploads/images/categories_icon/';
	            $newName = $file->getRandomName();
	            $file->move($path,$newName);
				
				$data =array(
								'name' => $this->request->getPost('category_name'),
								'user_id' => $this->session->get('user_details')->admin_id,
								'icon_url' => $path.$newName,
								'created_by' => $this->session->get('user_details')->user_role,
							);

				$this->db->transStart();
				$this->categories->insert($data);
				$inserted_id = $this->categories->getInsertID();
				if(!$inserted_id){
					// $this->session->setFlashData('exception',display('Sorry, Unable to add category'));
					$this->session->setFlashData('exception',$this->categories->errors());
					return redirect()->to(base_url('admin/categories'));
				} 
				$this->db->transComplete();
				$this->session->setFlashData('message',display('Category Added Successfully'));
				return redirect()->to(base_url('admin/categories'));
			
	        }
	    }
	    return redirect()->to(base_url('admin/categories'))->with('exception','please upload valid icon file...');

	}

	public function category_edit()
	{
			
		$id = $this->request->getPost('id');
		$file = $this->request->getFile('avatar');
		$old_icon_url = $this->request->getPost('old_icon_url');
        $data['name'] = $this->request->getPost('category_name');
		if (! $file->isValid())
        {
            // throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
            $data['icon_url'] = $this->request->getPost('old_icon_url');
        } else {

	        $db      = \Config\Database::connect();
	        $builder = $db->table('categories');
	        $validated = $this->validate([
	            'avatar' => [
	                'max_dims[avatar,24,24]',
	                'mime_in[avatar,image/png]',
	                'max_size[avatar,24]',
	            ],
	            
	        ]);

	        if ($validated) {
		        if ($file->isValid() && ! $file->hasMoved())
		        {
		            $path = 'public/assets/uploads/images/categories_icon/';
		            $newName = $file->getRandomName();
		            $file->move($path,$newName);
					
					$data['icon_url'] = $path.$newName;
		            if(file_exists($old_icon_url)){ unlink($old_icon_url);}
				}
			} else {
			    return redirect()->to(base_url('admin/categories'))->with('exception','please upload valid icon file...');		
			}
        }

        $this->db->transStart();
		$this->categories->update($id,$data);
		if(!$this->categories->affectedRows()){
			if(file_exists($path.$newName)){ unlink($path.$newName); }
			$this->session->setFlashData('exception',display('Sorry, Unable to update category'));
			return redirect()->to(base_url('admin/categories'));
		} 
		$this->db->transComplete();
		$this->session->setFlashData('message',display('Category Updated Successfully'));
		return redirect()->to(base_url('admin/categories'));

	}	

	public function delete_category()
	{
		$category_id = $this->request->getPost('category_id');
		$category = $this->categories->find($category_id);
		$category->deleted = 1;
		if($this->categories->save($category)){
			$data = array('status' => 'success','message'=> display('Category Deleted Successfully'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry , Unable to delete category'));
		}
		echo json_encode($data);
		
	}	

	public function get_category_details()
	{
		$category_id = $this->request->getPost('category_id');
		$category = $this->categories->asObject()->where('id', $category_id)->first();
		if (!empty($category)) {
			$data = array('status' => 'success','category'=>$category);
		} else{
			$data = array('status' => 'failed','category'=>'');
		}
		echo json_encode($data);
	}

	public function category_activate()
	{
		$category_id = $this->request->getPost('category_id');		
		$this->categories->allowCallbacks(false);
		if($this->categories->update($category_id,['status' => 'Active'])){
			$data = array('status' => 'success','message'=> display('Category has been activated'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to activated the category'));
		}
		echo json_encode($data);
	}	

	public function category_deactivate()
	{
		$category_id = $this->request->getPost('category_id');		
		$this->categories->allowCallbacks(false);
		if($this->categories->update($category_id,['status' => 'Inactive'])){
			$data = array('status' => 'success','message'=> display('Category has been inactivated'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to inactivated the category'));
		}
		echo json_encode($data);
		
	}


	//--------------------------------------------------------------------

}
