<?php 
namespace App\Controllers\admin;

use App\Models\supplier\Supplier_model;
use App\Models\AdminModel;
use App\Models\QuotationModel;
use App\Models\OrdersModel;
use App\Models\ReviewandRatingModel;
use App\Controllers\BaseController;

class Reviews extends BaseController
{
	
	private $admin = null;
	private $supplier = null;
	private $quote = null;
	private $order = null;
	private $review = null;

	public function __construct()
	{
		helper('text');
		$this->admin = new AdminModel();
		$this->review = new ReviewandRatingModel();
		$this->supplier = new Supplier_model();
		$this->quote = new QuotationModel();
		$this->order = new OrdersModel();
		
	}
	
	public function reviews_list()
	{

	    $db      = \Config\Database::connect();
		$builder = $db->table('buyer_reviews');
		$builder->select('buyer_reviews.buyer_id,user.firstname,user.lastname');
		$builder->distinct('buyer_id');
		$builder->join('application_users AS user', 'user.id = buyer_reviews.buyer_id');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select Customer');
		if(!empty($users)){
			foreach ($users as $customer) {
				$users_list[$customer->buyer_id] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$data['users_list'] = $users_list;
		$data['title'] = "Reviews & Rating";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/reviews',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function ajaxReviewslist() 
	{
		$pageNo = $this->request->getGet('page');
        $data['user_id'] = $user_id = $this->session->get('user_details')->admin_id;

        if($pageNo == '' || $pageNo == null){ $pageNo = 1;}

        $data['reviews'] = $this->review->orderBy('created_date','DESC')->paginate();
        $total = $this->review->countAllResults();
        
        $pager = $this->review->pager;
        $data['pagination'] = $pager->makeLinks($pageNo,env('pager.perPage'),$total,'seller_pager');
        $response = ['html' => view('admin/ajax_reviews_list',$data),'page_no' => $pageNo];
        echo json_encode($response);
	}


	//--------------------------------------------------------------------
}
