<?php 
namespace App\Controllers\admin;

use App\Libraries\Tpdf;
use App\Models\AdminModel;
use App\Models\OrdersModel;
use App\Models\QuotationModel;
use App\Models\NotificationModel;
use App\Controllers\BaseController;

class Orders extends BaseController
{
	
	private $admin = null;
	private $quote = null;
	private $order = null;
	private $notifiy = null;

	public function __construct()
	{
		helper('text');
		$this->admin = new AdminModel();
		$this->notifiy = new NotificationModel();
		$this->quote = new QuotationModel();
		$this->order = new OrdersModel();
		
	}
	
	public function orders_list()
	{
	  	

	    $admin_id = session()->get('user_details')->admin_id;
	    // echo json_encode($_POST);die();
	    $_SESSION['order']['order_no'] = '';
        $_SESSION['order']['quote_no'] = '';
        $_SESSION['order']['user_id'] = '';
        $_SESSION['order']['status'] = '';
        $_SESSION['order']['start'] = '';
        $_SESSION['order']['end'] = '';
        $_SESSION['order']['customer_id'] = '';

        if(!empty($_POST['order_filter']))
        {
            $_SESSION['order']['order_no'] =(isset($_POST['order_no']) && !empty($_POST['order_no']))?trim($_POST['order_no']):'';
            $_SESSION['order']['quote_no'] =(isset($_POST['quote_no']) && !empty($_POST['quote_no']))?trim($_POST['quote_no']):'';
            $_SESSION['order']['user_id'] =(isset($_POST['user_id']) && !empty($_POST['user_id']))?trim($_POST['user_id']):'';
            $_SESSION['order']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['order']['customer_id'] =(isset($_POST['customer_id']) && !empty($_POST['customer_id']))?trim($_POST['customer_id']):'';
            $_SESSION['order']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['order']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
            $_SESSION['order']['order_no'] ='';
            $_SESSION['order']['quote_no'] = '';
            $_SESSION['order']['user_id'] = '';
            $_SESSION['order']['status'] = '';
            $_SESSION['order']['start'] = '';
	        $_SESSION['order']['end'] = '';
	        $_SESSION['order']['customer_id'] = '';
        }

        $data['orders'] = $this->order->getalladminOrdersList($_SESSION['order'],$admin_id);
        // echo  $this->order->getLastQuery();die();
	    $db      = \Config\Database::connect();
		$builder = $db->table('orders');
		$builder->select('orders.supplier_id,user.firstname,user.lastname');
		$builder->distinct('supplier_id');
		$builder->join('application_users AS user', 'user.id = orders.supplier_id');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select Seller');
		if(!empty($users)){
			foreach ($users as $user) {
				$users_list[$user->supplier_id] = $user->firstname.' '.$user->lastname;
			}
		}

		$builder = $db->table('orders');
		$builder->select('orders.buyer_id,user.firstname,user.lastname');
		$builder->distinct('buyer_id');
		$builder->join('application_users AS user', 'user.id = orders.buyer_id');
		$buyer  = $builder->get()->getResult();

		$customer_list[' '] = display('Please Select Customer');
		if(!empty($buyer)){
			foreach ($buyer as $customer) {
				$customer_list[$customer->buyer_id] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$data['seller_list'] = $users_list;
		$data['customer_list'] = $customer_list;
		$data['title'] = "orders";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/orders/order_list',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function orders_details($order_id)
	{
		// dd($order_id);
		$admin_id = $this->session->get('user_details')->admin_id;

		$builder = $this->db->table('orders AS ord');
		$builder->select("ord.*,quote.id AS quote_id,quote.amount AS quote_amounts,add.address,add.city,add.state,add.country,CONCAT(u.firstname,' ',u.lastname) AS buyer_name,CONCAT(sup.firstname,' ',sup.lastname) AS seller_name");
		$builder->join('quotations AS quote', 'quote.id = ord.q_id','left');
		$builder->join('user_addresses AS add', 'add.id = ord.address_id','left');
		$builder->join('application_users AS u','u.id = ord.buyer_id','left');
		$builder->join('application_users AS sup','sup.id = ord.supplier_id','left');
		$builder->where('ord.id', $order_id);
		$builder->orderBy('ord.id','DESC');
		$data['order']  = $builder->get()->getRow();
		// echo $this->db->getLastQuery();
		// dd($data['order']);
		
		$data['title'] = "Order details";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/orders/order_details',$data);
    	return view('admin/layout/admin_wrapper', $data);
	}

	public function order_status()
	{
		$order_id = $this->request->getPost('order_id');
		$order_status = $this->request->getPost('state');
		$order_detail = $this->order->where(['id' => $order_id])->first();
		$customer = get_user_details_by_id($order_detail->buyer_id,3);
		if($this->order->update($order_id,['order_status' => $order_status])){

			$notification = array(
									'title' => 'Order status',
									'description' => 'Status changed from '.ucfirst($order_detail->order_status).' to '.ucfirst($order_status),
									'user_id' => $order_detail->supplier_id,
									'user_role' => 3,
									'read_status' => 'Unread',
								 );
			$this->notifiy->insert($notification);
			$inserted_id = $this->notifiy->getInsertID();

			$notification = array(	
								array( 
									'title' => 'Order status',
									'description' => 'Status changed from '.ucfirst($order_detail->order_status).' to '.ucfirst($order_status),
									'user_id' => $order_detail->buyer_id,
									'user_role' => 2,
									'read_status' => 'Unread',
								 ),
								array( 
									'title' => 'Order status',
									'description' => 'Status changed from '.ucfirst($order_detail->order_status).' to '.ucfirst($order_status),
									'user_id' => $order_detail->buyer_id,
									'user_role' => 3,
									'read_status' => 'Unread',
								 )
							);
			$db      = \Config\Database::connect();
			$builder = $db->table('platform_notifications');
			$builder->insertBatch($notification);

			// $msg['data']  = '<p style="text-align: center;"><strong>'.display('Hello').', '.$customer->firstname.' '.$customer->lastname.'<br />Your Order Number #'.$order_detail->order_no.' status has been changed from Pending to In_progress</strong></p>'."\r\n";
			// $msg['data'].='<br>'.display('Thanks & Regards');

			// $email = \Config\Services::email();

			// $email->setTo($customer->email);
			// $email->setFrom('Order status changed');
			
			// $msg = view('common/email_template',$msg);
			// $email->setSubject('Order status changed');
			// $email->setMessage($msg);
			// if (!$email->send()) 
			// {
					
			// } 
			if ($order_status == 'delivered') {
				$this->createReceipt($order_detail->order_no);
			}
			$data = array('status' => 'success','message'=> display('Order status has been changed successfully.'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to change the status of order.'));
		}
		echo json_encode($data);
	}

	public function createReceipt()
	{
		// create new PDF document
		$pdf = new Tpdf();
		$order_id = 'ORD00002';
		$admin_id = $this->session->get('user_details')->admin_id;
		// $data['receipt'] = $this->receipt->find($receipt_no);
		// echo json_encode($data);die();
		// getReceiptInvoice($data);

        // $order_id = 'ORD00002';
        
        // $db = Database::connect();

		$builder = $this->db->table('receipts');
		$builder->select('receipts.*,to.address_id,to.payment_mode,quote.id AS quote_id,quote.amount AS quote_amounts,add.address,add.city,add.state,add.country,add.id AS add_id');
		$builder->join('orders AS to', 'receipts.order_id = to.id');
		$builder->join('quotations AS quote', 'quote.id = to.q_id');
		$builder->join('user_addresses AS add', 'add.id = to.address_id');
		$builder->where('receipts.order_no', $order_id);
		// $builder->where('receipts.paid_to', $admin_id);
		$data['receipt'] = $receipt  = $builder->get()->getRow();
		// echo $this->db->getLastQuery();
		// echo json_encode($data['receipt']);exit;
		$seller_company = get_company_by_user_id($receipt->paid_to);
		$buyer_company = get_company_by_user_id($receipt->paid_by);
		
		$AdderssModel = model('App\Models\UserAddressModel');
		$address_details = $AdderssModel->where(['id'=>$receipt->add_id])->first();
		$data['seller_company'] = ($seller_company->company_name)??'';
		$data['company_address'] = ($seller_company->address)??'';
		$data['seller_city'] = ($seller_company->city)??'';
		$data['seller_zip_code'] = ($seller_company->zip_code)??'';
		$data['seller_country'] = (!empty($seller_company) AND isset($seller_company->country))?get_country_by_id($seller_company->country)->name:'';
		if(!empty($buyer_company)){
			$data['buyer_company'] = ($buyer_company->company_name)??'';
			$data['buyer_address'] = ($buyer_company->address)??'';
			$data['buyer_city'] = ($buyer_company->city)??'';
			$data['buyer_zip_code'] = ($buyer_company->zip_code)??'';
			$data['buyer_country'] = (!empty($buyer_company) AND isset($buyer_company->country))?get_country_by_id($buyer_company->country)->name:'';
		} else {
			$data['buyer_company'] = 'dummy company';
			$data['buyer_address'] = 'dummy address';
			$data['buyer_city'] = 'dummy city';
			$data['buyer_zip_code'] = 'dummy zip code';
			$data['buyer_country'] = 'dummy country';//(!empty($buyer_company) AND isset($buyer_company->country))?get_country_by_id($buyer_company->country)->name:'';
		}
		// return view('common/payment_receipt', $data);
		$data['html'] = view('common/payment_receipt', $data);
		$pdf->generateReceipt($data);
	}

	//--------------------------------------------------------------------
}
