<?php 
namespace App\Controllers\admin;

use App\Models\AdminModel;
use App\Models\Countries_model;
use App\Models\supplier\Supplier_model;
use App\Models\PlatformCommissionModel;
use App\Controllers\BaseController;

class PlatformCommision extends BaseController
{
	private $country = null;
	private $admin = null;
	private $commision = null;
	private $seller = null;

	public function __construct()
	{
		$this->country = new Countries_model();
		$this->admin = new AdminModel();
		$this->commision = new PlatformCommissionModel();
		$this->seller = new Supplier_model();
	}

	public function commision_list()
	{
		$data['commisions'] = $this->commision->where('deleted',2)->findAll();
		$data['title'] = "Commision";
	    $data['path'] = base_url('admin/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('admin/commision/commision_list',$data);
    	return view('admin/layout/admin_wrapper', $data);
		
	}

	public function add_commision()
	{
			$data =array(
							'commission_charge' => $this->request->getPost('add_commision_percent'),
							'user_id' => $this->session->get('user_details')->admin_id,
							'created_by' => $this->session->get('user_details')->user_role,
						);
			$this->commision->insert($data);
			$inserted_id = $this->commision->getInsertID();
			if(!$inserted_id){

				$this->session->setFlashData('exception',display('Sorry, Unable to add platform commision!'));
				return redirect()->to(base_url('admin/commision'));
			} 
			$this->session->setFlashData('message',display('Platform commision added Successfully'));
			return redirect()->to(base_url('admin/commision'));
	}
	
	public function edit_commision()
	{
		$id = $this->request->getPost('commision_id');
		$commision_charge = $this->request->getPost('upd_commision_percent');
		$commision_details = $this->commision->where(['id' => $id])->first();
		$data =array(
						'commission_charge' => $this->request->getPost('upd_commision_percent'),
						'user_id' => $this->session->get('user_details')->admin_id,
						'created_by' => $this->session->get('user_details')->user_role,
					);
		$this->commision->update($id,$data);
		if(!$this->commision->affectedRows()){
			$this->session->setFlashData('exception',display('Sorry, Unable to update platform commision'));
			return redirect()->to(base_url('admin/commision'));
		} else{
			
			$seller = $this->seller->where('user_role',2)->where('status','Active')->findAll();
			$notification = [];
			if (!empty($seller) && isset($seller)) {
				foreach ($seller as $key => $value) {
				
				$notification [] =  [
										'title' => 'Platform Commision',
										'description' => 'Platform Commision Update From '.$commision_details->commission_charge.' To '.$commision_charge.' Percent',
										'user_id' => $value->id,
										'user_role' => $value->user_role,
										'read_status' => 'Unread',
										'created_date' => date('Y-m-d Hi:s'),
									];
				}
				// echo json_encode($notification);die();
				$db      = \Config\Database::connect();
				$builder = $db->table('platform_notifications');
				$builder->insertBatch($notification);
			}
			$this->session->setFlashData('message',display('Platform commision updated Successfully'));
			return redirect()->to(base_url('admin/commision'));
		}
	}

	public function get_commision_details()
	{
		$commision_id = $this->request->getPost('commision_id');
		$commision = $this->commision->where('id', $commision_id)->first();
		if (!empty($commision)) {
			$data = array('status' => 'success','commision'=>$commision);
		} else{
			$data = array('status' => 'failed','commision'=>'');
		}
		echo json_encode($data);
	}

	//--------------------------------------------------------------------

}
