<?php 
namespace App\Controllers\admin;

use App\Models\UserModel;
use App\Models\ProductModel;
use App\Models\Countries_model;
use App\Models\UserAddressModel;
use App\Controllers\BaseController;
use App\Models\admin\Categories_model;
use App\Models\supplier\Supplier_model;
use App\Models\admin\Subcategories_model;

class Products extends BaseController
{
	
	private $user = null;
	private $country = null;
	private $address = null;
	private $supplier = null;
	protected $subCatModel = null;
	protected $categoryModel = null;
	protected $productModel = null;
	
	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->address = new UserAddressModel();
		$this->country = new Countries_model();
		$this->supplier = new Supplier_model();
		$this->subCatModel = new Subcategories_model();
		$this->categoryModel = new Categories_model();
		$this->productModel = new ProductModel();
	}
	
	public function index()
	{
		
	}

	public function product_list()
	{

		$user_id = $this->session->get('user_details')->admin_id;
		// echo json_encode($_POST);die();

		$_SESSION['product']['user_id'] = '';
        $_SESSION['product']['category'] = '';
        $_SESSION['product']['sub_category'] = '';
        $_SESSION['product']['status'] = '';
        $_SESSION['product']['publish'] = '';
        $_SESSION['product']['start'] = '';
        $_SESSION['product']['end'] = '';

        if(!empty($_POST['product_filter']))
        {
            $_SESSION['product']['user_id'] =(isset($_POST['user_id']) && !empty($_POST['user_id']))?trim($_POST['user_id']):'';
            $_SESSION['product']['category'] =(isset($_POST['category']) && !empty($_POST['category']))?trim($_POST['category']):'';
            $_SESSION['product']['sub_category'] =(isset($_POST['sub_category']) && !empty($_POST['sub_category']))?trim($_POST['sub_category']):'';
            $_SESSION['product']['publish'] =(isset($_POST['publish']) && !empty($_POST['publish']))?trim($_POST['publish']):'';
            $_SESSION['product']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['product']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['product']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
            $_SESSION['product']['user_id'] ='';
            $_SESSION['product']['category'] = '';
            $_SESSION['product']['sub_category'] = '';
            $_SESSION['product']['status'] = '';
            $_SESSION['product']['publish'] = '';
            $_SESSION['product']['start'] = '';
            $_SESSION['product']['end'] = '';
        }

		$db      = \Config\Database::connect();
		$builder = $db->table('products');
		$builder->select('products.user_id,user.firstname,user.lastname');
		$builder->distinct('user_id');
		$builder->join('application_users AS user', 'user.id = products.user_id');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select supplier');
		if(!empty($users)){
			foreach ($users as $customer) {
				$users_list[$customer->user_id] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$categories = $this->categoryModel->findAll();
		$sub_categories = $this->subCatModel->findAll();
		$category_list[' '] = display('Select Category');
		if(!empty($categories)){
			foreach ($categories as $c) {
				$category_list[$c->id] = $c->name;
			}
		}
		$sub_category_list[' '] = display('Select Sub Category');
		if(!empty($sub_categories)){
			foreach ($sub_categories as $sub) {
				$sub_category_list[$sub->id] = $sub->name;
			}
		}


        $data['products'] = $this->productModel->getalladminProductList($_SESSION['product'],$user_id);
        $data['seller_list'] = $users_list;
        $data['categories'] = $category_list;
		$data['sub_categories'] = $sub_category_list;
		$data['title'] = "Products";
	    $data['path'] = base_url('admin/dashboard');
		$data['controller_name'] = "Dashboard";
	    $data['content'] = view('admin/products/product_list',$data);
    	return view('admin/layout/admin_wrapper', $data);
		
	}


	public function editProduct($product_id)
	{
		$data['title'] = "Product Edit";
	    $data['path'] = base_url('supplier/dashboard');
		$data['controller_name'] = "Dashboard";
		$data['product'] = $this->productModel->find($product_id);

		$categories = $this->categoryModel->findAll();
		$sub_categories = $this->subCatModel->findAll();
		$category_list[' '] = display('Select Category');
		if(!empty($categories)){
			foreach ($categories as $c) {
				$category_list[$c->id] = $c->name;
			}
		}
		$sub_category_list[' '] = display('Select Sub Category');
		if(!empty($sub_categories)){
			foreach ($sub_categories as $sub) {
				$sub_category_list[$sub->id] = $sub->name;
			}
		}

		$data['categories'] = $category_list;
		$data['sub_categories'] = $sub_category_list;
	    $data['content'] = view('supplier/products/product_edit',$data);
    	return view('supplier/layout/supplier_wrapper', $data);	
	}

	public function Product_edit($product_id)
	{
		$data['title'] = "Product Edit";
	    $data['path'] = base_url('admin/dashboard');
		$data['controller_name'] = "Dashboard";
		$data['product'] = $this->productModel->find($product_id);

		$categories = $this->categoryModel->findAll();
		$sub_categories = $this->subCatModel->findAll();
		$category_list[' '] = display('Select Category');
		if(!empty($categories)){
			foreach ($categories as $c) {
				$category_list[$c->id] = $c->name;
			}
		}
		$sub_category_list[' '] = display('Select Sub Category');
		if(!empty($sub_categories)){
			foreach ($sub_categories as $sub) {
				$sub_category_list[$sub->id] = $sub->name;
			}
		}

		$data['categories'] = $category_list;
		$data['sub_categories'] = $sub_category_list;
	    $data['content'] = view('admin/products/product_edit',$data);
    	return view('admin/layout/admin_wrapper', $data);	
	}

	public function updateProduct()
	{
		if($this->request->getMethod() == 'post')
		{
			// dd($this->request->getPost());
			$category_id = $this->request->getPost('category_id');
			$category_details = $this->categoryModel->find($category_id);
			$sub_category_id = $this->request->getPost('sub_category_id');
			$sub_cat_details = $this->subCatModel->find($sub_category_id);
			$product_id = $this->request->getPost('product_id');
			$data = [
				'name' => $this->request->getPost('product_name'),
				'category_id' => $category_id,
				'category_name' => $category_details->name,
				'sub_category_id' => $sub_category_id,
				'sub_category_name' => $sub_cat_details->name,
				'is_published' => $this->request->getPost('publish_status'),
				'description' => $this->request->getPost('description'),
			];
			if($this->productModel->update($product_id,$data))
			{
				$this->session->setFlashData('message',display('Product Information updated'));
			} else {
				$this->session->setFlashData('exception',display('Error occured in update'));
			}
			return redirect()->back();
		}
		else {
			$this->session->setFalshData('exception',display('Form is not submitted'));
			redirect()->back();
		}
	}

	public function getSubCategories()//call by ajax
	{
		$category_id = $this->request->getPost('category_id');
		$res = $this->subCatModel->where('category_id',$category_id)->findAll();
		$list[' '] = display('Select Sub Category');
		if(!empty($res)){
			foreach ($res as $sub) {
				$list[$sub->id] = $sub->name;
			}
		}
		$response = ['list' => form_dropdown('sub_category_id',$list,'',' class="select2 form-control" id="sub_category_id" ')];
		echo json_encode($response);
	}

	public function uploadProductImage()
	{
		if($this->request->getMethod() == 'post')
		{
			$files = $this->request->getFiles();
			$product_id = $this->request->getPost('product_id');
			$product = $this->productModel->asArray()->find($product_id);
			// var_dump($product['image_1']);exit;
			$data = array();
			$old_images = array();
			if($files)
			{
				$path = 'public/assets/uploads/images/products/';
				for ($i=1; $i <= count($files) ; $i++) { 
					if($files['image_'.$i]->isValid() && ! $files['image_'.$i]->hasMoved())
					{
						$newName = $files['image_'.$i]->getRandomName();
						$data['image_'.$i] = $path.$newName;
						$old_images['image_'.$i] = $product['image_'.$i];
						$files['image_'.$i]->move($path, $newName);
					}
				}
			}
			if(!empty($data))
			{
				if($this->productModel->update($product_id,$data)){
					foreach ($old_images as $v) {
						if(!empty($v) AND file_exists($v))
						{
							unlink('./'.$v);
						}
					}
					$this->session->setFlashData('message',display('Images are updated successfully'));
				} else {
					$this->session->setFlashData('exception',display('Error occured in update'));
				}
			} else {
				$this->session->setFlashData('exception', display('Nothing is changed'));
			}
			return redirect()->back();
		}
		else {
			$this->session->setFlashData('exception',display('Form is not submitted'));
			return redirect()->back();
		}
	}

}
