<?php 
namespace App\Controllers;

use App\Models\UserModel;
use App\Models\ProductModel;
use App\Models\Countries_model;
use App\Models\UserAddressModel;
use App\Controllers\BaseController;
use App\Models\admin\Categories_model;
use App\Models\admin\Subcategories_model;

class Home extends BaseController
{
	private $user = null;
	private $address = null;
	private $country = null;
	private $categoryModel = null;
	private $productModel = null;
	private $subCatModel = null;
	public function __construct()
	{
		$this->user = new UserModel();
		$this->address = new UserAddressModel();
		$this->country = new Countries_model();
		$this->categoryModel = new Categories_model();
		$this->productModel = new ProductModel();
		$this->subCatModel = new Subcategories_model();
	}
	public function index()
	{
		echo "welcome to index page";
	}

	public function language_toggle()
    {
    	$lang = $this->request->getPost('lang');
        $this->session->remove('lang');
        $this->session->set('lang', $lang);
    }

	public function dashboard()
	{
		$data['title'] = "Dashboard";
	    $data['path'] = base_url();
	    $data['controller_name'] = "Home";
		$data['categories'] = $this->categoryModel->where('deleted','No')->where('status','Active')->findAll();
		$data['top_products'] = $res = $this->productModel->where('status','Active')->where('deleted','No')->groupBy('sub_category_id')->orderBy('order_counter','DESC')->findAll(10,0);
		$data['new_products'] = $res = $this->productModel->where('status','Active')->where('deleted','No')->orderBy('created_date','DESC')->findAll(5,0);
		$data['topSubCategories'] = $this->subCatModel->where('status','Active')->where('deleted','No')->findAll(8,0);

		$data['best_products'] = $res = $this->productModel->where('status','Active')->where('deleted','No')->orderBy('order_counter','DESC')->findAll(9,0);

		$product_category = $this->db->query("SELECT p.id,p.category_id,c.name FROM `products` AS p JOIN categories AS c ON c.id = p.category_id WHERE order_counter = (SELECT MAX(order_counter) FROM products ORDER BY updated_date DESC LIMIT 1)")->getRow();
		$data['cate_title'] = explode(" ", $product_category->name, 2);

		$data['sub_category'] = $res = $this->subCatModel->where(['deleted'=>'No','category_id'=>$product_category->category_id,'status'=>'Active'])->findAll(6,0);

		// echo json_encode($data['sub_categories']);die();
		$data['content'] = view('buyer/home/dashboard',$data);
    	return view('layouts/buyer_wrapper', $data);
	}

	public function login()
	{
		// dd($_POST);
		$email = $this->request->getPost('email');
		$password = $this->request->getPost('password');
		$is_user = $this->user->authenticate($this->request->getPost());
		if($is_user)
		{
			$this->session->set('email',$is_user->email);
			if ($is_user->status == 'Inactive') {
					$response = ['status'=>'failed','message'=>display('Your account not activated, so can not logged in to system')];
			} else {
				if($this->sendOTP($is_user->id,$is_user))
				{
					$response = ['status'=>'success','message'=>display('OTP Send Successfully. Please check your email.')];
				} else {
					$response = ['status'=>'failed','message'=>display("OTP not send due to some errors")];
				}
			}
		} else {
			$response = ['status'=>'failed','message'=>display('Incorrect Email OR Password')];
		}
		echo json_encode($response);exit;
	}

	public function verification_otp()
	{
		if($this->request->getMethod() == 'post')
		{
			$otp = $this->request->getPost('OTP');
			$email = session()->get('email');
			$is_user = $this->user->where(['email'=> $email,'otp'=>$otp])->first();
			
            if(!empty($is_user))
			{
				$session_data = (object) [
					'user_id' => $is_user->id,
					'fullname' => $is_user->firstname.' '.$is_user->lastname,
					'email' => $is_user->email,
					'mobile' => $is_user->mobile,
					'user_role' => $is_user->user_role,
					'isLoggedIn' => true,
				];
				
				$this->session->set('user_details',$session_data);

				$this->user->allowCallbacks(false);
				$data['otp'] = '';
				if($this->user->update($is_user->id,$data)){
					$response = ['status'=>'success','message'=>display('OTP verified successfully.')];
				}
			} else {
					$response = ['status'=>'failed','message'=>display("invalid OTP")];
			}
			echo json_encode($response);
		} 
	}

	public function resend_otp_verification()
	{
		if($this->request->getMethod() == 'post')
		{
			$email = session()->get('email');
			$is_user = $this->user->where(['email'=> $email])->first();
			if($this->sendOTP($is_user->id,$is_user))
			{
				$response = array('status'=>'success','message'=>display('OTP Re-Send Successfully. Please check your email.'));
			} else {
				$response = array('status'=>'failed','message'=>display("OTP not send due to some errors"));
			}
			echo json_encode($response);
		}
	}	

	public function reset_otp()
	{
		if($this->request->getMethod() == 'post')
		{
			$email = session()->get('email');
			$is_user = $this->user->where(['email'=> $email])->first();
			$this->user->allowCallbacks(false);
			$data['otp'] = '';
			if($this->user->update($is_user->id,$data))
			{
				$response = array('status'=>'success','message'=>display('OTP Reset Successfully.'));
			} else {
				$response = array('status'=>'failed','message'=>display("OTP not reset due to some errors"));
			}
			echo json_encode($response);
		}
	}
	
	public function logout()
	{
		$this->session->remove('user_details');//some session data name for user details
		$this->session->destroy();
		$this->session->setFlashData('message',display('You are logged out Successfully'));
		return redirect()->to(base_url());
		// set flash data
		// redirect to home
	}

	public function registration()
	{
		if(loggedIn()) { return redirect()->to(base_url());}
		$data['title'] = display('Registration');
		$data['path'] = base_url();
		$data['controller_name'] = display('Home');
		$countries = $this->country->findAll();
		$country_list[' '] = display('Select Country');
		foreach ($countries as $v) {
			$country_list[$v->id] = $v->name;
		}
		$data['countries'] = $country_list;
		$data['content'] = view('buyer/home/registration',$data);
		return view('layouts/buyer_wrapper', $data);
	}
	public function register_buyer()
	{
		if($this->request->getMethod() == 'post')
		{
			// dd($this->request->getPost());
			$user_data = [
				'firstname' => $this->request->getPost('firstname'),
				'lastname' => $this->request->getPost('lastname'),
				'password' => $this->request->getPost('password'),
				'email' => $this->request->getPost('email'),
				'mobile' => $this->request->getPost('mobile'),
				'user_role' => 3,
				'gender' => $this->request->getPost('gender'),
				'city' => $this->request->getPost('city'),
				'state' => $this->request->getPost('state'),
				'country_id' => $this->request->getPost('country'),
				'country' => $this->country->find($this->request->getPost('country'))->name,
				'country_code' => $this->country->find($this->request->getPost('country'))->dial_code,
				'zip_code' => $this->request->getPost('zip_code'),
				'conf_password' => $this->request->getPost('conf_password'),
				'status' => 2,
			];
			// dd($user_data);
			$this->db->transStart();
			$this->user->insert($user_data);
			if(!$inserted_id = $this->user->getInsertID())
			{
				$this->session->setFlashData('exception',$this->user->errors());
				return redirect()->to('register-buyer')->withInput();
			}
			// $admin_id = $this->user->insertID();
			$address_data = [
				'user_id' => $inserted_id,
				'address' => $this->request->getPost('address'),
				'city' => $this->request->getPost('city'),
				'state' => $this->request->getPost('state'),
				'country' => $this->country->find($this->request->getPost('country'))->name,
				'country_code' => $this->country->find($this->request->getPost('country'))->dial_code,
				'currency_code' => $this->country->find($this->request->getPost('country'))->currency_code,
				'zip_code' => $this->request->getPost('zip_code'),
				'is_primary' => 1,
			];
			if(!$this->address->insert($address_data))
			{
				$this->session->setFlashData('exception',$this->address->errors());	
				return redirect()->to('register-buyer')->withInput();
			}
			$this->db->transComplete();
			if($this->db->transStatus() !== FALSE)
			{
				helper('text');
				$token = random_string('alnum',20);

				$this->user->allowCallbacks(false);

				if ($this->user->update($inserted_id,['token' => $token])) {
					$email = \Config\Services::email();
					$email->setTo($user_data['email']);
					$email->setFrom(display('Confirm Registration'));
					$msg['data'] = '<span class="position">';
					$msg['data'] .= '<h3 class="name">'.display('Hello').', '.$user_data['firstname'].' '.$user_data['lastname'].'</h3>';
					$msg['data'] .= 'Wowwee! Thanks for registering an account with YAHODEHIME!.<br>';
					$msg['data'] .= 'Before we get started, we will need to activate your account.<br>';
					'</span>';
					$msg['data'] .='<p>';
					$msg['data'] .= '<a href="'.base_url('account-activation/'.$token).'" target="_blank" style="text-decoration:none;padding: 10px; box-shadow: 6px 6px 5px; font-weight: 500; background: transparent; color: #fb9678; cursor: pointer; border-radius: 10px; border: 1px solid #fb9678;">Clik Here</a>';
					$msg['data'] .='</p>';
					$message = view('common/email_template',$msg);
					$email->setSubject(display('Confirm Registration'));
					$email->setMessage($message);

					if (!$email->send()) 
					{
					    $data = $email->printDebugger(['headers']);
					    print_r($data);
					}
				
					$this->session->setFlashData('message','User registered successfully, account activation link sent on your email....');
					return redirect()->to('register-buyer');
				}
			}
			$this->session->setFlashData('message',display('User registered Successfully'));	
			return redirect()->to('register-buyer');
		}
	}
	public function forgotPassword()
	{
		$email = $this->request->getPost('email');
		$isExist = $this->user->getWhere(['email' => $email])->getRow();
		$response = array();
		if($isExist){
			if($this->sendPassword($isExist->id,$isExist))
			{
				$response = ['status' => 'success', 'message' => display('Password Reset Successfully. Please check your email.')];
			} else {
				$response = ['failed' => 'failed', 'message' => display("Password is not changed due to some errors")];
			}
		} else {
			$response = ['failed' => 'failed', 'message' => display("Email does not exist.")];
		}
		echo json_encode($response);exit;
	}

	public function getProductdetails($product_id)
	{
		$data['product'] = $this->productModel->find($product_id);
		$data['title'] = display('Registration');
		$data['path'] = base_url();
		$data['controller_name'] = display('Home');
		$data['content'] = view('buyer/products/product_detail',$data);
		return view('layouts/buyer_wrapper', $data);
	}

	private function sendPassword($user_id,$user){
		helper('text');
		$password = random_string('alnum',10);
        $data['password'] = $password;
		// echo json_encode($data);
		// exit;
		if($this->user->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>').$password.'</b>'."\r\n";
			$mail['data'].='<br>'.display('Please Update your password.');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('Forgot Password');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Test subject');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}

	// ajax call
	public function getProductBySubCategory()
	{
		$sub_category_id = $this->request->getPost('sub_category_id');
		$data['products'] = $this->productModel->where('sub_category_id',$sub_category_id)->orderBy('created_date','DESC')->findAll(2,0);
		if($data['products']){ 
			echo view('buyer/partials/dashboard_products',$data);	
		}
		else {
			echo '<p>'.display('No products found from this category'),'</p>';
		}
	}

	// ajax call
	public function getOrdermaxProductBySubCategory()
	{
		$sub_category_id = $this->request->getPost('sub_category_id');
		$data['products'] = $this->productModel->where('sub_category_id',$sub_category_id)->orderBy('created_date','DESC')->findAll(12,0);
		$data['sub_category'] = $this->subCatModel->where(['deleted'=>'No','id'=>$sub_category_id,'status'=>'Active'])->first();
		if($data['products']){ 
			echo view('buyer/partials/order_max_products',$data);	
		}
		else {
			echo '<p>'.display('No products found from this category'),'</p>';
		}
	}

	public function account_activation($token)
	{
		$data['token'] = $token;
		$data['admin'] = $this->user->where(['token'=> $token])->first();
		return view('common/account_template',$data);	
	}

	public function activate_account()
	{
		$admin_id = $this->request->getPost('id');
		$this->user->allowCallbacks(false);
		if ($this->user->update($admin_id,['token' => '', 'status' => 'Active'])) 
		{
			$response = array('status'=>'success','message'=>'Your account activated successfully..');
		} else {
			$response = array('status'=>'failed','message'=>'Something went wrong your account not activated....');
		}
		echo json_encode($response);
			
	}

	private function sendOTP($user_id,$user){
		helper('text');
		$OTP = random_string('numeric',6);
        $data['otp'] = $OTP;
		// echo json_encode($user_id);
		// exit;
		$this->user->allowCallbacks(false);
		if($this->user->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Your YAHODEHIME Login Verification Code is <b>').$OTP.'</b>'."\r\n";
			$mail['data'].='<br>'.display('Please do not share with anyone');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('YAHODEHIME');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Login Verification Code');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}
	//--------------------------------------------------------------------
	// Test Mail function
	public function sendMail()
	{
		// helper('text');
		// $token = random_string('alnum',20);
		// $email = \Config\Services::email();

        // $email->setTo('mm5866733@gmail.com');
        // $email->setFrom('Request For Quotation');
 
		$msg['data']  = '<p style="float:right;">'.display('From').'<br>';
		$msg['data'] .= 'Jhon Doe</p>';
		$msg['data'] .= '<div style="padding:50px 0px;"></div>';

		$msg['data'] .= '<p>'.display('To').'<br>';
		$msg['data'] .= 'Mirza Mohsin</p>';

		$msg['data'] .= '<p>This letter is with reference to quote number [123456] which I received from you on [5 July 2015].</p>';
		$msg['data'] .= '<p>After a close and comprehensive study of your quote, the management has given their approval. Enclosed with this letter is the purchase order corresponding to your quotation. Please review our order and do the necessary to proceed with it.</p>';
		$msg['data'] .= '<p>We look forward to doing business with you.</p>';
		$msg['data'] .= '<p>Regards</p>';

		// $msg['data'] .='';
		// $msg['data'] .='';
		// $msg = view('common/email_template',$msg);
  //       $email->setSubject('Request For Quotation');
  //       $email->setMessage($msg);

  //       if ($email->send()) 
		// {
  //           echo 'Email successfully sent';
  //       } 
		// else 
		// {
  //           $data = $email->printDebugger(['headers']);
  //           print_r($data);
		// }

		return view('common/email_template',$msg);
		// return view('common/quotation_template',$msg);
	}

}
