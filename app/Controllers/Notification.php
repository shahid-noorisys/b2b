<?php 
namespace App\Controllers;

use App\Models\NotificationModel;
use App\Controllers\BaseController;

class Notification extends BaseController
{
	
	private $notifiy = null;

	public function __construct()
	{
		helper('text');
		$this->notifiy = new NotificationModel();
		$this->admin_id = 1;
		
	}
	
	public function notifications()
	{
	  
	    $admin_id = session()->get('user_details')->admin_id;
	    $admin_role = session()->get('user_details')->user_role;
		$data['title'] = "Notifications";
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('common/notification',$data);
	    if (isset($admin_role) && $admin_role == 2):
	    	return view('supplier/layout/supplier_wrapper', $data);
	    else:
	    	return view('admin/layout/admin_wrapper', $data);
	    endif;
	}

	public function notification_list()
	{
		$draw = $_POST['draw'];
      	$row = $_POST['start'];
      	$rowperpage = $_POST['length']; // Rows display per page
      	$columnIndex = $_POST['order'][0]['column']; // Column index
      	$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
      	$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
      	$searchValue = $_POST['search']['value']; // Search value
      
      	$db      = \Config\Database::connect();
		$builder = $db->table('platform_notifications');
		$searchQuery = " ";
		if($searchValue != ''){
		   $searchQuery = " 1 AND (title like '%".$searchValue."%' OR description like '%".$searchValue."%' OR read_status like'%".$searchValue."%' ) AND";
		} else {
			$searchQuery = " 1 AND";
		}
		
		$admin_id = session()->get('user_details')->admin_id;
		$admin_role = session()->get('user_details')->user_role;


		$records =$db->query("SELECT COUNT(*) AS allcount FROM platform_notifications WHERE user_id = '".$admin_id."' AND user_role = '".$admin_role."'")->getRow();
		$totalRecords = $records->allcount;

		$records = $db->query("SELECT COUNT(*) AS allcount FROM platform_notifications WHERE".$searchQuery." user_id = '".$admin_id."' AND user_role = '".$admin_role."'")->getRow();
		$totalRecordwithFilter = $records->allcount;


		$notifications = $db->query("SELECT * FROM platform_notifications WHERE".$searchQuery." user_id = '".$admin_id."' AND user_role = '".$admin_role."' ORDER BY ".$columnName." ".$columnSortOrder." LIMIT "." ".$row.",".$rowperpage)->getResult();

		$data = array();
		if(!empty($notifications)):
			foreach ($notifications as $key => $value):
			
			$builder->where('id', $value->id)->update(['read_status' => 'Read']);

				$status =($value->read_status == 'Read')?'<span class="label label-success font-weight-100">'.display('Seen').'</span>':'<span class="label label-primary font-weight-100">'.display('Unseen').'</span>';
				$data[] = array( 
				      "id"=> $value->id,
				      "title"=>$value->title,
				      "description"=>character_limiter($value->description,80),
				      "created_date"=> date('d M Y',strtotime($value->created_date)).' '.date('H:i A',strtotime($value->created_date)),
				      "read_status"=> $status
			    );
			endforeach;
		endif;
	
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $totalRecords,
		  "iTotalDisplayRecords" => $totalRecordwithFilter,
		  "aaData" => $data
		);

		echo json_encode($response);
	
	}

	public function get_notification_counter()
	{
		$admin_id = session()->get('user_details')->admin_id;
		$admin_role = session()->get('user_details')->user_role;
	    $data['notifications']=$notification = $this->notifiy->where(['user_id'=>$admin_id,'user_role'=>$admin_role,'read_status'=> 2])->findAll();
	    $notification_list =  view('common/list_notification_dropdown',$data);
	    $response = array('status'=>'success','counter'=>count($notification),'list'=>$notification_list);
	    echo json_encode($response);
	}
	//--------------------------------------------------------------------

}
