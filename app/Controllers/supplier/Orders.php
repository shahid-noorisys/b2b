<?php 
namespace App\Controllers\supplier;

use App\Models\supplier\Supplier_model;
use App\Models\AdminModel;
use App\Models\QuotationModel;
use App\Models\OrdersModel;
use App\Models\NotificationModel;
use App\Controllers\BaseController;

class Orders extends BaseController
{
	
	private $admin = null;
	private $supplier = null;
	private $quote = null;
	private $order = null;
	private $notifiy = null;

	public function __construct()
	{
		helper('text');
		$this->admin = new AdminModel();
		$this->notifiy = new NotificationModel();
		$this->supplier = new Supplier_model();
		$this->quote = new QuotationModel();
		$this->order = new OrdersModel();
		
	}
	
	public function orders_list()
	{
	  	

	    $admin_id = session()->get('user_details')->admin_id;

	    $_SESSION['order']['order_no'] = '';
        $_SESSION['order']['quote_no'] = '';
        $_SESSION['order']['user_id'] = '';
        $_SESSION['order']['status'] = '';
        $_SESSION['order']['start'] = '';
        $_SESSION['order']['end'] = '';

        if(!empty($_POST['order_filter']))
        {
            $_SESSION['order']['order_no'] =(isset($_POST['order_no']) && !empty($_POST['order_no']))?trim($_POST['order_no']):'';
            $_SESSION['order']['quote_no'] =(isset($_POST['quote_no']) && !empty($_POST['quote_no']))?trim($_POST['quote_no']):'';
            $_SESSION['order']['user_id'] =(isset($_POST['user_id']) && !empty($_POST['user_id']))?trim($_POST['user_id']):'';
            $_SESSION['order']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['order']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['order']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
            $_SESSION['order']['order_no'] ='';
            $_SESSION['order']['quote_no'] = '';
            $_SESSION['order']['user_id'] = '';
            $_SESSION['order']['status'] = '';
            $_SESSION['order']['start'] = '';
            $_SESSION['order']['end'] = '';
        }

        $data['orders'] = $this->order->getallSellerordersList($_SESSION['order'],$admin_id);

	    $db      = \Config\Database::connect();
		$builder = $db->table('orders');
		$builder->select('orders.buyer_id,user.firstname,user.lastname');
		$builder->distinct('buyer_id');
		$builder->join('application_users AS user', 'user.id = orders.buyer_id');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select Customer');
		if(!empty($users)){
			foreach ($users as $customer) {
				$users_list[$customer->buyer_id] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$data['users_list'] = $users_list;
		$data['title'] = "orders";
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('supplier/orders/order_list',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
	}

	public function orders_details($order_id)
	{
	
		$admin_id = session()->get('user_details')->admin_id;
	    $db      = \Config\Database::connect();

		$builder = $db->table('orders');
		$builder->select('orders.*,quote.id AS quote_id,quote.amount AS quote_amounts,add.address,add.city,add.state,add.country');
		$builder->join('quotations AS quote', 'quote.id = orders.q_id');
		$builder->join('user_addresses AS add', 'add.id = orders.address_id');
		$builder->where('orders.id', $order_id);
		$builder->where('orders.supplier_id', $admin_id);
		$builder->OrderBy('orders.id','DESC');
		$data['order']  = $builder->get()->getRow();

		// echo json_encode($data);die();
		$data['title'] = "Order details";
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('supplier/orders/order_details',$data);
    	return view('supplier/layout/supplier_wrapper', $data);

	}

	public function order_status()
	{
		$order_id = $this->request->getPost('order_id');
		$order_status = $this->request->getPost('state');
		$order_detail = $this->order->where(['id' => $order_id])->first();
		$customer = get_user_details_by_id($order_detail->buyer_id,3);
		if($this->order->update($order_id,['order_status' => $order_status])){

			$notification = array(
									'title' => 'Order status',
									'description' => 'Status changed from '.ucfirst($order_detail->order_status).' to '.ucfirst($order_status),
									'user_id' => $order_detail->buyer_id,
									'user_role' => 3,
									'read_status' => 'Unread',
								 );
			$this->notifiy->insert($notification);
			$inserted_id = $this->notifiy->getInsertID();

			$msg['data']  = '<p style="text-align: center;"><strong>'.display('Hello').', '.$customer->firstname.' '.$customer->lastname.'<br />Your Order Number #'.$order_detail->order_no.' status has been changed from Pending to In_progress</strong></p>'."\r\n";
			$msg['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($customer->email);
			$email->setFrom('Order status changed');
			
			$msg = view('common/email_template',$msg);
			$email->setSubject('Order status changed');
			$email->setMessage($msg);
			if (!$email->send()) 
			{
				
			} 
			$data = array('status' => 'success','message'=> display('Order status has been changed successfully.'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to change the status of order.'));
		}
		echo json_encode($data);
	}

	//--------------------------------------------------------------------
}
