<?php 
namespace App\Controllers\supplier;

use App\Models\supplier\Supplier_model;
use App\Models\Countries_model;
use App\Models\UserModel;
use App\Models\UserAddressModel;
use App\Models\UserCompanyModel;
use App\Controllers\BaseController;

class Profile extends BaseController
{
	
	private $user = null;
	private $country = null;
	private $address = null;
	private $supplier = null;
	private $company = null;

	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->address = new UserAddressModel();
		$this->company = new UserCompanyModel();
		$this->country = new Countries_model();
		$this->supplier = new Supplier_model();
		
	}
	
	public function profile()
	{
	    $user = $this->session->get('user_details');
	    $data['profile'] = $this->supplier->find($user->admin_id);
	    $data['company'] = $this->company->where('user_id',$user->admin_id)->first();
	    $data['address'] = $this->address->where('user_id',$user->admin_id)->where('is_primary',1)->first();
	    $countries = $this->country->findAll();
		$country_list[' '] = display('Select Country');
		foreach ($countries as $v) {
			$country_list[$v->id] = $v->name;
		}
		$data['countries'] = $country_list;
		$data['title'] = "Profile";
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('supplier/profile',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
	}

	public function change_password()
	{
		if($this->request->getMethod() == 'post')
		{
			$old_pswd = $this->request->getPost('old_pswd');
			$new_pswd = $this->request->getPost('new_pswd');
			$confrim_pswd = $this->request->getPost('confrim_pswd');
			$user = $this->session->get('user_details') ?? null;
			$details = $this->supplier->find($user->admin_id);	
			if(!empty($details))
	        {
	            $verify = password_verify($old_pswd, $details->password);
	            if($verify)
	            {
					$this->supplier->update($user->admin_id, ['password' => $confrim_pswd]);
					if ($this->supplier->affectedRows()) {
		                $this->session->setFlashData('message',display('Your credential updated Successfully'));
						return redirect()->to(base_url('supplier/profile'));
					}
	            } else {
	            	$this->session->setFlashData('exception',display('Sorry, Unable to update your credential'));
					return redirect()->to(base_url('supplier/profile'));
	            }

	        } else {
					$this->session->setFlashData('exception',$this->supplier->errors());
					return redirect()->to(base_url('supplier/login'))->withInput();
	        }
			
		} 
	}

	public function profile_edit()
	{

		$user = $this->session->get('user_details');	
		$update_data = [
						'firstname' => $this->request->getPost('firstname'),
						'lastname' => $this->request->getPost('lastname'),
						'email' => $this->request->getPost('email'),
						'mobile' => $this->request->getPost('mobile'),
						'company_name' => $this->request->getPost('company_name'),
						'company_email' => $this->request->getPost('company_email'),
					];
		$this->supplier->allowCallbacks(false);
		$this->supplier->update($user->admin_id, $update_data);
		if ($this->supplier->affectedRows()) {
                $this->session->setFlashData('message',display('Your profile details successfully updated'));
				return redirect()->to(base_url('supplier/profile'));
        } else {
        	$this->session->setFlashData('exception',display('Sorry, Unable to update profile details'));
			return redirect()->to(base_url('supplier/profile'));
        }
	    
	}	

	public function company_profile_add()
	{
		$user_id = $this->session->get('user_details')->admin_id;	
		$data = [
						'user_id' => $user_id,
						'company_name' => $this->request->getPost('company_name'),
						'company_email' => $this->request->getPost('company_email'),
						'contact' => $this->request->getPost('contact'),
						'address' => $this->request->getPost('address'),
						'city' => $this->request->getPost('city'),
						'country' => $this->request->getPost('country'),
						'country_code' => $this->country->find($this->request->getPost('country'))->dial_code,
						'introduction' => $this->request->getPost('intro'),
						
					];			
		$this->company->insert($data);
		if ($this->company->getInsertID()) {
                $this->session->setFlashData('message',display('Your company profile details successfully added'));
				return redirect()->to(base_url('supplier/profile'));
        } else {
        	$this->session->setFlashData('exception',display('Sorry, Unable to add company profile details'));
			return redirect()->to(base_url('supplier/profile'));
        }

	}

	public function company_profile_edit()
	{
		$user_id = $this->session->get('user_details')->admin_id;	
		$company_id = $this->request->getPost('company_id');	
		$data = [
						
						'company_name' => $this->request->getPost('company_name'),
						'company_email' => $this->request->getPost('company_email'),
						'contact' => $this->request->getPost('contact'),
						'address' => $this->request->getPost('address'),
						'city' => $this->request->getPost('city'),
						'country' => $this->request->getPost('country'),
						'country_code' => $this->country->find($this->request->getPost('country'))->dial_code,
						'introduction' => $this->request->getPost('intro'),
						
					];			
		$this->company->update($company_id,$data);
		if ($this->company->affectedRows()) {
                $this->session->setFlashData('message',display('Your company profile details successfully updated'));
				return redirect()->to(base_url('supplier/profile'));
        } else {
        	$this->session->setFlashData('exception',display('Sorry, Unable to update company profile details'));
			return redirect()->to(base_url('supplier/profile'));
        }

	}

	public function profile_image_edit()
	{
		// echo json_encode('File Upload');die();
		$user_id = $this->session->get('user_details')->admin_id;	

        $file = $this->request->getFile('profile_pic');
        $old_file = $this->request->getPost('old_image');
        if (! $file->isValid())
        {
            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
        }
        if ($file->isValid() && ! $file->hasMoved())
        {
            $path = 'public/assets/uploads/images/users/';
            $newName = $file->getRandomName();
            $file->move($path,$newName);

            $builder = $this->db->table('application_users');
            if($builder->update(['thumbnail' => $path.$newName],['id' => $user_id])){
                if(file_exists($old_file)){ unlink($old_file); }
                $data = array('status' => 'success','message'=> display('Profile Image updated successfully'));
            } else {
                if(file_exists($path.$newName)){ unlink($path.$newName); }
                $data = array('status' => 'failed','message'=> display('Profile Image updated successfully')); 
            }
        }
        else {
            $data = array('status' => 'failed','message'=> display('You are not selected any file to upload'));
        }
        echo json_encode($data);
	}

	//--------------------------------------------------------------------

}
