<?php 
namespace App\Controllers\supplier;

use App\Models\UserModel;
use App\Models\AdminModel;
use App\Models\MessageModel;
use App\Models\QuotationModel;
use App\Models\UserAddressModel;
use App\Models\UserCompanyModel;
use App\Models\ProductModel;
use App\Models\NotificationModel;
use App\Models\QuotationProductModel;
use App\Controllers\BaseController;

class Quotations extends BaseController
{
	
	private $user = null;
	private $admin = null;
	private $address = null;
	private $supplier = null;
	private $quote = null;
	private $company = null;
	private $product = null;
	private $quote_product = null;
	private $notifiy = null;

	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->message = new MessageModel();
		$this->admin = new AdminModel();
		$this->address = new UserAddressModel();
		$this->company = new UserCompanyModel();
		$this->quote = new QuotationModel();
		$this->quote_product = new QuotationProductModel();
		$this->product = new ProductModel();
		$this->notifiy = new NotificationModel();
		$this->admin_id = 1;
		
	}
	
	public function quotations_list()
	{
	  
	    $admin_id = session()->get('user_details')->admin_id;

        $_SESSION['quote']['quote_no'] = '';
        $_SESSION['quote']['user_id'] = '';
        $_SESSION['quote']['status'] = '';
	    $_SESSION['quote']['criteria'] = '';
        $_SESSION['quote']['start'] = '';
        $_SESSION['quote']['end'] = '';

        if(!empty($_POST['quote_filter']))
        {
            $_SESSION['quote']['criteria'] =(isset($_POST['criteria']) && !empty($_POST['criteria']))?trim($_POST['criteria']):'';
            $_SESSION['quote']['quote_no'] =(isset($_POST['quote_no']) && !empty($_POST['quote_no']))?trim($_POST['quote_no']):'';
            $_SESSION['quote']['user_id'] =(isset($_POST['user_id']) && !empty($_POST['user_id']))?trim($_POST['user_id']):'';
            $_SESSION['quote']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['quote']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['quote']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
            $_SESSION['quote']['criteria'] ='';
            $_SESSION['quote']['quote_no'] = '';
            $_SESSION['quote']['user_id'] = '';
            $_SESSION['quote']['status'] = '';
            $_SESSION['quote']['start'] = '';
            $_SESSION['quote']['end'] = '';
        }

        $data['quotation'] = $this->quote->getallSellerquotationList($_SESSION['quote'],$admin_id);
        
	    $builder = $this->db->table('quotations');
		$builder->select('quotations.request_from,user.firstname,user.lastname');
		$builder->distinct('request_from');
		$builder->join('application_users AS user', 'user.id = quotations.request_from');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select Customer');
		if(!empty($users)){
			foreach ($users as $customer) {
				$users_list[$customer->request_from] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$data['users_list'] = $users_list;
		$data['title'] = display('Quotations');
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = display('Home');
	    $data['content'] = view('supplier/quotation/list',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
	}

	public function quotation_details($quotation_id)
	{
	    $admin_id = session()->get('user_details')->admin_id;

	    $data['quote'] = $this->quote->where(['id'=>$quotation_id,'request_to'=>$admin_id])->first();

	    $data['seller_address'] = $this->address->where(['user_id'=>$admin_id,'is_primary'=>1])->first();
	    $data['buyer_address'] = $this->address->where(['user_id'=>$data['quote']->request_from,'is_primary'=>1])->first();
	    $data['seller_company'] = $this->company->where(['user_id'=>$admin_id])->first();
	    $data['buyer_company'] = $this->company->where(['user_id'=>$data['quote']->request_from])->first();

		$data['title'] = display('Quotations details');
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = display('Home');
	    $data['content'] = view('supplier/quotation/view',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
	}

	public function product_details()
	{
		$product_id = $this->request->getPost('product_id'); 
		$data['product'] = $this->product->find($product_id);
		$product_info =  view('supplier/quotation/product_detail',$data);
		echo $product_info;
	}	

	public function update_quotation_amount()
	{

		$quotation_id= $this->request->getPost('quotation_id'); 
		$product_amount = $this->request->getPost('product_amount'); 
		$quotation_amount = $this->request->getPost('quotation_amount'); 
		$product_id = $this->request->getPost('product_id'); 

		$quotation = $this->quote->find($quotation_id);
		$product = json_decode($quotation->product_json,true);

		foreach ($product as $key => $value) {
			 if ($value['product_id'] == $product_id) {
		        $product[$key]['product_amount'] = $product_amount;
		    }
		}
		// echo json_encode($product);die();
		$this->quote->update($quotation_id,['amount'=> $quotation_amount,'product_json'=>json_encode($product)]);
		if($this->quote->affectedRows()){
				$this->response = array('status'=>'success','message'=>'successfully update....');
		} else {
			$this->response = array('status'=>'failed','message'=>'sorry, unable to update....');
		}
	
		echo json_encode($this->response);
	}

	public function reject_quotation()
	{		
		$quote_id= $this->request->getPost('quote_id'); 
		$this->quote->update($quote_id,['is_rejected'=> 1]);
		$quote_detail = $this->quote->where(['id' => $quote_id])->first();
		if($this->quote->affectedRows()){

			$notification = array(
									'title' => 'Quotation Reject',
									'description' => 'Your Quotation number '.$quote_detail->q_no.' has been rejected by the seller',
									'user_id' => $quote_detail->request_from,
									'user_role' => 3,
									'read_status' => 'Unread',
								 );
			$this->notifiy->insert($notification);
			$inserted_id = $this->notifiy->getInsertID();

			$this->response = array('status'=>'success','message'=>display('Quotation has been rejected successfully'));
		} else {
			$this->response = array('status'=>'failed','message'=>'Sorry, something goes wrong, so quotation cant not reject');
		}
	
		echo json_encode($this->response);
	}

	public function send_quotation()
	{		
		$quote_id= $this->request->getPost('quote_id');
		$quotation = $this->quote->find($quote_id);
		if (!empty($quotation)) {
			if (isset($quotation)) {
				$admin = get_user_details_by_id($this->admin_id,1);
				$buyer = get_user_details_by_id($quotation->request_from,3);
				$data['is_accepted'] = 1;
				if ($quotation->amount < 1500) {
					$emails = array($admin->email);
					$data['is_validated'] = 1;
				} else {
					$emails = array($admin->email,$buyer->email);
					$data['is_validated'] = 2;
				}
				if($this->quotation_send_email($quotation,$emails,$data))
				{
					$notification = '';
					if ($quotation->is_accepted == 'Yes') {

						$notification = array(
												'title' => 'Quotation Updated',
												'description' => 'Your Quotation number '.$quotation->q_no.' amount has been changed by the seller',
												'user_id' => $quotation->request_from,
												'user_role' => 3,
												'read_status' => 'Unread',
											 );
					} else {

						$notification = array(
												'title' => 'Quotation Accepted',
												'description' => 'Your Quotation number '.$quotation->q_no.' has been accepted by the seller',
												'user_id' => $quotation->request_from,
												'user_role' => 3,
												'read_status' => 'Unread',
											 );
					}
						$this->notifiy->insert($notification);
						$inserted_id = $this->notifiy->getInsertID();

					$this->response = array('status'=>'success','message'=>display('Quotation has been send successfully'));

				} else {
					$this->response = array('status'=>'failed','message'=>'Sorry, something goes wrong, so quotation cant not send');
				}
				$this->quote->update($quote_id,$data);
			}
		} else{
			
			$this->response = array('status'=>'failed','message'=>'Sorry, something goes wrong, so quotation cant not send');
		}
		echo json_encode($this->response);
	} 

	public function quotation_send_email($quotation,$emails,$data)
	{

		if($this->quote->update($quotation->id,$data))
		{
		    $mail['data']  = '<p style="float:right;">'.display('From').'<br>';
			$mail['data'] .= 'Jhon Doe</p>';
			$mail['data'] .= '<div style="padding:50px 0px;"></div>';

			$mail['data'] .= '<p>'.display('To').'<br>';
			$mail['data'] .= 'Mirza Mohsin</p>';

			$mail['data'] .= '<p>This letter is with reference to quote number [123456] which I received from you on [5 July 2015].</p>';
			$mail['data'] .= '<p>After a close and comprehensive study of your quote, the management has given their approval. Enclosed with this letter is the purchase order corresponding to your quotation. Please review our order and do the necessary to proceed with it.</p>';
			$mail['data'] .= '<p>We look forward to doing business with you.</p>';
			$mail['data'] .= '<p>Regards</p>';

			$email = \Config\Services::email();

			$email->setTo($emails);
			$email->setFrom('YAHODEHIME');
			
			$msg = view('common/quotation_template',$mail);
			$email->setSubject('Quotation Accepted');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			}
			return true;
		} else {
				return false;
		}
	}
	//--------------------------------------------------------------------

}
