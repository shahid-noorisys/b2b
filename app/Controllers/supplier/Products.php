<?php 
namespace App\Controllers\supplier;

use App\Models\UserModel;
use App\Models\ProductModel;
use App\Models\Countries_model;
use App\Models\UserAddressModel;
use App\Controllers\BaseController;
use App\Models\admin\Categories_model;
use App\Models\supplier\Supplier_model;
use App\Models\admin\Subcategories_model;

class Products extends BaseController
{
	
	private $user = null;
	private $country = null;
	private $address = null;
	private $supplier = null;
	protected $subCatModel = null;
	protected $categoryModel = null;
	protected $productModel = null;
	
	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->address = new UserAddressModel();
		$this->country = new Countries_model();
		$this->supplier = new Supplier_model();
		$this->subCatModel = new Subcategories_model();
		$this->categoryModel = new Categories_model();
		$this->productModel = new ProductModel();
	}
	
	public function product_list()
	{

		$data['title'] = "Products";
	    $data['path'] = base_url('supplier/dashboard');
		$data['controller_name'] = "Dashboard";

		$user_id = $this->session->get('user_details')->admin_id;

		$categories = $this->categoryModel->findAll();
		$sub_categories = $this->subCatModel->findAll();
		$category_list[' '] = display('Select Category');
		if(!empty($categories)){
			foreach ($categories as $c) {
				$category_list[$c->id] = $c->name;
			}
		}
		$sub_category_list[' '] = display('Select Sub Category');
		if(!empty($sub_categories)){
			foreach ($sub_categories as $sub) {
				$sub_category_list[$sub->id] = $sub->name;
			}
		}

		
        $_SESSION['product']['category'] = '';
        $_SESSION['product']['sub_category'] = '';
        $_SESSION['product']['status'] = '';
        $_SESSION['product']['publish'] = '';
        $_SESSION['product']['start'] = '';
        $_SESSION['product']['end'] = '';

        if(!empty($_POST['product_filter']))
        {
            $_SESSION['product']['category'] =(isset($_POST['category']) && !empty($_POST['category']))?trim($_POST['category']):'';
            $_SESSION['product']['sub_category'] =(isset($_POST['sub_category']) && !empty($_POST['sub_category']))?trim($_POST['sub_category']):'';
            $_SESSION['product']['publish'] =(isset($_POST['publish']) && !empty($_POST['publish']))?trim($_POST['publish']):'';
            $_SESSION['product']['status'] =(isset($_POST['status']) && !empty($_POST['status']))?trim($_POST['status']):'';
            $_SESSION['product']['start'] =($_POST['start']!='')?$_POST['start']:"";
         	$_SESSION['product']['end'] =($_POST['end']!='')?$_POST['end']:""; 
        }
        if(!empty($_POST['remove_filter']))
        {
         
            $_SESSION['product']['category'] = '';
            $_SESSION['product']['sub_category'] = '';
            $_SESSION['product']['status'] = '';
            $_SESSION['product']['publish'] = '';
            $_SESSION['product']['start'] = '';
            $_SESSION['product']['end'] = '';
        }
        $data['products'] = $this->productModel->getallProductsList($_SESSION['product'],$user_id);

		// $data['products'] = $this->productModel->where('user_id',$user_id)->orderBy('created_date','DESC')->findAll();
		$data['categories'] = $category_list;
		$data['sub_categories'] = $sub_category_list;
		
	    $data['content'] = view('supplier/products/product_list',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
		
	}

	public function product_add()
	{
		$data['title'] = "Product Add";
	    $data['path'] = base_url('supplier/dashboard');
		$data['controller_name'] = "Dashboard";
		$categories = $this->categoryModel->findAll();
		$category_list[' '] = display('Select Category');
		foreach ($categories as $c) {
			$category_list[$c->id] = $c->name;
		}	
		$data['categories'] = $category_list;
	    $data['content'] = view('supplier/products/product_add',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
		
	}

	public function createProduct(){
		// dd($this->request->getPost());
		if($this->request->getMethod() == 'post')
		{
			$category_id = $this->request->getPost('category_id');
			$category_details = $this->categoryModel->find($category_id);
			$sub_category_id = $this->request->getPost('sub_category_id');
			$sub_cat_details = $this->subCatModel->find($sub_category_id);
			// dd($sub_cat_details);
			$user_id = $this->session->get('user_details')->admin_id;
			$data = [
				'name' => $this->request->getPost('product_name'),
				'category_id' => $category_id,
				'category_name' => $category_details->name,
				'sub_category_id' => $sub_category_id,
				'sub_category_name' => $sub_cat_details->name,
				'description' => $this->request->getPost('description'),
				'user_id' => $user_id,
				'is_published' => $this->request->getPost('publish_status'),
			];
			// dd($data);
			$imagefile = $this->request->getFiles();
			if($imagefile)
			{
				$path = 'public/assets/uploads/images/products/';
				foreach($imagefile['product_images'] as $k => $img)
				{
					if ($img->isValid() && ! $img->hasMoved())
					{
						$newName = $img->getRandomName();
						$data['image_'.($k+1)] = $path.$newName;
						$img->move($path, $newName);
					}
				}
			}
			if($this->productModel->insert($data)){
				$this->session->setFlashData('message',display('Product Added Successfully'));
			} else {
				$this->session->setFlashData('exception',display('Error Occured while addming products'));
			}
			return redirect()->to(base_url('supplier/products'));
		} else {
			$this->session->setFlashData('exception',display('Form is not submitted'));
			return redirect()->back();
		}
	}

	public function editProduct($product_id)
	{
		$data['title'] = "Product Edit";
	    $data['path'] = base_url('supplier/dashboard');
		$data['controller_name'] = "Dashboard";
		$data['product'] = $this->productModel->find($product_id);

		$categories = $this->categoryModel->findAll();
		$sub_categories = $this->subCatModel->findAll();
		$category_list[' '] = display('Select Category');
		if(!empty($categories)){
			foreach ($categories as $c) {
				$category_list[$c->id] = $c->name;
			}
		}
		$sub_category_list[' '] = display('Select Sub Category');
		if(!empty($sub_categories)){
			foreach ($sub_categories as $sub) {
				$sub_category_list[$sub->id] = $sub->name;
			}
		}

		$data['categories'] = $category_list;
		$data['sub_categories'] = $sub_category_list;
	    $data['content'] = view('supplier/products/product_edit',$data);
    	return view('supplier/layout/supplier_wrapper', $data);	
	}

	public function updateProduct()
	{
		if($this->request->getMethod() == 'post')
		{
			// dd($this->request->getPost());
			$category_id = $this->request->getPost('category_id');
			$category_details = $this->categoryModel->find($category_id);
			$sub_category_id = $this->request->getPost('sub_category_id');
			$sub_cat_details = $this->subCatModel->find($sub_category_id);
			$product_id = $this->request->getPost('product_id');
			$data = [
				'name' => $this->request->getPost('product_name'),
				'category_id' => $category_id,
				'category_name' => $category_details->name,
				'sub_category_id' => $sub_category_id,
				'sub_category_name' => $sub_cat_details->name,
				'is_published' => $this->request->getPost('publish_status'),
				'description' => $this->request->getPost('description'),
			];
			if($this->productModel->update($product_id,$data))
			{
				$this->session->setFlashData('message',display('Product Information updated'));
			} else {
				$this->session->setFlashData('exception',display('Error occured in update'));
			}
			return redirect()->back();
		}
		else {
			$this->session->setFalshData('exception',display('Form is not submitted'));
			redirect()->back();
		}
	}

	public function getSubCategories()//call by ajax
	{
		$category_id = $this->request->getPost('category_id');
		$res = $this->subCatModel->where('category_id',$category_id)->findAll();
		$list[' '] = display('Select Sub Category');
		if(!empty($res)){
			foreach ($res as $sub) {
				$list[$sub->id] = $sub->name;
			}
		}
		$response = ['list' => form_dropdown('sub_category_id',$list,'',' class="select2 form-control" id="sub_category_id" ')];
		echo json_encode($response);
	}

	public function uploadProductImage()
	{
		if($this->request->getMethod() == 'post')
		{
			$files = $this->request->getFiles();
			$product_id = $this->request->getPost('product_id');
			$product = $this->productModel->asArray()->find($product_id);
			// var_dump($product['image_1']);exit;
			$data = array();
			$old_images = array();
			if($files)
			{
				$path = 'public/assets/uploads/images/products/';
				for ($i=1; $i <= count($files) ; $i++) { 
					if($files['image_'.$i]->isValid() && ! $files['image_'.$i]->hasMoved())
					{
						$newName = $files['image_'.$i]->getRandomName();
						$data['image_'.$i] = $path.$newName;
						$old_images['image_'.$i] = $product['image_'.$i];
						$files['image_'.$i]->move($path, $newName);
					}
				}
			}
			if(!empty($data))
			{
				if($this->productModel->update($product_id,$data)){
					foreach ($old_images as $v) {
						if(!empty($v) AND file_exists($v))
						{
							unlink('./'.$v);
						}
					}
					$this->session->setFlashData('message',display('Images are updated successfully'));
				} else {
					$this->session->setFlashData('exception',display('Error occured in update'));
				}
			} else {
				$this->session->setFlashData('exception', display('Nothing is changed'));
			}
			return redirect()->back();
		}
		else {
			$this->session->setFlashData('exception',display('Form is not submitted'));
			return redirect()->back();
		}
	}

	public function product_delete()
	{
		$product_id = $this->request->getPost('product_id');
		if($this->productModel->update($product_id,['deleted' => 'Yes'])){
			$data = array('status' => 'success','message'=> display('Product has been deleted successfully.'));
		} else{
			$data = array('status' => 'failed','message'=> display('Sorry, Unable to delete the product...'));
		}
		echo json_encode($data);
	}


	//--------------------------------------------------------------------
	// called from buyer side

	public function getProduct()
	{
		if($this->request->getMethod() == 'post')
		{
			$product_id = $this->request->getPost('product_id');
			$p_details = $this->productModel->find($product_id);
			if(!empty($p_details)){
				$res = ['status' => 'success', 'product' => $p_details,'message' => "product details success"];
			} else {
				$res = ['status' => 'failed', 'product' => $p_details,'message' => display("Product details not found.")];
			}
		} else {
			$res = ['status' => 'failed', 'product' => null,'message' => display("Form is not submitted")];
		}
		echo json_encode($res);
	}

}
