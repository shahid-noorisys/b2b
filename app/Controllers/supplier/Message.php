<?php 
namespace App\Controllers\supplier;

use App\Models\supplier\Supplier_model;
use App\Models\UserModel;
use App\Models\AdminModel;
use App\Models\MessageModel;
use App\Models\QuotationModel;
use App\Controllers\BaseController;

class Message extends BaseController
{
	
	private $user = null;
	private $admin = null;
	private $address = null;
	private $supplier = null;
	private $quote = null;


	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->message = new MessageModel();
		$this->admin = new AdminModel();
		$this->supplier = new Supplier_model();
		$this->quote = new QuotationModel();
		$this->admin_id = 1;
		
	}
	
	public function messages()
	{
	  
	    $admin_id = session()->get('user_details')->admin_id;
	    $data['quotation'] = $this->quote->where(['request_to'=>$admin_id])->orderBy('created_date')->findAll();
	    // echo json_encode($data);die();
		$data['title'] = "Chat";
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('supplier/chat_room/chatroom',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
	}

	public function send_message()
	{
		// $name = $file->getClientMimeType();
		$file = $this->request->getFile('file_name');
		if (!empty($file)) {
			if (! $file->isValid()) {
	            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
	        }
	        if ($file->isValid() && ! $file->hasMoved()) {
	            $path = 'public/assets/uploads/images/message_attachment/';
	            $newName = $file->getRandomName();
	            $file->move($path,$newName);

				$data['attachment_url'] = $path.$newName; 
				$data['attachment_file_type'] = $file->getClientExtension();
	        }
		} 
		
		$data['text_msg'] = $this->request->getPost('message'); 
		$data['text_msg'] = $this->request->getPost('message'); 
		$data['q_id'] = $quotation_id = $this->request->getPost('quotation_id');
		$data['sender_id'] = $this->session->get('user_details')->admin_id;
		$data['receiver_id'] = $this->admin->find($this->admin_id)->id;
		$data['send_by'] = 2;
		$data['user_role'] = $this->session->get('user_details')->user_role;
		$data['read_status'] = 'No';

		$this->message->insert($data);
		if ($inserted_id = $this->message->getInsertID()) {
			// $response = array('status' => 'success','message'=>'message send successfully');
			$data['admin_id'] = $admin_id = $this->session->get('user_details')->admin_id;
			$data['user_role'] = $user_role = $this->session->get('user_details')->user_role;
			$data['messages'] = $this->message->where(['user_role'=>$user_role,'q_id'=>$quotation_id])->orderBy('created_date','ASC')->findAll();
			// echo json_encode($data);die();
			$message_list =  view('supplier/chat_room/message_list',$data);

			echo $message_list;
		} 

	}

	public function get_messages()
	{
		$quotation_id = $this->request->getPost('quotation_id'); 
		$user_id = $this->request->getPost('supplier_id'); 
		// logged-in user_id
		$data['admin_id'] = $this->session->get('user_details')->admin_id;
		$data['user_role'] = $user_role = $this->session->get('user_details')->user_role;

		$data['messages'] = $this->message->where(['user_role'=>$user_role,'q_id'=>$quotation_id])->orderBy('created_date','ASC')->findAll();
		// echo $this->message->getLastQuery();
		// echo json_encode($data);die();
		$message_list =  view('supplier/chat_room/message_list',$data);

		echo $message_list;
	}

	//--------------------------------------------------------------------

}
