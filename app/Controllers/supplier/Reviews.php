<?php 
namespace App\Controllers\supplier;

use App\Models\supplier\Supplier_model;
use App\Models\AdminModel;
use App\Models\QuotationModel;
use App\Models\OrdersModel;
use App\Models\ReviewandRatingModel;
use App\Controllers\BaseController;

class Reviews extends BaseController
{
	
	private $admin = null;
	private $supplier = null;
	private $quote = null;
	private $order = null;
	private $review = null;

	public function __construct()
	{
		helper('text');
		$this->admin = new AdminModel();
		$this->review = new ReviewandRatingModel();
		$this->supplier = new Supplier_model();
		$this->quote = new QuotationModel();
		$this->order = new OrdersModel();
		
	}
	
	public function reviews_list()
	{
	  	// echo json_encode($_POST);die();
	    $admin_id = session()->get('user_details')->admin_id;

	    // $_SESSION['review']['rating'] = '';
     //    $_SESSION['review']['user_id'] = '';
       
     //    if(!empty($_POST['review_filter']))
     //    {
     //        $_SESSION['review']['rating'] =(isset($_POST['rating']) && !empty($_POST['rating']))?trim($_POST['rating']):'';
     //        $_SESSION['review']['user_id'] =(isset($_POST['user_id']) && !empty($_POST['user_id']))?trim($_POST['user_id']):'';
     //    }
     //    if(!empty($_POST['remove_filter']))
     //    {
     //        $_SESSION['review']['rating'] = '';
     //        $_SESSION['review']['user_id'] = '';
     //    }

     //    if(isset($_SESSION['review']['rating']) && $_SESSION['review']['rating'] != "" && $_SESSION['review']['user_id'] == "")
	    // {
	    //    $data['reviews'] = $this->review->where('supplier_id',$admin_id)->where('rating',$_SESSION['review']['rating'])->paginate(4);
	    // }
     //    if(isset($_SESSION['review']['user_id']) && $_SESSION['review']['user_id'] != "" && $_SESSION['review']['rating'] == "") 
     //    {
     //       $data['reviews'] = $this->review->where('supplier_id',$admin_id)->where('buyer_id',$_SESSION['review']['user_id'])->paginate(4);
     //    }
     //    if(isset($_SESSION['review']['rating']) && $_SESSION['review']['rating'] != "" && isset($_SESSION['review']['user_id']) && $_SESSION['review']['user_id'] != "")
     //    {
     //       $data['reviews'] = $this->review->where('supplier_id',$admin_id)->where('rating',$_SESSION['review']['rating'])->where('buyer_id',$_SESSION['review']['user_id'])->paginate(4);
     //    }
     //    if($_SESSION['review']['rating'] == "" && $_SESSION['review']['user_id'] == "")
     //    {
     //    	$data['reviews'] = $this->review->where('supplier_id',$admin_id)->paginate(4);
     //    }
	    
    	$data['reviews'] = $this->review->where('supplier_id',$admin_id)->paginate(4);
        $data['pager'] = $this->review->pager;
        $data['pagination'] = $data['pager']->links('default','seller_pager');
	    // $data = [
     //        'reviews' => $this->review->where('supplier_id',$admin_id)->paginate(4),
     //        'pager' => $this->review->pager
     //    ];


	    $db      = \Config\Database::connect();
		$builder = $db->table('buyer_reviews');
		$builder->select('buyer_reviews.buyer_id,user.firstname,user.lastname');
		$builder->distinct('buyer_id');
		$builder->join('application_users AS user', 'user.id = buyer_reviews.buyer_id');
		$users  = $builder->get()->getResult();

		$users_list[' '] = display('Please Select Customer');
		if(!empty($users)){
			foreach ($users as $customer) {
				$users_list[$customer->buyer_id] = $customer->firstname.' '.$customer->lastname;
			}
		}

		$data['users_list'] = $users_list;
		$data['title'] = "Reviews & Rating";
	    $data['path'] = base_url('supplier/dashboard');
	    $data['controller_name'] = "Home";
	    $data['content'] = view('supplier/reviews',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
	}


	//--------------------------------------------------------------------
}
