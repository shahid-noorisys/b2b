<?php 
namespace App\Controllers\supplier;

use App\Models\supplier\Supplier_model;
use App\Models\Countries_model;
use App\Models\UserModel;
use App\Models\UserAddressModel;
use App\Models\SurveyModel;
use App\Models\CompanyTypeModel;
use App\Models\UserCompanyModel;
use App\Models\UserBankModel;
use App\Models\QuotationModel;
use App\Models\OrdersModel;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{
	
	private $user = null;
	private $country = null;
	private $address = null;
	private $supplier = null;
	private $survey = null;
	private $compnay = null;
	private $compnay_type = null;
	private $quote = null;
	private $order = null;
	private $bank = null;

	public function __construct()
	{
		helper('text');
		$this->user = new UserModel();
		$this->address = new UserAddressModel();
		$this->country = new Countries_model();
		$this->supplier = new Supplier_model();
		$this->order = new OrdersModel();
		$this->survey = new SurveyModel();
		$this->quote = new QuotationModel();
		$this->company = new UserCompanyModel();
		$this->compnay_type = new CompanyTypeModel();
		$this->bank = new UserBankModel();
	}
	
	public function dashboard()
	{

		$data['title'] = "Dashboard";
	    $data['path'] = base_url('supplier/dashboard');
	    $admin_id = session()->get('user_details')->admin_id;

	   	$db      = \Config\Database::connect();
		$order = $db->table('orders');
		$order->select('*');
		$order->where('supplier_id',$admin_id);
		$order->orderBy('id','DESC');
		$order->limit(5);
		$data['orders'] = $order->get()->getResult();

		$quote = $db->table('quotations');
		$quote->select('*');
		$quote->where('request_to',$admin_id);
		$quote->orderBy('id','DESC');
		$quote->limit(5);
		$data['quotation'] = $quote->get()->getResult();
		
	    $data['controller_name'] = "Home";
	    $data['content'] = view('supplier/dashboard',$data);
    	return view('supplier/layout/supplier_wrapper', $data);
	}	

	public function registration()
	{
		if($this->request->getMethod() == 'post')
		{
			// echo json_encode($_POST);exit;
			helper('text');
			$token = random_string('alnum',20);
			$user_data = [
				'email' => $this->request->getPost('email'),
				'mobile' => $this->request->getPost('mobile'),
				'token' => $token,
				'status' => 'Inactive',
				'user_role' => 2,
				'platform_id' => $this->request->getPost('platform_type'),
			];
			// dd($user_data);
			$this->db->transStart();
			$this->supplier->allowCallbacks(false);
			$this->supplier->insert($user_data);
			$inserted_id = $this->supplier->getInsertID();
			if(!$inserted_id){

				$this->session->setFlashData('exception',$this->supplier->errors());
				return redirect()->to(base_url('supplier/registration'))->withInput();
			}
			
			$this->db->transComplete();
			if($this->db->transStatus() !== FALSE)
			{
				$email = \Config\Services::email();
				$email->setTo($user_data['email']);
				$email->setFrom(display('Confirm Registration'));
				$msg['data'] = '<span class="position">';
				$msg['data'] .= 'Wowwee! Thanks for registering an account with YAHODEHIME!.<br>';
				$msg['data'] .= 'Before we get started, we will need to activate your account.<br>';
				'</span>';
				$msg['data'] .='<p>';
				$msg['data'] .= '<a href="'.base_url('supplier/registration-details/'.$token).'" target="_blank" style="text-decoration:none;padding: 10px; box-shadow: 6px 6px 5px; font-weight: 500; background: transparent; color: #fb9678; cursor: pointer; border-radius: 10px; border: 1px solid #fb9678;">Rgister Now</a>';
				$msg['data'] .='</p>';
				$message = view('common/email_template',$msg);
				$email->setSubject(display('Confirm Registration'));
				$email->setMessage($message);

				if (!$email->send()) 
				{
				    $data = $email->printDebugger(['headers']);
				    print_r($data);
				}
				$this->session->setFlashData('message',display('Please click on link that has just been sent to your email account to verify your email and continue the registration process.'));
				return redirect()->to(base_url('supplier/registration'));
			} else {
				$this->session->setFlashData('exception','Something went wrong');
				return redirect()->to(base_url('supplier/login'));
			}

			// return redirect()->to('otp-verify');
		}
		else {
			$data['countries'] = $this->country->findAll(); 
			$data['survey'] = $this->survey->findAll(); 
			$data['content'] = view('supplier/registeration_survey',$data);
    		return view('common/wrapper', $data);
		}
	}

	public function registration_details($token)
	{	
		$data['token'] = $token;
		$data['isExist'] = $this->supplier->getWhere(['token' => $token])->getRow();	
		$data['comapny_types'] = $this->compnay_type->findAll(); 	
		$data['countries'] = $this->country->findAll(); 
		$data['content'] = view('supplier/registeration_details',$data);
    	return view('common/wrapper', $data);
	}

	public function validate_registration()
	{
		// echo json_encode($_FILES);die();
		// echo json_encode($_POST);die();
		$user_id = $this->request->getPost('user_id');
		$token = $this->request->getPost('token');
		$isExist = $this->user->getWhere(['id' => $user_id,'token'=>$token])->getRow();
		if(!empty($isExist))
		{
			$files = $this->request->getFiles();
			$user_data['firstname'] = $this->request->getPost('firstName');
			$user_data['lastname'] = $this->request->getPost('lastName');
			$user_data['gender'] = $this->request->getPost('gender');
			$user_data['token'] = "";
			$user_data['designation'] = $this->request->getPost('designation');
			$user_data['city'] = $this->request->getPost('user_city');
			$user_data['state'] = $this->request->getPost('user_state');
			$user_data['country_id'] = $this->request->getPost('country_id');
			$user_data['country'] = $this->country->find($this->request->getPost('country_id'))->name;
			$user_data['country_code'] = $this->country->find($this->request->getPost('country_id'))->dial_code;
			$user_data['zip_code'] = $this->request->getPost('user_postal_code');

			if ($user_data['designation'] == 2){
				
				if (! $files['manger_avatar']->isValid())
		        {
		            throw new RuntimeException($files['manger_avatar']->getErrorString().'('.$files['manger_avatar']->getError().')');
		        }
		        if ($files['manger_avatar']->isValid() && ! $files['manger_avatar']->hasMoved())
		        {
		            $path = 'public/assets/uploads/images/documents/';
		            $newName = $files['manger_avatar']->getRandomName();
		            $files['manger_avatar']->move($path,$newName);
					$user_data['manger_document'] = $path.$newName;
		        }
				$user_data['manager_firstName'] = $this->request->getPost('manager_firstName');
				$user_data['manger_gender'] = $this->request->getPost('manger_gender');
				$user_data['manager_lastName'] = $this->request->getPost('manager_lastName');
			} else {
				
				if (!$files['document']->isValid())
				{
		            throw new RuntimeException($document->getErrorString().'('.$document->getError().')');
		        }
		        if ($files['document']->isValid() && ! $files['document']->hasMoved())
		        {
		            $path = 'public/assets/uploads/images/documents/';
		            $newName = $files['document']->getRandomName();
		            $files['document']->move($path,$newName);
					$user_data['manger_document'] = $path.$newName;
		        }
			}	

			$this->db->transStart();
			$this->supplier->allowCallbacks(false);
			$this->supplier->update($user_id,$user_data);
			if(!$this->supplier->affectedRows()){

				$this->session->setFlashData('exception',$this->supplier->errors());
				return redirect()->to(base_url('supplier/registration-details/'.$token))->withInput();
			}
			if (! $files['company_document']->isValid())
	        {
	            throw new RuntimeException($files['company_document']->getErrorString().'('.$files['company_document']->getError().')');
	        }
	        if ($files['company_document']->isValid() && ! $files['company_document']->hasMoved())
	        {
	            $path = 'public/assets/uploads/images/documents/';
	            $companies_doc = $files['company_document']->getRandomName();
	            $files['company_document']->move($path,$companies_doc);
				$comapny_data['thumbnail'] = $path.$companies_doc;
	        }
				$comapny_data['user_id'] = $user_id;
				$comapny_data['company_name'] = $this->request->getPost('company_name');
				$comapny_data['address'] = $this->request->getPost('company_address');
				$comapny_data['city'] = $this->request->getPost('city');
				$comapny_data['company_type'] = $this->request->getPost('company_type');
				$comapny_data['zip_code'] = $this->request->getPost('zipcode');

			$this->company->insert($comapny_data);
			if(!$this->company->getInsertID())
			{

				$this->session->setFlashData('exception',$this->address->errors());	
				return redirect()->to(base_url('supplier/registration-details/'.$token))->withInput();
			}

			if (! $files['bank_avatar']->isValid())
	        {
	            throw new RuntimeException($files['bank_avatar']->getErrorString().'('.$files['bank_avatar']->getError().')');
	        }
	        if ($files['bank_avatar']->isValid() && ! $files['bank_avatar']->hasMoved())
	        {
	            $path = 'public/assets/uploads/images/documents/';
	            $bank_doc = $files['bank_avatar']->getRandomName();
	            $files['bank_avatar']->move($path,$bank_doc);
				$bank_data['document'] = $path.$bank_doc;
	        }
				$bank_data['user_id'] = $user_id;
				$bank_data['bank_name'] = $this->request->getPost('bank_name');
				$bank_data['address'] = $this->request->getPost('bank_address');
				$bank_data['city'] = $this->request->getPost('bank_city');
				$bank_data['IBAN'] = $this->request->getPost('iban');
				$bank_data['BIC'] = $this->request->getPost('bic');
				$bank_data['zip_code'] = $this->request->getPost('bank_zipcode');

			$this->bank->insert($bank_data);
			$this->db->transComplete();
			if($this->db->transStatus() !== FALSE)
			{
				$email = \Config\Services::email();
				$email->setTo($isExist->email);
				$email->setFrom(display('Confirm Registration'));
				$msg['data'] = '<h3 class="name">'.display('Hello').$isExist->firstname.' '.$isExist->lastname.'</h3>';
				$msg['data'] .= '<span class="position">';
				$msg['data'] .= display('You are amongst us now.').'<br>';
				$msg['data'] .= display('Get your quotes of your favourite products now. Enjoy!').'</span>';
				$message = view('common/email_template',$msg);
				$email->setSubject(display('Confirm Registration'));
				$email->setMessage($message);

				if (!$email->send()) 
				{
				    $data = $email->printDebugger(['headers']);
				    print_r($data);
				}
				return redirect()->to(base_url('supplier/registration-details/'.$token));withInput();
			}
		}
	}

	public function otp_verification()
	{
		$data['content'] = view('common/otp_verification');
		return view('common/wrapper', $data);
	}	

	public function user_otp_verification()
	{
		if($this->request->getMethod() == 'post')
		{
			$otp = $this->request->getPost('OTP');
			$email = session()->get('email');
			$is_user = $this->user->where(['email'=> $email,'otp'=>$otp])->first();
            if(!empty($is_user))
			{
	           	$session_data = (object)["fullname" => $is_user->firstname.' '.$is_user->lastname, "admin_id" => $is_user->id,"mobile" => $is_user->mobile, "user_role" => $is_user->user_role, "email" =>$is_user->email, "is_logged_in" => true];
				$this->session->set('user_details',$session_data);
				$this->user->allowCallbacks(false);
				$data['otp'] = '';
				if($this->user->update($is_user->id,$data)){
					$this->response = array('status'=>'success','message'=>display('OTP verified successfully.'));
				}
			} else {
				$this->response = array('status'=>'failed','message'=>display("invalid OTP"));
			}
			echo json_encode($this->response);
		} 
	}

	public function resend_otp_verification()
	{
		if($this->request->getMethod() == 'post')
		{
			$email = session()->get('email');
			$is_user = $this->user->where(['email'=> $email])->first();
			if($this->sendOTP($is_user->id,$is_user))
			{
				$this->response = array('status'=>'success','message'=>display('OTP Send Successfully. Please check your email.'));
			} else {
				$this->response = array('status'=>'failed','message'=>display("OTP not send due to some errors"));
			}
			echo json_encode($this->response);
		}
	}	

	public function reset_otp_verification()
	{
		if($this->request->getMethod() == 'post')
		{
			$email = session()->get('email');
			$is_user = $this->user->where(['email'=> $email])->first();
			$this->user->allowCallbacks(false);
			$data['otp'] = '';
			if($this->user->update($is_user->id,$data))
			{
				$this->response = array('status'=>'success','message'=>display('OTP Reset Successfully.'));
			} else {
				$this->response = array('status'=>'failed','message'=>display("OTP not reset due to some errors"));
			}
			echo json_encode($this->response);
		}
	}

	public function logout(){
		$this->session->remove('user_details');//some session data name for user details
		$this->session->setFlashData('message',display('You are logged out Successfully'));
		return redirect()->to(base_url('supplier/login'));
		// set flash data
		// redirect to home
	}

	public function login()
	{
		if($this->request->getMethod() == 'post')
		{
			// echo json_encode($_POST);
			$email = $this->request->getPost('email');
			$password = $this->request->getPost('password');
			// $details = $this->supplier->where(['email' => $email])->first();
			$is_user = $this->supplier->authenticate($this->request->getPost());
	        if(!empty($is_user))
	        {
        		$this->session->set('email',$is_user->email);
        		if ($is_user->status == 'Inactive') {
        			$this->session->setFlashData('message',display('Your account not activated, so can not logged in to system'));
					return redirect()->to(base_url('supplier/login'))->withInput();
        		} else {
					if($this->sendOTP($is_user->id,$is_user))
					{
						$this->session->setFlashData('message',display('OTP Send Successfully. Please check your email.'));
						return redirect()->to(base_url('otp-verify'));
					} else {
						$this->session->setFlashData('exception',display("OTP not send due to some errors"));
						return redirect()->to(base_url('supplier/login'))->withInput();
					}
        		}
	           
	        } else {
					$this->session->setFlashData('exception',display('Incorrect Email OR Password'));
					return redirect()->to(base_url('supplier/login'))->withInput();
	        }
	        
		} else {
			$data['content'] = view('supplier/login');
    		return view('common/wrapper', $data);
		}
		
	}

	public function reset_password($token)
	{	
		$data['token'] = $token;
		$data['isExist'] = $this->supplier->getWhere(['token' => $token])->getRow();		
		$data['content'] = view('supplier/change_password',$data);
    	return view('common/wrapper', $data);
	}

	public function forgotPassword()
	{
		// dd($this->request->getPost());
	    $email = $this->request->getPost('recover_email');
		$isExist = $this->user->getWhere(['email' => $email])->getRow();
		$response = array();
		if($isExist){
			if($this->sendPassword($isExist->id,$isExist))
			{
				$this->session->setFlashData('message',display('Password Reset Successfully. Please check your email.'));
			} else {
				$this->session->setFlashData('exception',display("Password is not changed due to some errors"));
			}
		} else {
			$this->session->setFlashData('exception',display("Email does not exist."));
		}
		// echo json_encode($response);exit;
		return redirect()->to(base_url('supplier/login'));
	}

	private function sendPassword($user_id,$user){
		helper('text');
		$password = random_string('alnum',10);
        $data['password'] = $password;
		// echo json_encode($data);
		// exit;

		if($this->user->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>').$password.'</b>'."\r\n";
			$mail['data'].='<br>'.display('Please Update your password.');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('Forgot Password');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Test subject');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}

	private function sendOTP($user_id,$user){
		helper('text');
		$OTP = random_string('numeric',6);
        $data['otp'] = $OTP;
		// echo json_encode($user_id);
		// exit;
		$this->user->allowCallbacks(false);
		if($this->user->update($user_id,$data))
		{
			$mail['data']=display('Hello').', '.$user->firstname.' '.$user->lastname.','. "\r\n";
			$mail['data'].=display('Your YAHODEHIME Login Verification Code is <b>').$OTP.'</b>'."\r\n";
			$mail['data'].='<br>'.display('Please do not share with anyone');
			$mail['data'].='<br>'.display('Thanks & Regards');

			$email = \Config\Services::email();

			$email->setTo($user->email);
			$email->setFrom('YAHODEHIME');
			
			$msg = view('common/email_template',$mail);
			$email->setSubject('Login Verification Code');
			$email->setMessage($msg);

			if ($email->send()) 
			{
				// echo 'Email successfully sent';
				// maintain log here
			} 
			else 
			{
			    // $data = $email->printDebugger(['headers']);
				// print_r($data);
				// maintain error log here
			}
			return true;
		} else {
			return false;
		}
	}
	//--------------------------------------------------------------------

}
