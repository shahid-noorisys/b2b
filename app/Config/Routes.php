<?php 
namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('dashboard');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->post('/language-toggle', 'Home::language_toggle');

$routes->get('/', 'Home::dashboard');
$routes->post('login-buyer', 'Home::login');
$routes->get('logout-buyer', 'Home::logout',['filter' => 'isLoggedIn']);
$routes->get('register-buyer', 'Home::registration');
$routes->post('onboard-buyer', 'Home::register_buyer');
$routes->post('password-forgot', 'Home::forgotPassword');
$routes->post('password-forgot', 'Home::forgotPassword');
$routes->post('sub-category-based-product', 'Home::getProductBySubCategory');
$routes->post('order-max-category-based-product', 'Home::getOrdermaxProductBySubCategory');

// ********************************** //

// ------------  Buyer chat-Rooom ----------------//
$routes->match(['get','post'],'verification-otp', 'Home::verification_otp');
$routes->match(['get','post'],'customer-otp-resend', 'Home::resend_otp_verification');
$routes->match(['get','post'],'otp-reset', 'Home::reset_otp');
// ------------ Buyer chat-Rooom ----------------//


$routes->match(['get','post'],'supplier/login', 'supplier\Dashboard::login');
$routes->match(['get','post'],'supplier/logout', 'supplier\Dashboard::logout');
$routes->get('supplier/dashboard', 'supplier\Dashboard::dashboard',['filter' => 'isSupplierLoggedIn']);
$routes->match(['get','post'],'supplier/registration', 'supplier\Dashboard::registration');
$routes->match(['get','post'],'supplier/validate-registration', 'supplier\Dashboard::validate_registration');
$routes->match(['get','post'],'/otp-verify', 'supplier\Dashboard::otp_verification');
$routes->match(['get','post'],'/otp-verification', 'supplier\Dashboard::user_otp_verification');
$routes->match(['get','post'],'/otp-resend', 'supplier\Dashboard::resend_otp_verification');
$routes->match(['get','post'],'/verification-otp-reset', 'supplier\Dashboard::reset_otp_verification');


$routes->match(['get','post'],'/password-reset', 'Cron::password_reset');

// ------------  Prducts ----------------//
$routes->match(['get','post'],'supplier/products', 'supplier\Products::product_list',['filter' => 'isSupplierLoggedIn']);
$routes->get('supplier/product_add', 'supplier\Products::product_add',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/product-delete', 'supplier\Products::product_delete',['filter' => 'isSupplierLoggedIn']);
$routes->match(['get','post'],'supplier/product_create', 'supplier\Products::createProduct',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/sub-category-list', 'supplier\Products::getSubCategories');
$routes->match(['get','post'],'supplier/product-update', 'supplier\Products::updateProduct',['filter' => 'isSupplierLoggedIn']);
$routes->match(['get','post'],'supplier/product-image-upload', 'supplier\Products::uploadProductImage');
// ------------  Prducts ----------------//


// ------------ Profile ----------------//
$routes->post('supplier/password-forgot', 'supplier\Dashboard::forgotPassword');
$routes->get('supplier/profile', 'supplier\Profile::profile',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/change-password', 'supplier\Profile::change_password');
$routes->post('supplier/profile-edit', 'supplier\Profile::profile_edit',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/add-company', 'supplier\Profile::company_profile_add',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/edit-company', 'supplier\Profile::company_profile_edit',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/profile-image-edit', 'supplier\Profile::profile_image_edit',['filter' => 'isSupplierLoggedIn']);
// ------------ Profile ----------------//


// ------------  Supplier chat-Rooom ----------------//
$routes->match(['get','post'],'supplier/messages/(:any)', 'supplier\Message::messages',['filter' => 'isSupplierLoggedIn']);
$routes->match(['get','post'],'supplier/messages', 'supplier\Message::messages',['filter' => 'isSupplierLoggedIn']);
$routes->match(['get','post'],'supplier/send_message', 'supplier\Message::send_message',['filter' => 'isSupplierLoggedIn']);
$routes->match(['get','post'],'supplier/get_messages', 'supplier\Message::get_messages',['filter' => 'isSupplierLoggedIn']);
// ------------ Supplier chat-Rooom ----------------//

// ------------  Supplier chat-Rooom ----------------//
$routes->match(['get','post'],'/notifications', 'Notification::notifications',['filter' => 'isSupplierLoggedIn'],['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'/notification-list', 'Notification::notification_list',['filter' => 'isSupplierLoggedIn'],['filter' => 'isAdminLoggedIn']);
$routes->post('/get-notification-counter', 'Notification::get_notification_counter',['filter' => 'isSupplierLoggedIn'],['filter' => 'isAdminLoggedIn']);
// ------------ Supplier chat-Rooom ----------------//


// ------------  Supplier Orders ----------------//
$routes->match(['get','post'],'supplier/orders', 'supplier\Orders::orders_list',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/order-status', 'supplier\Orders::order_status',['filter' => 'isSupplierLoggedIn']);
// ------------ Supplier Orders ----------------//

// ------------  Supplier Review & Rating ----------------//
$routes->match(['get','post'],'supplier/reviews', 'supplier\Reviews::reviews_list',['filter' => 'isSupplierLoggedIn']);
// ------------ Supplier Review & Rating ----------------//

// ------------ admin manage products  ----------------//
$routes->match(['get','post'],'admin/products', 'admin\Products::product_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/getSubCategoriesList', 'admin\Products::getSubCategories',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/product-update', 'admin\Products::updateProduct',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/product-image-upload', 'admin\Products::uploadProductImage');
// ------------admin manage products  ----------------//

// ------------  Supplier Transaction History ----------------//
$routes->match(['get','post'],'supplier/payment-history', 'supplier\PaymentHistorty::payment_historty_list',['filter' => 'isSupplierLoggedIn']);
// ------------ Supplier Transaction History ----------------//

// ------------  Admin Transaction History ----------------//
$routes->match(['get','post'],'admin/payment-history', 'admin\PaymentHistorty::payment_historty_list',['filter' => 'isAdminLoggedIn']);
// ------------ Admin Transaction History ----------------//


// ------------  admin Review & Rating ----------------//
$routes->match(['get','post'],'admin/reviews', 'admin\Reviews::reviews_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/reviews-ajax', 'admin\Reviews::ajaxReviewslist',['filter' => 'isAdminLoggedIn']);
// ------------ admin Review & Rating ----------------//


// ------------  supplier quotations  ----------------//
$routes->match(['get','post'],'supplier/quotations', 'supplier\Quotations::quotations_list',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/product-details', 'supplier\Quotations::product_details',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/quotation-amount', 'supplier\Quotations::update_quotation_amount',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/reject-quotation', 'supplier\Quotations::reject_quotation',['filter' => 'isSupplierLoggedIn']);
$routes->post('supplier/send-quotation', 'supplier\Quotations::send_quotation',['filter' => 'isSupplierLoggedIn']);
// ------------ supplier quotations  ----------------//

$routes->post('admin/valid-quotation', 'admin\Quotations::valid_quotation',['filter' => 'isAdminLoggedIn']);
// ------------  admin quotations  ----------------//
$routes->match(['get','post'],'admin/quotations', 'admin\Quotations::quotations_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/product-details', 'admin\Quotations::product_details',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/quotation-amount', 'admin\Quotations::update_quotation_amount',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/reject-quotation', 'admin\Quotations::reject_quotation',['filter' => 'isAdminLoggedIn']);
// ------------ admin quotations  ----------------//

// ------------  Admin chat-Rooom ----------------//
$routes->match(['get','post'],'admin/messages/(:any)', 'admin\AdminMessages::messages',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/messages', 'admin\AdminMessages::messages',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/send_message', 'admin\AdminMessages::send_message',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/get_messages', 'admin\AdminMessages::get_messages',['filter' => 'isAdminLoggedIn']);

// ------------ Admin chat-Rooom ----------------//

// ------------  admin Orders ----------------//
$routes->match(['get','post'],'admin/orders', 'admin\Orders::orders_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/order-status', 'admin\Orders::order_status',['filter' => 'isAdminLoggedIn']);
// ------------ admin Orders ----------------//

// ------------  admin Orders ----------------//
$routes->match(['get','post'],'admin/commision', 'admin\PlatformCommision::commision_list',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/commision_add', 'admin\PlatformCommision::add_commision',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/commision_edit', 'admin\PlatformCommision::edit_commision',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/get_commision_info', 'admin\PlatformCommision::get_commision_details',['filter' => 'isAdminLoggedIn']);
// ------------ admin Orders ----------------//

// ------------  Buyer chat-Rooom ----------------//
$routes->get('messages', 'buyer\UserMessages::message_view',['filter' => 'isLoggedIn']);
$routes->post('get-messages', 'buyer\UserMessages::get_quotation_messages',['filter' => 'isLoggedIn']);
$routes->post('send-message', 'buyer\UserMessages::send_message',['filter' => 'isLoggedIn']);
// ------------ Buyer chat-Rooom ----------------//

// ************************************** //

$routes->match(['get','post'],'admin/login', 'admin\Dashboard::login');
$routes->match(['get','post'],'admin/logout', 'admin\Dashboard::logout');
$routes->get('admin/dashboard', 'admin\Dashboard::dashboard',['filter' => 'isAdminLoggedIn']);

// ------------ Manage categories ----------------//
$routes->get('admin/categories', 'admin\Categories::category_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/category_add', 'admin\Categories::category_add',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/category_edit', 'admin\Categories::category_edit',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/get_category', 'admin\Categories::get_category_details',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/delete_category', 'admin\Categories::delete_category',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/category_deactivate', 'admin\Categories::category_deactivate',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/category_activate', 'admin\Categories::category_activate',['filter' => 'isAdminLoggedIn']);
// ------------ Manage categories ----------------//

// ------------  Manage Sub-categories ----------------//
$routes->get('admin/sub_categories', 'admin\Sub_Categories::sub_category_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/subcategory_add', 'admin\Sub_Categories::subcategory_add',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/subcategory_edit', 'admin\Sub_Categories::subcategory_edit',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/get_subcategory', 'admin\Sub_Categories::get_subcategory_details',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/delete_subcategory', 'admin\Sub_Categories::delete_subcategory',['filter' => 'isAdminLoggedIn']);

$routes->post('admin/sub_category_deactivate', 'admin\Sub_Categories::sub_category_deactivate',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/sub_category_activate', 'admin\Sub_Categories::sub_category_activate',['filter' => 'isAdminLoggedIn']);
// ------------  Manage Sub-categories ----------------//

// ------------  Manage Supplier ----------------//
$routes->match(['get','post'],'admin/suppliers', 'admin\ManageSupplier::supplier_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/supplier_activate', 'admin\ManageSupplier::supplier_activate',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/supplier_deactivate', 'admin\ManageSupplier::supplier_deactivate',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/supplier_credential', 'admin\ManageSupplier::supplier_credential',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/seller-notify', 'admin\ManageSupplier::Send_notification',['filter' => 'isAdminLoggedIn']);
// ------------  Manage Supplier ----------------//


// ------------  Manage Buyer ----------------//
$routes->post('admin/buyer-notify', 'admin\ManageBuyer::Send_notification',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/buyers', 'admin\ManageBuyer::buyer_list',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/buyer_activate', 'admin\ManageBuyer::buyer_activate',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/buyer_deactivate', 'admin\ManageBuyer::buyer_deactivate',['filter' => 'isAdminLoggedIn']);
$routes->post('admin/buyer_credential', 'admin\ManageBuyer::buyer_credential',['filter' => 'isAdminLoggedIn']);
// ------------  Manage Buyer ----------------//

$routes->match(['get','post'],'change-password', 'admin\Dashboard::change_password',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/profile', 'admin\Dashboard::profile',['filter' => 'isAdminLoggedIn']);
$routes->match(['get','post'],'admin/profile-image', 'admin\Dashboard::profile_image',['filter' => 'isAdminLoggedIn']);
	
$routes->get('/send-mail', 'Home::sendMail');


//------------  Buyer profile section  -------//
$routes->get('buyer-profile','buyer\UserProfile::home',['filter' => 'isLoggedIn']);
$routes->post('buyer/company-profile-add','buyer\UserProfile::CompanyprofileAdd',['filter' => 'isLoggedIn']);
$routes->post('buyer/company-profile-edit','buyer\UserProfile::CompanyprofileEdit',['filter' => 'isLoggedIn']);
$routes->post('buyer/profile-update','buyer\UserProfile::updateProfile',['filter' => 'isLoggedIn']);
$routes->post('buyer/profile-password-change','buyer\UserProfile::updatePassword',['filter' => 'isLoggedIn']);
$routes->post('buyer/profile-image-update','buyer\UserProfile::updateProfileImage',['filter' => 'isLoggedIn']);
$routes->post('get-company','buyer\UserProfile::getCompanydetails',['filter' => 'isLoggedIn']);
//------------  Buyer profile section  -------//

//------------  Buyer Manage Quotation  -------//
$routes->get('buyer/quotations','buyer\Quotations::quotationList',['filter' => 'isLoggedIn']);
$routes->post('ajax-quotations','buyer\Quotations::ajaxQuotationList',['filter' => 'isLoggedIn']);
$routes->post('buyer/submit-quote','buyer\Quotations::submitQuoteRequest',['filter' => 'isLoggedIn']);
$routes->match(['get','post'],'add-to-quote','buyer\Quotations::addToQuote',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Quotation  -------//
//------------  Buyer Manage Cart  -------//
$routes->get('cart-list','buyer\Carts::cartList',['filter' => 'isLoggedIn']);
$routes->post('add-to-cart','buyer\Carts::addToCart',['filter' => 'isLoggedIn']);
$routes->post('update-cart','buyer\Carts::updateCart',['filter' => 'isLoggedIn']);
$routes->post('get-item-count','buyer\Carts::getTotalCartCount');
$routes->post('remove-from-cart','buyer\Carts::removeFromCart',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Cart  -------//
//------------  Buyer View Product Details  -------//
$routes->post('view-product-details', 'supplier\Products::getProduct');

//------------  Buyer View Product Details  -------//

//------------  Buyer Manage Orders  -------//
$routes->get('orders', 'buyer\Orders::orderList',['filter' => 'isLoggedIn']);
$routes->post('create-order', 'buyer\Orders::createOrder',['filter' => 'isLoggedIn']);
$routes->post('order-ajax', 'buyer\Orders::ajaxOrderList',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Orders  -------//

$routes->post('address-ajax', 'buyer\UserAddresses::ajaxAddressList',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Reviews  -------//
$routes->post('reviews-ajax', 'buyer\UserReviews::ajaxReviewsList',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Reviews  -------//

//------------  Buyer Manage Transaction History  -------//
$routes->get('transactions','buyer\Transactions::transactionList',['filter' => 'isLoggedIn']);
$routes->post('ajax-transaction','buyer\Transactions::ajaxTransactionList',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Transaction History  -------//

//------------  Buyer Manage Product list  -------//
$routes->post('product-dynamic','buyer\Products::ajaxProductList');
//------------  Buyer Manage Product list  -------//

//------------  Buyer Manage Addresses  -------//
$routes->post('address-new','buyer\UserAddresses::addNewAddress',['filter' => 'isLoggedIn']);
$routes->post('get-address','buyer\UserAddresses::getAddressdetails',['filter' => 'isLoggedIn']);
$routes->post('address-edit','buyer\UserAddresses::editAddressdetails',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Addresses  -------//

//------------  Buyer Manage Review & Rating  -------//
$routes->post('review-add','buyer\UserReviews::addNewReviews',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Review & Rating  -------//

//------------  Buyer Manage Inapp Notification  -------//
$routes->get('list-notification','buyer\Notifications::noteList',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Inapp Notification  -------//
//------------  Admin Create Receipt when order marked as delivered  -------//
$routes->match(['get','post'],'admin/receipt-create','admin\Orders::createReceipt',['filter' => 'isAdminLoggedIn']);
//------------  Admin Create Receipt when order marked as delivered  -------//

//------------  Contact form to send mail  -------//
$routes->match(['get','post'],'contact-us','buyer\Contact::showForm');
//------------  Contact form to send mail  -------//


//------------  Static Pages  -------//
$routes->match(['get','post'],'about-us','buyer\StaticPages::about_us');
$routes->match(['get','post'],'faq','buyer\StaticPages::faq');
$routes->match(['get','post'],'terms-and-condition','buyer\StaticPages::faq');
$routes->match(['get','post'],'privacy-policy','buyer\StaticPages::privacy_policy');
$routes->match(['get','post'],'terms','buyer\StaticPages::terms_condition');
$routes->match(['get','post'],'return-refund','buyer\StaticPages::return_refund');
//------------  Static Pages  -------//

//*********************************************************************//
//------------  Dynamic Route section  -------//

$routes->match(['get','post'],'product-view/(:num)', 'Home::getProductdetails/$1');
$routes->match(['get','post'],'quotations-info/(:num)', 'buyer\Quotations::getQuotatationdetails/$1',['filter' => 'isLoggedIn']);
//------------  Manage Products  -------//
$routes->get('supplier/(:num)/product-edit', 'supplier\Products::editProduct/$1',['filter' => 'isSupplierLoggedIn']);
//------------  Manage Products  -------//

//------------  Manage Products  -------//
$routes->get('admin/(:num)/product-edit', 'admin\Products::Product_edit/$1',['filter' => 'isAdminLoggedIn']);
//------------  Manage Products  -------//


$routes->get('account-activation/(:any)', 'Home::account_activation/$1');
$routes->match(['get','post'],'activate-account', 'Home::activate_account');

$routes->get('supplier/registration-details/(:any)', 'supplier\Dashboard::registration_details/$1');
$routes->get('supplier/reset-password/(:any)', 'supplier\Dashboard::reset_password/$1');
//------------  Manage supplier  -------//
$routes->get('admin/view_supplier/(:num)', 'admin\ManageSupplier::view_supplier_profile',['filter' => 'isAdminLoggedIn']);
//------------  Manage supplier  -------//

// ------------  supplier quotations  ----------------//
$routes->get('supplier/quotation/view/(:num)', 'supplier\Quotations::quotation_details/$1',['filter' => 'isSupplierLoggedIn']);
// ------------ supplier quotations  ----------------//

// ------------  Supplier Orders ----------------//
$routes->get('supplier/orders/view/(:num)', 'supplier\Orders::orders_details/$1',['filter' => 'isSupplierLoggedIn']);
// ------------ Supplier Orders ----------------//

// ------------  Supplier Orders ----------------//
$routes->match(['get','post'],'supplier/receipt/download/(:any)', 'supplier\PaymentHistorty::download_receipt/$1',['filter' => 'isSupplierLoggedIn']);
// ------------ Supplier Orders ----------------//

// ------------  Admin Orders ----------------//
$routes->get('admin/orders/view/(:num)', 'admin\Orders::orders_details/$1',['filter' => 'isAdminLoggedIn']);
// ------------ Admin Orders ----------------//

// ------------ Buyer Manage Orders ----------------//
$routes->get('order-details/(:num)', 'buyer\Orders::orderDetails/$1',['filter' => 'isLoggedIn']);
$routes->get('order-info/(:num)', 'buyer\Orders::orderInformation/$1',['filter' => 'isLoggedIn']);
// ------------ Buyer Manage Orders ----------------//


// ------------  admin quotations  ----------------//
$routes->get('admin/quotation/view/(:num)', 'admin\Quotations::quotation_details/$1');
// ------------ admin quotations  ----------------//

//------------  Manage Buyer  -------//
$routes->get('admin/view_buyer/(:num)', 'admin\ManageBuyer::view_buyer_profile');
//------------  Manage Buyer  -------//

//------------  Manage Buyer  -------//
$routes->get('messages/(:num)', 'buyer\UserMessages::message_view',['filter' => 'isLoggedIn']);
//------------  Manage Buyer  -------//
//------------  Buyer Manage Quotations  -------//
$routes->get('buyer/quotation-request/(:num)','buyer\Quotations::requestToQuote/$1',['filter' => 'isLoggedIn']);
//------------  Buyer Manage Quotations  -------//
//------------  Buyer Manage Product list  -------//
$routes->match(['get','post'],'list-product/(:num)/(:num)','buyer\Products::productList/$1/$2');
//------------  Buyer Manage Product list  -------//
//------------  Dynamic Route section  -------//
//*********************************************************************//

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
