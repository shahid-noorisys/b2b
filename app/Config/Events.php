<?php namespace Config;

use CodeIgniter\Services;
use CodeIgniter\Events\Events;
use App\Models\NotificationModel;
use CodeIgniter\Exceptions\FrameworkException;

/*
 * --------------------------------------------------------------------
 * Application Events
 * --------------------------------------------------------------------
 * Events allow you to tap into the execution of the program without
 * modifying or extending core files. This file provides a central
 * location to define your events, though they can always be added
 * at run-time, also, if needed.
 *
 * You create code that can execute by subscribing to events with
 * the 'on()' method. This accepts any form of callable, including
 * Closures, that will be executed when the event is triggered.
 *
 * Example:
 *      Events::on('create', [$myInstance, 'myMethod']);
 */

Events::on('pre_system', function () {
	if (ENVIRONMENT !== 'testing')
	{
		if (ini_get('zlib.output_compression'))
		{
			throw FrameworkException::forEnabledZlibOutputCompression();
		}

		while (ob_get_level() > 0)
		{
			ob_end_flush();
		}

		ob_start(function ($buffer) {
			return $buffer;
		});
	}

	/*
	 * --------------------------------------------------------------------
	 * Debug Toolbar Listeners.
	 * --------------------------------------------------------------------
	 * If you delete, they will no longer be collected.
	 */
	if (ENVIRONMENT !== 'production')
	{
		Events::on('DBQuery', 'CodeIgniter\Debug\Toolbar\Collectors\Database::collect');
		Services::toolbar()->respond();
	}
	/*
	* --------------------------------------------------------------------
	* Notification Counter Listeners.
	* --------------------------------------------------------------------
	* If you delete, counter will no longer be shown.
	*/
	$session = Services::session();
	$user_details = $session->get('user_details');
	// echo json_encode($user_details);exit;
	if(! empty($user_details) AND $user_details->user_role == 3)
	{
		$isLoggedIn = (isset($user_details->isLoggedIn))?$session->get('user_details')->isLoggedIn:null;
		if($isLoggedIn){
			$noteModel = new NotificationModel();
			$user_id = $user_details->user_id;
			$note_count = $noteModel->where('user_id',$user_id)->where('user_role',3)->where('read_status','Unread')->countAllResults();
			$user_details->note_count = $note_count;
			$session->set('user_details',$user_details);
		}
	}
	else if(! empty($user_details) AND $user_details->user_role == 2)
	{
		// apply logic for the supplier notification counter
	}
});

Events::on('get_current_note_counter', function ($session,$noteModel) {
	/*
	* --------------------------------------------------------------------
	* Notification Counter Listeners.
	* --------------------------------------------------------------------
	* If you delete, counter will no longer be shown.
	*/
	$user_details = $session->get('user_details');
	// echo json_encode($user_details);exit;
	if(! empty($user_details) AND $user_details->user_role == 3)
	{
		$isLoggedIn = (isset($user_details->isLoggedIn))?$session->get('user_details')->isLoggedIn:null;
		if($isLoggedIn){
			// $noteModel = new NotificationModel();
			$user_id = $user_details->user_id;
			$note_count = $noteModel->where('user_id',$user_id)->where('user_role',3)->where('read_status','Unread')->countAllResults();
			$user_details->note_count = $note_count;
			$session->set('user_details',$user_details);
		}
	}
	else if(! empty($user_details) AND $user_details->user_role == 2)
	{
		// apply logic for the supplier notification counter
	}
});

Events::on('update_buyer_notification', function ($note_data,$notifyModel) {
	if(!empty($note_data)){
		foreach ($note_data as $v) {
			if($v->read_status == 'Unread'){
				$notifyModel->update($v->id,['read_status' => 'Read']);
			}
		}
	}
},100);
