<?php
namespace App\Models;

use CodeIgniter\Model;

class QuotationModel extends Model
{
    protected $table      = 'quotations';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['q_no','product_ids','request_from','request_to','amount','total_items','product_json','is_accepted','is_paid','is_rejected','is_validated'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    

    public function getallSellerquotationList(array $quote_filters , $admin_id = null)
    {
        $criterial = '';
        if($admin_id !=null ) { 

            if(isset($quote_filters['quote_no']) && $quote_filters['quote_no'] != "")
             {
               $criterial .= " AND tc.q_no = '".$quote_filters['quote_no']."'";
             }
            if(isset($quote_filters['user_id']) && $quote_filters['user_id'] != "")
             {
               $criterial .= " AND tc.request_from = '".$quote_filters['user_id']."'";
             }

            if(isset($quote_filters['start']) && $quote_filters['start'] != "" && isset($quote_filters['end']) && $quote_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($quote_filters['start']))."' AND '".date('Y-m-d',strtotime($quote_filters['end']))."'";
            }

            if(isset($quote_filters['start']) && $quote_filters['start'] != "" && isset($quote_filters['end']) && $quote_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($quote_filters['start']))."' AND '".date('Y-m-d',strtotime($quote_filters['end']))."'";
            }


            if((isset($quote_filters['status']) && $quote_filters['status'] != "") && (isset($quote_filters['criteria']) && $quote_filters['criteria'] != ""))
            {
               if ($quote_filters['status'] == "Accepted" && $quote_filters['criteria'] == "All")
               {
                    $criterial = '';
               }
               if ($quote_filters['status'] == "Accepted" && $quote_filters['criteria'] == "Yes")
               {
                   $criterial .= " AND tc.is_accepted = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Accepted" && $quote_filters['criteria'] == "No") {
                  
                   $criterial .= " AND tc.is_accepted = '".$quote_filters['criteria']."'";
               }

               if ($quote_filters['status'] == "Paid" && $quote_filters['criteria'] == "All") 
               {
                    $criterial = '';
               }
               if ($quote_filters['status'] == "Paid" && $quote_filters['criteria'] == "Yes") 
               {
                   $criterial .= " AND tc.is_paid = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Paid" && $quote_filters['criteria'] == "No") 
               {
                  
                   $criterial .= " AND tc.is_paid = '".$quote_filters['criteria']."'";
               }

               if ($quote_filters['status'] == "Validate" && $quote_filters['criteria'] == "All")
               {
                    $criterial = '';
               }
               if ($quote_filters['status'] == "Validate" && $quote_filters['criteria'] == "Yes")
               {
                  
                   $criterial .= " AND tc.is_validated = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Validate" && $quote_filters['criteria'] == "No") 
                {
                  
                   $criterial .= " AND tc.is_validated = '".$quote_filters['criteria']."'";
                }

                if ($quote_filters['status'] == "Rejected" && $quote_filters['criteria'] == "All")
                {
                    $criterial = '';
                }
               if ($quote_filters['status'] == "Rejected" && $quote_filters['criteria'] == "Yes")
               {
                  
                   $criterial .= " AND tc.is_rejected = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Rejected" && $quote_filters['criteria'] == "No") {
                  
                   $criterial .= " AND tc.is_rejected = '".$quote_filters['criteria']."'";
               }

            }

            $query = "SELECT * FROM quotations AS tc WHERE tc.request_to ='".$admin_id."'";
            $query .= $criterial;
            $query .= " ORDER BY tc.id DESC";
            return $this->db->query($query)->getResult();
        } return false;
    }

    public function getalladminQuotationList(array $quote_filters , $admin_id = null)
    {
        $criterial = '';
        if($admin_id !=null ) { 

            if(isset($quote_filters['quote_no']) && $quote_filters['quote_no'] != "")
             {
               $criterial .= " AND tc.q_no = '".$quote_filters['quote_no']."'";
             }
            if(isset($quote_filters['user_id']) && $quote_filters['user_id'] != "")
             {
               $criterial .= " AND tc.request_from = '".$quote_filters['user_id']."'";
             }

            if(isset($quote_filters['start']) && $quote_filters['start'] != "" && isset($quote_filters['end']) && $quote_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($quote_filters['start']))."' AND '".date('Y-m-d',strtotime($quote_filters['end']))."'";
            }

            if(isset($quote_filters['start']) && $quote_filters['start'] != "" && isset($quote_filters['end']) && $quote_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($quote_filters['start']))."' AND '".date('Y-m-d',strtotime($quote_filters['end']))."'";
            }

            if((isset($quote_filters['status']) && $quote_filters['status'] != "") && (isset($quote_filters['criteria']) && $quote_filters['criteria'] != ""))
            {
               if ($quote_filters['status'] == "Accepted" && $quote_filters['criteria'] == "All")
               {
                    $criterial = '';
               }
               if ($quote_filters['status'] == "Accepted" && $quote_filters['criteria'] == "Yes")
               {
                   $criterial .= " AND tc.is_accepted = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Accepted" && $quote_filters['criteria'] == "No") {
                  
                   $criterial .= " AND tc.is_accepted = '".$quote_filters['criteria']."'";
               }

               if ($quote_filters['status'] == "Paid" && $quote_filters['criteria'] == "All") 
               {
                    $criterial = '';
               }
               if ($quote_filters['status'] == "Paid" && $quote_filters['criteria'] == "Yes") 
               {
                   $criterial .= " AND tc.is_paid = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Paid" && $quote_filters['criteria'] == "No") 
               {
                  
                   $criterial .= " AND tc.is_paid = '".$quote_filters['criteria']."'";
               }

               if ($quote_filters['status'] == "Validate" && $quote_filters['criteria'] == "All")
               {
                    $criterial = '';
               }
               if ($quote_filters['status'] == "Validate" && $quote_filters['criteria'] == "Yes")
               {
                  
                   $criterial .= " AND tc.is_validated = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Validate" && $quote_filters['criteria'] == "No") 
                {
                  
                   $criterial .= " AND tc.is_validated = '".$quote_filters['criteria']."'";
                }

                if ($quote_filters['status'] == "Rejected" && $quote_filters['criteria'] == "All")
                {
                    $criterial = '';
                }
               if ($quote_filters['status'] == "Rejected" && $quote_filters['criteria'] == "Yes")
               {
                  
                   $criterial .= " AND tc.is_rejected = '".$quote_filters['criteria']."'";
               } 
               if ($quote_filters['status'] == "Rejected" && $quote_filters['criteria'] == "No") {
                  
                   $criterial .= " AND tc.is_rejected = '".$quote_filters['criteria']."'";
               }

            }

            $query = "SELECT * FROM quotations AS tc WHERE 1";
            $query .= $criterial;
            $query .= " ORDER BY tc.id DESC";
            return $this->db->query($query)->getResult();
        } return false;
    }
}