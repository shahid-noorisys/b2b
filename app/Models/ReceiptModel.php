<?php
namespace App\Models;

use CodeIgniter\Model;

class ReceiptModel extends Model
{
    protected $table      = 'receipts';
    protected $primaryKey = 'receipt_id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['receipt_id','q_id','q_no','order_id','order_no','paid_by','paid_to','order_amount','currency_code','product_json','pdf_url','payment_mehtod'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;
}