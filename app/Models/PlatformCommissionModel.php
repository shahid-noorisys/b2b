<?php 
namespace App\Models;

use CodeIgniter\Model;

class PlatformCommissionModel extends Model
{
    protected $table      = 'platform_commission';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['supplier_id','commission_charge','country_id','country_code','user_id','created_by','status','deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    
}