<?php
namespace App\Models;

use CodeIgniter\Model;

class BuyerTransactionModel extends Model
{
    protected $table      = 'buyer_transactions';
    protected $primaryKey = 'transaction_id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['transaction_id','q_id','q_no','order_id','order_no','paid_by','paid_to','transaction_amount','currency_code','type','receipt_id','payment_mehtod','reason','status','created_date'];

    // protected $useTimestamps = true;
    // protected $createdField  = 'created_date';
    // protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;
}