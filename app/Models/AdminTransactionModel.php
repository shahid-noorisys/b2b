<?php
namespace App\Models;

use CodeIgniter\Model;

class AdminTransactionModel extends Model
{
    protected $table      = 'admin_transactions';
    protected $primaryKey = 'transaction_id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['transaction_id','q_id','q_no','order_id','order_no','paid_by','paid_to','transaction_amount','currency_code','type','receipt_id','payment_mehtod','reason','status','created_date'];

    // protected $useTimestamps = true;
    // protected $createdField  = 'created_date';
    // protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_at';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function getallAdmintransactionList(array $transaction_filters , $admin_id = null)
    {
        $criterial = '';
        if($admin_id !=null ) { 
            if(isset($transaction_filters['transaction_no']) && $transaction_filters['transaction_no'] != "")
             {
               $criterial .= " AND tc.transaction_id = '".$transaction_filters['transaction_no']."'";
             }
            if(isset($transaction_filters['order_no']) && $transaction_filters['order_no'] != "")
             {
               $criterial .= " AND tc.order_no = '".$transaction_filters['order_no']."'";
             }
            if(isset($transaction_filters['quote_no']) && $transaction_filters['quote_no'] != "")
             {
               $criterial .= " AND tc.q_no = '".$transaction_filters['quote_no']."'";
             }
            if(isset($transaction_filters['seller_id']) && $transaction_filters['seller_id'] != "")
             {
               $criterial .= " AND tc.paid_to = '".$transaction_filters['seller_id']."'";
             }
            if(isset($transaction_filters['user_id']) && $transaction_filters['user_id'] != "")
             {
               $criterial .= " AND tc.paid_by = '".$transaction_filters['user_id']."'";
             }
            if(isset($transaction_filters['type']) && $transaction_filters['type'] != "")
             {
               $criterial .= " AND tc.type = '".$transaction_filters['type']."'";
             }
            if(isset($transaction_filters['method']) && $transaction_filters['method'] != "")
             {
               $criterial .= " AND tc.payment_method = '".$transaction_filters['method']."'";
             }

            if(isset($transaction_filters['start']) && $transaction_filters['start'] != "" && isset($transaction_filters['end']) && $transaction_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($transaction_filters['start']))."' AND '".date('Y-m-d',strtotime($transaction_filters['end']))."'";
            }

             $query = "SELECT * FROM admin_transactions AS tc WHERE 1";
             $query .= $criterial;
             $query .= " ORDER BY tc.transaction_id DESC";
            return $this->db->query($query)->getResult();
        } return false;
    }
}