<?php 
namespace App\Models;

use CodeIgniter\Model;

class OrdersModel extends Model
{
    protected $table      = 'orders';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['order_no','q_id','q_no','product_ids','buyer_id','supplier_id','address_id','amount','total_items','payment_mode','order_status','product_json','order_note','payment_method','transaction_number'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';


    public function getallSellerordersList(array $order_filters , $admin_id = null)
    {
    	$criterial = '';
    	if($admin_id !=null ) {	
	    	if(isset($order_filters['order_no']) && $order_filters['order_no'] != "")
	         {
	           $criterial .= " AND tc.order_no = '".$order_filters['order_no']."'";
	         }
	        if(isset($order_filters['quote_no']) && $order_filters['quote_no'] != "")
	         {
	           $criterial .= " AND tc.q_no = '".$order_filters['quote_no']."'";
	         }
	        if(isset($order_filters['user_id']) && $order_filters['user_id'] != "")
	         {
	           $criterial .= " AND tc.buyer_id = '".$order_filters['user_id']."'";
	         }
	        if(isset($order_filters['status']) && $order_filters['status'] != "")
	         {
	           $criterial .= " AND tc.order_status = '".$order_filters['status']."'";
	         }

	        if(isset($order_filters['start']) && $order_filters['start'] != "" && isset($order_filters['end']) && $order_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($order_filters['start']))."' AND '".date('Y-m-d',strtotime($order_filters['end']))."'";
            }

        	 $query = "SELECT * FROM orders AS tc WHERE tc.supplier_id ='".$admin_id."'";
        	 $query .= $criterial;
        	 $query .= " ORDER BY tc.id DESC";
        	return $this->db->query($query)->getResult();
        } return false;
    }

    public function getalladminOrdersList(array $order_filters , $admin_id = null)
    {
    	$criterial = '';
    	if($admin_id !=null ) {	
	    	if(isset($order_filters['order_no']) && $order_filters['order_no'] != "")
	         {
	           $criterial .= " AND tc.order_no = '".$order_filters['order_no']."'";
	         }
	        if(isset($order_filters['quote_no']) && $order_filters['quote_no'] != "")
	         {
	           $criterial .= " AND tc.q_no = '".$order_filters['quote_no']."'";
	         }
	        if(isset($order_filters['user_id']) && $order_filters['user_id'] != "")
	         {
	           $criterial .= " AND tc.supplier_id = '".$order_filters['user_id']."'";
	         }
	        if(isset($order_filters['status']) && $order_filters['status'] != "")
	         {
	           $criterial .= " AND tc.order_status = '".$order_filters['status']."'";
	         }

	        if(isset($order_filters['customer_id']) && $order_filters['customer_id'] != "")
	         {
	           $criterial .= " AND tc.buyer_id = '".$order_filters['customer_id']."'";
	         }

	        if(isset($order_filters['start']) && $order_filters['start'] != "" && isset($order_filters['end']) && $order_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($order_filters['start']))."' AND '".date('Y-m-d',strtotime($order_filters['end']))."'";
            }


        	 $query = "SELECT * FROM orders AS tc WHERE 1";
        	 $query .= $criterial;
        	 $query .= " ORDER BY tc.id DESC";
        	return $this->db->query($query)->getResult();
        } return false;
    }
}