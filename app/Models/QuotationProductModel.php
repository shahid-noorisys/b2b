<?php
namespace App\Models;

use CodeIgniter\Model;

class QuotationProductModel extends Model
{
    protected $table      = 'quotations_product';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['quotation_id','product_id','quantity','product_amount','status','deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    // protected $readField  = 'read_date';

    // protected $beforeInsert   = [];
    // protected $beforeUpdate   = [];

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

}