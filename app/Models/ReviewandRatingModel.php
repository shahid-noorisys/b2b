<?php 
namespace App\Models;

use CodeIgniter\Model;

class ReviewandRatingModel extends Model
{
    protected $table      = 'buyer_reviews';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['review_msg','rating','buyer_id','supplier_id','order_id','q_id','deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';

    public function getallSellerReviewsList(array $rating_filters , $admin_id = null)
    {
        $criterial = '';
        if($admin_id !=null ) { 

            if(isset($rating_filters['rating']) && $rating_filters['rating'] != "")
             {
               $criterial .= " AND tc.rating = '".$rating_filters['rating']."'";
             }
            if(isset($rating_filters['user_id']) && $rating_filters['user_id'] != "")
             {
               $criterial .= " AND tc.buyer_id = '".$rating_filters['user_id']."'";
             }

            $query = "SELECT * FROM buyer_reviews AS tc WHERE tc.supplier_id ='".$admin_id."'";
            $query .= $criterial;
            $query .= " ORDER BY tc.id DESC";
            return $this->db->query($query);
        } return false;
    }
    
}