<?php 
namespace App\Models;

use CodeIgniter\Model;

class UserBankModel extends Model
{
    protected $table      = 'user_bank_details';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id','address','bank_name','city','IBAN','BIC','zip_code','document','status','deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    
}