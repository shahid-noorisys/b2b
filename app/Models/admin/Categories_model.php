<?php 
namespace App\Models\admin;

use CodeIgniter\Model;

class Categories_model extends Model
{
	protected  $table = 'categories';
	// private  $countries = 'countries';	
	protected $primaryKey = 'id';
	protected $DBGroup = ENVIRONMENT;
	protected $returnType = 'object';
	protected $allowedFields = ['name','description','user_id','icon_url','created_by','status','deleted'];
	// protected $useSoftDeletes = true;
	protected $useTimestamps = true;
	protected $dateFormat = 'datetime';
	protected $createdField = 'created_date';
	protected $updatedField = 'updated_date';
	protected $deletedField  = 'deleted_date';

	// protected $validationRules = [
 //        'icon' => 'max_dims[icon,24,24]|mime_in[icon,png]|is_image[icon]'
 //    ];
}