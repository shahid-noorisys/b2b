<?php 
namespace App\Models\admin;

use CodeIgniter\Model;

class Subcategories_model extends Model
{
	protected  $table = 'sub_categories';
	protected $primaryKey = 'id';
	
	protected $DBGroup = ENVIRONMENT;
	protected $returnType = 'object';
	protected $allowedFields = ['name','category_id','icon_url','user_id','created_by','status','deleted'];
	// protected $useSoftDeletes = true;
	protected $useTimestamps = true;
	protected $dateFormat = 'datetime';
	protected $createdField = 'created_date';
	protected $updatedField = 'updated_date';
	protected $deletedField  = 'deleted_date';
}