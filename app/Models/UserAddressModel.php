<?php 
namespace App\Models;

use CodeIgniter\Model;

class UserAddressModel extends Model
{
    protected $table      = 'user_addresses';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id','full_name','address','city','state','country','country_code','currency_code','zip_code','is_primary','status','deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_date';

    protected $validationRules    = [
        'address'     => 'required',
        'city'     => 'required|alpha_numeric_space|min_length[3]',
        'state'        => 'required|alpha_numeric_space',
        'country'     => 'required',
        'zip_code' => 'required',
    ];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;
}