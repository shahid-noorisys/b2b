<?php 
namespace App\Models;

use CodeIgniter\Model;

class CompanyTypeModel extends Model
{
    protected $table = 'compinies_type';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected useSoftDeletes = true;

    protected $allowedFields = [];
    
    // protected useTimestamps = false;
    // protected createdField  = 'created_at';
    // protected updatedField  = 'updated_at';
    // protected deletedField  = 'deleted_at';

    // protected validationRules    = [];
    // protected validationMessages = [];
    // protected skipValidation     = false;
}