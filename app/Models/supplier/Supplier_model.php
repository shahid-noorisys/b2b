<?php 
namespace App\Models\supplier;

use CodeIgniter\Model;

class Supplier_model extends Model
{
	protected  $table = 'application_users';
    protected $primaryKey = 'id';

	protected $returnType = 'object';
    // protected $useSoftDeletes = true;
	
	protected $allowedFields = ['firstname','lastname','password','email','mobile','thumbnail','user_type','user_role','city','state','country','country_id','country_code','gender','zip_code','status','deleted','token','platform_id','designation','manager_firstName','manager_lastName','manger_gender','manger_document','link_send_date'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_date';

    protected $beforeInsert   = ['hashPassword'];
    protected $beforeUpdate   = ['hashPassword'];

    public function hashPassword($data)
    {
        $data['data']['password'] = password_hash($data['data']['password'],PASSWORD_DEFAULT);
        return $data;
    }

    public function authenticate($admin)
    {
        $details = $this->getWhere(['email' => $admin['email']]);
        // dd($this->getLastQuery());
        if($details->resultID->num_rows > 0)
        {
            $details = $details->getRow();
            $verify = password_verify($admin['password'], $details->password);
            if($verify){
                return $details;
            } else {
                return false;
            }
        }
        return false;
    }

    public function getallCustomerList(array $buyer_filters)
    {
        $criterial = '';
        if(is_array($buyer_filters)) { 
            if(isset($buyer_filters['country_id']) && $buyer_filters['country_id'] != "")
             {
               $criterial .= " AND tc.country_id = '".$buyer_filters['country_id']."'";
             }
            if(isset($buyer_filters['state']) && $buyer_filters['state'] != "")
             {
               $criterial .= " AND tc.state = '".$buyer_filters['state']."'";
             }
            if(isset($buyer_filters['city']) && $buyer_filters['city'] != "")
             {
               $criterial .= " AND tc.city = '".$buyer_filters['city']."'";
             }
            if(isset($buyer_filters['status']) && $buyer_filters['status'] != "" && $buyer_filters['status'] != 'All')
             {
               $criterial .= " AND tc.status = '".$buyer_filters['status']."'";
             }

            if(isset($buyer_filters['gender']) && $buyer_filters['gender'] != "" && $buyer_filters['gender'] != 'All')
             {
               $criterial .= " AND tc.gender = '".$buyer_filters['gender']."'";
             }

            if(isset($buyer_filters['start']) && $buyer_filters['start'] != "" && isset($buyer_filters['end']) && $buyer_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($buyer_filters['start']))."' AND '".date('Y-m-d',strtotime($buyer_filters['end']))."'";
            }


             $query = "SELECT * FROM application_users AS tc WHERE tc.user_role = 3 AND tc.deleted = 2 ";
             $query .= $criterial;
             $query .= " ORDER BY tc.id DESC";
            return $this->db->query($query)->getResult();
        } return false;
    }

    public function getallSellerList(array $seller_filters)
    {
        $criterial = '';
        if(is_array($seller_filters)) { 
            if(isset($seller_filters['country_id']) && $seller_filters['country_id'] != "")
             {
               $criterial .= " AND tc.country_id = '".$seller_filters['country_id']."'";
             }
            if(isset($seller_filters['state']) && $seller_filters['state'] != "")
             {
               $criterial .= " AND tc.state = '".$seller_filters['state']."'";
             }
            if(isset($seller_filters['city']) && $seller_filters['city'] != "")
             {
               $criterial .= " AND tc.city = '".$seller_filters['city']."'";
             }
            if(isset($seller_filters['status']) && $seller_filters['status'] != "" && $seller_filters['status'] != 'All')
             {
               $criterial .= " AND tc.status = '".$seller_filters['status']."'";
             }

            if(isset($seller_filters['gender']) && $seller_filters['gender'] != "" && $seller_filters['gender'] != 'All')
             {
               $criterial .= " AND tc.gender = '".$seller_filters['gender']."'";
             }

            if(isset($seller_filters['start']) && $seller_filters['start'] != "" && isset($seller_filters['end']) && $seller_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($seller_filters['start']))."' AND '".date('Y-m-d',strtotime($seller_filters['end']))."'";
            }


             $query = "SELECT * FROM application_users AS tc WHERE tc.user_role = 2 AND tc.deleted = 2 ";
             $query .= $criterial;
             $query .= " ORDER BY tc.id DESC";
            return $this->db->query($query)->getResult();
        } return false;
    }


}