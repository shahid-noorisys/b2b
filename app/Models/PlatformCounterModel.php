<?php
namespace App\Models;

use CodeIgniter\Model;

class PlatformCounterModel extends Model
{
    protected $table      = 'platform_counters';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['type','prefix','counter_value','min_length','can_append_symbol','description','status','deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    protected $deletedField  = 'deleted_date';

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;
}