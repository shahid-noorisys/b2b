<?php 
namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'application_users';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['firstname','lastname','password','email','mobile','thumbnail','user_type','user_role','city','state','otp','country','country_id','country_code','gender','zip_code','status','deleted','token'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_date';

    protected $beforeInsert   = ['hashPassword'];
    protected $beforeUpdate   = ['hashPassword'];

    protected $validationRules    = [
        'firstname'     => 'required|alpha_numeric_space|min_length[3]',
        'lastname'     => 'required|alpha_numeric_space|min_length[3]',
        'email'        => 'required|valid_email|is_unique[application_users.email]',
        'password'     => 'required|min_length[8]',
        'conf_password' => 'required_with[password]|matches[password]',
        'mobile'     => 'required|regex_match[/^[0-9]{10}$/]',
    ];

    // protected $validationMessages = [
    //     'old_password'        => [
    //         'verify' => 'Sorry. The old password is not matched.'
    //     ]
    // ];
    // protected $skipValidation     = false;

    public function hashPassword($data)
    {
        $data['data']['password'] = password_hash($data['data']['password'],PASSWORD_DEFAULT);
        return $data;
    }

    public function authenticate($user)
    {
        $details = $this->getWhere(['email' => $user['email']]);
        // dd($this->getLastQuery());
        if($details->resultID->num_rows > 0)
        {
            $details = $details->getRow();
            $verify = password_verify($user['password'], $details->password);
            if($verify){
                return $details;
            } else {
                return false;
            }
        }
        return false;
    }

}