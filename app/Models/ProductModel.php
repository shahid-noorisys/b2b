<?php
namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{
    protected $table      = 'products';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['name','description','category_id','category_name','sub_category_id','sub_category_name','user_id','image_1','image_2','image_3','image_4','image_5','is_published','status','deleted','order_counter','quote_counter'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_at';

    // protected $beforeInsert   = [];
    // protected $beforeUpdate   = [];

    // protected $validationRules    = [];
    // protected $validationMessages = [];
    // protected $skipValidation     = false;

    public function getallProductsList(array $product_filters , $admin_id = null)
    {
        $criterial = '';
        if($admin_id !=null ) { 
            if(isset($product_filters['category']) && $product_filters['category'] != "")
             {
               $criterial .= " AND tc.category_id = '".$product_filters['category']."'";
             }
            if(isset($product_filters['sub_category']) && $product_filters['sub_category'] != "")
             {
               $criterial .= " AND tc.sub_category_id = '".$product_filters['sub_category']."'";
             }
            if(isset($product_filters['status']) && $product_filters['status'] != "")
             {
               $criterial .= " AND tc.status = '".$product_filters['status']."'";
             }

            if(isset($product_filters['publish']) && $product_filters['publish'] != "")
             {
               $criterial .= " AND tc.is_published = '".$product_filters['publish']."'";
             }

            if(isset($product_filters['start']) && $product_filters['start'] != "" && isset($product_filters['end']) && $product_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($product_filters['start']))."' AND '".date('Y-m-d',strtotime($product_filters['end']))."'";
            }

             $query = "SELECT * FROM products AS tc WHERE tc.user_id ='".$admin_id."'";
             $query .= $criterial;
             $query .= " ORDER BY tc.created_date DESC";
            return $this->db->query($query)->getResult();
        } return false;
    }

    public function getalladminProductList(array $product_filters , $admin_id = null)
    {
        $criterial = '';
        if($admin_id !=null ) { 
            if(isset($product_filters['category']) && $product_filters['category'] != "")
             {
               $criterial .= " AND tc.category_id = '".$product_filters['category']."'";
             }
            if(isset($product_filters['sub_category']) && $product_filters['sub_category'] != "")
             {
               $criterial .= " AND tc.sub_category_id = '".$product_filters['sub_category']."'";
             }
            if(isset($product_filters['user_id']) && $product_filters['user_id'] != "")
             {
               $criterial .= " AND tc.user_id = '".$product_filters['user_id']."'";
             }
            if(isset($product_filters['status']) && $product_filters['status'] != "")
             {
               $criterial .= " AND tc.status = '".$product_filters['status']."'";
             }

            if(isset($product_filters['publish']) && $product_filters['publish'] != "")
             {
               $criterial .= " AND tc.is_published = '".$product_filters['publish']."'";
             }

            if(isset($product_filters['start']) && $product_filters['start'] != "" && isset($product_filters['end']) && $product_filters['end'] != "")
            {
               $criterial .= " AND tc.created_date BETWEEN '".date('Y-m-d',strtotime($product_filters['start']))."' AND '".date('Y-m-d',strtotime($product_filters['end']))."'";
            }


             $query = "SELECT * FROM products AS tc WHERE 1";
             $query .= $criterial;
             $query .= " ORDER BY tc.id DESC";
            return $this->db->query($query)->getResult();
        } return false;
    }
}