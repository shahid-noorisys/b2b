<?php
namespace App\Models;

use CodeIgniter\Model;

class MessageModel extends Model
{
    protected $table      = 'user_quotation_chatrooms';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['text_msg','attachment_url','attachment_file_type','q_id','order_id','sender_id','receiver_id','user_role','read_status','send_by'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'read_date';
  
}