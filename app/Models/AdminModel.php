<?php 
namespace App\Models;

use CodeIgniter\Model;

class AdminModel extends Model
{
    protected $table      = 'platform_users';
    protected $primaryKey = 'id';

    protected $returnType = 'object';
    // protected $useSoftDeletes = true;

    protected $allowedFields = ['firstname','lastname','password','email','mobile','city','user_role','status','deleted'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_date';
    protected $updatedField  = 'updated_date';
    // protected $deletedField  = 'deleted_date';

    protected $beforeInsert   = ['hashPassword'];
    protected $beforeUpdate   = ['hashPassword'];

    public function hashPassword($data)
    {
        $data['data']['password'] = password_hash($data['data']['password'],PASSWORD_DEFAULT);
        return $data;
    }

    public function authenticate($admin)
    {
        $details = $this->getWhere(['email' => $admin['email']]);
        // dd($this->getLastQuery());
        if($details->resultID->num_rows > 0)
        {
            $details = $details->getRow();
            $verify = password_verify($admin['password'], $details->password);
            if($verify){
                return $details;
            } else {
                return false;
            }
        }
        return false;
    }

}