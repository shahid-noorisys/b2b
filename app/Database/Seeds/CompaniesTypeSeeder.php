<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CompaniesTypeSeeder extends Seeder
{
	public function run()
	{
		// Simple Queries
		$this->db->query("INSERT INTO `compinies_type` (`id`, `type`, `status`, `deleted`, `created_date`, `updated_date`, `deleted_date`) VALUES
		(1, 'Computer Industry', 'Active', 'No', '', '', ''),
		(2, 'Construction Industry', 'Active', 'No', '', '', ''),
		(3, 'Education Industry', 'Active', 'No', '', '', ''),
		(4, 'Electronics Industry', 'Active', 'No', '', '', ''),
		(5, 'Manufacturing Industry', 'Active', 'No', '', '', ''),
		(6, 'Hospitality Industry', 'Active', 'No', '', '', '');");
	}
}