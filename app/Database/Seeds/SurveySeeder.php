<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SurveySeeder extends Seeder
{
	public function run()
	{
		// Simple Queries
		$this->db->query("INSERT INTO `platform_survey` (`id`, `platform_type`, `status`, `deleted`, `created_date`, `updated_date`, `deleted_date`) VALUES
		(1, 'Website', 'Active', 'No', '', '', ''),
		(2, 'Advertisement', 'Active', 'No', '', '', ''),
		(3, 'Social media', 'Active', 'No', '', '', ''),
		(4, 'News', 'Active', 'No', '', '', ''),
		(5, 'Other', 'Active', 'No', '', '', '');");
	}
}
