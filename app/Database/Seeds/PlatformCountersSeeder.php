<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class PlatformCountersSeeder extends Seeder
{
	public function run()
	{
		$this->db->query("INSERT INTO `platform_counters` (`id`, `type`, `prefix`, `counter_value`, `min_length`, `can_append_symbol`, `symb_to_append`, `description`,`created_date`) VALUES
		(1, 'QUOTATION', 'QTN', '0', '5', 'Yes', '0', 'Quotation Number',date('Y-m-d H:i:s')),
		(2, 'ORDER', 'ORD', '0', '5', 'Yes', '0', 'Order Number',date('Y-m-d H:i:s')),
		(3, 'TEMP_QUOTE', 'TQ', '0', '5', 'Yes', '0', 'Temporary Quotation Number used for Cart', date('Y-m-d H:i:s')),
		(4, 'BR_TRANSACTION', 'TRC', '0', '12', 'Yes', '0', 'Unique Transaction ID for Buyer OR Customer', date('Y-m-d H:i:s')),
		(5, 'SR_TRANSACTION', 'TRS', '0', '12', 'Yes', '0', 'Unique Transaction ID for Supplier', date('Y-m-d H:i:s')),
		(6, 'AD_TRANSACTION', 'TRA', '0', '12', 'Yes', '0', 'Unique Transaction ID for Admin', date('Y-m-d H:i:s')),
		(7, 'RECEIPT', 'RT', '0', '12', 'Yes', '0', 'Unique Receipt Number', date('Y-m-d H:i:s'))");
	}
}
