<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserRoleToChatroom extends Migration
{
	public function up()
	{
		$fields = [
			'user_role' => [
				'type' => 'VARCHAR',
				'constraint' => 30,
				'after' => 'sender_id',
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => 30,
				'after' => 'read_date',
			],
		];
		$this->forge->addColumn('user_quotation_chatrooms', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['user_role','updated_date'];
		$this->forge->dropColumn('user_quotation_chatrooms',$fields);
	}
}
