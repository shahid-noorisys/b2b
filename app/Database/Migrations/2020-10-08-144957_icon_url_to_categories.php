<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class IconUrlToCategories extends Migration
{
	public function up()
	{
		$fields = [
			'icon_url' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'after' => 'user_id',
				'comment' => 'Icon image urls',
			]
		];
		$this->forge->addColumn('categories', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropColumn('categories','icon_url');
	}
}
