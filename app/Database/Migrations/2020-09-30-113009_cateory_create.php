<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CateoryCreate extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
				null => false,
			],
			'name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				null => false,
			],
			'description' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'user_id' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => false,
				'comment' => 'ID of user OR admin who creates it',
			],
			'created_by' => [
				'type' => 'ENUM',
				'constraint' => ['Admin','User'],
				'default' => 'Admin',
				null => false,
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				null => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				null => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('categories');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('categories');
	}
}
