<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'user_id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				null => false,
				'comment' => 'ID of users',
			],
			'company_name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			
			'company_email' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'comment' => 'alternate email',
			],
			'contact' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => false,
			],
			'address' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'city' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'country' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'country_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'thumbnail' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				null => true,
			],
			'introduction' => [
				'type' => 'TEXT',
				null => true,
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'deleted_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('user_company');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_company');
	}
}
