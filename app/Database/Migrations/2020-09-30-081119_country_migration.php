<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CountryMigration extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'code' => [
				'type' => 'VARCHAR',
				'constraint' => '4',
				'null' => false,
			],
			'name' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
				'null' => false,
			],
			'dial_code' => [
				'type' => 'INT',
				'constraint' => '11',
				'null' => false,
			],
			'currency_name' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => false,
			],
			'currency_symbol' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => false,
			],
			'currency_code' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => false,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('countries');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('countries');
	}
}
