<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PlatformCommission extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
				null => false,
			],		
			'supplier_id' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'commission_charge' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],	
			'country_id' => [
				'type' => 'INT',
				'constraint' => 11,
				null => false,
			],
			'country_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'currency_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			
			'user_id' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => false,
				'comment' => 'ID of user OR admin who creates it',
			],
			'created_by' => [
				'type' => 'ENUM',
				'constraint' => ['Admin','User'],
				'default' => 'Admin',
				null => false,
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				null => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				null => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('platform_commission');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('platform_commission');
	}
}
