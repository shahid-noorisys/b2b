<?php 
namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class BankDetails extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'user_id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				null => false,
				'comment' => 'ID of users',
			],
			'bank_name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],			
			'address' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'city' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'zip_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'IBAN' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
				'comment' => 'Internatinal Bank Account Number',
			],
			'BIC' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
				'comment' => 'BIC/Swift Code',
			],
			'document' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				 null => true,
				'comment' => 'Bank Verification document',
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('user_bank_details');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_bank_details');
	}
}
