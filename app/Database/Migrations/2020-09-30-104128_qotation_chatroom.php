<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class QotationChatroom extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
				null => false,
			],
			'text_msg' => [
				'type' => 'TEXT',
				null => false,
			],
			'attachment_url' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'attachment_file_type' => [
				'type' => 'VARCHAR',
				'constraint' => '100',
			],
			'q_id' => [
				'type' => 'VARCHAR',
				'constraint' => '100',
				null => false,
				'comment' => 'ID of quotation',
			],
			'order_id' => [
				'type' => 'VARCHAR',
				'constraint' => '100',
				null => false,
				'comment' => 'ID of Order',
			],
			'sender_id' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => false,
				'comment' => 'ID of user who send message',
			],
			'read_status' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				null => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'read_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('user_quotation_chatrooms');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_quotation_chatrooms');
	}
}
