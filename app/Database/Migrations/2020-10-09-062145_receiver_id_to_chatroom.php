<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ReceiverIdToChatroom extends Migration
{
	public function up()
	{
		$fields = [
			'receiver_id' => [
				'type' => 'VARCHAR',
				'constraint' => 30,
				'after' => 'sender_id',
				'comment' => 'Id who receives the message',
			],
		];
		$this->forge->addColumn('user_quotation_chatrooms', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropColumn('user_quotation_chatrooms','receiver_id');
	}
}
