<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MangerAndOtherUser extends Migration
{
	public function up()
	{
		$fields = [

			'designation' => [
				'type' => 'ENUM',
				'constraint' => ['Manager','Other'],
				'default' => 'Manager',
				'after' => 'company_email',
				'null' => true,
				'comment' => 'Job Title',
			],

			'manager_firstName' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'after' => 'designation',
				'null' => true,
			],
			'manager_lastName' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'after' => 'manager_firstName',
				'null' => true,
			],
			'manger_gender' => [
				'type' => 'ENUM',
				'constraint' => ['Male','Female'],
				'default' => 'Male',
				'after' => 'manager_lastName',
				'null' => true,
			],
			'manger_document' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'after' => 'manger_gender',
				'comment' => 'Manager Comapny ID Proof',
			],
			
		];
		$this->forge->addColumn('application_users', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['designation','manager_firstName','manager_lastName','manger_gender','manger_document'];
		$this->forge->dropColumn('application_users',$fields);
	}
}