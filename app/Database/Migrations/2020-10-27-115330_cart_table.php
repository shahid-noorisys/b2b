<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CartTable extends Migration
{
	public function up()
	{
		$fields = [
			'temp_id' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				null => false,
				'unique' => true,
			],
			'supplier_id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				null => false,
			],
			'user_id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				null => false,
				'comment' => 'Buyer id who add this product to cart',
			],
			'total_products' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				null => false,
				'comment' => 'Total items in cart',
			],
			'product_json' => [
				'type' => 'LONGTEXT',
				null => false,
				'comment' => 'product json array of the cart',
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		// $this->forge->addKey('id', true);
		$this->forge->createTable('cart');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('cart');
	}
}
