<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PlatformUsers extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'user_role' =>[
				'type' =>'INT',
				'constraint' => 5,
				'unsigned' => true,
				'null' => false,
			],
			'firstname' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'lastname' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => false,
			],
			'password' => [
				'type' => 'TEXT',
				'null' => false,
			],
			'mobile' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => false,
			],
			'thumbnail' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				null => true,
			],
			'address' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'second_address' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'city' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'country' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'country_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('platform_users');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('platform_users');
	}
}
