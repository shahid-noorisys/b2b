<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OrderCounterToProduct extends Migration
{
	public function up()
	{
		$fields = [
			'order_counter' => [
				'type' => 'BIGINT',
				'constraint' => 30,
				'after' => 'deleted',
				'comment' => 'how many times product will be ordered',
			],
			'quote_counter' => [
				'type' => 'BIGINT',
				'constraint' => 30,
				'after' => 'order_counter',
				'comment' => 'how many times product will be quoted',
			],
		];
		$this->forge->addColumn('products', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['order_counter','quote_counter'];
		$this->forge->dropColumn('products',$fields);
	}
}
