<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class IconUrlToSubCategories extends Migration
{
	public function up()
	{
		$fields = [
			'icon_url' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'after' => 'user_id',
				'comment' => 'Icon image urls',
			]
		];
		$this->forge->addColumn('sub_categories', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropColumn('sub_categories','icon_url');
	}
}
