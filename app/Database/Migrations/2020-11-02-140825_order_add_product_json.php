<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OrderAddProductJson extends Migration
{
	public function up()
	{
		$fields = [
			'product_json' => [
				'type' => 'LONGTEXT',
				'null' => false,
				'after' => 'total_items',
				'comment' => 'json object of all products with price',
			]
		];
		$this->forge->addColumn('orders', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['product_json'];
		$this->forge->dropColumn('orders', $fields);
	}
}
