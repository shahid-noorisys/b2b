<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UniqueCounters extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'type' => [
				'type' => 'VARCHAR',
				'constraint' => '155',
				'comment' => 'e.g. QUOTATION,ORDER',
				'null' => false,
			],
			'prefix' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'comment' => 'e.g. Q for quotation',
				'null' => false,
			],
			'counter_value' => [
				'type' => 'INT',
				'constraint' => '20',
				'null' => false,
			],
			'min_length' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => false,
			],
			'can_append_symbol' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'Yes',
				'null' => false,
			],
			'symb_to_append' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'default' => 0,
				'null' => true,
			],
			'description' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => true,
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'deleted_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('platform_counters');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('platform_counters');
	}
}
