<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserResetPasswodDate extends Migration
{
	public function up()
	{
		$fields = [
			'previous_password' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'comment' => 'Token for activation link',
			],
			'reset_password_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'comment' => 'User Reset Password Date',
				
			],
			'link_send_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'comment' => 'Reset password link send date after every 3 month',
				
			],
		];
		$this->forge->addColumn('application_users', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['previous_password','reset_password_date','link_send_date'];
		$this->forge->dropColumn('application_users',$fields);
	}
}
