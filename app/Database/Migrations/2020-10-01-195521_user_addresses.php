<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserAddresses extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'user_id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				null => false,
				'comment' => 'ID of users',
			],
			'address' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'city' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'state' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'country' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
			],
			'country_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'currency_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'zip_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'is_primary' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
				'comment' => 'IF is set primary address',
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'deleted_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'comment' => 'used for soft delete',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('user_addresses');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_addresses');
	}
}
