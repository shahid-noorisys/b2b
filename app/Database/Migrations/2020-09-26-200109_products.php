<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Products extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'description' => [
				'type' => 'TEXT',
				'null' => false,
			],
			'category_id' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => false,
			],
			'category_name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'sub_category_id' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => false,
			],
			'sub_category_name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'user_id' => [
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => false,
				'comment' => 'Spplier ID of this product',
			],
			'image_1' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'image_2' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'image_3' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'image_4' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'image_5' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'is_published' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('products');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('products');
	}
}
