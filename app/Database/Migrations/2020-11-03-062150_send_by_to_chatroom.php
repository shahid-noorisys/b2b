<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SendByToChatroom extends Migration
{
	public function up()
	{
		$fields = [
			'send_by' => [
				'type' => 'ENUM',
				'constraint' => ['Admin', 'Seller', 'Buyer'],
				'default' => 'Buyer',
				'after' => 'user_role',
				 null => false,
			],
		];
		$this->forge->addColumn('user_quotation_chatrooms', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['send_by'];
		$this->forge->dropColumn('user_quotation_chatrooms',$fields);
	}
}