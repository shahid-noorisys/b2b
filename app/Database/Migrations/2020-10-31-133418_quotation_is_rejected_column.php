<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class QuotationIsRejectedColumn extends Migration
{
	public function up()
	{
		$fields = [
			'is_rejected' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				null => false,
				'after' => 'is_validated',
			],
		];
		$this->forge->addColumn('quotations',$fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['is_rejected'];
		$this->forge->dropColumn('quotations', $fields);
	}
}
