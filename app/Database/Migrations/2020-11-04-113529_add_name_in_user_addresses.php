<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddNameInUserAddresses extends Migration
{
	public function up()
	{
		$fields = [
			'full_name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'comment' => 'used to store user full name',
				null => true,
				'after' => 'user_id',
			],
		];
		$this->forge->addColumn('user_addresses',$fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['full_name'];
		$this->forge->dropColumn('user_addresses',$fields);
	}
}
