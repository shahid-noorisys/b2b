<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddOrderNoteToOrders extends Migration
{
	public function up()
	{
		$fields = [
			'order_note' => [
				'type' => 'TEXT',
				'null' => false,
				'after' => 'product_json',
				'comment' => 'order short description',
			],
		];
		$this->forge->addColumn('orders',$fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropColumn('orders',['order_note']);
	}
}
