<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserPlatformNotification extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'title' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				null => false,
			],
			'description' => [
				'type' => 'TEXT',
				null => true,
			],
			'user_id' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => false,
				'comment' => 'user id of whom this notice belongs to',
			],
			'user_role' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => false,
			],
			'read_status' => [
				'type' => 'ENUM',
				'constraint' => ['Read','Unread'],
				'default' => 'REad',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'read_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('platform_notifications');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('platform_notifications');
	}
}
