<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PlatformTypeAndTokenUser extends Migration
{
	public function up()
	{
		$fields = [
			'token' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'comment' => 'Token for activation link',
			],
			'platform_id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'comment' => 'Where did you hear about Yahodihime',
				
			],
		];
		$this->forge->addColumn('application_users', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['token','platform_id'];
		$this->forge->dropColumn('application_users', $fields);
	}
}
