<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SurveyMigration extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'platform_type' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => false,
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'deleted_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('platform_survey');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('platform_survey');
	}
}
