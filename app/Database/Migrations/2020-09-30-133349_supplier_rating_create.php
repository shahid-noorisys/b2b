<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SupplierRatingCreate extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
				null => false,
			],
			'review_msg' => [
				'type' => 'TEXT',
				null => false,
			],
			'rating' => [
				'type' => 'TINYINT',
				'constraint' => 1,
			],
			'buyer_id' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'supplier_id' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'order_id' => [
				'type' => 'VARCHAR',
				'constraint' => '100',
				null => false,
				'comment' => 'ID of Order',
			],
			'q_id' => [
				'type' => 'VARCHAR',
				'constraint' => '100',
				null => false,
				'comment' => 'ID of quotation',
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				null => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'deleted_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('buyer_reviews');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('buyer_reviews');
	}
}
