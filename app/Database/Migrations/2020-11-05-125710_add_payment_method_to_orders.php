<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPaymentMethodToOrders extends Migration
{
	public function up()
	{
		$fileds = [
			'payment_method' => [
				'type' => 'ENUM',
				'constraint' => ['mtn','togo','orange'],
				'after' => 'product_json',
				'null' => true,
			]
		];

		$this->forge->addColumn('orders',$fileds);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fileds = ['payment_method'];
		$this->forge->dropColumn('orders',$fileds);
	}
}
