<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		$fields = [
			'id' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'firstname' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'lastname' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'null' => false,
			],
			'password' => [
				'type' => 'TEXT',
				'null' => false,
			],
			'mobile' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => false,
			],
			'thumbnail' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				null => true,
			],
			'gender' => [
				'type' => 'ENUM',
				'constraint' => ['Male','Female'],
				'default' => 'Male',
				'null' => false,
			],
			'user_type' => [
				'type' => 'ENUM',
				'constraint' => ['Individual','Company'],
				'default' => 'Individual',
				'null' => false,
			],
			'user_role' =>[
				'type' =>'INT',
				'constraint' => 5,
				'unsigned' => true,
				'null' => false,
			],
			'city' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
				'comment' => 'User living city',
			],
			'state' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
				'comment' => 'User living state',
			],
			'country_id' => [
				'type' => 'INT',
				'constraint' => 11,
				null => false,
			],
			'country' => [
				'type' => 'VARCHAR',
				'constraint' => '150',
				'comment' => 'User living country',
			],
			'country_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'zip_code' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
			],
			'company_name' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
			],
			'otp' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'comment' => '6 digit number',
			],
			'company_email' => [
				'type' => 'VARCHAR',
				'constraint' => '255',
				'comment' => 'alternate email',
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['Active','Inactive'],
				'default' => 'Active',
				'null' => false,
			],
			'deleted' => [
				'type' => 'ENUM',
				'constraint' => ['Yes','No'],
				'default' => 'No',
				'null' => false,
			],
			'created_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'updated_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
			'deleted_date' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				null => true,
			],
		];
		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->createTable('application_users');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('application_users');
	}
}
