<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class QuotationAddProductJson extends Migration
{
	public function up()
	{
		$fields = [
			'product_json' => [
				'type' => 'LONGTEXT',
				'null' => false,
				'after' => 'total_items',
				'comment' => 'json object of all products with price',
			]
		];
		$this->forge->addColumn('quotations', $fields);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$fields = ['product_json'];
		$this->forge->dropColumn('quotations', $fields);
	}
}
