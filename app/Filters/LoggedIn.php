<?php namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class LoggedIn implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $user = session()->get('user_details');
        if(!$user OR !$user->isLoggedIn)
        {
            if($request->isAJAX()){
                echo json_encode(['status' => 'not_logged_in', lang('You are not Logged In.')]);exit;
            }
            session()->setFlashData('exception',lang('You are not Logged In.'));
            return redirect()->back();
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        
    }
}