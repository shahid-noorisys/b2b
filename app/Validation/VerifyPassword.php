<?php

namespace App\Validation;

use Config\Services;
use App\Models\UserModel;

class VerifyPassword
{
    private $request;
    public function __construct(RequestInterface $request = null)
	{
		if (is_null($request))
		{
			$request = Services::request();
		}

		$this->request = $request;
    }
    public function verify(string $password, string &$error = null)
    {
        $userModel = new UserModel();
        $user = $userModel->find(session()->get('user_details')->user_id);
        $verify = password_verify($password, $user->password);
        if(! $verify){
            $error = display('The Old Password is not matched.');
            return false;
        }
    }
}