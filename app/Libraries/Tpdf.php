<?php
namespace App\Libraries;

// require_once APPPATH.'vendor/tecnickcom/tcpdf/tcpdf.php';
require_once APPPATH.'/../vendor/tecnickcom/tcpdf/tcpdf.php';

use \TCPDF;
use Config\Database;

class Tpdf extends TCPDF
{
    public function __construct()
    {
        parent::__construct();
	}
	
	//Page header
    // public function Header() {
    //     // Logo
	// 	$image_file = FCPATH.'public/assets/img/logo/pdf-logo.png';
	// 	// echo APPPATH;
    //     $this->Image($image_file, 70, 0, 60, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	// }
	
	public function generateReceipt($data = array())
	{
		// create new PDF document
		// $pdf = new Tpdf('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$this->SetCreator(PDF_CREATOR);
		$this->SetAuthor('Yahodehime');
		$this->SetTitle('Order Receipt');
		$this->SetSubject('');
		$this->SetKeywords('');

		// remove default header/footer
		$this->setPrintHeader(false);
		$this->setPrintFooter(false);

		// set default monospaced font
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$this->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
		// $this->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		$this->SetFont('helvetica', '', 10);
		
		// add a page
		$this->AddPage();
		
		// ---------------------------------------------------------
		
		if (!is_dir('public/assets/uploads/images/invoices')) {
			mkdir('public/assets/uploads/images/invoices', 0777, TRUE);
		}
		$pdf_file = 'public/assets/uploads/images/invoices/invoice_'. strtotime(date("Y-m-d H:i:s"));
		$pdf_path = $pdf_file.'.pdf';
		ob_start();
		// $html = view('common/payment_receipt', $data);
		$this->WriteHTML($data['html'], true, false, true, false, '');
		// echo json_encode(FCPATH);exit;
		ob_end_flush();
		//Close and output PDF document
		$this->Output(FCPATH.$pdf_path, 'I');exit;
	}
}
