<?php 
namespace App\Libraries;

use Config\Services;
use Config\Database;

/**
 * The Cart class is a basic port of the CodeIgniter 3 cart module for 
 * CodeIgniter 4.
 *
 * @package    App\Libraries
 * @author     RHM Dev Team
 * @since      1.0.0
 * @deprecated 3.0.0
 */
class Cart
{
    /**
     * These are the regular expression rules that we use to validate the product ID and product name
     * alpha-numeric, dashes, underscores, or periods
     *
     * @var string
     */
    public $productIdRules = '\.a-z0-9_-';

    /**
     * These are the regular expression rules that we use to validate the product ID and product name
     * alpha-numeric, dashes, underscores, colons or periods
     *
     * @var string
     */
    public $productNameRules = '\w \-\.\:';

    /**
     * only allow safe product names
     *
     * @var bool
     */
    public $productNameSafe = true;

    /**
     * Contents of the cart
     *
     * @var array
     */
    protected $cartContents = [];

    /**
     * Session Service
     *
     * @var \CodeIgniter\Session\Session $session
     */
    protected $session;


    /**
     * Database Service
     *
     * @var \CodeIgniter\Database\Database $db
     */
    protected $db;

    /**
     * Database Table
     *
     * @var $table
     */
    protected $table;
    
    /**
     * Session User Details
     *
     * @var $user
     */
    protected $user;


    /**
     * Database product response
     *
     * @var $res
     */
    protected $res = null;

    // ------------------------------------------------------------------------

    /**
     * Shopping Class Constructor
     *
     * The constructor loads the Session class, used to store the shopping cart contents.
     */
    public function __construct()
    {
        $this->session = Services::session();
        $this->db = Database::connect();

        // Grab the shopping cart array from the session table
        // $this->cartContents = $this->session->get('db_cart_contents');
        // Grab the shopping cart array from the database table
        $this->user = $this->session->user_details;
        $this->table = $this->db->table('cart');
        if ( isset($this->user->user_id) ) {
            $this->res = $this->table->where('user_id',$this->user->user_id)->get()->getRow();
            // echo $this->user->user_id;
            // echo json_encode($this->res);exit;
            if(!empty($this->res))
            {
                $this->cartContents = ['temp_id' => $this->res->temp_id, 'supplier_id' => $this->res->supplier_id, 'user_id' => $this->res->user_id, 'total_products' => $this->res->total_products, 'product_json' => $this->res->product_json];
            } else {
                // No cart exists so we'll set some base values
                $this->cartContents = array();
            }
        } else {
            $this->cartContents = array();
        }
        log_message('info', 'Cart Class Initialized');
    }

    // --------------------------------------------------------------------

    /**
     * Initialize Cart Contents
     *
     * @param array $user
     *
     * @return bool
     */
    public function initialize($user = []): bool
    {
        if ( ! is_array($user) || count($user) === 0 ) {
            log_message('error', 'The initialize method must be passed an array containing user data.');
            return false;
        }
        
        if(empty($this->res))
        {
            if(isset($user['user_id']) && ! empty($user['user_id']))
            {
                $this->db->transStart();
                $user['temp_id'] = getCounter('temp_quote');
                $this->table->insert($user);
                $this->db->transComplete();
                if($this->db->transStatus() !== FALSE){
                    log_message('info', 'Cart is Initialized for '.$user['user_id']);
                    return true;
                } else {
                    log_message('info', 'Cart is not Initialized Due to some error for '.$user['user_id']);
                    return false;
                }
            } else { 
                log_message('info', 'Cart is not Initialized due to user_id is not set');
                return false;
            }
        }
        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Get Cart Contents from database
     */
    public function getData(): void
    {
        $this->res = $this->table->where('user_id',$this->user->user_id)->get()->getRow();
        if(!empty($this->res))
        {
            $this->cartContents = ['temp_id' => $this->res->temp_id, 'supplier_id' => $this->res->supplier_id, 'user_id' => $this->res->user_id, 'total_products' => $this->res->total_products, 'product_json' => $this->res->product_json];
        } else {
            // No cart exists so we'll set some base values
            $this->cartContents = array();
        }
    }

    // --------------------------------------------------------------------

    /**
     * Cart Contents
     *
     * Returns the entire cart array OR bool
     *
     * @return array|bool
     */
    public function getItems()
    {
        $product = (isset($this->cartContents['product_json']))?json_decode($this->cartContents['product_json']):'';
        if(!empty($product)){
            return $this->cartContents;
        }
        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Insert items into the cart tabel and save it
     *
     * @param array $items
     *
     * @return bool
     */
    public function insert($items = []): bool
    {
        // Was any cart data passed? No? Bah...
        if ( ! is_array($items) || count($items) === 0 ) {
            log_message('error', 'The insert method must be passed an array containing data.');
            return false;
        }
        $save_cart = false;
        // if ( !empty($items) && isset($items[ 'supplier_id' ]) ) {
            // if (! $this->getItems() ) {
                // $this->cartContents['supplier_id'] = $items['supplier_id'];
                // $this->cartContents['user_id'] = $items['user_id'];
                if ( $this->_insert($items) ) {
                    $save_cart = true;
                }
            // } else {
            //     if ( $this->_update($items['product_json']) ) {
            //         $save_cart = true;
            //     }
            // }
        // } 

        // Save the cart data if the insert was successful
        if ( $save_cart === true ) {
            return $this->saveCart();
            // return $rowid ?? true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Insert
     *
     * @param array $items
     * @return bool|string
     */
    protected function _insert($items = [])
    {
        // Was any cart data passed? No? Bah...
        if ( ! is_array($items) || count($items) === 0 ) {
            log_message('error', 'The insert method must be passed an array containing data.');
            return false;
        }
        // echo json_encode($items);exit;
        // $arr = array();
        // $counter = 0;
        // foreach ($items as $item) {
        //     # code...
        //     array_push($arr,$item);
        //     $counter++;
        // }
        // echo json_encode($arr);
        // exit;
        $this->getData();
        $this->cartContents['product_json'] = json_encode(array($items));
        $this->cartContents['total_products'] = (isset($this->cartContents['total_products']) && $this->cartContents['total_products'] != '')?$this->cartContents['total_products']+1:1;

        return true;
    }

    // ------------------------------------------------------------------------

    /**
     * Update the cart
     *
     * This function permits the quantity of a given item to be changed.
     * Typically it is called from the "view cart" page if a user makes
     * changes to the quantity before checkout. That array must contain the
     * product ID and quantity for each item.
     *
     * @param array $items
     * @return bool
     */
    public function update($items = []): bool
    {
        // Was any cart data passed?
        if ( ! is_array($items) || count($items) === 0 ) {
            return false;
        }

        $save_cart = false;
        // if ( !empty($items) && isset($items[ 'supplier_id' ]) ) {
        //     if (! $this->getItems() ) {
                // if ( $this->_insert($items['product_json']) ) {
                //     $save_cart = true;
                // }
        //     } else {
                if ( $this->_update($items) ) {
                    $save_cart = true;
                }
        //     }
        // } 

        // Save the cart data if the update was successful
        if ( $save_cart === true ) {
            $this->saveCart();
            return true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Update Single Item in the cart
     *
     * This function permits changing item properties.
     *
     * @param array $items
     * @return bool
     */
    protected function _update($items = []): bool
    {
        // Was any cart data passed? No? Bah...
        if ( ! is_array($items) || count($items) === 0 ) {
            log_message('error', 'The insert method must be passed an array containing data.');
            return false;
        }
        $this->getData();
        // echo json_encode($items);exit;
        $arr = json_decode($this->cartContents['product_json']);
        $new_array = array();
        $is_new = true;
        $counter = 0;
        foreach ($arr as $p) {
            # code...
            if($p->product_id == $items['product_id'] )
            {
                $is_new = false;
                $p->quantity= $p->quantity + 1;
                array_push($new_array,$p);
                // continue;
            } else {
                array_push($new_array,$p);
            }
            $counter++;
        }
        if($is_new){ array_push($new_array,$items); $counter++;}
        // echo json_encode($counter);exit;
        // echo json_encode($arr);
        // exit;
        $this->cartContents['product_json'] = json_encode($new_array);
        $this->cartContents['total_products'] = $counter;

        return true;
    }

    // ------------------------------------------------------------------------

    /**
     * Update Whole cart items
     *
     * This function permits the quantity of a given item to be changed.
     * Typically it is called from the "view cart" page if a user makes
     * changes to the quantity before checkout. That array must contain the
     * product ID and quantity for each item.
     *
     * @param array $items
     * @return bool
     */
    public function updateAll($items = []): bool
    {
        // Was any cart data passed?
        if ( ! is_array($items) || count($items) === 0 ) {
            return false;
        }
        
        $save_cart = false;
        if ( !empty($items) && isset($items[ 'product_json' ]) ) {
                $save_cart = true;
                $this->cartContents['product_json'] = json_encode($items['product_json']);
                $this->cartContents['total_products'] = $items['total_products'];
        } 

        // Save the cart data if the update was successful
        if ( $save_cart === true ) {
            return $this->saveCart();
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Save the cart array to the session DB
     *
     * @return bool
     */
    protected function saveCart(): bool
    {
        if(! empty($this->cartContents))
        {
            $update_arr = ['total_products' => $this->cartContents['total_products'], 'product_json' => $this->cartContents['product_json'], 'updated_date' => date('Y-m-d H:i:s')];
            // echo "update";
            // echo json_encode($this->cartContents);exit;
            if($this->table->where('temp_id',$this->cartContents['temp_id'])->update($update_arr)){
                return true;
            }
        }
        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Remove Item
     *
     * Removes an item from the cart
     *
     * @param $product_id
     *
     * @return bool
     */
    public function removeItem($product_id): bool
    {
        $this->getData();
        // echo json_encode($items);exit;
        $arr = json_decode($this->cartContents['product_json']);
        $new_array = array();
        // $is_new = true;
        // $counter = 0;
        foreach ($arr as $p) {
            if($p->product_id != $product_id)
            {
                array_push($new_array,$p);
                $this->cartContents['total_products'] = ($this->cartContents['total_products'] != null)?$this->cartContents['total_products']-1:0;
            }
        }
        // if($is_new){ array_push($new_array,$items); $counter++;}
        // echo json_encode($counter);exit;
        // echo json_encode($arr);
        // exit;
        $this->cartContents['product_json'] = (! empty($new_array))?json_encode($new_array):'';
        // $this->cartContents['total_products'] = $counter;
        // $this->getData();
        if(empty($new_array)){
            $this->db->transStart();
            $this->destroy();
            $this->db->transComplete();
            if($this->db->transStatus() !== FALSE){
                return true;
                $this->getData();
            } else { return false; }
        } else {
            return $this->saveCart();
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Total Items
     *
     * Returns the total item count
     *
     * @return mixed
     */
    public function totalItems()
    {
        if(isset($this->cartContents['total_products']))
        {
            return $this->cartContents['total_products'];
        }
        return 0;
    }

    // ------------------------------------------------------------------------

    /**
     * Get total Product IDs
     *
     * Returns an array of total product IDs
     *
     * @return array
     */
    public function totalProductIds()
    {
        $arr = json_decode($this->res->product_json);
        $result = array();
        foreach ($arr as $v) {
            # code...
            array_push($result,$v->product_id);
        }
        return $result;
    }

    // ------------------------------------------------------------------------

    /**
     * Get cart item
     *
     * Returns the details of a specific item in the cart
     *
     * @param $row_id
     * @return bool|mixed
     */
    public function getItem($product_id)
    {
        $arr = json_decode($this->cartContents['product_json']);
        return ( in_array($product_id, $arr) OR ! isset($this->cartContents[ $product_id ]) )
            ? false
            : $this->cartContents[ $product_id ];
    }

    // ------------------------------------------------------------------------

    /**
     * Has options
     *
     * Returns TRUE if the rowid passed to this function correlates to an item
     * that has options associated with it.
     *
     * @param string $row_id
     * @return bool
     */
    public function hasOptions($row_id = ''): bool
    {
        return ( isset($this->cartContents[ $row_id ][ 'options' ]) && count($this->cartContents[ $row_id ][ 'options' ]) !== 0 );
    }

    // ------------------------------------------------------------------------

    /**
     * Product options
     *
     * Returns the an array of options, for a particular product row ID
     *
     * @param string $row_id
     * @return array|mixed
     */
    public function productOptions($row_id = '')
    {
        return $this->cartContents[ $row_id ][ 'options' ] ?? [];
    }

    // ------------------------------------------------------------------------

    /**
     * Format Number
     *
     * Returns the supplied number with commas and a decimal point.
     *
     * @param string $n
     * @return string
     */
    public function formatNumber($n = ''): string
    {
        return ( $n === '' ) ? '' : number_format((float)$n, 2);
    }

    // ------------------------------------------------------------------------

    /**
     * Destroy the cart
     *
     * Empties the cart and delete the data from DB
     */
    public function destroy(): void
    {
        if(isset($this->cartContents['temp_id']) AND ! empty($this->cartContents['temp_id'])){
            if($this->table->where('temp_id',$this->cartContents['temp_id'])->delete()){
                $this->cartContents = array();
            }
        }
        // $this->cartContents = [ 'cart_total' => 0, 'total_items' => 0 ];
        // $this->session->remove('db_cart_contents');
    }

    // ------------------------------------------------------------------------
}
